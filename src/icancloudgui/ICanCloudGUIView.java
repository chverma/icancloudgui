/*      */ package icancloudgui;
/*      */ 
/*      */ import icancloudgui.TabbedPanels.CPUsPanel;
/*      */ import icancloudgui.TabbedPanels.CloudsPanel;
/*      */ import icancloudgui.TabbedPanels.DataCenterPanel;
/*      */ import icancloudgui.TabbedPanels.DataCenterSaveDialog;
/*      */ import icancloudgui.TabbedPanels.DiskPanel;
/*      */ import icancloudgui.TabbedPanels.MemoryPanel;
/*      */ import icancloudgui.TabbedPanels.MetamorphicPanel;
/*      */ import icancloudgui.TabbedPanels.NodePanel;
/*      */ import icancloudgui.TabbedPanels.PSUsPanel;
/*      */ import icancloudgui.TabbedPanels.RackPanel;
/*      */ import icancloudgui.TabbedPanels.SwitchPanel;
/*      */ import icancloudgui.TabbedPanels.UsersPanel;
/*      */ import icancloudgui.TabbedPanels.VirtualizationPanel;
/*      */ import icancloudgui.Utils.Configuration;
/*      */ import icancloudgui.Utils.Utils;
/*      */ import java.awt.Dimension;
/*      */ import java.awt.event.ActionEvent;
/*      */ import java.awt.event.ActionListener;
/*      */ import java.awt.event.MouseAdapter;
/*      */ import java.awt.event.MouseEvent;
/*      */ import javax.swing.ActionMap;
/*      */ import javax.swing.JDialog;
/*      */ import javax.swing.JFrame;
/*      */ import javax.swing.JMenu;
/*      */ import javax.swing.JMenuBar;
/*      */ import javax.swing.JMenuItem;
/*      */ import javax.swing.JOptionPane;
/*      */ import javax.swing.JPanel;
/*      */ import javax.swing.JPopupMenu;
/*      */ import javax.swing.JPopupMenu.Separator;
/*      */ import javax.swing.JScrollPane;
/*      */ import javax.swing.JTabbedPane;
/*      */ import javax.swing.JTree;
/*      */ import javax.swing.event.MenuEvent;
/*      */ import javax.swing.event.MenuListener;
/*      */ import javax.swing.tree.DefaultMutableTreeNode;
/*      */ import javax.swing.tree.DefaultTreeModel;
/*      */ import javax.swing.tree.TreePath;
/*      */ import javax.swing.tree.TreeSelectionModel;
/*      */ import org.jdesktop.application.Action;
/*      */ import org.jdesktop.application.Application;
/*      */ import org.jdesktop.application.ApplicationContext;
/*      */ import org.jdesktop.application.FrameView;
/*      */ import org.jdesktop.application.ResourceMap;
/*      */ import org.jdesktop.application.SingleFrameApplication;
/*      */ import org.netbeans.lib.awtextra.AbsoluteConstraints;
/*      */ import org.netbeans.lib.awtextra.AbsoluteLayout;
/*      */ 
/*      */ public class ICanCloudGUIView extends FrameView
/*      */ {
/*      */   CPUsPanel cpuPanel;
/*      */   JScrollPane cpuScroll;
/*      */   DefaultMutableTreeNode cpuNode;
/*      */   DiskPanel diskPanel;
/*      */   JScrollPane diskScroll;
/*      */   DefaultMutableTreeNode diskNode;
/*      */   MemoryPanel memoryPanel;
/*      */   JScrollPane memoryScroll;
/*      */   DefaultMutableTreeNode memoryNode;
/*      */   PSUsPanel psuPanel;
/*      */   JScrollPane psuScroll;
/*      */   DefaultMutableTreeNode psuNode;
/*      */   NodePanel nodePanel;
/*      */   JScrollPane nodeScroll;
/*      */   DefaultMutableTreeNode nodeNode;
/*      */   DefaultMutableTreeNode nodeComputingNode;
/*      */   DefaultMutableTreeNode nodeStorageNode;
/*      */   SwitchPanel switchPanel;
/*      */   JScrollPane switchScroll;
/*      */   DefaultMutableTreeNode switchNode;
/*      */   RackPanel rackPanel;
/*      */   JScrollPane rackScroll;
/*      */   DefaultMutableTreeNode rackNode;
/*      */   DataCenterPanel dataCenterPanel;
/*      */   JScrollPane dataCenterScroll;
/*      */   DefaultMutableTreeNode dataCenterNode;
/*      */   VirtualizationPanel virtualizationPanel;
/*      */   JScrollPane virtualizationScroll;
/*      */   DefaultMutableTreeNode virtualizationNode;
/*      */   UsersPanel usersPanel;
/*      */   JScrollPane usersScroll;
/*      */   DefaultMutableTreeNode usersNode;
/*      */   CloudsPanel cloudsPanel;
/*      */   JScrollPane cloudsScroll;
/*      */   DefaultMutableTreeNode cloudsNode;
/*      */   MetamorphicPanel metamorphicPanel;
/*      */   JScrollPane metamorphicScroll;
/*      */   private JMenuItem itemNewDataCenter;
/*      */   private JMenuItem itemSaveAsDataCenter;
/*      */   private JMenuItem itemSaveDataCenter;
/*      */   private JPopupMenu.Separator jSeparator1;
/*      */   private JMenuItem leafComponentAdd;
/*      */   private JMenuItem leafComponentEdit;
/*      */   private JMenuItem leafComponentRemove;
/*      */   private JPopupMenu leafNodePopup;
/*      */   private JPanel mainPanel;
/*      */   private JMenuBar menuBar;
/*      */   private JMenu menuDataCenter;
/*      */   private JScrollPane scrollTree;
/*      */   private JPopupMenu.Separator separator;
/*      */   private JTree treeArea;
/*      */   private JTabbedPane workingArea;
/*      */   private JDialog aboutBox;
/*      */   
/*      */   public ICanCloudGUIView(SingleFrameApplication app)
/*      */   {
/*  109 */     super(app);
/*      */     
/*      */ 
/*  112 */     initComponents();
/*      */     
/*      */ 
/*  115 */     getFrame().setResizable(true);
/*      */     
/*      */ 
/*  118 */     setUpTabs();
/*      */     
/*      */ 
/*  121 */     setUpTree();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   private void setUpTabs()
/*      */   {
/*  130 */     this.cpuPanel = new CPUsPanel(this);
/*  131 */     this.cpuScroll = new JScrollPane();
/*  132 */     this.cpuScroll.setHorizontalScrollBarPolicy(31);
/*  133 */     this.cpuScroll.setVerticalScrollBarPolicy(21);
/*  134 */     this.cpuScroll.setViewportView(this.cpuPanel);
/*  135 */     this.workingArea.addTab("CPUs", this.cpuScroll);
/*      */     
/*      */ 
/*  138 */     this.diskPanel = new DiskPanel(this);
/*  139 */     this.diskScroll = new JScrollPane();
/*  140 */     this.diskScroll.setHorizontalScrollBarPolicy(31);
/*  141 */     this.diskScroll.setVerticalScrollBarPolicy(21);
/*  142 */     this.diskScroll.setViewportView(this.diskPanel);
/*  143 */     this.workingArea.addTab("Disks", this.diskScroll);
/*      */     
/*      */ 
/*  146 */     this.memoryPanel = new MemoryPanel(this);
/*  147 */     this.memoryScroll = new JScrollPane();
/*  148 */     this.memoryScroll.setHorizontalScrollBarPolicy(31);
/*  149 */     this.memoryScroll.setVerticalScrollBarPolicy(21);
/*  150 */     this.memoryScroll.setViewportView(this.memoryPanel);
/*  151 */     this.workingArea.addTab("Memories", this.memoryScroll);
/*      */     
/*      */ 
/*  154 */     this.psuPanel = new PSUsPanel(this);
/*  155 */     this.psuScroll = new JScrollPane();
/*  156 */     this.psuScroll.setHorizontalScrollBarPolicy(31);
/*  157 */     this.psuScroll.setVerticalScrollBarPolicy(21);
/*  158 */     this.psuScroll.setViewportView(this.psuPanel);
/*  159 */     this.workingArea.addTab("PSUs", this.psuScroll);
/*      */     
/*      */ 
/*  162 */     this.nodePanel = new NodePanel(this);
/*  163 */     this.nodeScroll = new JScrollPane();
/*  164 */     this.nodeScroll.setHorizontalScrollBarPolicy(31);
/*  165 */     this.nodeScroll.setVerticalScrollBarPolicy(20);
/*  166 */     this.nodeScroll.setViewportView(this.nodePanel);
/*  167 */     this.workingArea.addTab("Nodes", this.nodeScroll);
/*      */     
/*      */ 
/*  170 */     this.switchPanel = new SwitchPanel(this);
/*  171 */     this.switchScroll = new JScrollPane();
/*  172 */     this.switchScroll.setHorizontalScrollBarPolicy(31);
/*  173 */     this.switchScroll.setVerticalScrollBarPolicy(21);
/*  174 */     this.switchScroll.setViewportView(this.switchPanel);
/*  175 */     this.workingArea.addTab("Switches", this.switchScroll);
/*      */     
/*      */ 
/*  178 */     this.rackPanel = new RackPanel(this);
/*  179 */     this.rackScroll = new JScrollPane();
/*  180 */     this.rackScroll.setHorizontalScrollBarPolicy(31);
/*  181 */     this.rackScroll.setVerticalScrollBarPolicy(20);
/*  182 */     this.rackScroll.setViewportView(this.rackPanel);
/*  183 */     this.workingArea.addTab("Racks", this.rackScroll);
/*      */     
/*      */ 
/*  186 */     this.dataCenterPanel = new DataCenterPanel(this);
/*  187 */     this.dataCenterScroll = new JScrollPane();
/*  188 */     this.dataCenterScroll.setHorizontalScrollBarPolicy(31);
/*  189 */     this.dataCenterScroll.setVerticalScrollBarPolicy(20);
/*  190 */     this.dataCenterScroll.setViewportView(this.dataCenterPanel);
/*  191 */     this.workingArea.addTab("Data-Center", this.dataCenterScroll);
/*      */     
/*      */ 
/*  194 */     this.virtualizationPanel = new VirtualizationPanel(this);
/*  195 */     this.virtualizationScroll = new JScrollPane();
/*  196 */     this.virtualizationScroll.setHorizontalScrollBarPolicy(31);
/*  197 */     this.virtualizationScroll.setVerticalScrollBarPolicy(21);
/*  198 */     this.virtualizationScroll.setViewportView(this.virtualizationPanel);
/*  199 */     this.workingArea.addTab("Virtualization", this.virtualizationScroll);
/*      */     
/*      */ 
/*  202 */     this.usersPanel = new UsersPanel(this);
/*  203 */     this.usersScroll = new JScrollPane();
/*  204 */     this.usersScroll.setHorizontalScrollBarPolicy(31);
/*  205 */     this.usersScroll.setVerticalScrollBarPolicy(21);
/*  206 */     this.usersScroll.setViewportView(this.usersPanel);
/*  207 */     this.workingArea.addTab("Users", this.usersScroll);
/*      */     
/*      */ 
/*  210 */     this.cloudsPanel = new CloudsPanel(this);
/*  211 */     this.cloudsScroll = new JScrollPane();
/*  212 */     this.cloudsScroll.setHorizontalScrollBarPolicy(31);
/*  213 */     this.cloudsScroll.setVerticalScrollBarPolicy(20);
/*  214 */     this.cloudsScroll.setViewportView(this.cloudsPanel);
/*  215 */     this.workingArea.addTab("Clouds", this.cloudsScroll);
/*      */     
/*      */ 
/*  218 */     this.metamorphicPanel = new MetamorphicPanel();
/*  219 */     this.metamorphicScroll = new JScrollPane();
/*  220 */     this.metamorphicScroll.setHorizontalScrollBarPolicy(31);
/*  221 */     this.metamorphicScroll.setVerticalScrollBarPolicy(21);
/*  222 */     this.metamorphicScroll.setViewportView(this.metamorphicPanel);
/*  223 */     this.workingArea.addTab("Metamorphic testing", this.metamorphicScroll);
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   private void setUpTree()
/*      */   {
/*  236 */     DefaultMutableTreeNode root = new DefaultMutableTreeNode("iCanCloud");
/*  237 */     DefaultTreeModel treeModel = new DefaultTreeModel(root);
/*      */     
/*      */ 
/*  240 */     DefaultMutableTreeNode node = new DefaultMutableTreeNode("Hardware");
/*      */     
/*  242 */     this.cpuNode = new DefaultMutableTreeNode("CPUs");
/*  243 */     node.add(this.cpuNode);
/*      */     
/*  245 */     this.memoryNode = new DefaultMutableTreeNode("Memories");
/*  246 */     node.add(this.memoryNode);
/*      */     
/*  248 */     this.diskNode = new DefaultMutableTreeNode("Disks");
/*  249 */     node.add(this.diskNode);
/*      */     
/*  251 */     this.psuNode = new DefaultMutableTreeNode("PSUs");
/*  252 */     node.add(this.psuNode);
/*      */     
/*  254 */     root.add(node);
/*      */     
/*      */ 
/*  257 */     this.nodeNode = new DefaultMutableTreeNode("Nodes");
/*  258 */     this.nodeComputingNode = new DefaultMutableTreeNode("Computing Nodes");
/*  259 */     this.nodeStorageNode = new DefaultMutableTreeNode("Storage Nodes");
/*  260 */     this.nodeNode.add(this.nodeComputingNode);
/*  261 */     this.nodeNode.add(this.nodeStorageNode);
/*  262 */     root.add(this.nodeNode);
/*      */     
/*      */ 
/*      */ 
/*  266 */     this.switchNode = new DefaultMutableTreeNode("Switches");
/*  267 */     root.add(this.switchNode);
/*      */     
/*      */ 
/*  270 */     this.rackNode = new DefaultMutableTreeNode("Racks");
/*  271 */     root.add(this.rackNode);
/*      */     
/*      */ 
/*  274 */     this.dataCenterNode = new DefaultMutableTreeNode("Data-Center");
/*  275 */     root.add(this.dataCenterNode);
/*      */     
/*      */ 
/*  278 */     this.virtualizationNode = new DefaultMutableTreeNode("Virtualization");
/*  279 */     root.add(this.virtualizationNode);
/*      */     
/*      */ 
/*  282 */     this.usersNode = new DefaultMutableTreeNode("Users");
/*  283 */     root.add(this.usersNode);
/*      */     
/*      */ 
/*  286 */     this.cloudsNode = new DefaultMutableTreeNode("Clouds");
/*  287 */     root.add(this.cloudsNode);
/*      */     
/*      */ 
/*      */ 
/*  291 */     this.treeArea.setRootVisible(true);
/*  292 */     this.treeArea.setModel(treeModel);
/*      */     
/*      */ 
/*  295 */     this.treeArea.getSelectionModel().setSelectionMode(1);
/*      */     
/*      */ 
/*  298 */     updateTree();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */   @Action
/*      */   public void showAboutBox()
/*      */   {
/*  306 */     if (this.aboutBox == null) {
/*  307 */       JFrame mainFrame = ICanCloudGUIApp.getApplication().getMainFrame();
/*  308 */       this.aboutBox = new ICanCloudGUIAboutBox(mainFrame);
/*  309 */       this.aboutBox.setLocationRelativeTo(mainFrame);
/*      */     }
/*  311 */     ICanCloudGUIApp.getApplication().show(this.aboutBox);
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void updateTree()
/*      */   {
/*  320 */     Utils.updateTreeNode(Configuration.REPOSITORY_CPUS_FOLDER, this.cpuNode);
/*      */     
/*      */ 
/*  323 */     Utils.updateTreeNode(Configuration.REPOSITORY_MEMORIES_FOLDER, this.memoryNode);
/*      */     
/*      */ 
/*  326 */     Utils.updateTreeNode(Configuration.REPOSITORY_DISKS_FOLDER, this.diskNode);
/*      */     
/*      */ 
/*  329 */     Utils.updateTreeNode(Configuration.REPOSITORY_PSU_FOLDER, this.psuNode);
/*      */     
/*      */ 
/*  332 */     Utils.updateTreeNode(Configuration.REPOSITORY_COMPUTING_NODES_FOLDER, this.nodeComputingNode);
/*      */     
/*      */ 
/*  335 */     Utils.updateTreeNode(Configuration.REPOSITORY_STORAGE_NODES_FOLDER, this.nodeStorageNode);
/*      */     
/*      */ 
/*  338 */     Utils.updateTreeNode(Configuration.REPOSITORY_SWITCHES, this.switchNode);
/*      */     
/*      */ 
/*  341 */     Utils.updateTreeNode(Configuration.REPOSITORY_RACKS, this.rackNode);
/*      */     
/*      */ 
/*  344 */     Utils.updateTreeNode(Configuration.REPOSITORY_DATA_CENTER, this.dataCenterNode);
/*      */     
/*      */ 
/*  347 */     Utils.updateTreeNode(Configuration.REPOSITORY_VM, this.virtualizationNode);
/*      */     
/*      */ 
/*  350 */     Utils.updateTreeNode(Configuration.REPOSITORY_USERS, this.usersNode);
/*      */     
/*      */ 
/*  353 */     Utils.updateTreeNode(Configuration.REPOSITORY_CLOUD, this.cloudsNode);
/*      */     
/*      */ 
/*      */ 
/*  357 */     this.nodePanel.reloadPanel();
/*      */     
/*      */ 
/*  360 */     this.rackPanel.reloadPanel();
/*      */     
/*      */ 
/*  363 */     this.usersPanel.reloadPanel();
/*      */     
/*      */ 
/*  366 */     this.cloudsPanel.reloadPanel();
/*      */     
/*      */ 
/*  369 */     this.treeArea.updateUI();
/*      */   }
/*      */   
/*      */ 
/*      */   public void saveDataCenter(String name)
/*      */   {
/*  375 */     this.dataCenterPanel.writeDataCentertoFile(name);
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   private void initComponents()
/*      */   {
/*  388 */     this.mainPanel = new JPanel();
/*  389 */     this.scrollTree = new JScrollPane();
/*  390 */     this.treeArea = new JTree();
/*  391 */     this.workingArea = new JTabbedPane();
/*  392 */     this.menuBar = new JMenuBar();
/*  393 */     JMenu fileMenu = new JMenu();
/*  394 */     JMenuItem exitMenuItem = new JMenuItem();
/*  395 */     this.menuDataCenter = new JMenu();
/*  396 */     this.itemNewDataCenter = new JMenuItem();
/*  397 */     this.jSeparator1 = new JPopupMenu.Separator();
/*  398 */     this.itemSaveAsDataCenter = new JMenuItem();
/*  399 */     this.itemSaveDataCenter = new JMenuItem();
/*  400 */     JMenu helpMenu = new JMenu();
/*  401 */     JMenuItem aboutMenuItem = new JMenuItem();
/*  402 */     this.leafNodePopup = new JPopupMenu();
/*  403 */     this.leafComponentRemove = new JMenuItem();
/*  404 */     this.leafComponentEdit = new JMenuItem();
/*  405 */     this.separator = new JPopupMenu.Separator();
/*  406 */     this.leafComponentAdd = new JMenuItem();
/*      */     
/*  408 */     this.mainPanel.setMinimumSize(new Dimension(1020, 610));
/*  409 */     this.mainPanel.setName("mainPanel");
/*  410 */     this.mainPanel.setPreferredSize(new Dimension(1020, 600));
/*  411 */     this.mainPanel.setLayout(new AbsoluteLayout());
/*      */     
/*  413 */     this.scrollTree.setName("scrollTree");
/*  414 */     this.scrollTree.setPreferredSize(new Dimension(80, 570));
/*      */     
/*  416 */     ResourceMap resourceMap = ((ICanCloudGUIApp)Application.getInstance(ICanCloudGUIApp.class)).getContext().getResourceMap(ICanCloudGUIView.class);
/*  417 */     this.treeArea.setFont(resourceMap.getFont("treeArea.font"));
/*  418 */     this.treeArea.setName("treeArea");
/*  419 */     this.treeArea.addMouseListener(new MouseAdapter() {
/*      */       public void mouseClicked(MouseEvent evt) {
/*  421 */         ICanCloudGUIView.this.treeAreaMouseClicked(evt);
/*      */       }
/*  423 */     });
/*  424 */     this.scrollTree.setViewportView(this.treeArea);
/*      */     
/*  426 */     this.mainPanel.add(this.scrollTree, new AbsoluteConstraints(10, 10, 200, 600));
/*      */     
/*  428 */     this.workingArea.setBackground(resourceMap.getColor("workingArea.background"));
/*  429 */     this.workingArea.setAutoscrolls(true);
/*  430 */     this.workingArea.setMinimumSize(new Dimension(820, 600));
/*  431 */     this.workingArea.setName("workingArea");
/*  432 */     this.workingArea.setPreferredSize(new Dimension(820, 600));
/*  433 */     this.mainPanel.add(this.workingArea, new AbsoluteConstraints(210, 10, 870, -1));
/*      */     
/*  435 */     this.menuBar.setName("menuBar");
/*      */     
/*  437 */     fileMenu.setText(resourceMap.getString("fileMenu.text", new Object[0]));
/*  438 */     fileMenu.setName("fileMenu");
/*      */     
/*  440 */     ActionMap actionMap = ((ICanCloudGUIApp)Application.getInstance(ICanCloudGUIApp.class)).getContext().getActionMap(ICanCloudGUIView.class, this);
/*  441 */     exitMenuItem.setAction(actionMap.get("quit"));
/*  442 */     exitMenuItem.setName("exitMenuItem");
/*  443 */     fileMenu.add(exitMenuItem);
/*      */     
/*  445 */     this.menuBar.add(fileMenu);
/*      */     
/*  447 */     this.menuDataCenter.setText(resourceMap.getString("menuDataCenter.text", new Object[0]));
/*  448 */     this.menuDataCenter.setName("menuDataCenter");
/*  449 */     this.menuDataCenter.addMenuListener(new MenuListener() {
/*      */       public void menuCanceled(MenuEvent evt) {}
/*      */       
/*      */       public void menuDeselected(MenuEvent evt) {}
/*      */       
/*      */       public void menuSelected(MenuEvent evt) {
/*  455 */         ICanCloudGUIView.this.menuDataCenterMenuSelected(evt);
/*      */       }
/*      */       
/*  458 */     });
/*  459 */     this.itemNewDataCenter.setText(resourceMap.getString("itemNewDataCenter.text", new Object[0]));
/*  460 */     this.itemNewDataCenter.setName("itemNewDataCenter");
/*  461 */     this.itemNewDataCenter.addActionListener(new ActionListener() {
/*      */       public void actionPerformed(ActionEvent evt) {
/*  463 */         ICanCloudGUIView.this.itemNewDataCenterActionPerformed(evt);
/*      */       }
/*  465 */     });
/*  466 */     this.menuDataCenter.add(this.itemNewDataCenter);
/*      */     
/*  468 */     this.jSeparator1.setName("jSeparator1");
/*  469 */     this.menuDataCenter.add(this.jSeparator1);
/*      */     
/*  471 */     this.itemSaveAsDataCenter.setText(resourceMap.getString("itemSaveAsDataCenter.text", new Object[0]));
/*  472 */     this.itemSaveAsDataCenter.setName("itemSaveAsDataCenter");
/*  473 */     this.itemSaveAsDataCenter.addActionListener(new ActionListener() {
/*      */       public void actionPerformed(ActionEvent evt) {
/*  475 */         ICanCloudGUIView.this.itemSaveAsDataCenterActionPerformed(evt);
/*      */       }
/*  477 */     });
/*  478 */     this.menuDataCenter.add(this.itemSaveAsDataCenter);
/*      */     
/*  480 */     this.itemSaveDataCenter.setText(resourceMap.getString("itemSaveDataCenter.text", new Object[0]));
/*  481 */     this.itemSaveDataCenter.setName("itemSaveDataCenter");
/*  482 */     this.itemSaveDataCenter.addActionListener(new ActionListener() {
/*      */       public void actionPerformed(ActionEvent evt) {
/*  484 */         ICanCloudGUIView.this.itemSaveDataCenterActionPerformed(evt);
/*      */       }
/*  486 */     });
/*  487 */     this.menuDataCenter.add(this.itemSaveDataCenter);
/*      */     
/*  489 */     this.menuBar.add(this.menuDataCenter);
/*      */     
/*  491 */     helpMenu.setText(resourceMap.getString("helpMenu.text", new Object[0]));
/*  492 */     helpMenu.setName("helpMenu");
/*      */     
/*  494 */     aboutMenuItem.setAction(actionMap.get("showAboutBox"));
/*  495 */     aboutMenuItem.setName("aboutMenuItem");
/*  496 */     helpMenu.add(aboutMenuItem);
/*      */     
/*  498 */     this.menuBar.add(helpMenu);
/*      */     
/*  500 */     this.leafNodePopup.setToolTipText(resourceMap.getString("leafNodePopup.toolTipText", new Object[0]));
/*  501 */     this.leafNodePopup.setName("leafNodePopup");
/*      */     
/*  503 */     this.leafComponentRemove.setText(resourceMap.getString("leafComponentRemove.text", new Object[0]));
/*  504 */     this.leafComponentRemove.setToolTipText(resourceMap.getString("leafComponentRemove.toolTipText", new Object[0]));
/*  505 */     this.leafComponentRemove.setName("leafComponentRemove");
/*  506 */     this.leafComponentRemove.addActionListener(new ActionListener() {
/*      */       public void actionPerformed(ActionEvent evt) {
/*  508 */         ICanCloudGUIView.this.leafComponentRemoveActionPerformed(evt);
/*      */       }
/*  510 */     });
/*  511 */     this.leafNodePopup.add(this.leafComponentRemove);
/*      */     
/*  513 */     this.leafComponentEdit.setText(resourceMap.getString("leafComponentEdit.text", new Object[0]));
/*  514 */     this.leafComponentEdit.setToolTipText(resourceMap.getString("leafComponentEdit.toolTipText", new Object[0]));
/*  515 */     this.leafComponentEdit.setName("leafComponentEdit");
/*  516 */     this.leafComponentEdit.addActionListener(new ActionListener() {
/*      */       public void actionPerformed(ActionEvent evt) {
/*  518 */         ICanCloudGUIView.this.leafComponentEditActionPerformed(evt);
/*      */       }
/*  520 */     });
/*  521 */     this.leafNodePopup.add(this.leafComponentEdit);
/*      */     
/*  523 */     this.separator.setName("separator");
/*  524 */     this.leafNodePopup.add(this.separator);
/*      */     
/*  526 */     this.leafComponentAdd.setText(resourceMap.getString("leafComponentAdd.text", new Object[0]));
/*  527 */     this.leafComponentAdd.setToolTipText(resourceMap.getString("leafComponentAdd.toolTipText", new Object[0]));
/*  528 */     this.leafComponentAdd.setName("leafComponentAdd");
/*  529 */     this.leafComponentAdd.addActionListener(new ActionListener() {
/*      */       public void actionPerformed(ActionEvent evt) {
/*  531 */         ICanCloudGUIView.this.leafComponentAddActionPerformed(evt);
/*      */       }
/*  533 */     });
/*  534 */     this.leafNodePopup.add(this.leafComponentAdd);
/*      */     
/*  536 */     setComponent(this.mainPanel);
/*  537 */     setMenuBar(this.menuBar);
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   private void treeAreaMouseClicked(MouseEvent evt)
/*      */   {
/*  552 */     if ((evt.getClickCount() == 2) && (evt.getButton() != 3))
/*      */     {
/*      */ 
/*  555 */       if (this.treeArea.getPathForLocation(evt.getX(), evt.getY()) != null)
/*      */       {
/*      */ 
/*  558 */         DefaultMutableTreeNode node = (DefaultMutableTreeNode)this.treeArea.getSelectionPath().getLastPathComponent();
/*      */         
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*  567 */         if (node.isLeaf())
/*      */         {
/*      */ 
/*  570 */           if (this.treeArea.getSelectionPath().getParentPath().getLastPathComponent().toString().equals("CPUs"))
/*      */           {
/*      */ 
/*  573 */             this.workingArea.setSelectedIndex(Utils.getTabIndexFromName(this.workingArea, "CPUs"));
/*      */             
/*      */ 
/*  576 */             this.cpuPanel.loadCpuInPanel(this.treeArea.getSelectionPath().getLastPathComponent().toString());
/*      */           }
/*      */           
/*      */ 
/*  580 */           if (this.treeArea.getSelectionPath().getParentPath().getLastPathComponent().toString().equals("Memories"))
/*      */           {
/*      */ 
/*  583 */             this.workingArea.setSelectedIndex(Utils.getTabIndexFromName(this.workingArea, "Memories"));
/*      */             
/*      */ 
/*  586 */             this.memoryPanel.loadMemoryInPanel(this.treeArea.getSelectionPath().getLastPathComponent().toString());
/*      */           }
/*      */           
/*      */ 
/*  590 */           if (this.treeArea.getSelectionPath().getParentPath().getLastPathComponent().toString().equals("PSUs"))
/*      */           {
/*      */ 
/*  593 */             this.workingArea.setSelectedIndex(Utils.getTabIndexFromName(this.workingArea, "PSUs"));
/*      */             
/*      */ 
/*  596 */             this.psuPanel.loadPSUInPanel(this.treeArea.getSelectionPath().getLastPathComponent().toString());
/*      */           }
/*      */           
/*      */ 
/*  600 */           if (this.treeArea.getSelectionPath().getParentPath().getLastPathComponent().toString().equals("Disks"))
/*      */           {
/*      */ 
/*  603 */             this.workingArea.setSelectedIndex(Utils.getTabIndexFromName(this.workingArea, "Disks"));
/*      */             
/*      */ 
/*  606 */             this.diskPanel.loadDiskInPanel(this.treeArea.getSelectionPath().getLastPathComponent().toString());
/*      */           }
/*      */           
/*      */ 
/*  610 */           if (this.treeArea.getSelectionPath().getParentPath().getLastPathComponent().toString().equals("Computing Nodes"))
/*      */           {
/*      */ 
/*  613 */             this.workingArea.setSelectedIndex(Utils.getTabIndexFromName(this.workingArea, "Nodes"));
/*      */             
/*      */ 
/*  616 */             this.nodePanel.loadNodeInPanel(false, this.treeArea.getSelectionPath().getLastPathComponent().toString());
/*      */           }
/*      */           
/*      */ 
/*  620 */           if (this.treeArea.getSelectionPath().getParentPath().getLastPathComponent().toString().equals("Storage Nodes"))
/*      */           {
/*      */ 
/*  623 */             this.workingArea.setSelectedIndex(Utils.getTabIndexFromName(this.workingArea, "Nodes"));
/*      */             
/*      */ 
/*  626 */             this.nodePanel.loadNodeInPanel(true, this.treeArea.getSelectionPath().getLastPathComponent().toString());
/*      */           }
/*      */           
/*      */ 
/*  630 */           if (this.treeArea.getSelectionPath().getParentPath().getLastPathComponent().toString().equals("Switches"))
/*      */           {
/*      */ 
/*  633 */             this.workingArea.setSelectedIndex(Utils.getTabIndexFromName(this.workingArea, "Switches"));
/*      */             
/*      */ 
/*  636 */             this.switchPanel.loadSwitchInPanel(this.treeArea.getSelectionPath().getLastPathComponent().toString());
/*      */           }
/*      */           
/*      */ 
/*  640 */           if (this.treeArea.getSelectionPath().getParentPath().getLastPathComponent().toString().equals("Racks"))
/*      */           {
/*      */ 
/*  643 */             this.workingArea.setSelectedIndex(Utils.getTabIndexFromName(this.workingArea, "Racks"));
/*      */             
/*      */ 
/*  646 */             this.rackPanel.loadRackInPanel(this.treeArea.getSelectionPath().getLastPathComponent().toString());
/*      */           }
/*      */           
/*      */ 
/*  650 */           if (this.treeArea.getSelectionPath().getParentPath().getLastPathComponent().toString().equals("Virtualization"))
/*      */           {
/*      */ 
/*  653 */             this.workingArea.setSelectedIndex(Utils.getTabIndexFromName(this.workingArea, "Virtualization"));
/*      */             
/*      */ 
/*  656 */             this.virtualizationPanel.loadVMInPanel(this.treeArea.getSelectionPath().getLastPathComponent().toString());
/*      */           }
/*      */           
/*      */ 
/*  660 */           if (this.treeArea.getSelectionPath().getParentPath().getLastPathComponent().toString().equals("Users"))
/*      */           {
/*      */ 
/*  663 */             this.workingArea.setSelectedIndex(Utils.getTabIndexFromName(this.workingArea, "Users"));
/*      */             
/*      */ 
/*  666 */             this.usersPanel.loadUserInPanel(this.treeArea.getSelectionPath().getLastPathComponent().toString());
/*      */           }
/*      */           
/*      */ 
/*  670 */           if (this.treeArea.getSelectionPath().getParentPath().getLastPathComponent().toString().equals("Data-Center"))
/*      */           {
/*      */ 
/*  673 */             this.workingArea.setSelectedIndex(Utils.getTabIndexFromName(this.workingArea, "Data-Center"));
/*      */             
/*      */ 
/*  676 */             this.dataCenterPanel.loadDataCenterInPanel(this.treeArea.getSelectionPath().getLastPathComponent().toString());
/*      */           }
/*      */           
/*      */ 
/*  680 */           if (this.treeArea.getSelectionPath().getParentPath().getLastPathComponent().toString().equals("Clouds"))
/*      */           {
/*      */ 
/*  683 */             this.workingArea.setSelectedIndex(Utils.getTabIndexFromName(this.workingArea, "Clouds"));
/*      */             
/*      */ 
/*  686 */             this.cloudsPanel.loadCloudInPanel(this.treeArea.getSelectionPath().getLastPathComponent().toString());
/*      */ 
/*      */           }
/*      */           
/*      */ 
/*      */         }
/*      */         else
/*      */         {
/*      */ 
/*  695 */           if (this.treeArea.getSelectionPath().getLastPathComponent().toString().equals("Nodes"))
/*      */           {
/*      */ 
/*  698 */             this.workingArea.setSelectedIndex(Utils.getTabIndexFromName(this.workingArea, "Nodes"));
/*      */           }
/*      */           
/*      */ 
/*  702 */           if (this.treeArea.getSelectionPath().getLastPathComponent().toString().equals("Switches"))
/*      */           {
/*      */ 
/*  705 */             this.workingArea.setSelectedIndex(Utils.getTabIndexFromName(this.workingArea, "Switches"));
/*      */           }
/*      */           
/*      */ 
/*  709 */           if (this.treeArea.getSelectionPath().getLastPathComponent().toString().equals("Racks"))
/*      */           {
/*      */ 
/*  712 */             this.workingArea.setSelectedIndex(Utils.getTabIndexFromName(this.workingArea, "Racks"));
/*      */           }
/*      */           
/*      */ 
/*  716 */           if (this.treeArea.getSelectionPath().getLastPathComponent().toString().equals("Users"))
/*      */           {
/*      */ 
/*  719 */             this.workingArea.setSelectedIndex(Utils.getTabIndexFromName(this.workingArea, "Users"));
/*      */           }
/*      */           
/*      */ 
/*  723 */           if (this.treeArea.getSelectionPath().getLastPathComponent().toString().equals("Virtualization"))
/*      */           {
/*      */ 
/*  726 */             this.workingArea.setSelectedIndex(Utils.getTabIndexFromName(this.workingArea, "Virtualization"));
/*      */           }
/*      */           
/*      */ 
/*  730 */           if (this.treeArea.getSelectionPath().getLastPathComponent().toString().equals("Data-Center"))
/*      */           {
/*      */ 
/*  733 */             this.workingArea.setSelectedIndex(Utils.getTabIndexFromName(this.workingArea, "Data-Center"));
/*      */           }
/*      */           
/*      */ 
/*  737 */           if (this.treeArea.getSelectionPath().getLastPathComponent().toString().equals("Clouds"))
/*      */           {
/*      */ 
/*  740 */             this.workingArea.setSelectedIndex(Utils.getTabIndexFromName(this.workingArea, "Clouds"));
/*      */           }
/*      */           
/*      */         }
/*      */         
/*      */       }
/*      */       
/*      */ 
/*      */     }
/*  749 */     else if (evt.getButton() == 3)
/*      */     {
/*      */ 
/*  752 */       if (this.treeArea.getPathForLocation(evt.getX(), evt.getY()) != null)
/*      */       {
/*      */ 
/*  755 */         DefaultMutableTreeNode node = (DefaultMutableTreeNode)this.treeArea.getSelectionPath().getLastPathComponent();
/*      */         
/*      */ 
/*  758 */         if (node.isLeaf())
/*      */         {
/*      */ 
/*  761 */           if ((this.treeArea.getSelectionPath().getParentPath().getLastPathComponent().toString().equals("CPUs")) || (this.treeArea.getSelectionPath().getParentPath().getLastPathComponent().toString().equals("Memories")) || (this.treeArea.getSelectionPath().getParentPath().getLastPathComponent().toString().equals("Disks")) || (this.treeArea.getSelectionPath().getParentPath().getLastPathComponent().toString().equals("PSUs")) || (this.treeArea.getSelectionPath().getParentPath().getLastPathComponent().toString().equals("Virtualization")) || (this.treeArea.getSelectionPath().getParentPath().getLastPathComponent().toString().equals("Users")) || (this.treeArea.getSelectionPath().getParentPath().getLastPathComponent().toString().equals("Clouds")) || (this.treeArea.getSelectionPath().getParentPath().getLastPathComponent().toString().equals("Data-Center")))
/*      */           {
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*  770 */             this.leafComponentAdd.setEnabled(false);
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */           }
/*  777 */           else if (((this.treeArea.getSelectionPath().getParentPath().getLastPathComponent().toString().equals("Storage Nodes")) || (this.treeArea.getSelectionPath().getParentPath().getLastPathComponent().toString().equals("Computing Nodes")) || (this.treeArea.getSelectionPath().getParentPath().getLastPathComponent().toString().equals("Switches")) || (!this.treeArea.getSelectionPath().getParentPath().getLastPathComponent().toString().equals("Racks"))) || 
/*      */           
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*  786 */             (this.workingArea.getSelectedIndex() == Utils.getTabIndexFromName(this.workingArea, "Data-Center")))
/*      */           {
/*      */ 
/*  789 */             this.leafComponentAdd.setEnabled(true);
/*      */ 
/*      */           }
/*      */           else
/*      */           {
/*  794 */             this.leafComponentAdd.setEnabled(false);
/*      */           }
/*      */           
/*      */ 
/*      */ 
/*  799 */           this.leafNodePopup.show(evt.getComponent(), evt.getX(), evt.getY());
/*      */         }
/*      */       }
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   private void leafComponentRemoveActionPerformed(ActionEvent evt)
/*      */   {
/*  817 */     boolean requiredUpdate = false;
/*      */     
/*      */ 
/*      */ 
/*  821 */     if (this.treeArea.getSelectionPath().getParentPath().getLastPathComponent().toString().equals("CPUs"))
/*      */     {
/*      */ 
/*  824 */       this.workingArea.setSelectedIndex(Utils.getTabIndexFromName(this.workingArea, "CPUs"));
/*      */       
/*      */ 
/*  827 */       requiredUpdate = Utils.removeComponent(Configuration.REPOSITORY_CPUS_FOLDER, this.treeArea.getSelectionPath().getLastPathComponent().toString());
/*      */       
/*      */ 
/*  830 */       if (requiredUpdate) {
/*  831 */         updateTree();
/*  832 */         this.cpuPanel.clear();
/*      */       }
/*      */     }
/*      */     
/*      */ 
/*  837 */     if (this.treeArea.getSelectionPath().getParentPath().getLastPathComponent().toString().equals("Memories"))
/*      */     {
/*      */ 
/*  840 */       this.workingArea.setSelectedIndex(Utils.getTabIndexFromName(this.workingArea, "Memories"));
/*      */       
/*      */ 
/*  843 */       requiredUpdate = Utils.removeComponent(Configuration.REPOSITORY_MEMORIES_FOLDER, this.treeArea.getSelectionPath().getLastPathComponent().toString());
/*      */       
/*      */ 
/*  846 */       if (requiredUpdate) {
/*  847 */         updateTree();
/*  848 */         this.memoryPanel.clear();
/*      */       }
/*      */     }
/*      */     
/*      */ 
/*  853 */     if (this.treeArea.getSelectionPath().getParentPath().getLastPathComponent().toString().equals("PSUs"))
/*      */     {
/*      */ 
/*  856 */       this.workingArea.setSelectedIndex(Utils.getTabIndexFromName(this.workingArea, "PSUs"));
/*      */       
/*      */ 
/*  859 */       requiredUpdate = Utils.removeComponent(Configuration.REPOSITORY_PSU_FOLDER, this.treeArea.getSelectionPath().getLastPathComponent().toString());
/*      */       
/*      */ 
/*  862 */       if (requiredUpdate) {
/*  863 */         updateTree();
/*  864 */         this.psuPanel.clear();
/*      */       }
/*      */     }
/*      */     
/*      */ 
/*  869 */     if (this.treeArea.getSelectionPath().getParentPath().getLastPathComponent().toString().equals("Disks"))
/*      */     {
/*      */ 
/*  872 */       this.workingArea.setSelectedIndex(Utils.getTabIndexFromName(this.workingArea, "Disks"));
/*      */       
/*      */ 
/*  875 */       requiredUpdate = Utils.removeComponent(Configuration.REPOSITORY_DISKS_FOLDER, this.treeArea.getSelectionPath().getLastPathComponent().toString());
/*      */       
/*      */ 
/*  878 */       if (requiredUpdate) {
/*  879 */         updateTree();
/*  880 */         this.diskPanel.clear();
/*      */       }
/*      */     }
/*      */     
/*      */ 
/*  885 */     if (this.treeArea.getSelectionPath().getParentPath().getLastPathComponent().toString().equals("Computing Nodes"))
/*      */     {
/*      */ 
/*  888 */       this.workingArea.setSelectedIndex(Utils.getTabIndexFromName(this.workingArea, "Nodes"));
/*      */       
/*      */ 
/*  891 */       requiredUpdate = Utils.removeComponent(Configuration.REPOSITORY_COMPUTING_NODES_FOLDER, this.treeArea.getSelectionPath().getLastPathComponent().toString());
/*      */       
/*      */ 
/*  894 */       if (requiredUpdate) {
/*  895 */         updateTree();
/*  896 */         this.nodePanel.clear();
/*      */       }
/*      */     }
/*      */     
/*      */ 
/*  901 */     if (this.treeArea.getSelectionPath().getParentPath().getLastPathComponent().toString().equals("Storage Nodes"))
/*      */     {
/*      */ 
/*  904 */       this.workingArea.setSelectedIndex(Utils.getTabIndexFromName(this.workingArea, "Nodes"));
/*      */       
/*      */ 
/*  907 */       requiredUpdate = Utils.removeComponent(Configuration.REPOSITORY_STORAGE_NODES_FOLDER, this.treeArea.getSelectionPath().getLastPathComponent().toString());
/*      */       
/*      */ 
/*  910 */       if (requiredUpdate) {
/*  911 */         updateTree();
/*  912 */         this.nodePanel.clear();
/*      */       }
/*      */     }
/*      */     
/*      */ 
/*      */ 
/*  918 */     if (this.treeArea.getSelectionPath().getParentPath().getLastPathComponent().toString().equals("Switches"))
/*      */     {
/*      */ 
/*  921 */       this.workingArea.setSelectedIndex(Utils.getTabIndexFromName(this.workingArea, "Switches"));
/*      */       
/*      */ 
/*  924 */       requiredUpdate = Utils.removeComponent(Configuration.REPOSITORY_SWITCHES, this.treeArea.getSelectionPath().getLastPathComponent().toString());
/*      */       
/*      */ 
/*  927 */       if (requiredUpdate) {
/*  928 */         updateTree();
/*  929 */         this.switchPanel.clear();
/*      */       }
/*      */     }
/*      */     
/*      */ 
/*  934 */     if (this.treeArea.getSelectionPath().getParentPath().getLastPathComponent().toString().equals("Racks"))
/*      */     {
/*      */ 
/*  937 */       this.workingArea.setSelectedIndex(Utils.getTabIndexFromName(this.workingArea, "Racks"));
/*      */       
/*      */ 
/*  940 */       requiredUpdate = Utils.removeComponent(Configuration.REPOSITORY_RACKS, this.treeArea.getSelectionPath().getLastPathComponent().toString());
/*      */       
/*      */ 
/*  943 */       if (requiredUpdate) {
/*  944 */         updateTree();
/*  945 */         this.rackPanel.clear();
/*      */       }
/*      */     }
/*      */     
/*      */ 
/*  950 */     if (this.treeArea.getSelectionPath().getParentPath().getLastPathComponent().toString().equals("Virtualization"))
/*      */     {
/*      */ 
/*  953 */       this.workingArea.setSelectedIndex(Utils.getTabIndexFromName(this.workingArea, "Virtualization"));
/*      */       
/*      */ 
/*  956 */       requiredUpdate = Utils.removeComponent(Configuration.REPOSITORY_VM, this.treeArea.getSelectionPath().getLastPathComponent().toString());
/*      */       
/*      */ 
/*  959 */       if (requiredUpdate) {
/*  960 */         updateTree();
/*  961 */         this.virtualizationPanel.clear();
/*      */       }
/*      */     }
/*      */     
/*      */ 
/*  966 */     if (this.treeArea.getSelectionPath().getParentPath().getLastPathComponent().toString().equals("Users"))
/*      */     {
/*      */ 
/*  969 */       this.workingArea.setSelectedIndex(Utils.getTabIndexFromName(this.workingArea, "Users"));
/*      */       
/*      */ 
/*  972 */       requiredUpdate = Utils.removeComponent(Configuration.REPOSITORY_USERS, this.treeArea.getSelectionPath().getLastPathComponent().toString());
/*      */       
/*      */ 
/*  975 */       if (requiredUpdate) {
/*  976 */         updateTree();
/*  977 */         this.usersPanel.clear();
/*      */       }
/*      */     }
/*      */     
/*      */ 
/*  982 */     if (this.treeArea.getSelectionPath().getParentPath().getLastPathComponent().toString().equals("Data-Center"))
/*      */     {
/*      */ 
/*  985 */       this.workingArea.setSelectedIndex(Utils.getTabIndexFromName(this.workingArea, "Data-Center"));
/*      */       
/*      */ 
/*  988 */       requiredUpdate = Utils.removeComponent(Configuration.REPOSITORY_DATA_CENTER, this.treeArea.getSelectionPath().getLastPathComponent().toString());
/*      */       
/*      */ 
/*  991 */       if (requiredUpdate) {
/*  992 */         updateTree();
/*  993 */         this.dataCenterPanel.clear();
/*      */       }
/*      */     }
/*      */     
/*      */ 
/*  998 */     if (this.treeArea.getSelectionPath().getParentPath().getLastPathComponent().toString().equals("Clouds"))
/*      */     {
/*      */ 
/* 1001 */       this.workingArea.setSelectedIndex(Utils.getTabIndexFromName(this.workingArea, "Clouds"));
/*      */       
/*      */ 
/* 1004 */       requiredUpdate = Utils.removeComponent(Configuration.REPOSITORY_CLOUD, this.treeArea.getSelectionPath().getLastPathComponent().toString());
/*      */       
/*      */ 
/* 1007 */       if (requiredUpdate) {
/* 1008 */         updateTree();
/* 1009 */         this.cloudsPanel.clear();
/*      */       }
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   private void leafComponentEditActionPerformed(ActionEvent evt)
/*      */   {
/* 1023 */     if (this.treeArea.getSelectionPath().getParentPath().getLastPathComponent().toString().equals("CPUs"))
/*      */     {
/*      */ 
/* 1026 */       this.workingArea.setSelectedIndex(Utils.getTabIndexFromName(this.workingArea, "CPUs"));
/*      */       
/*      */ 
/* 1029 */       this.cpuPanel.loadCpuInPanel(this.treeArea.getSelectionPath().getLastPathComponent().toString());
/*      */     }
/*      */     
/*      */ 
/* 1033 */     if (this.treeArea.getSelectionPath().getParentPath().getLastPathComponent().toString().equals("Memories"))
/*      */     {
/*      */ 
/* 1036 */       this.workingArea.setSelectedIndex(Utils.getTabIndexFromName(this.workingArea, "Memories"));
/*      */       
/*      */ 
/* 1039 */       this.memoryPanel.loadMemoryInPanel(this.treeArea.getSelectionPath().getLastPathComponent().toString());
/*      */     }
/*      */     
/*      */ 
/* 1043 */     if (this.treeArea.getSelectionPath().getParentPath().getLastPathComponent().toString().equals("PSUs"))
/*      */     {
/*      */ 
/* 1046 */       this.workingArea.setSelectedIndex(Utils.getTabIndexFromName(this.workingArea, "PSUs"));
/*      */       
/*      */ 
/* 1049 */       this.psuPanel.loadPSUInPanel(this.treeArea.getSelectionPath().getLastPathComponent().toString());
/*      */     }
/*      */     
/*      */ 
/* 1053 */     if (this.treeArea.getSelectionPath().getParentPath().getLastPathComponent().toString().equals("Disks"))
/*      */     {
/*      */ 
/* 1056 */       this.workingArea.setSelectedIndex(Utils.getTabIndexFromName(this.workingArea, "Disks"));
/*      */       
/*      */ 
/* 1059 */       this.diskPanel.loadDiskInPanel(this.treeArea.getSelectionPath().getLastPathComponent().toString());
/*      */     }
/*      */     
/*      */ 
/* 1063 */     if (this.treeArea.getSelectionPath().getParentPath().getLastPathComponent().toString().equals("Computing Nodes"))
/*      */     {
/*      */ 
/* 1066 */       this.workingArea.setSelectedIndex(Utils.getTabIndexFromName(this.workingArea, "Nodes"));
/*      */       
/*      */ 
/* 1069 */       this.nodePanel.loadNodeInPanel(false, this.treeArea.getSelectionPath().getLastPathComponent().toString());
/*      */     }
/*      */     
/*      */ 
/* 1073 */     if (this.treeArea.getSelectionPath().getParentPath().getLastPathComponent().toString().equals("Storage Nodes"))
/*      */     {
/*      */ 
/* 1076 */       this.workingArea.setSelectedIndex(Utils.getTabIndexFromName(this.workingArea, "Nodes"));
/*      */       
/*      */ 
/* 1079 */       this.nodePanel.loadNodeInPanel(true, this.treeArea.getSelectionPath().getLastPathComponent().toString());
/*      */     }
/*      */     
/*      */ 
/* 1083 */     if (this.treeArea.getSelectionPath().getParentPath().getLastPathComponent().toString().equals("Switches"))
/*      */     {
/*      */ 
/* 1086 */       this.workingArea.setSelectedIndex(Utils.getTabIndexFromName(this.workingArea, "Switches"));
/*      */       
/*      */ 
/* 1089 */       this.switchPanel.loadSwitchInPanel(this.treeArea.getSelectionPath().getLastPathComponent().toString());
/*      */     }
/*      */     
/*      */ 
/* 1093 */     if (this.treeArea.getSelectionPath().getParentPath().getLastPathComponent().toString().equals("Racks"))
/*      */     {
/*      */ 
/* 1096 */       this.workingArea.setSelectedIndex(Utils.getTabIndexFromName(this.workingArea, "Racks"));
/*      */       
/*      */ 
/* 1099 */       this.rackPanel.loadRackInPanel(this.treeArea.getSelectionPath().getLastPathComponent().toString());
/*      */     }
/*      */     
/*      */ 
/* 1103 */     if (this.treeArea.getSelectionPath().getParentPath().getLastPathComponent().toString().equals("Virtualization"))
/*      */     {
/*      */ 
/* 1106 */       this.workingArea.setSelectedIndex(Utils.getTabIndexFromName(this.workingArea, "Virtualization"));
/*      */       
/*      */ 
/* 1109 */       this.virtualizationPanel.loadVMInPanel(this.treeArea.getSelectionPath().getLastPathComponent().toString());
/*      */     }
/*      */     
/*      */ 
/* 1113 */     if (this.treeArea.getSelectionPath().getParentPath().getLastPathComponent().toString().equals("Users"))
/*      */     {
/*      */ 
/* 1116 */       this.workingArea.setSelectedIndex(Utils.getTabIndexFromName(this.workingArea, "Users"));
/*      */       
/*      */ 
/* 1119 */       this.usersPanel.loadUserInPanel(this.treeArea.getSelectionPath().getLastPathComponent().toString());
/*      */     }
/*      */     
/*      */ 
/* 1123 */     if (this.treeArea.getSelectionPath().getParentPath().getLastPathComponent().toString().equals("Data-Center"))
/*      */     {
/*      */ 
/* 1126 */       this.workingArea.setSelectedIndex(Utils.getTabIndexFromName(this.workingArea, "Data-Center"));
/*      */       
/*      */ 
/* 1129 */       this.dataCenterPanel.loadDataCenterInPanel(this.treeArea.getSelectionPath().getLastPathComponent().toString());
/*      */     }
/*      */     
/*      */ 
/* 1133 */     if (this.treeArea.getSelectionPath().getParentPath().getLastPathComponent().toString().equals("Clouds"))
/*      */     {
/*      */ 
/* 1136 */       this.workingArea.setSelectedIndex(Utils.getTabIndexFromName(this.workingArea, "Clouds"));
/*      */       
/*      */ 
/* 1139 */       this.cloudsPanel.loadCloudInPanel(this.treeArea.getSelectionPath().getLastPathComponent().toString());
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   private void leafComponentAddActionPerformed(ActionEvent evt)
/*      */   {
/* 1151 */     if (this.treeArea.getSelectionPath().getParentPath().getLastPathComponent().toString().equals("Computing Nodes")) {
/* 1152 */       this.dataCenterPanel.addElementToDataCenter("Computing", this.treeArea.getSelectionPath().getLastPathComponent().toString());
/*      */     }
/*      */     
/*      */ 
/* 1156 */     if (this.treeArea.getSelectionPath().getParentPath().getLastPathComponent().toString().equals("Storage Nodes")) {
/* 1157 */       this.dataCenterPanel.addElementToDataCenter("Storage", this.treeArea.getSelectionPath().getLastPathComponent().toString());
/*      */     }
/*      */     
/*      */ 
/* 1161 */     if (this.treeArea.getSelectionPath().getParentPath().getLastPathComponent().toString().equals("Switches")) {
/* 1162 */       this.dataCenterPanel.addElementToDataCenter("Switch", this.treeArea.getSelectionPath().getLastPathComponent().toString());
/*      */     }
/*      */     
/*      */ 
/* 1166 */     if (this.treeArea.getSelectionPath().getParentPath().getLastPathComponent().toString().equals("Racks")) {
/* 1167 */       this.dataCenterPanel.addElementToDataCenter("Rack", this.treeArea.getSelectionPath().getLastPathComponent().toString());
/*      */     }
/*      */     
/*      */ 
/* 1171 */     this.dataCenterPanel.updateConnections();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   private void itemSaveDataCenterActionPerformed(ActionEvent evt)
/*      */   {
/* 1181 */     saveDataCenter(this.dataCenterPanel.getDataCenterName());
/*      */   }
/*      */   
/*      */ 
/*      */   private void menuDataCenterMenuSelected(MenuEvent evt)
/*      */   {
/* 1187 */     if (this.workingArea.getSelectedIndex() == Utils.getTabIndexFromName(this.workingArea, "Data-Center"))
/*      */     {
/* 1189 */       this.itemNewDataCenter.setEnabled(true);
/* 1190 */       this.itemSaveAsDataCenter.setEnabled(true);
/*      */       
/*      */ 
/* 1193 */       if (Utils.checkName(this.dataCenterPanel.getDataCenterName())) {
/* 1194 */         this.itemSaveDataCenter.setEnabled(true);
/*      */       }
/*      */       else {
/* 1197 */         this.itemSaveDataCenter.setEnabled(false);
/*      */       }
/*      */     } else {
/* 1200 */       this.itemNewDataCenter.setEnabled(false);
/* 1201 */       this.itemSaveAsDataCenter.setEnabled(false);
/* 1202 */       this.itemSaveDataCenter.setEnabled(false);
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   private void itemSaveAsDataCenterActionPerformed(ActionEvent evt)
/*      */   {
/* 1211 */     DataCenterSaveDialog saveDialog = new DataCenterSaveDialog(getFrame(), this, this.dataCenterPanel.getDataCenterName());
/* 1212 */     saveDialog.setVisible(true);
/* 1213 */     saveDialog.clear();
/* 1214 */     saveDialog.setDefaultCloseOperation(0);
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   private void itemNewDataCenterActionPerformed(ActionEvent evt)
/*      */   {
/* 1222 */     int response = JOptionPane.showConfirmDialog(null, "Are you sure to create a new data-center?\nAll changes will be lost!", "Confirm Overwrite", 2, 3);
/*      */     
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/* 1229 */     if (response == 0) {
/* 1230 */       this.dataCenterPanel.createNewDataCenter();
/*      */     }
/*      */   }
/*      */ }


/* Location:              /home/chverma/Descargas/iCanCloudGUI/iCanCloudGUI.jar!/icancloudgui/ICanCloudGUIView.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */