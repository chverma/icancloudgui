/*     */ package icancloudgui.Utils;
/*     */ 
/*     */ import java.io.File;
/*     */ import java.io.FileInputStream;
/*     */ import java.io.FileOutputStream;
/*     */ import java.io.IOException;
/*     */ import java.util.Properties;
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ public class Configuration
/*     */ {
/*  18 */   Properties properties = null;
/*     */   
/*     */ 
/*  21 */   public static final String CONFIG_FILE_NAME = System.getProperty("user.dir") + File.separatorChar + "Config" + File.separatorChar + "Configuration.properties";
/*     */   
/*     */ 
/*  24 */   public static final String CONFIG_FILE_FOLDER = System.getProperty("user.dir") + File.separatorChar + "Config";
/*     */   
/*     */ 
/*     */   public static final String HOME_PATH = "iCanCloudHome";
/*     */   
/*     */ 
/*  30 */   public static final String REPOSITORY_FOLDER = System.getProperty("user.dir") + File.separatorChar + "Repository";
/*     */   
/*     */ 
/*  33 */   public static final String REPOSITORY_HARDWARE_FOLDER = REPOSITORY_FOLDER + File.separatorChar + "Hardware";
/*     */   
/*     */ 
/*  36 */   public static final String REPOSITORY_CPUS_FOLDER = REPOSITORY_HARDWARE_FOLDER + File.separatorChar + "CPUs";
/*     */   
/*     */ 
/*  39 */   public static final String REPOSITORY_DISKS_FOLDER = REPOSITORY_HARDWARE_FOLDER + File.separatorChar + "Disks";
/*     */   
/*     */ 
/*  42 */   public static final String REPOSITORY_MEMORIES_FOLDER = REPOSITORY_HARDWARE_FOLDER + File.separatorChar + "Memories";
/*     */   
/*     */ 
/*  45 */   public static final String REPOSITORY_PSU_FOLDER = REPOSITORY_HARDWARE_FOLDER + File.separatorChar + "PSUs";
/*     */   
/*     */ 
/*  48 */   public static final String REPOSITORY_NODES_FOLDER = REPOSITORY_FOLDER + File.separatorChar + "Nodes";
/*     */   
/*     */ 
/*  51 */   public static final String REPOSITORY_COMPUTING_NODES_FOLDER = REPOSITORY_NODES_FOLDER + File.separatorChar + "Computing";
/*     */   
/*     */ 
/*  54 */   public static final String REPOSITORY_STORAGE_NODES_FOLDER = REPOSITORY_NODES_FOLDER + File.separatorChar + "Storage";
/*     */   
/*     */ 
/*  57 */   public static final String REPOSITORY_SWITCHES = REPOSITORY_FOLDER + File.separatorChar + "Switches";
/*     */   
/*     */ 
/*  60 */   public static final String REPOSITORY_RACKS = REPOSITORY_FOLDER + File.separatorChar + "Racks";
/*     */   
/*     */ 
/*  63 */   public static final String REPOSITORY_DATA_CENTER = REPOSITORY_FOLDER + File.separatorChar + "DataCenter";
/*     */   
/*     */ 
/*  66 */   public static final String REPOSITORY_VM = REPOSITORY_FOLDER + File.separatorChar + "VMs";
/*     */   
/*     */ 
/*  69 */   public static final String REPOSITORY_USERS = REPOSITORY_FOLDER + File.separatorChar + "Users";
/*     */   
/*     */ 
/*  72 */   public static final String REPOSITORY_CLOUD = REPOSITORY_FOLDER + File.separatorChar + "Clouds";
/*     */   
/*     */ 
/*     */   public static final String COMM_LINK_10M = "Ethernet 10 Mbps";
/*     */   
/*     */ 
/*     */   public static final String COMM_LINK_100M = "Ethernet 100 Mbps";
/*     */   
/*     */ 
/*     */   public static final String COMM_LINK_1G = "Ethernet 1 Gbps";
/*     */   
/*     */ 
/*     */   public static final String COMM_LINK_10G = "Ethernet 10 Gbps";
/*     */   
/*     */ 
/*     */   public static final String COMM_LINK_40G = "Ethernet 40 Gbps";
/*     */   
/*     */ 
/*     */   public static final String COMM_LINK_100G = "Ethernet 100 Gbps";
/*     */   
/*     */ 
/*     */   public static final int defaultNumRowsEnergyTable = 10;
/*     */   
/*     */ 
/*     */   public static final String defaultCloudModelConfiguration = "omnetpp.ini";
/*     */   
/*     */ 
/*     */   public static final String defaultCloudModelTopology = "scenario.ned";
/*     */   
/*     */ 
/*     */   public static final String defaultSimulationsFolder = "simulations";
/*     */   
/*     */ 
/*     */   public static final String CLOUD_MANAGER_RANDOM = "Random";
/*     */   
/*     */ 
/*     */   public static final String CLOUD_MANAGER_FIRST_FIT = "FCFS";
/*     */   
/*     */ 
/*     */   public static final String CLOUD_MANAGER_ROUND_ROBIN = "Round-Robin";
/*     */   
/*     */ 
/*     */   public static final String HYPERVISOR_CITRIX_XEN6_L = "CitrixXenServer6_Linux";
/*     */   
/*     */ 
/*     */   public static final String HYPERVISOR_CITRIX_XEN6_PVL = "CitrixXenServer6_PV_Linux";
/*     */   
/*     */ 
/*     */   public static final String HYPERVISOR_CITRIX_XEN6_W = "CitrixXenServer6_Windows";
/*     */   
/*     */ 
/*     */   public static final String HYPERVISOR_HYPERV_L = "HyperV_Win2008RP2SP1_Linux";
/*     */   
/*     */ 
/*     */   public static final String HYPERVISOR_HYPERV_W = "HyperV_Win2008RP2SP1_Linux";
/*     */   
/*     */   public static final String HYPERVISOR_REDHAT_2_2_L = "RedHatEnterprise_Virtualization2_2_Linux.";
/*     */   
/*     */   public static final String HYPERVISOR_REDHAT_2_2_W = "RedHatEnterprise_Virtualization2_2_Windows";
/*     */   
/*     */   public static final String HYPERVISOR_VMWARE_ESXi5_W = "VMWareESXi5_VSphere5_Windows";
/*     */   
/*     */   public static final String HYPERVISOR_VMWARE_ESXi5_L = "VMWareESXi5_VSphere5_Linux";
/*     */   
/*     */   public static final String HYPERVISOR_NO_OVERHEAD = "HypervisorNoOverhead";
/*     */   
/*     */   public static final String TENANT_DISTRIBUTION_NULL = "no_distribution";
/*     */   
/*     */   public static final String TENANT_DISTRIBUTION_NORMAL = "normal";
/*     */   
/*     */   public static final String TENANT_DISTRIBUTION_EXPONENTIAL = "exponential";
/*     */   
/*     */   public static final String TENANT_DISTRIBUTION_UNIFORM = "uniform";
/*     */   
/*     */   public static final String TENANT_DISTRIBUTION_TRUNCNORMAL = "truncnormal";
/*     */   
/*     */   public static final String TENANT_DISTRIBUTION_GAMMA = "gamma_d";
/*     */   
/*     */   public static final String TENANT_DISTRIBUTION_TSTUDENT = "student_t";
/*     */   
/*     */   public static final String TENANT_DISTRIBUTION_CAUCHY = "cauchy";
/*     */   
/*     */   public static final String TENANT_DISTRIBUTION_TRIANGULAR = "triang";
/*     */   
/*     */   public static final String TENANT_DISTRIBUTION_LOGNORMAL = "lognormal";
/*     */   
/*     */   public static final String TENANT_DISTRIBUTION_WEIBULL = "weibull";
/*     */   
/*     */   public static final String TENANT_DISTRIBUTION_PARETOSHIFTED = "pareto_shifted";
/*     */   
/*     */   public static final String TENANT_DISTRIBUTION_INTUNIFORM = "intuniform";
/*     */   
/*     */   public static final String TENANT_DISTRIBUTION_BERNOULLI = "bernoulli";
/*     */   
/*     */   public static final String TENANT_DISTRIBUTION_BINOMIAL = "binomial";
/*     */   
/*     */   public static final String TENANT_DISTRIBUTION_GEOMETRIC = "geometric";
/*     */   
/*     */   public static final String TENANT_DISTRIBUTION_NEGBINOMIAL = "negbinomial";
/*     */   
/*     */   public static final String TENANT_DISTRIBUTION_POISSON = "poisson";
/*     */   
/*     */   public static final String NAME_RULES_MESSAGE = "A name must start with a letter and can contain alphanumeric characters, '-' and '_'";
/*     */   
/*     */   public static final String defaultRunScriptName = "run";
/*     */   
/*     */   public static final String defaultChartOutputFolder = "charts";
/*     */   
/*     */ 
/*     */   public Configuration()
/*     */   {
/* 183 */     this.properties = new Properties();
/*     */     
/*     */ 
/*     */     try
/*     */     {
/* 188 */       File file = new File(CONFIG_FILE_NAME);
/*     */       
/*     */ 
/* 191 */       if (file.exists()) {
/* 192 */         this.properties.load(new FileInputStream(CONFIG_FILE_NAME));
/*     */ 
/*     */       }
/*     */       else
/*     */       {
/*     */ 
/* 198 */         this.properties.setProperty("iCanCloudHome", "");
/* 199 */         saveToFile();
/*     */       }
/*     */     }
/*     */     catch (IOException ex)
/*     */     {
/* 204 */       ex.printStackTrace();
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getProperty(String key)
/*     */   {
/* 216 */     return this.properties.getProperty(key);
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setProperty(String key, String value)
/*     */   {
/* 226 */     this.properties.setProperty(key, value);
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void saveToFile()
/*     */   {
/*     */     try
/*     */     {
/* 239 */       this.properties.store(new FileOutputStream(CONFIG_FILE_NAME), null);
/*     */     }
/*     */     catch (IOException ex) {
/* 242 */       ex.printStackTrace();
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public static String getChannelBandwidth(String channel)
/*     */   {
/* 257 */     String text = "";
/*     */     
/* 259 */     if (channel.equals("Ethernet 10 Mbps")) {
/* 260 */       text = "10Mbps";
/* 261 */     } else if (channel.equals("Ethernet 100 Mbps")) {
/* 262 */       text = "100Mbps";
/* 263 */     } else if (channel.equals("Ethernet 1 Gbps")) {
/* 264 */       text = "1Gbps";
/* 265 */     } else if (channel.equals("Ethernet 10 Gbps")) {
/* 266 */       text = "10Gbps";
/* 267 */     } else if (channel.equals("Ethernet 40 Gbps")) {
/* 268 */       text = "40Gbps";
/* 269 */     } else if (channel.equals("Ethernet 100 Gbps")) {
/* 270 */       text = "100Gbps";
/*     */     } else {
/* 272 */       text = "Unsupported channel!";
/*     */     }
/* 274 */     return text;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   public static String generateChannelName(int id, String cloudName, boolean isRack)
/*     */   {
/*     */     String text;
/*     */     
/*     */ 
/*     */ 
/*     */     
/*     */ 
/* 289 */     if (!isRack) {
/* 290 */       text = "Channel_" + Integer.toString(id) + "_" + cloudName;
/*     */     } else {
/* 292 */       text = "RackChannel_" + Integer.toString(id) + "_" + cloudName;
/*     */     }
/* 294 */     return text;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public static String generateSchedulerName(String scheduler)
/*     */   {
/* 303 */     String text = "Unsupported scheduler";
/*     */     
/* 305 */     if (scheduler.equals("FCFS")) {
/* 306 */       text = "CloudSchedulerFCFS";
/* 307 */     } else if (scheduler.equals("Round-Robin")) {
/* 308 */       text = "CloudSchedulerRR";
/* 309 */     } else if (scheduler.equals("Random")) {
/* 310 */       text = "CloudSchedulerRandom";
/*     */     }
/*     */     
/* 313 */     return text;
/*     */   }
/*     */   
/*     */   public static String generateSwitchName(int id, boolean isRack)
/*     */   {
/*     */     String text;
/* 320 */     if (!isRack) {
/* 321 */       text = "switch_" + Integer.toString(id);
/*     */     } else {
/* 323 */       text = "rackSwitch_" + Integer.toString(id);
/*     */     }
/* 325 */     return text;
/*     */   }
/*     */   
/*     */   public static String generateNodeName(int id, boolean isStorage, String nodeName)
/*     */   {
/*     */     String text;
/* 332 */     if (!isStorage) {
/* 333 */       text = "nc_" + Integer.toString(id) + "_" + nodeName.replaceAll(" ", "_");
/*     */     } else {
/* 335 */       text = "ns_" + Integer.toString(id) + "_" + nodeName.replaceAll(" ", "_");
/*     */     }
/* 337 */     return text;
/*     */   }
/*     */   
/*     */   public static String generateRackDefinition(int id, boolean isStorage, String rackName, int numNodes)
/*     */   {
/*     */     String text;
/* 344 */     if (!isStorage) {
/* 345 */       text = "rc_" + Integer.toString(id) + "_" + rackName.replaceAll(" ", "_") + "[" + Integer.toString(numNodes) + "]";
/*     */     } else {
/* 347 */       text = "rs_" + Integer.toString(id) + "_" + rackName.replaceAll(" ", "_") + "[" + Integer.toString(numNodes) + "]";
/*     */     }
/* 349 */     return text;
/*     */   }
/*     */   
/*     */   public static String generateRackName(int id, boolean isStorage, String rackName)
/*     */   {
/*     */     String text;
/* 356 */     if (!isStorage) {
/* 357 */       text = "rc_" + Integer.toString(id) + "_" + rackName.replaceAll(" ", "_");
/*     */     } else {
/* 359 */       text = "rs_" + Integer.toString(id) + "_" + rackName.replaceAll(" ", "_");
/*     */     }
/* 361 */     return text;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */   public static String generateConnectionsForRack(String rackName, String switchName, String channelName, int numNodes)
/*     */   {
/* 368 */     String text = "\t// Connections for rack " + rackName + "\n";
/* 369 */     text = text + "\tfor i=0.." + Integer.toString(numNodes - 1) + "{\n";
/* 370 */     text = text + "\t   " + rackName + "[i].ethg++ <--> " + channelName + " <--> " + switchName + ".ethg++;\n";
/* 371 */     text = text + "\t}\n\n";
/*     */     
/* 373 */     return text;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */   public static String generateVirtualMachineName(String name)
/*     */   {
/* 380 */     String text = name.replaceAll(" ", "_");
/*     */     
/* 382 */     return text;
/*     */   }
/*     */ }


/* Location:              /home/chverma/Descargas/iCanCloudGUI/iCanCloudGUI.jar!/icancloudgui/Utils/Configuration.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */