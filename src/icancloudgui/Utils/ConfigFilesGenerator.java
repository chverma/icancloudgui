/*     */ package icancloudgui.Utils;
/*     */ 
/*     */ import icancloudgui.TabbedPanels.dataClasses.Cloud;
/*     */ import java.io.BufferedWriter;
/*     */ import java.io.File;
/*     */ import java.io.FileOutputStream;
/*     */ import java.io.OutputStreamWriter;
/*     */ import java.io.PrintStream;
/*     */ import java.io.Writer;
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ public class ConfigFilesGenerator
/*     */ {
/*     */   private File cloudFolderPath;
/*     */   private GeneratorHelper helper;
/*     */   private Cloud cloudModelObject;
/*     */   
/*     */   public ConfigFilesGenerator(String cloudName, File cloudFolderPath)
/*     */   {
/*  32 */     this.cloudFolderPath = cloudFolderPath;
/*     */     
/*     */ 
/*  35 */     this.cloudModelObject = Utils.loadCloudObject(cloudName);
/*     */     
/*     */ 
/*  38 */     this.helper = new GeneratorHelper(cloudFolderPath.getName(), this.cloudModelObject);
/*     */     
/*     */ 
/*  41 */     this.helper.createDir(cloudFolderPath);
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void generateNedFile()
/*     */   {
/*     */     try
/*     */     {
/*  57 */       File nedFile = new File(this.cloudFolderPath.getAbsolutePath() + File.separatorChar + "scenario.ned");
/*  58 */       Writer writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(nedFile)));
/*     */       
/*     */ 
/*  61 */       String currentElement = this.helper.getNedHeader();
/*  62 */       writer.write(currentElement);
/*     */       
/*     */ 
/*  65 */       currentElement = this.helper.getChannels();
/*  66 */       writer.write(currentElement);
/*     */       
/*     */ 
/*  69 */       currentElement = this.helper.getCloudDefinitionMainParameters();
/*  70 */       writer.write(currentElement);
/*     */       
/*     */ 
/*  73 */       currentElement = this.helper.getNetworkConfigurator();
/*  74 */       writer.write(currentElement);
/*     */       
/*     */ 
/*  77 */       currentElement = this.helper.getNetworkManager();
/*  78 */       writer.write(currentElement);
/*     */       
/*     */ 
/*  81 */       currentElement = this.helper.getCloudScheduler();
/*  82 */       writer.write(currentElement);
/*     */       
/*     */ 
/*  85 */       currentElement = this.helper.getUserGenerator();
/*  86 */       writer.write(currentElement);
/*     */       
/*     */ 
/*  89 */       currentElement = this.helper.getSwitches();
/*  90 */       writer.write(currentElement);
/*     */       
/*     */ 
/*  93 */       currentElement = this.helper.getNodes(false);
/*  94 */       writer.write(currentElement);
/*     */       
/*     */ 
/*  97 */       currentElement = this.helper.getNodes(true);
/*  98 */       writer.write(currentElement);
/*     */       
/*     */ 
/* 101 */       currentElement = this.helper.getTopologyAndVMSetModules();
/* 102 */       writer.write(currentElement);
/*     */       
/*     */ 
/* 105 */       currentElement = this.helper.getRacks();
/* 106 */       writer.write(currentElement);
/*     */       
/*     */ 
/* 109 */       currentElement = this.helper.getConnections();
/* 110 */       writer.write(currentElement);
/*     */       
/*     */ 
/*     */ 
/* 114 */       writer.flush();
/* 115 */       writer.close();
/*     */     }
/*     */     catch (Exception e) {
/* 118 */       e.printStackTrace();
/* 119 */       System.out.println(e.getMessage());
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void generateIniFile()
/*     */   {
/*     */     try
/*     */     {
/* 133 */       File iniFile = new File(this.cloudFolderPath.getAbsolutePath() + File.separatorChar + "omnetpp.ini");
/* 134 */       Writer writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(iniFile)));
/*     */       
/*     */ 
/* 137 */       String currentElement = this.helper.getIniHeader();
/* 138 */       writer.write(currentElement);
/*     */       
/*     */ 
/* 141 */       currentElement = this.helper.getIniMainParameters();
/* 142 */       writer.write(currentElement);
/*     */       
/*     */ 
/* 145 */       currentElement = this.helper.getDataCenterTopology();
/* 146 */       writer.write(currentElement);
/*     */       
/*     */ 
/* 149 */       currentElement = this.helper.getIniUserParameters();
/* 150 */       writer.write(currentElement);
/*     */       
/*     */ 
/* 153 */       currentElement = this.helper.getIniNodes(true);
/* 154 */       writer.write(currentElement);
/*     */       
/*     */ 
/* 157 */       currentElement = this.helper.getIniNodes(false);
/* 158 */       writer.write(currentElement);
/*     */       
/*     */ 
/* 161 */       currentElement = this.helper.getIniRacks();
/* 162 */       writer.write(currentElement);
/*     */       
/*     */ 
/* 165 */       currentElement = this.helper.getIniVMs();
/* 166 */       writer.write(currentElement);
/*     */       
/*     */ 
/* 169 */       writer.flush();
/* 170 */       writer.close();
/*     */     }
/*     */     catch (Exception e) {
/* 173 */       e.printStackTrace();
/* 174 */       System.out.println(e.getMessage());
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void generateRunScript()
/*     */   {
/*     */     try
/*     */     {
/* 188 */       File runFile = new File(this.cloudFolderPath.getAbsolutePath() + File.separatorChar + "run");
/*     */       
/* 190 */       if (runFile.exists()) {
/* 191 */         runFile.delete();
/*     */       }
/*     */       
/* 194 */       runFile.createNewFile();
/*     */       
/*     */ 
/* 197 */       runFile.setExecutable(true);
/*     */       
/*     */ 
/* 200 */       Writer writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(runFile)));
/*     */       
/*     */ 
/* 203 */       String currentElement = " #!/bin/sh\n";
/* 204 */       currentElement = currentElement + ".." + File.separatorChar + ".." + File.separatorChar + "src" + File.separatorChar + "run_iCanCloud $*";
/*     */       
/*     */ 
/* 207 */       writer.write(currentElement);
/*     */       
/*     */ 
/* 210 */       writer.flush();
/* 211 */       writer.close();
/*     */     }
/*     */     catch (Exception e)
/*     */     {
/* 215 */       e.printStackTrace();
/* 216 */       System.out.println(e.getMessage());
/*     */     }
/*     */   }
/*     */ }


/* Location:              /home/chverma/Descargas/iCanCloudGUI/iCanCloudGUI.jar!/icancloudgui/Utils/ConfigFilesGenerator.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */