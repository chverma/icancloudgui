/*      */ package icancloudgui.Utils;
/*      */ 
/*      */ import icancloudgui.TabbedPanels.dataClasses.Applications.CPUintensiveApp;
/*      */ import icancloudgui.TabbedPanels.dataClasses.Applications.HPCApp;
/*      */ import icancloudgui.TabbedPanels.dataClasses.Applications.ServerApp;
/*      */ import icancloudgui.TabbedPanels.dataClasses.CPU;
/*      */ import icancloudgui.TabbedPanels.dataClasses.Cloud;
/*      */ import icancloudgui.TabbedPanels.dataClasses.Connection;
/*      */ import icancloudgui.TabbedPanels.dataClasses.DataCenter;
/*      */ import icancloudgui.TabbedPanels.dataClasses.DataCenterUnit;
/*      */ import icancloudgui.TabbedPanels.dataClasses.Disk;
/*      */ import icancloudgui.TabbedPanels.dataClasses.EnergyEntry;
/*      */ import icancloudgui.TabbedPanels.dataClasses.Memory;
/*      */ import icancloudgui.TabbedPanels.dataClasses.Node;
/*      */ import icancloudgui.TabbedPanels.dataClasses.PSU;
/*      */ import icancloudgui.TabbedPanels.dataClasses.Rack;
/*      */ import icancloudgui.TabbedPanels.dataClasses.User;
/*      */ import icancloudgui.TabbedPanels.dataClasses.UserEntry;
/*      */ import icancloudgui.TabbedPanels.dataClasses.VMEntry;
/*      */ import icancloudgui.TabbedPanels.dataClasses.VirtualMachine;
/*      */ import java.io.File;
/*      */ import java.util.Iterator;
/*      */ import java.util.LinkedList;
/*      */ import javax.swing.JFrame;
/*      */ import javax.swing.JOptionPane;
/*      */ 
/*      */ 
/*      */ 
/*      */ public class GeneratorHelper
/*      */ {
/*   31 */   private final String WILD_CARD = "XXXXX";
/*      */   
/*   33 */   private final String NED_HEADER = "package icancloud.simulations.XXXXX; \n\nimport ned.DatarateChannel; \nimport inet.nodes.ethernet.EtherSwitch; \nimport icancloud.src.Architecture.Node.NodeVL.NodeVL; \nimport icancloud.src.Management.CloudManagement.ResourcesProvisioning.**.*; \nimport icancloud.src.Management.NetworkManager.NetworkManager; \nimport icancloud.src.Management.Topology.Topology; \nimport icancloud.src.Management.NetworkManager.NetworkManager; \nimport icancloud.src.Users.UserGenerator.UserGenerator; \nimport icancloud.src.Virtualization.VirtualMachines.SetDefinition.VmSet; \nimport inet.networklayer.autorouting.ipv4.IPv4NetworkConfigurator; \n";
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*   46 */   private final String CHANNEL_SECTION = "// ---------------------------------------------------------------\n//   Definition of channels\n// ---------------------------------------------------------------\n";
/*      */   
/*      */ 
/*      */ 
/*   50 */   private final String CLOUD_DEFINITION = "// ---------------------------------------------------------------\n//   Definition of the cloud\n// ---------------------------------------------------------------\n";
/*      */   
/*      */ 
/*      */ 
/*   54 */   private final String CLOUD_SUBMODULES = "   // -------------------------------------------------------\n   //   Definition of main modules\n   // -------------------------------------------------------\n";
/*      */   
/*      */ 
/*      */ 
/*   58 */   private final String NETWORK_CONFIG = "\t// ---------------------------------------------------------------\n\t//   Network configurator\n\t// ---------------------------------------------------------------\n\tconfigurator: IPv4NetworkConfigurator{\n\t   parameters:\n\t     config = xml(\"<config><interface hosts='**' address='192.168.x.x' netmask='255.x.x.x'/></config>\");\n\t}\n\n";
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*   66 */   private final String NETWORK_MANAGER = "\t// ---------------------------------------------------------------\n\t//    Network Manager\n\t// ---------------------------------------------------------------\n\tnetworkManager: NetworkManager{\n\t}\n\n";
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*   72 */   private final String CLOUD_SCHEDULER = "\t// ---------------------------------------------------------------\n\t//   Definition of cloud scheduler\n\t// ---------------------------------------------------------------\n\t manager: XXXXX{\n\t   parameters:\n\t     numberOfPFSRemoteServers = 2;\n\t     remote_storage_VMs_per_node = 2;\n\t     minimum_percent_storageNode_free = 0.15;\n\t     virtualIPsBasis = \"10.0.0.1\";\n\t}\n\n";
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*   83 */   private final String USER_GENERATOR = "\t// ---------------------------------------------------------------\n\t//   Definition of user generator\n\t// ---------------------------------------------------------------\n\tuserGenerator: UserGenerator {\n\t   parameters:\n\t     numCells = XXXXX;\n\t}\n\n";
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*   91 */   private final String SWITCHES_SECTION = "\t// ---------------------------------------------------------------\n\t//   Definition of switches\n\t// ---------------------------------------------------------------\n";
/*      */   
/*      */ 
/*      */ 
/*   95 */   private final String VM_SECTION = "\t// ---------------------------------------------------------------\n\t//   Definition of Virtual Machine set and topology \n\t// ---------------------------------------------------------------\n\t vmSet: VmSet {} \n\n\t topology: Topology {}\n";
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*  105 */   private final String COMPUTING_NODE_SECTION = "\t// ---------------------------------------------------------------\n\t//   Definition of Computing Nodes\n\t// ---------------------------------------------------------------\n";
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*  110 */   private final String STORAGE_NODE_SECTION = "\t// ---------------------------------------------------------------\n\t//   Definition of Storage Nodes\n\t// ---------------------------------------------------------------\n";
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*  115 */   private final String RACK_SECTION = "\t// ---------------------------------------------------------------\n\t//   Definition of Racks\n\t// ---------------------------------------------------------------\n";
/*      */   
/*      */ 
/*      */ 
/*  119 */   private final String CONNECTIONS_SECTION = "\t// ---------------------------------------------------------------\n\t//   Connections section\n\t// ---------------------------------------------------------------\n";
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*  124 */   private final String INI_HEADER = "[General]\nnetwork = XXXXX\ntkenv-plugin-path = ../../../etc/plugins\nned-path = ../../../inet/src\n\n**.vector-recording = false\n**.scalar-recording = false\n\n";
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*  132 */   private final String INI_MAIN_PARAMETERS = "################################################################\n### Main parameters\n################################################################\n\n";
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*  137 */   private final String INI_USER_PARAMETERS = "################################################################\n### Definition of users\n################################################################\n\n";
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*  142 */   private final String INI_NODE_SECTION = "################################################################\n### Definition of XXXXX\n################################################################\n\n";
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*  147 */   private final String INI_NODE_DEFINITION = "\t################################################################\n\t### Definition of XXXXX\n\t################################################################\n\n";
/*      */   
/*      */ 
/*      */ 
/*  151 */   private final String INI_NODE_GENERAL = "\t################################################################\n\t### General Configuration \n\t################################################################\n\n";
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*  156 */   private final String INI_VM_PARAMETERS = "################################################################\n### Definition of Virtual Machines\n################################################################\n\n";
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   private String cloudName;
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   private Cloud cloudModelObject;
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   private DataCenter dataCenter;
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public GeneratorHelper(String cloudName, Cloud cloudObject)
/*      */   {
/*  180 */     this.cloudName = cloudName;
/*      */     
/*      */ 
/*  183 */     this.cloudModelObject = cloudObject;
/*      */     
/*      */ 
/*  186 */     this.dataCenter = Utils.loadDataCenterObject(cloudObject.getDataCenter());
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void createDir(File path)
/*      */   {
/*  196 */     if (!path.exists()) {
/*  197 */       path.mkdir();
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public String getNedHeader()
/*      */   {
/*  210 */     String header = "package icancloud.simulations.XXXXX; \n\nimport ned.DatarateChannel; \nimport inet.nodes.ethernet.EtherSwitch; \nimport icancloud.src.Architecture.Node.NodeVL.NodeVL; \nimport icancloud.src.Management.CloudManagement.ResourcesProvisioning.**.*; \nimport icancloud.src.Management.NetworkManager.NetworkManager; \nimport icancloud.src.Management.Topology.Topology; \nimport icancloud.src.Management.NetworkManager.NetworkManager; \nimport icancloud.src.Users.UserGenerator.UserGenerator; \nimport icancloud.src.Virtualization.VirtualMachines.SetDefinition.VmSet; \nimport inet.networklayer.autorouting.ipv4.IPv4NetworkConfigurator; \n".replaceFirst("XXXXX", this.cloudName) + "\n";
/*      */     
/*  212 */     return header;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public String getChannels()
/*      */   {
/*  231 */     String text = "// ---------------------------------------------------------------\n//   Definition of channels\n// ---------------------------------------------------------------\n";
/*  232 */     String connection = "";
/*  233 */     int connectionIndex = 0;
/*      */     
/*      */ 
/*  236 */     Iterator<Connection> iterator = this.dataCenter.getConnectionListIterator();
/*      */     
/*      */ 
/*  239 */     while (iterator.hasNext())
/*      */     {
/*      */ 
/*  242 */       Connection currentConnection = (Connection)iterator.next();
/*      */       
/*      */ 
/*  245 */       connection = "channel " + Configuration.generateChannelName(connectionIndex, this.cloudName, false) + " extends DatarateChannel{\n" + "   delay = " + Double.toString(currentConnection.getDelay()) + "us;\n" + "   datarate = " + Configuration.getChannelBandwidth(currentConnection.getCommunicationLink()) + ";\n" + "   ber = " + Double.toString(currentConnection.getBer()) + ";\n" + "   per = " + Double.toString(currentConnection.getPer()) + ";\n" + "}\n\n";
/*      */       
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*  253 */       text = text + connection;
/*      */       
/*      */ 
/*  256 */       connectionIndex++;
/*      */     }
/*      */     
/*      */ 
/*      */ 
/*  261 */     Iterator<DataCenterUnit> iteratorRack = this.dataCenter.getRackList();
/*  262 */     connectionIndex = 0;
/*      */     
/*      */ 
/*  265 */     while (iteratorRack.hasNext())
/*      */     {
/*      */ 
/*  268 */       Rack currentRack = Utils.loadRackObject(((DataCenterUnit)iteratorRack.next()).getName());
/*      */       
/*      */ 
/*  271 */       if (currentRack != null)
/*      */       {
/*      */ 
/*  274 */         connection = "channel " + Configuration.generateChannelName(connectionIndex, this.cloudName, true) + " extends DatarateChannel{\n" + "   delay = " + Double.toString(currentRack.getDelay().doubleValue()) + "us;\n" + "   datarate = " + Configuration.getChannelBandwidth(currentRack.getCommLink()) + ";" + "// RackChannelBandwidth" + "\n" + "   ber = " + Double.toString(currentRack.getBer().doubleValue()) + ";\n" + "   per = " + Double.toString(currentRack.getPer().doubleValue()) + ";\n" + "}\n\n";
/*      */         
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*  282 */         text = text + connection;
/*      */         
/*      */ 
/*  285 */         connectionIndex++;
/*      */       }
/*      */     }
/*      */     
/*  289 */     return text;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public String getCloudDefinitionMainParameters()
/*      */   {
/*  302 */     String text = "// ---------------------------------------------------------------\n//   Definition of the cloud\n// ---------------------------------------------------------------\n";
/*      */     
/*  304 */     text = text + "network " + this.cloudName + "{\n\n";
/*  305 */     text = text + "   // -------------------------------------------------------\n   //   Definition of main modules\n   // -------------------------------------------------------\n";
/*  306 */     text = text + "   submodules:\n\n\n";
/*      */     
/*  308 */     return text;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */   public String getNetworkConfigurator()
/*      */   {
/*  315 */     getClass();String text = "\t// ---------------------------------------------------------------\n\t//   Network configurator\n\t// ---------------------------------------------------------------\n\tconfigurator: IPv4NetworkConfigurator{\n\t   parameters:\n\t     config = xml(\"<config><interface hosts='**' address='192.168.x.x' netmask='255.x.x.x'/></config>\");\n\t}\n\n";
/*      */     
/*  317 */     return text;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */   public String getNetworkManager()
/*      */   {
/*  324 */     getClass();String text = "\t// ---------------------------------------------------------------\n\t//    Network Manager\n\t// ---------------------------------------------------------------\n\tnetworkManager: NetworkManager{\n\t}\n\n";
/*      */     
/*  326 */     return text;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */   public String getCloudScheduler()
/*      */   {
/*  333 */     String text = "\t// ---------------------------------------------------------------\n\t//   Definition of cloud scheduler\n\t// ---------------------------------------------------------------\n\t manager: XXXXX{\n\t   parameters:\n\t     numberOfPFSRemoteServers = 2;\n\t     remote_storage_VMs_per_node = 2;\n\t     minimum_percent_storageNode_free = 0.15;\n\t     virtualIPsBasis = \"10.0.0.1\";\n\t}\n\n".replace("XXXXX", Configuration.generateSchedulerName(this.cloudModelObject.getCloudManager()));
/*      */     
/*  335 */     return text;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public String getUserGenerator()
/*      */   {
/*  346 */     int numUsers = 0;
/*      */     
/*      */ 
/*  349 */     LinkedList<UserEntry> userData = this.cloudModelObject.getUserData();
/*      */     
/*      */ 
/*  352 */     for (int i = 0; i < userData.size(); i++)
/*      */     {
/*  354 */       if (((UserEntry)userData.get(i)).getInstances() > 0)
/*      */       {
/*      */ 
/*  357 */         User currentUser = Utils.loadUserObject(((UserEntry)userData.get(i)).getName());
/*      */         
/*  359 */         if ((currentUser.isCpuAppActive()) || (currentUser.isServerAppActive())) {
/*  360 */           numUsers++;
/*      */         }
/*  362 */         if (currentUser.isHpcAppActive()) {
/*  363 */           numUsers++;
/*      */         }
/*      */       }
/*      */     }
/*  367 */     String text = "\t// ---------------------------------------------------------------\n\t//   Definition of user generator\n\t// ---------------------------------------------------------------\n\tuserGenerator: UserGenerator {\n\t   parameters:\n\t     numCells = XXXXX;\n\t}\n\n".replace("XXXXX", Integer.toString(numUsers));
/*      */     
/*  369 */     return text;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public String getSwitches()
/*      */   {
/*  380 */     String text = "\t// ---------------------------------------------------------------\n\t//   Definition of switches\n\t// ---------------------------------------------------------------\n";
/*  381 */     String switchText = "";
/*  382 */     int switchIndex = 0;
/*      */     
/*      */ 
/*  385 */     Iterator<DataCenterUnit> iterator = this.dataCenter.getSwitchesList();
/*      */     
/*      */ 
/*  388 */     while (iterator.hasNext())
/*      */     {
/*      */ 
/*  391 */       DataCenterUnit currentElement = (DataCenterUnit)iterator.next();
/*      */       
/*      */ 
/*  394 */       switchText = "\t" + Configuration.generateSwitchName(switchIndex, false) + ":EtherSwitch{\n" + "\t}\n\n";
/*      */       
/*      */ 
/*      */ 
/*  398 */       text = text + switchText;
/*      */       
/*      */ 
/*  401 */       switchIndex++;
/*      */     }
/*      */     
/*      */ 
/*  405 */     iterator = this.dataCenter.getRackList();
/*  406 */     switchIndex = 0;
/*      */     
/*      */ 
/*  409 */     while (iterator.hasNext())
/*      */     {
/*      */ 
/*  412 */       DataCenterUnit currentElement = (DataCenterUnit)iterator.next();
/*      */       
/*      */ 
/*  415 */       switchText = "\t" + Configuration.generateSwitchName(switchIndex, true) + ":EtherSwitch{\n" + "\t}\n\n";
/*      */       
/*      */ 
/*      */ 
/*  419 */       text = text + switchText;
/*      */       
/*      */ 
/*  422 */       switchIndex++;
/*      */     }
/*      */     
/*  425 */     return text;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   public String getNodes(boolean isStorage)
/*      */   {
/*      */     String text;
/*      */     
/*      */ 
/*  436 */     String nodeText = text = "";
/*  437 */     int nodeIndex = 0;
/*      */     
/*      */     Iterator<DataCenterUnit> iteratorNode;
/*  440 */     if (!isStorage) {
/*  441 */       iteratorNode = this.dataCenter.getComputingNodeList();
/*  442 */       text = "\t// ---------------------------------------------------------------\n\t//   Definition of Computing Nodes\n\t// ---------------------------------------------------------------\n";
/*      */     }
/*      */     else {
/*  445 */       iteratorNode = this.dataCenter.getStorageNodeList();
/*  446 */       text = "\t// ---------------------------------------------------------------\n\t//   Definition of Storage Nodes\n\t// ---------------------------------------------------------------\n";
/*      */     }
/*      */     
/*      */ 
/*  450 */     while (iteratorNode.hasNext())
/*      */     {
/*      */ 
/*  453 */       DataCenterUnit currentNode = (DataCenterUnit)iteratorNode.next();
/*      */       
/*      */ 
/*  456 */       nodeText = "\t" + Configuration.generateNodeName(nodeIndex, isStorage, currentNode.getName()) + ":NodeVL;\n\n";
/*      */       
/*      */ 
/*  459 */       text = text + nodeText;
/*      */       
/*      */ 
/*  462 */       nodeIndex++;
/*      */     }
/*      */     
/*  465 */     return text;
/*      */   }
/*      */   
/*      */   public String getTopologyAndVMSetModules()
/*      */   {
/*  470 */     String text = "";
/*      */     
/*  472 */     text = text + "\t//------------------------------------------------------\n\t// Modules that defines the topology and the set of VMs \n\t//------------------------------------------------------\n";
/*      */     
/*      */ 
/*  475 */     text = text + "\t  vmSet: VmSet {} \n";
/*  476 */     text = text + "\t  topology: Topology {} \n";
/*  477 */     text = text + "\n\n";
/*      */     
/*  479 */     return text;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public String getRacks()
/*      */   {
/*  490 */     String text = "\t// ---------------------------------------------------------------\n\t//   Definition of Racks\n\t// ---------------------------------------------------------------\n";
/*  491 */     String rackText = "";
/*  492 */     int rackIndex = 0;
/*      */     
/*      */ 
/*  495 */     Iterator<DataCenterUnit> iteratorRack = this.dataCenter.getRackList();
/*      */     
/*      */ 
/*  498 */     while (iteratorRack.hasNext())
/*      */     {
/*      */ 
/*  501 */       Rack rackObject = Utils.loadRackObject(((DataCenterUnit)iteratorRack.next()).getName());
/*      */       
/*      */ 
/*  504 */       rackText = "\t" + Configuration.generateRackDefinition(rackIndex, rackObject.isIsStorage(), rackObject.getName(), rackObject.getNumNodes()) + ":NodeVL;\n\n";
/*      */       
/*      */ 
/*      */ 
/*  508 */       text = text + rackText;
/*      */       
/*      */ 
/*  511 */       rackIndex++;
/*      */     }
/*      */     
/*  514 */     return text;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public String getConnections()
/*      */   {
/*  530 */     String text = "\t// ---------------------------------------------------------------\n\t//   Connections section\n\t// ---------------------------------------------------------------\n\tconnections allowunconnected:\n\n";
/*  531 */     String destinationName; String sourceName; String connection = sourceName = destinationName = "";
               int sourceIndex;
/*  532 */     int destinationIndex; int connectionIndex = sourceIndex = destinationIndex = 0;
/*  533 */     boolean destinationFound; 
               boolean sourceFound = destinationFound = false;
/*      */     
/*      */ 
/*      */ 
/*  537 */     Iterator<DataCenterUnit> iteratorRack = this.dataCenter.getRackList();
/*      */     
/*      */ 
/*  540 */     while (iteratorRack.hasNext())
/*      */     {
/*      */ 
/*  543 */       Rack rackObject = Utils.loadRackObject(((DataCenterUnit)iteratorRack.next()).getName());
/*      */       
/*      */ 
/*  546 */       connection = Configuration.generateConnectionsForRack(Configuration.generateRackName(sourceIndex, rackObject.isIsStorage(), rackObject.getName()), Configuration.generateSwitchName(sourceIndex, true), Configuration.generateChannelName(connectionIndex, this.cloudName, true), rackObject.getNumNodes());
/*      */       
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*  552 */       text = text + connection;
/*      */       
/*      */ 
/*  555 */       sourceIndex++;
/*      */     }
/*      */     
/*      */ 
/*      */ 
/*      */ 
/*  561 */     Iterator<Connection> iterator = this.dataCenter.getConnectionListIterator();
/*      */     
/*      */ 
/*      */ 
/*      */ 
/*  567 */     while (iterator.hasNext())
/*      */     {
/*      */ 
/*  570 */       Connection currentConnection = (Connection)iterator.next();
/*  571 */       sourceFound = destinationFound = false;
/*      */       
/*      */ 
/*      */ 
/*  575 */       if (!sourceFound)
/*      */       {
/*      */ 
/*  578 */         DataCenterUnit sourceUnit = Utils.isDataCenterUnitInList(this.dataCenter.getComputingNodeList(), currentConnection.getSource());
/*      */         
/*      */ 
/*  581 */         if (sourceUnit != null) {
/*  582 */           sourceIndex = Utils.getDataCenterUnitIndex(this.dataCenter.getComputingNodeList(), sourceUnit);
/*  583 */           sourceName = Configuration.generateNodeName(sourceIndex, false, sourceUnit.getName());
/*  584 */           sourceFound = true;
/*      */         }
/*      */       }
/*      */       
/*      */ 
/*      */ 
/*  590 */       if (!sourceFound)
/*      */       {
/*      */ 
/*  593 */         DataCenterUnit sourceUnit = Utils.isDataCenterUnitInList(this.dataCenter.getStorageNodeList(), currentConnection.getSource());
/*      */         
/*      */ 
/*  596 */         if (sourceUnit != null) {
/*  597 */           sourceIndex = Utils.getDataCenterUnitIndex(this.dataCenter.getStorageNodeList(), sourceUnit);
/*  598 */           sourceName = Configuration.generateNodeName(sourceIndex, true, sourceUnit.getName());
/*  599 */           sourceFound = true;
/*      */         }
/*      */       }
/*      */       
/*      */ 
/*  604 */       if (!sourceFound)
/*      */       {
/*      */ 
/*  607 */         DataCenterUnit sourceUnit = Utils.isDataCenterUnitInList(this.dataCenter.getSwitchesList(), currentConnection.getSource());
/*      */         
/*      */ 
/*  610 */         if (sourceUnit != null) {
/*  611 */           sourceIndex = Utils.getDataCenterUnitIndex(this.dataCenter.getSwitchesList(), sourceUnit);
/*  612 */           sourceName = Configuration.generateSwitchName(sourceIndex, false);
/*  613 */           sourceFound = true;
/*      */         }
/*      */       }
/*      */       
/*      */ 
/*  618 */       if (!sourceFound)
/*      */       {
/*      */ 
/*  621 */         DataCenterUnit sourceUnit = Utils.isDataCenterUnitInList(this.dataCenter.getRackList(), currentConnection.getSource());
/*      */         
/*      */ 
/*  624 */         if (sourceUnit != null) {
/*  625 */           sourceIndex = Utils.getDataCenterUnitIndex(this.dataCenter.getRackList(), sourceUnit);
/*  626 */           sourceName = Configuration.generateSwitchName(sourceIndex, true);
/*  627 */           sourceFound = true;
/*      */         }
/*      */       }
/*      */       
/*      */ 
/*      */ 
/*      */ 
/*  634 */       if (!sourceFound)
/*      */       {
/*  636 */         JOptionPane.showMessageDialog(new JFrame(), "Source element not found!", "Error generating connection!", 0);
/*      */       }
/*      */       
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*  644 */       if (!destinationFound)
/*      */       {
/*      */ 
/*  647 */         DataCenterUnit destinationUnit = Utils.isDataCenterUnitInList(this.dataCenter.getComputingNodeList(), currentConnection.getDestination());
/*      */         
/*      */ 
/*  650 */         if (destinationUnit != null) {
/*  651 */           destinationIndex = Utils.getDataCenterUnitIndex(this.dataCenter.getComputingNodeList(), destinationUnit);
/*  652 */           destinationName = Configuration.generateNodeName(destinationIndex, false, destinationUnit.getName());
/*  653 */           destinationFound = true;
/*      */         }
/*      */       }
/*      */       
/*      */ 
/*  658 */       if (!destinationFound)
/*      */       {
/*      */ 
/*  661 */         DataCenterUnit destinationUnit = Utils.isDataCenterUnitInList(this.dataCenter.getStorageNodeList(), currentConnection.getDestination());
/*      */         
/*      */ 
/*  664 */         if (destinationUnit != null) {
/*  665 */           destinationIndex = Utils.getDataCenterUnitIndex(this.dataCenter.getStorageNodeList(), destinationUnit);
/*  666 */           destinationName = Configuration.generateNodeName(destinationIndex, true, destinationUnit.getName());
/*  667 */           destinationFound = true;
/*      */         }
/*      */       }
/*      */       
/*      */ 
/*  672 */       if (!destinationFound)
/*      */       {
/*      */ 
/*  675 */         DataCenterUnit destinationUnit = Utils.isDataCenterUnitInList(this.dataCenter.getSwitchesList(), currentConnection.getDestination());
/*      */         
/*      */ 
/*  678 */         if (destinationUnit != null) {
/*  679 */           destinationIndex = Utils.getDataCenterUnitIndex(this.dataCenter.getSwitchesList(), destinationUnit);
/*  680 */           destinationName = Configuration.generateSwitchName(destinationIndex, false);
/*  681 */           destinationFound = true;
/*      */         }
/*      */       }
/*      */       
/*      */ 
/*  686 */       if (!destinationFound)
/*      */       {
/*      */ 
/*  689 */         DataCenterUnit destinationUnit = Utils.isDataCenterUnitInList(this.dataCenter.getRackList(), currentConnection.getDestination());
/*      */         
/*      */ 
/*  692 */         if (destinationUnit != null) {
/*  693 */           destinationIndex = Utils.getDataCenterUnitIndex(this.dataCenter.getRackList(), destinationUnit);
/*  694 */           destinationName = Configuration.generateSwitchName(destinationIndex, true);
/*  695 */           destinationFound = true;
/*      */         }
/*      */       }
/*      */       
/*      */ 
/*  700 */       if (!destinationFound)
/*      */       {
/*  702 */         JOptionPane.showMessageDialog(new JFrame(), "Destination element not found!", "Error generating connection!", 0);
/*      */       }
/*      */       
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*  709 */       connection = "\t   " + sourceName + ".ethg++ <--> " + Configuration.generateChannelName(connectionIndex, this.cloudName, false) + " <--> " + destinationName + ".ethg++;\n";
/*      */       
/*      */ 
/*  712 */       text = text + connection;
/*      */       
/*      */ 
/*  715 */       connectionIndex++;
/*      */     }
/*      */     
/*      */ 
/*  719 */     text = text + "}\n";
/*      */     
/*  721 */     return text;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public String getIniHeader()
/*      */   {
/*  730 */     String header = "[General]\nnetwork = XXXXX\ntkenv-plugin-path = ../../../etc/plugins\nned-path = ../../../inet/src\n\n**.vector-recording = false\n**.scalar-recording = false\n\n".replaceFirst("XXXXX", this.cloudName);
/*      */     
/*  732 */     return header;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */   public String getIniMainParameters()
/*      */   {
/*  739 */     String text = "################################################################\n### Main parameters\n################################################################\n\n";
/*      */     
/*  741 */     text = text + this.cloudName + ".manager.timeBetweenLogResults_s = 30\n";
/*  742 */     text = text + this.cloudName + ".manager.numberOfVMperNode = " + this.cloudModelObject.getNumVMsPerNode() + "\n\n";
/*      */     
/*  744 */     return text;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public String getDataCenterTopology()
/*      */   {
               String nodeText;
/*  759 */     String line = nodeText = "";
/*  760 */     int totalNodes; int nodeIndex = totalNodes = 0;
/*      */     
/*      */ 
/*  763 */     String prefix = "\t" + this.cloudName + ".topology.";
/*      */     
/*      */ 
/*  766 */     Iterator<DataCenterUnit> iteratorNode = this.dataCenter.getComputingNodeList();
/*      */     
/*  768 */     int i = 0;
/*      */     
/*  770 */     while (iteratorNode.hasNext())
/*      */     {
/*      */ 
/*  773 */       DataCenterUnit currentNode = (DataCenterUnit)iteratorNode.next();
/*      */       
/*      */ 
/*  776 */       nodeText = nodeText + prefix + "computeNode[" + i + "].id = \"" + Configuration.generateNodeName(nodeIndex, false, currentNode.getName()) + "\"\n";
/*  777 */       nodeText = nodeText + prefix + "computeNode[" + i + "].quantity = 1\n";
/*      */       
/*  779 */       nodeIndex++;
/*      */       
/*      */ 
/*  782 */       i++;
/*      */     }
/*      */     
/*      */ 
/*  786 */     totalNodes += nodeIndex;
/*  787 */     nodeIndex = 0;
/*      */     
/*      */ 
/*  790 */     iteratorNode = this.dataCenter.getRackList();
/*      */     
/*      */ 
/*  793 */     while (iteratorNode.hasNext())
/*      */     {
/*      */ 
/*  796 */       DataCenterUnit currentNode = (DataCenterUnit)iteratorNode.next();
/*      */       
/*      */ 
/*  799 */       Rack rackObject = Utils.loadRackObject(currentNode.getName());
/*      */       
/*      */ 
/*  802 */       if (!rackObject.isIsStorage())
/*      */       {
/*      */ 
/*  805 */         nodeText = nodeText + prefix + "computeNode[" + i + "].id = \"" + Configuration.generateRackName(nodeIndex, false, currentNode.getName()) + "\"\n";
/*  806 */         nodeText = nodeText + prefix + "computeNode[" + i + "].quantity = " + Integer.toString(rackObject.getNumNodes()) + "\n";
/*      */         
/*      */ 
/*  809 */         totalNodes++;
/*      */       }
/*      */       
/*      */ 
/*  813 */       nodeIndex++;
/*  814 */       i++;
/*      */     }
/*      */     
/*      */ 
/*      */ 
/*  819 */     line = line + prefix + "computeNodeQuantity = " + totalNodes + "\n";
/*  820 */     line = line + nodeText;
/*      */     
/*  822 */     nodeText = "";
/*  823 */     nodeIndex = totalNodes = 0;
/*  824 */     i = 0;
/*      */     
/*      */ 
/*  827 */     iteratorNode = this.dataCenter.getStorageNodeList();
/*      */     
/*      */ 
/*  830 */     while (iteratorNode.hasNext())
/*      */     {
/*      */ 
/*  833 */       DataCenterUnit currentNode = (DataCenterUnit)iteratorNode.next();
/*      */       
/*      */ 
/*  836 */       nodeText = nodeText + prefix + "storageNode[" + i + "].id = \"" + Configuration.generateNodeName(nodeIndex, true, currentNode.getName()) + "\"\n";
/*  837 */       nodeText = nodeText + prefix + "storageNode[" + i + "].quantity = 1\n";
/*      */       
/*  839 */       nodeIndex++;
/*  840 */       i++;
/*      */     }
/*      */     
/*      */ 
/*  844 */     totalNodes += nodeIndex;
/*  845 */     nodeIndex = 0;
/*      */     
/*      */ 
/*  848 */     iteratorNode = this.dataCenter.getRackList();
/*      */     
/*      */ 
/*  851 */     while (iteratorNode.hasNext())
/*      */     {
/*      */ 
/*  854 */       DataCenterUnit currentNode = (DataCenterUnit)iteratorNode.next();
/*      */       
/*      */ 
/*  857 */       Rack rackObject = Utils.loadRackObject(currentNode.getName());
/*      */       
/*      */ 
/*  860 */       if (rackObject.isIsStorage())
/*      */       {
/*      */ 
/*  863 */         nodeText = nodeText + prefix + "storageNode[" + i + "].id = \"" + Configuration.generateRackName(nodeIndex, false, currentNode.getName()) + "\"\n";
/*  864 */         nodeText = nodeText + prefix + "storageNode[" + i + "].quantity = " + Integer.toString(rackObject.getNumNodes()) + "\n";
/*      */         
/*      */ 
/*  867 */         totalNodes++;
/*      */       }
/*      */       
/*      */ 
/*  871 */       nodeIndex++;
/*  872 */       i++;
/*      */     }
/*      */     
/*  875 */     line = line + prefix + "storageNodeQuantity = " + totalNodes + "\n";
/*  876 */     line = line + nodeText;
/*  877 */     line = line + "\n";
/*      */     
/*  879 */     return line;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public String getIniUserParameters()
/*      */   {
/*  891 */     int userIndex = 0;
/*  892 */     String text = "################################################################\n### Definition of users\n################################################################\n\n";
/*  893 */     String userText = "";
/*      */     
/*      */ 
/*  896 */     LinkedList<UserEntry> userData = this.cloudModelObject.getUserData();
/*      */     
/*      */ 
/*  899 */     for (int i = 0; i < userData.size(); i++)
/*      */     {
/*      */ 
/*  902 */       if (((UserEntry)userData.get(i)).getInstances() > 0)
/*      */       {
/*      */ 
/*  905 */         User currentUser = Utils.loadUserObject(((UserEntry)userData.get(i)).getName());
/*      */         
/*      */ 
/*  908 */         if (currentUser.isHpcAppActive())
/*      */         {
/*      */ 
/*  911 */           userText = userText + getUserDefinition(true, currentUser, userIndex, ((UserEntry)userData.get(i)).getInstances());
/*      */           
/*      */ 
/*  914 */           userIndex++;
/*      */         }
/*      */         
/*      */ 
/*  918 */         if ((currentUser.isCpuAppActive()) || (currentUser.isServerAppActive()))
/*      */         {
/*      */ 
/*  921 */           userText = userText + getUserDefinition(false, currentUser, userIndex, ((UserEntry)userData.get(i)).getInstances());
/*      */           
/*      */ 
/*  924 */           userIndex++;
/*      */         }
/*      */         
/*      */ 
/*  928 */         text = text + userText;
/*  929 */         userText = "";
/*      */       }
/*      */     }
/*      */     
/*  933 */     return text;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   private String getUserDefinition(boolean isHPC, User currentUser, int userIndex, int numInstances)
/*      */   {
/*      */     int numVMs;
/*      */     
/*      */ 
/*      */ 
/*  946 */     int numApps = numVMs = 0;
               String distribution;
               String prefix;
/*  947 */     String userText = distribution = prefix = "";
/*  948 */     boolean hasRemoteServers = false;
/*      */     
/*      */ 
/*  951 */     distribution = this.cloudModelObject.getDistribution();
/*      */     
/*      */ 
/*  954 */     Iterator<DataCenterUnit> iteratorNode = this.dataCenter.getStorageNodeList();
/*      */     
/*  956 */     if (iteratorNode.hasNext()) {
/*  957 */       hasRemoteServers = true;
/*      */     }
/*      */     
/*  960 */     if (isHPC) {
/*  961 */       numApps++;
/*      */     }
/*      */     else
/*      */     {
/*  965 */       if (currentUser.isCpuAppActive()) {
/*  966 */         numApps++;
/*      */       }
/*  968 */       if (currentUser.isServerAppActive()) {
/*  969 */         numApps++;
/*      */       }
/*      */     }
/*  972 */     userText = userText + "##############################################\n### Definition of user: " + currentUser.getName() + "\n" + "##############################################\n\n";
/*      */     
/*      */ 
/*      */ 
/*      */ 
/*  977 */     prefix = this.cloudName + ".userGenerator.cell[" + Integer.toString(userIndex) + "].";
/*      */     
/*      */ 
/*  980 */     userText = userText + prefix + "cell_type = \"" + "UserGeneratorDay" + "\"\n";
/*      */     
/*      */ 
/*  983 */     userText = userText + prefix + "name = \"" + currentUser.getName() + "\"\n";
/*      */     
/*      */ 
/*  986 */     if (isHPC) {
/*  987 */       userText = userText + prefix + "behavior = \"SmartUser\"\n";
/*      */     } else {
/*  989 */       userText = userText + prefix + "behavior = \"GeneralUser\"\n";
/*      */     }
/*      */     
/*  992 */     if (!distribution.contentEquals("no_distribution")) {
/*  993 */       userText = userText + prefix + "quantity_user_preloaded = " + Integer.toString(this.cloudModelObject.getPreLoadedTenants()) + "\n";
/*      */       
/*      */ 
/*  996 */       userText = userText + prefix + "total_users = " + Integer.toString(numInstances) + "\n";
/*      */       
/*      */ 
/*  999 */       userText = userText + prefix + "time_creation = " + Double.toString(this.cloudModelObject.getSimTime()) + "\n";
/* 1000 */       userText = userText + prefix + "repetitions = " + Double.toString(this.cloudModelObject.getRepetitions()) + "\n";
/*      */     } else {
/* 1002 */       userText = userText + prefix + "quantity_user_preloaded = " + Integer.toString(this.cloudModelObject.getPreLoadedTenants() + numInstances) + "\n";
/*      */       
/*      */ 
/* 1005 */       userText = userText + prefix + "total_users = 0\n";
/*      */       
/*      */ 
/* 1008 */       userText = userText + prefix + "time_creation = 0.0\n";
/* 1009 */       userText = userText + prefix + "repetitions = 0\n";
/*      */     }
/*      */     
/*      */ 
/*      */ 
/* 1014 */     userText = userText + "\n";
/*      */     
/*      */ 
/* 1017 */     if (hasRemoteServers) {
/* 1018 */       userText = userText + prefix + "remoteServers = \"NFS\"\n";
/*      */     } else {
/* 1020 */       userText = userText + prefix + "remoteServers = \"LOCAL\"\n";
/*      */     }
/*      */     
/*      */ 
/*      */ 
/*      */ 
/* 1026 */     userText = userText + prefix + "distribution.name = \"" + distribution + "\"\n";
/*      */     
/*      */ 
/* 1029 */     userText = userText + prefix + "distribution.params = \"";
/* 1030 */     if (this.cloudModelObject.getParameterX() != -1.0D) {
/* 1031 */       userText = userText + Double.toString(this.cloudModelObject.getParameterX());
/* 1032 */       if (this.cloudModelObject.getParameterY() != -1.0D) {
/* 1033 */         userText = userText + " " + Double.toString(this.cloudModelObject.getParameterY());
/* 1034 */         if (this.cloudModelObject.getParameterZ() != -1.0D) {
/* 1035 */           userText = userText + " " + Double.toString(this.cloudModelObject.getParameterZ());
/*      */         }
/*      */       }
/* 1038 */       userText = userText + "\"\n";
/*      */     } else {
/* 1040 */       userText = userText + "\"\n";
/*      */     }
/*      */     
/* 1043 */     userText = userText + "\n";
/*      */     
/*      */ 
/*      */ 
/* 1047 */     userText = userText + prefix + "appDefinition." + "appQuantity = " + Integer.toString(numApps) + "\n\n";
/*      */     
/*      */ 
/* 1050 */     for (int i = 0; i < numApps; i++)
/*      */     {
/*      */ 
/* 1053 */       if (isHPC) {
/* 1054 */         userText = userText + prefix + "appDefinition.application[" + i + "]." + "name = " + "\"App_HPC\"" + "\n";
/* 1055 */         userText = userText + prefix + "appDefinition.application[" + i + "]." + "appType = " + "\"ApplicationHPC\"" + "\n";
/*      */         
/*      */ 
/* 1058 */         int numFiles = 12;
/* 1059 */         userText = userText + prefix + "appDefinition.application[" + i + "]." + "numFiles = " + numFiles + "\n";
/* 1060 */         for (int j = 0; j < numFiles; j++) {
/* 1061 */           userText = userText + prefix + "appDefinition.application[" + i + "]." + "preloadFiles[" + j + "].name = \"/fileInput_" + j + ".dat\"" + "\n";
/* 1062 */           userText = userText + prefix + "appDefinition.application[" + i + "]." + "preloadFiles[" + j + "].size_KiB = 2000000" + "\n";
/*      */         }
/*      */       }
/*      */       
/*      */ 
/*      */ 
/* 1068 */       int numFiles = 2;
/*      */       
/* 1070 */       if (currentUser.isCpuAppActive())
/*      */       {
/* 1072 */         userText = userText + prefix + "appDefinition.application[" + i + "]." + "name = " + "\"App_CPU\"" + "\n";
/* 1073 */         userText = userText + prefix + "appDefinition.application[" + i + "]." + "appType = " + "\"CPU_Intensive\"" + "\n";
/*      */         
/*      */ 
/* 1076 */         userText = userText + prefix + "appDefinition.application[" + i + "]." + "numFiles = " + numFiles + "\n";
/* 1077 */         userText = userText + prefix + "appDefinition.application[" + i + "]." + "preloadFiles[0].name = \"/input.dat\"" + "\n";
/* 1078 */         userText = userText + prefix + "appDefinition.application[" + i + "]." + "preloadFiles[0].size_KiB = 1000000" + "\n";
/* 1079 */         userText = userText + prefix + "appDefinition.application[" + i + "]." + "preloadFiles[1].name = \"/output.dat\"" + "\n";
/* 1080 */         userText = userText + prefix + "appDefinition.application[" + i + "]." + "preloadFiles[1].size_KiB = 1000000" + "\n";
/*      */       }
/*      */       
/* 1083 */       if (currentUser.isServerAppActive()) {
/* 1084 */         userText = userText + prefix + "appDefinition.application[" + i + "]." + "name = " + "\"webServer\"" + "\n";
/* 1085 */         userText = userText + prefix + "appDefinition.application[" + i + "]." + "appType = " + "\"ServerApplication\"" + "\n";
/*      */         
/*      */ 
/*      */ 
/* 1089 */         userText = userText + prefix + "appDefinition.application[" + i + "]." + "numFiles = " + numFiles + "\n";
/* 1090 */         userText = userText + prefix + "appDefinition.application[" + i + "]." + "preloadFiles[0].name = \"/web.html\"" + "\n";
/* 1091 */         userText = userText + prefix + "appDefinition.application[" + i + "]." + "preloadFiles[0].size_KiB = 1246" + "\n";
/* 1092 */         userText = userText + prefix + "appDefinition.application[" + i + "]." + "preloadFiles[1].name = \"/log.dat\"" + "\n";
/* 1093 */         userText = userText + prefix + "appDefinition.application[" + i + "]." + "preloadFiles[1].size_KiB = 10" + "\n";
                   userText = userText + prefix + "appDefinition.application[" + i + "]." + "copies" + "= 1" + "\n";
/*      */       }
/*      */       
/*      */ 
/* 1097 */       userText = userText + "\n";
/*      */       
/*      */ 
/* 1100 */       userText = userText + prefix + "appDefinition.application[" + i + "]." + "numFSRoutes = " + "1" + "\n";
/*      */       
/* 1102 */       if (isHPC)
/*      */       {
/*      */ 
/* 1105 */         if (currentUser.isHpcAppActive()) {
/* 1106 */           if (hasRemoteServers) {
/* 1107 */             userText = userText + prefix + "appDefinition.application[" + i + "]." + "FSConfig[0].type = " + "\"NFS\"" + "\n";
/*      */           } else {
/* 1109 */             userText = userText + prefix + "appDefinition.application[" + i + "]." + "FSConfig[0].type = " + "\"LOCAL\"" + "\n";
/*      */           }
/*      */         }
/* 1112 */         userText = userText + prefix + "appDefinition.application[" + i + "]." + "FSConfig[0].route = " + "\"/\"" + "\n";
/*      */       } else {
/* 1114 */         userText = userText + prefix + "appDefinition.application[" + i + "]." + "FSConfig[0].type = " + "\"LOCAL\"" + "\n";
/* 1115 */         userText = userText + prefix + "appDefinition.application[" + i + "]." + "FSConfig[0].route = " + "\"/\"" + "\n";
/*      */       }
/*      */       
/* 1118 */       userText = userText + "\n";
/*      */       
/*      */ 
/* 1121 */       if (isHPC)
/*      */       {
/*      */ 
/* 1124 */         if (currentUser.isHpcAppActive())
/*      */         {
/*      */ 
/* 1127 */           userText = userText + prefix + "appDefinition.application[" + i + "].app." + "connectionDelay_s" + "= " + "5" + "\n";
/* 1128 */           userText = userText + prefix + "appDefinition.application[" + i + "].app." + "startDelay_s" + "= " + "10" + "\n";
/* 1129 */           userText = userText + prefix + "appDefinition.application[" + i + "].app." + "allToAllConnections" + "= " + "false" + "\n";
/* 1130 */           userText = userText + prefix + "appDefinition.application[" + i + "].app." + "workersRead" + "= " + Boolean.toString(currentUser.getHpcApp().isWorkersRead()) + "\n";
/* 1131 */           userText = userText + prefix + "appDefinition.application[" + i + "].app." + "workersWrite" + "= " + Boolean.toString(currentUser.getHpcApp().isWorkersWrite()) + "\n";
/* 1132 */           userText = userText + prefix + "appDefinition.application[" + i + "].app." + "sliceToMaster_KB" + "= " + Integer.toString(currentUser.getHpcApp().getDataCoordinator()) + "\n";
/* 1133 */           userText = userText + prefix + "appDefinition.application[" + i + "].app." + "sliceToWorkers_KB" + "= " + Integer.toString(currentUser.getHpcApp().getDataWorkers()) + "\n";
/* 1134 */           userText = userText + prefix + "appDefinition.application[" + i + "].app." + "sliceCPU" + "= " + Integer.toString(currentUser.getHpcApp().getProcessing()) + "\n";
/* 1135 */           userText = userText + prefix + "appDefinition.application[" + i + "].app." + "workersSet" + "= " + Integer.toString(currentUser.getHpcApp().getWorkersSet()) + "\n";
/* 1136 */           userText = userText + prefix + "appDefinition.application[" + i + "].app." + "numIterations" + "= " + Integer.toString(currentUser.getHpcApp().getIterations()) + "\n";
/* 1137 */           userText = userText + prefix + "appDefinition.application[" + i + "]." + "copies" + "= 0" + "\n";
/*      */         }
/*      */         
/*      */ 
/*      */       }
/*      */       else
/*      */       {
/*      */ 
/* 1145 */         if (currentUser.isCpuAppActive()) {
/* 1146 */           userText = userText + prefix + "appDefinition.application[" + i + "].app." + "startDelay" + "= " + "2" + "\n";
/* 1147 */           userText = userText + prefix + "appDefinition.application[" + i + "].app." + "inputSize" + "= " + Integer.toString(currentUser.getCpuApp().getInputSize()) + "\n";
/* 1148 */           userText = userText + prefix + "appDefinition.application[" + i + "].app." + "outputSize" + "= " + Integer.toString(currentUser.getCpuApp().getOutputSize()) + "\n";
/* 1149 */           userText = userText + prefix + "appDefinition.application[" + i + "].app." + "MIs" + "= " + Integer.toString(currentUser.getCpuApp().getProcessing()) + "\n";
/* 1150 */           userText = userText + prefix + "appDefinition.application[" + i + "]." + "copies" + "= " + Integer.toString(currentUser.getCpuApp().getIterations()) + "\n";
/*      */         }
/*      */         
/*      */ 
/*      */ 
/* 1155 */         if (currentUser.isServerAppActive())
/*      */         {
/*      */ 
/* 1158 */           userText = userText + prefix + "appDefinition.application[" + i + "].app." + "application_netType" + "= " + "\"INET\"" + "\n";
/* 1159 */           userText = userText + prefix + "appDefinition.application[" + i + "].app." + "startDelay" + "= " + "2" + "\n";
/* 1160 */           userText = userText + prefix + "appDefinition.application[" + i + "].app." + "inputSize" + "= " + "1246" + "\n";
/* 1161 */           userText = userText + prefix + "appDefinition.application[" + i + "].app." + "hitsPerHour" + "= " + Integer.toString(currentUser.getServerApp().getRequests()) + "\n";
/* 1162 */           userText = userText + prefix + "appDefinition.application[" + i + "].app." + "MIs" + "= " + Integer.toString(currentUser.getServerApp().getProcessing()) + "\n";
/* 1163 */           userText = userText + prefix + "appDefinition.application[" + i + "].app." + "uptimeLimit" + "= " + "200" + "\n";
/*      */         }
/*      */       }
/*      */       
/*      */ 
/* 1168 */       userText = userText + "\n";
/*      */     }
/*      */     
/*      */ 
/*      */ 
/* 1173 */     for (int j = 0; j < currentUser.getVMData().size(); j++)
/*      */     {
/*      */ 
/* 1176 */       if (((VMEntry)currentUser.getVMData().get(j)).getInstances() > 0) {
/* 1177 */         userText = userText + prefix + "vmDefinition.vmToRent[" + numVMs + "].name = \"" + ((VMEntry)currentUser.getVMData().get(j)).getName() + "\"\n";
/* 1178 */         userText = userText + prefix + "vmDefinition.vmToRent[" + numVMs + "].quantity = " + Integer.toString(((VMEntry)currentUser.getVMData().get(j)).getInstances()) + "\n";
/* 1179 */         numVMs++;
/*      */       }
/*      */     }
/* 1182 */     userText = userText + prefix + "vmsToRentTypesQuantity = " + numVMs + "\n";
/*      */     
/*      */ 
/* 1185 */     userText = userText + "\n\n\n";
/*      */     
/* 1187 */     return userText;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public String getIniNodes(boolean isStorage)
/*      */   {
/*      */     String nodeText;
/*      */     
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/* 1205 */     String prefix = nodeText = "";
/* 1206 */     int nodeIndex = 0;
/*      */     Iterator<DataCenterUnit> iteratorNode;
/* 1208 */     String text; 
                if (isStorage) {
/* 1209 */       getClass();
                 text = "################################################################\n### Definition of XXXXX\n################################################################\n\n".replaceFirst("XXXXX", "Storage nodes");
/* 1210 */       iteratorNode = this.dataCenter.getStorageNodeList();
/*      */     }
/*      */     else
/*      */     {
/* 1214 */       getClass();text = "################################################################\n### Definition of XXXXX\n################################################################\n\n".replaceFirst("XXXXX", "Computing nodes");
/* 1215 */       iteratorNode = this.dataCenter.getComputingNodeList();
/*      */     }
/*      */     
/*      */ 
/*      */ 
/* 1220 */     while (iteratorNode.hasNext())
/*      */     {
/*      */ 
/* 1223 */       DataCenterUnit currentNode = (DataCenterUnit)iteratorNode.next();
/*      */       
/*      */ 
/* 1226 */       Node nodeObject = Utils.loadNodeObject(isStorage, currentNode.getName());
/* 1227 */       CPU cpuObject = Utils.loadCPUobject(nodeObject.getCpu());
/* 1228 */       Disk diskObject = Utils.loadDiskObject(nodeObject.getDisk());
/* 1229 */       Memory memoryObject = Utils.loadMemoryObject(nodeObject.getMemory());
/* 1230 */       PSU psuObject = Utils.loadPSUobject(nodeObject.getPsu());
/*      */       
/*      */ 
/* 1233 */       getClass();nodeText = nodeText + "\t################################################################\n\t### Definition of XXXXX\n\t################################################################\n\n".replaceFirst("XXXXX", currentNode.getName());
/*      */       
/*      */ 
/* 1236 */       prefix = "\t" + this.cloudName + "." + Configuration.generateNodeName(nodeIndex, isStorage, currentNode.getName()) + ".";
/*      */       
/*      */ 
/* 1239 */       nodeText = nodeText + "\t### Energy Meter Components\n";
/* 1240 */       nodeText = nodeText + "\t" + prefix + "psu.wattage = " + Double.toString(psuObject.getWattage()) + "\n";
/* 1241 */       nodeText = nodeText + "\t" + prefix + "psu.scale = " + Double.toString(psuObject.getScale()) + "\n";
/* 1242 */       nodeText = nodeText + "\t" + prefix + "psu.psu_20 = " + ((EnergyEntry)psuObject.getEnergyData().get(0)).getValue() + "\n";
/* 1243 */       nodeText = nodeText + "\t" + prefix + "psu.psu_50 = " + ((EnergyEntry)psuObject.getEnergyData().get(1)).getValue() + "\n";
/* 1244 */       nodeText = nodeText + "\t" + prefix + "psu.psu_100 = " + ((EnergyEntry)psuObject.getEnergyData().get(2)).getValue() + "\n\n";
/*      */       
/*      */ 
/*      */ 
/* 1248 */       nodeText = nodeText + "\t### CPU\n";
/* 1249 */       nodeText = nodeText + prefix + "energyMeter.cpuMeter.componentName = \"" + cpuObject.getName() + "\"" + "\n";
/* 1250 */       nodeText = nodeText + Utils.energyListToString(cpuObject.getEnergyData(), new StringBuilder().append(prefix).append("energyMeter.cpuMeter.").toString()) + "\n";
/* 1251 */       nodeText = nodeText + prefix + "cpuModule.indepentCores = " + Boolean.toString(cpuObject.isIndependentCores()) + "\n\n";
/*      */       
/*      */ 
/* 1254 */       nodeText = nodeText + "\t### Disk\n";
/* 1255 */       nodeText = nodeText + prefix + "energyMeter.storageMeter.componentName = \"" + diskObject.getName() + "\"" + "\n";
/* 1256 */       nodeText = nodeText + Utils.energyListToString(diskObject.getEnergyData(), new StringBuilder().append(prefix).append("energyMeter.storageMeter.").toString()) + "\n\n";
/*      */       
/*      */ 
/* 1259 */       nodeText = nodeText + "\t### Memory\n";
/* 1260 */       nodeText = nodeText + prefix + "energyMeter.memoryMeter.componentName = \"" + memoryObject.getName() + "\"" + "\n";
/* 1261 */       nodeText = nodeText + Utils.energyListToString(memoryObject.getEnergyData(), new StringBuilder().append(prefix).append("energyMeter.memoryMeter.").toString()) + "\n";
/* 1262 */       nodeText = nodeText + prefix + "energyMeter.memoryMeter.numModules = " + Integer.toString(memoryObject.getNumModules()) + "\n";
/* 1263 */       nodeText = nodeText + prefix + "energyMeter.memoryMeter.numDRAMChips = " + Integer.toString(memoryObject.getNumDramChips()) + "\n\n";
/*      */       
/*      */ 
/* 1266 */       nodeText = nodeText + prefix + "energyMeter.networkMeter.componentName = \"" + "Ralink5412" + "\"" + "\n";
/* 1267 */       nodeText = nodeText + prefix + "energyMeter.networkMeter.consumptionBase = 0.0" + "\n";
/* 1268 */       nodeText = nodeText + prefix + "energyMeter.networkMeter.numEnergyStates = 2" + "\n";
/* 1269 */       nodeText = nodeText + prefix + "energyMeter.networkMeter.state[0].stateName = \"network_off\"" + "\n";
/* 1270 */       nodeText = nodeText + prefix + "energyMeter.networkMeter.state[0].value = 2.0" + "\n";
/* 1271 */       nodeText = nodeText + prefix + "energyMeter.networkMeter.state[1].stateName = \"network_on\"" + "\n";
/* 1272 */       nodeText = nodeText + prefix + "energyMeter.networkMeter.state[1].value = 9.0" + "\n";
/*      */       
/*      */ 
/* 1275 */       nodeText = nodeText + "\t\t#####################\n\t\t##### Node internals\n\t\t#####################\n\n";
/*      */       
/*      */ 
/*      */ 
/*      */ 
/* 1280 */       nodeText = nodeText + "\t\t### CPU system\n";
/* 1281 */       nodeText = nodeText + "\t" + prefix + "cpuModule.cpuCoreType = \"CPUcore\"\n";
/* 1282 */       nodeText = nodeText + "\t" + prefix + "cpuModule.CPUcore[*].speed = " + Integer.toString(cpuObject.getSpeed()) + "\n";
/* 1283 */       nodeText = nodeText + "\t" + prefix + "cpuModule.CPUcore[*].tick_s = " + Double.toString(cpuObject.getTick()) + "\n";
/*      */       
/*      */ 
/* 1286 */       nodeText = nodeText + "\t\t### Storage system\n";
/* 1287 */       nodeText = nodeText + "\t" + prefix + "storageSystem.device[*].deviceType = \"SimpleDisk\"\n";
/* 1288 */       nodeText = nodeText + "\t" + prefix + "storageSystem.device[*].cacheType = \"NullCache\"\n";
/* 1289 */       nodeText = nodeText + "\t" + prefix + "storageSystem.device[*].device.readBandwidth = " + Double.toString(diskObject.getReadBandwidth()) + "\n";
/* 1290 */       nodeText = nodeText + "\t" + prefix + "storageSystem.device[*].device.writeBandwidth = " + Double.toString(diskObject.getWriteBandwidth()) + "\n\n";
/*      */       
/*      */ 
/* 1293 */       nodeText = nodeText + "\t\t### Memory system\n";
/* 1294 */       nodeText = nodeText + "\t" + prefix + "memory.readLatencyTime_s = " + Double.toString(memoryObject.getReadLatency()) + "\n";
/* 1295 */       nodeText = nodeText + "\t" + prefix + "memory.writeLatencyTime_s = " + Double.toString(memoryObject.getWriteLatency()) + "\n";
/* 1296 */       nodeText = nodeText + "\t" + prefix + "memory.searchLatencyTime_s = " + Double.toString(memoryObject.getSearchLatency()) + "\n";
/* 1297 */       nodeText = nodeText + "\t" + prefix + "memory.numDRAMChips = " + Integer.toString(memoryObject.getNumDramChips()) + "\n";
/* 1298 */       nodeText = nodeText + "\t" + prefix + "memory.numModules = " + Integer.toString(memoryObject.getNumModules()) + "\n\n";
/*      */       
/*      */ 
/* 1301 */       nodeText = nodeText + "\t\t### Operating system\n";
/* 1302 */       nodeText = nodeText + "\t" + prefix + "osModule.cpuSchedulerType = \"CPU_Scheduler_FIFO\"\n\n";
/*      */       
/*      */ 
/* 1305 */       nodeText = nodeText + "\t\t### Volume manager\n";
/* 1306 */       nodeText = nodeText + "\t" + prefix + "osModule.vmModule.storageManagerType = \"NullStorageManager\"\n";
/* 1307 */       nodeText = nodeText + "\t" + prefix + "osModule.vmModule.schedulerType = \"NullStorageScheduler\"\n";
/* 1308 */       nodeText = nodeText + "\t" + prefix + "osModule.vmModule.cacheType = \"NullCache\"\n\n";
/*      */       
/*      */ 
/* 1311 */       nodeText = nodeText + "\t\t### File system\n";
/* 1312 */       nodeText = nodeText + "\t" + prefix + "osModule.fsModule[0].fsType = \"Node_FileSystem\"\n\n";
/*      */       
/*      */ 
/* 1315 */       nodeText = nodeText + "\t################################################################\n\t### General Configuration \n\t################################################################\n\n";
/* 1316 */       nodeText = nodeText + prefix + "numCores = " + Integer.toString(cpuObject.getNumCores() * 2) + "\n";
/* 1317 */       nodeText = nodeText + prefix + "storageSize_GB = " + Integer.toString(diskObject.getSizePerDevice()) + "\n";
/* 1318 */       nodeText = nodeText + prefix + "memorySize_MB = " + Integer.toString(memoryObject.getMemorysize()) + "\n";
/* 1319 */       nodeText = nodeText + prefix + "memoryBlockSize_KB = " + Integer.toString(memoryObject.getBlockSize()) + "\n";
/*      */       
/* 1321 */       nodeText = nodeText + prefix + "numStorageSystems = " + Integer.toString(diskObject.getNumStorageDevices()) + "\n";
/* 1322 */       nodeText = nodeText + prefix + "memoryType = \"RAMmemory\"\n";
/* 1323 */       nodeText = nodeText + prefix + "hypervisorType = \"" + this.cloudModelObject.getHypervisor() + "\"\n";
/* 1324 */       nodeText = nodeText + prefix + "initialState = \"off_state\"\n";
/* 1325 */       nodeText = nodeText + prefix + "hostName = \"" + currentNode.getCompleteName() + "\"\n";
/* 1326 */       nodeText = nodeText + prefix + "storageNode = " + Boolean.toString(isStorage) + "\n";
/* 1327 */       nodeText = nodeText + prefix + "activeEnergyMeter = true\n";
/*      */       
/*      */ 
/* 1330 */       text = text + nodeText + "\n\n";
/* 1331 */       nodeText = "";
/* 1332 */       nodeIndex++;
/*      */     }
/*      */     
/*      */ 
/* 1336 */     return text;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public String getIniRacks()
/*      */   {
/*      */     String nodeText;
/*      */     
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/* 1353 */     String prefix = nodeText = "";
/* 1354 */     int nodeIndex = 0;
/*      */     
/*      */ 
/* 1357 */     getClass();String text = "################################################################\n### Definition of XXXXX\n################################################################\n\n".replaceFirst("XXXXX", "Racks");
/* 1358 */     Iterator<DataCenterUnit> iteratorNode = this.dataCenter.getRackList();
/*      */     
/*      */ 
/* 1361 */     while (iteratorNode.hasNext())
/*      */     {
/*      */ 
/* 1364 */       DataCenterUnit currentRack = (DataCenterUnit)iteratorNode.next();
/*      */       
/*      */ 
/* 1367 */       Rack rackObject = Utils.loadRackObject(currentRack.getName());
/* 1368 */       Node nodeObject = Utils.loadNodeObject(rackObject.isIsStorage(), rackObject.getNodeType());
/* 1369 */       CPU cpuObject = Utils.loadCPUobject(nodeObject.getCpu());
/* 1370 */       Disk diskObject = Utils.loadDiskObject(nodeObject.getDisk());
/* 1371 */       Memory memoryObject = Utils.loadMemoryObject(nodeObject.getMemory());
/* 1372 */       PSU psuObject = Utils.loadPSUobject(nodeObject.getPsu());
/*      */       
/*      */ 
/* 1375 */       getClass();nodeText = nodeText + "\t################################################################\n\t### Definition of XXXXX\n\t################################################################\n\n".replaceFirst("XXXXX", currentRack.getName());
/*      */       
/*      */ 
/* 1378 */       prefix = "\t" + this.cloudName + "." + Configuration.generateRackName(nodeIndex, rackObject.isIsStorage(), currentRack.getName()) + "[*].";
/*      */       
/*      */ 
/* 1381 */       nodeText = nodeText + "\t### Energy Meter Components\n";
/* 1382 */       nodeText = nodeText + "\t" + prefix + "psu.wattage = " + Double.toString(psuObject.getWattage()) + "\n";
/* 1383 */       nodeText = nodeText + "\t" + prefix + "psu.scale = " + Double.toString(psuObject.getScale()) + "\n";
/* 1384 */       nodeText = nodeText + "\t" + prefix + "psu.psu_20 = " + ((EnergyEntry)psuObject.getEnergyData().get(0)).getValue() + "\n";
/* 1385 */       nodeText = nodeText + "\t" + prefix + "psu.psu_50 = " + ((EnergyEntry)psuObject.getEnergyData().get(1)).getValue() + "\n";
/* 1386 */       nodeText = nodeText + "\t" + prefix + "psu.psu_100 = " + ((EnergyEntry)psuObject.getEnergyData().get(2)).getValue() + "\n\n";
/*      */       
/*      */ 
/*      */ 
/* 1390 */       nodeText = nodeText + "\t### CPU\n";
/* 1391 */       nodeText = nodeText + prefix + "energyMeter.cpuMeter.componentName = \"" + cpuObject.getName() + "\"" + "\n";
/* 1392 */       nodeText = nodeText + Utils.energyListToString(cpuObject.getEnergyData(), new StringBuilder().append(prefix).append("energyMeter.cpuMeter.").toString()) + "\n";
/* 1393 */       nodeText = nodeText + prefix + "cpuModule.indepentCores = " + Boolean.toString(cpuObject.isIndependentCores()) + "\n\n";
/*      */       
/*      */ 
/* 1396 */       nodeText = nodeText + "\t### Disk\n";
/* 1397 */       nodeText = nodeText + prefix + "energyMeter.storageMeter.componentName = \"" + diskObject.getName() + "\"" + "\n";
/* 1398 */       nodeText = nodeText + Utils.energyListToString(diskObject.getEnergyData(), new StringBuilder().append(prefix).append("energyMeter.storageMeter.").toString()) + "\n\n";
/*      */       
/*      */ 
/* 1401 */       nodeText = nodeText + "\t### Memory\n";
/* 1402 */       nodeText = nodeText + prefix + "energyMeter.memoryMeter.componentName = \"" + memoryObject.getName() + "\"" + "\n";
/* 1403 */       nodeText = nodeText + Utils.energyListToString(memoryObject.getEnergyData(), new StringBuilder().append(prefix).append("energyMeter.memoryMeter.").toString()) + "\n" + "\n";
/* 1404 */       nodeText = nodeText + prefix + "energyMeter.memoryMeter.numModules = " + Integer.toString(memoryObject.getNumModules()) + "\n";
/* 1405 */       nodeText = nodeText + prefix + "energyMeter.memoryMeter.numDRAMChips = " + Integer.toString(memoryObject.getNumDramChips()) + "\n\n";
/*      */       
/*      */ 
/* 1408 */       nodeText = nodeText + "\t### Network\n";
/* 1409 */       nodeText = nodeText + prefix + "energyMeter.networkMeter.componentName = \"" + "Ralink5412" + "\"" + "\n";
/* 1410 */       nodeText = nodeText + prefix + "energyMeter.networkMeter.consumptionBase = 0.0" + "\n";
/* 1411 */       nodeText = nodeText + prefix + "energyMeter.networkMeter.numEnergyStates = 2" + "\n";
/* 1412 */       nodeText = nodeText + prefix + "energyMeter.networkMeter.state[0].stateName = \"network_off\"" + "\n";
/* 1413 */       nodeText = nodeText + prefix + "energyMeter.networkMeter.state[0].value = 2.0" + "\n";
/* 1414 */       nodeText = nodeText + prefix + "energyMeter.networkMeter.state[1].stateName = \"network_on\"" + "\n";
/* 1415 */       nodeText = nodeText + prefix + "energyMeter.networkMeter.state[1].value = 9.0" + "\n";
/*      */       
/*      */ 
/* 1418 */       nodeText = nodeText + "\t\t#####################\n\t\t##### Node internals\n\t\t#####################\n\n";
/*      */       
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/* 1424 */       nodeText = nodeText + "\t\t### CPU system\n";
/* 1425 */       nodeText = nodeText + "\t" + prefix + "cpuModule.cpuCoreType = \"CPUcore\"\n";
/* 1426 */       nodeText = nodeText + "\t" + prefix + "cpuModule.CPUcore[*].speed = " + Integer.toString(cpuObject.getSpeed()) + "\n";
/* 1427 */       nodeText = nodeText + "\t" + prefix + "cpuModule.CPUcore[*].tick_s = " + Double.toString(cpuObject.getTick()) + "\n";
/*      */       
/*      */ 
/* 1430 */       nodeText = nodeText + "\t\t### Storage system\n";
/* 1431 */       nodeText = nodeText + "\t" + prefix + "storageSystem.device[*].deviceType = \"SimpleDisk\"\n";
/* 1432 */       nodeText = nodeText + "\t" + prefix + "storageSystem.device[*].cacheType = \"NullCache\"\n";
/* 1433 */       nodeText = nodeText + "\t" + prefix + "storageSystem.device[*].device.readBandwidth = " + Double.toString(diskObject.getReadBandwidth()) + "\n";
/* 1434 */       nodeText = nodeText + "\t" + prefix + "storageSystem.device[*].device.writeBandwidth = " + Double.toString(diskObject.getWriteBandwidth()) + "\n\n";
/*      */       
/*      */ 
/* 1437 */       nodeText = nodeText + "\t\t### Memory system\n";
/*      */       
/* 1439 */       nodeText = nodeText + "\t" + prefix + "memory.readLatencyTime_s = " + Double.toString(memoryObject.getReadLatency()) + "\n";
/* 1440 */       nodeText = nodeText + "\t" + prefix + "memory.writeLatencyTime_s = " + Double.toString(memoryObject.getWriteLatency()) + "\n";
/* 1441 */       nodeText = nodeText + "\t" + prefix + "memory.searchLatencyTime_s = " + Double.toString(memoryObject.getSearchLatency()) + "\n";
/* 1442 */       nodeText = nodeText + "\t" + prefix + "memory.numDRAMChips = " + Integer.toString(memoryObject.getNumDramChips()) + "\n";
/* 1443 */       nodeText = nodeText + "\t" + prefix + "memory.numModules = " + Integer.toString(memoryObject.getNumModules()) + "\n\n";
/*      */       
/*      */ 
/* 1446 */       nodeText = nodeText + "\t\t### Operating system\n";
/* 1447 */       nodeText = nodeText + "\t" + prefix + "osModule.cpuSchedulerType = \"CPU_Scheduler_FIFO\"\n\n";
/*      */       
/*      */ 
/* 1450 */       nodeText = nodeText + "\t\t### Volume manager\n";
/* 1451 */       nodeText = nodeText + "\t" + prefix + "osModule.vmModule.storageManagerType = \"NullStorageManager\"\n";
/* 1452 */       nodeText = nodeText + "\t" + prefix + "osModule.vmModule.schedulerType = \"NullStorageScheduler\"\n";
/* 1453 */       nodeText = nodeText + "\t" + prefix + "osModule.vmModule.cacheType = \"NullCache\"\n\n";
/*      */       
/*      */ 
/* 1456 */       nodeText = nodeText + "\t\t### File system\n";
/* 1457 */       nodeText = nodeText + "\t" + prefix + "osModule.fsModule[0].fsType = \"Node_FileSystem\"\n\n";
/*      */       
/*      */ 
/* 1460 */       nodeText = nodeText + "\t################################################################\n\t### General Configuration \n\t################################################################\n\n";
/* 1461 */       nodeText = nodeText + prefix + "numCores = " + Integer.toString(cpuObject.getNumCores() * 2) + "\n";
/* 1462 */       nodeText = nodeText + prefix + "storageSize_GB = " + Integer.toString(diskObject.getSizePerDevice()) + "\n";
/* 1463 */       nodeText = nodeText + prefix + "memorySize_MB = " + Integer.toString(memoryObject.getMemorysize()) + "\n";
/* 1464 */       nodeText = nodeText + prefix + "numStorageSystems = " + Integer.toString(diskObject.getNumStorageDevices()) + "\n";
/* 1465 */       nodeText = nodeText + prefix + "memoryType = \"RAMmemory\"\n";
/* 1466 */       nodeText = nodeText + prefix + "hypervisorType = \"" + this.cloudModelObject.getHypervisor() + "\"\n";
/* 1467 */       nodeText = nodeText + prefix + "initialState = \"off_state\"\n";
/* 1468 */       nodeText = nodeText + prefix + "hostName = \"" + currentRack.getCompleteName() + "\"\n";
/* 1469 */       nodeText = nodeText + prefix + "storageNode = " + Boolean.toString(rackObject.isIsStorage()) + "\n";
/* 1470 */       nodeText = nodeText + prefix + "activeEnergyMeter = true\n";
/*      */       
/*      */ 
/* 1473 */       text = text + nodeText + "\n\n";
/* 1474 */       nodeText = "";
/* 1475 */       nodeIndex++;
/*      */     }
/*      */     
/* 1478 */     return text;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public String getIniVMs()
/*      */   {
/* 1489 */     getClass();String text = "################################################################\n### Definition of Virtual Machines\n################################################################\n\n";
/*      */     
/*      */ 
/* 1492 */     LinkedList<String> vmList = Utils.getVMListInCloud(this.cloudModelObject);
/*      */     
/*      */ 
/*      */ 
/* 1496 */     String prefix = "\t" + this.cloudName + ".vmSet.";
/*      */     
/* 1498 */     text = text + prefix + "vmImageQuantity = " + vmList.size() + "\n";
/*      */     
/*      */ 
/* 1501 */     for (int i = 0; i < vmList.size(); i++)
/*      */     {
/*      */ 
/* 1504 */       VirtualMachine vmObject = Utils.loadVMObject((String)vmList.get(i));
/*      */       
/* 1506 */       text = text + prefix + "vmImage[" + i + "].id = \"" + vmObject.getName() + "\"\n";
/* 1507 */       text = text + prefix + "vmImage[" + i + "].numCores = " + Integer.toString(vmObject.getCpuCores()) + "\n";
/* 1508 */       text = text + prefix + "vmImage[" + i + "].memorySize_MB = " + Double.toString(vmObject.getMemory()) + "\n";
/* 1509 */       text = text + prefix + "vmImage[" + i + "].storageSize_GB = " + Integer.toString(vmObject.getDisk()) + "\n";
/*      */     }
/*      */     
/*      */ 
/*      */ 
/* 1514 */     return text;
/*      */   }
/*      */ }


/* Location:              /home/chverma/Descargas/iCanCloudGUI/iCanCloudGUI.jar!/icancloudgui/Utils/GeneratorHelper.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */