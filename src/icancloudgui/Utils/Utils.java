/*      */ package icancloudgui.Utils;
/*      */ 
/*      */ import icancloudgui.Exceptions.iCanCloudGUIException;
/*      */ import icancloudgui.TabbedPanels.dataClasses.CPU;
/*      */ import icancloudgui.TabbedPanels.dataClasses.Cloud;
/*      */ import icancloudgui.TabbedPanels.dataClasses.DataCenter;
/*      */ import icancloudgui.TabbedPanels.dataClasses.DataCenterUnit;
/*      */ import icancloudgui.TabbedPanels.dataClasses.Disk;
/*      */ import icancloudgui.TabbedPanels.dataClasses.EnergyEntry;
/*      */ import icancloudgui.TabbedPanels.dataClasses.Memory;
/*      */ import icancloudgui.TabbedPanels.dataClasses.Node;
/*      */ import icancloudgui.TabbedPanels.dataClasses.PSU;
/*      */ import icancloudgui.TabbedPanels.dataClasses.Rack;
/*      */ import icancloudgui.TabbedPanels.dataClasses.Switch;
/*      */ import icancloudgui.TabbedPanels.dataClasses.User;
/*      */ import icancloudgui.TabbedPanels.dataClasses.UserEntry;
/*      */ import icancloudgui.TabbedPanels.dataClasses.VMEntry;
/*      */ import icancloudgui.TabbedPanels.dataClasses.VirtualMachine;
/*      */ import icancloudgui.Tables.EnergyTableModel;
/*      */ import icancloudgui.Tables.UserTableModel;
/*      */ import java.awt.Cursor;
/*      */ import java.awt.Dimension;
/*      */ import java.awt.Insets;
/*      */ import java.awt.event.MouseListener;
/*      */ import java.awt.event.MouseMotionListener;
/*      */ import java.io.BufferedInputStream;
/*      */ import java.io.File;
/*      */ import java.io.FileInputStream;
/*      */ import java.io.InputStream;
/*      */ import java.io.ObjectInput;
/*      */ import java.io.ObjectInputStream;
/*      */ import java.io.PrintStream;
/*      */ import java.util.Iterator;
/*      */ import java.util.LinkedList;
/*      */ import javax.swing.ImageIcon;
/*      */ import javax.swing.JComboBox;
/*      */ import javax.swing.JFrame;
/*      */ import javax.swing.JLabel;
/*      */ import javax.swing.JOptionPane;
/*      */ import javax.swing.JPanel;
/*      */ import javax.swing.JTabbedPane;
/*      */ import javax.swing.JTable;
/*      */ import javax.swing.table.TableColumn;
/*      */ import javax.swing.table.TableColumnModel;
/*      */ import javax.swing.table.TableModel;
/*      */ import javax.swing.tree.DefaultMutableTreeNode;
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ public class Utils
/*      */ {
/*      */   public static final String nodeRoot = "iCanCloud";
/*      */   public static final String nodeHardware = "Hardware";
/*      */   public static final String nodeCPU = "CPUs";
/*      */   public static final String nodeDisk = "Disks";
/*      */   public static final String nodeMemory = "Memories";
/*      */   public static final String nodePSU = "PSUs";
/*      */   public static final String nodeNodes = "Nodes";
/*      */   public static final String nodeComputingNode = "Computing Nodes";
/*      */   public static final String nodeStorageNode = "Storage Nodes";
/*      */   public static final String nodeSwitch = "Switches";
/*      */   public static final String nodeRack = "Racks";
/*      */   public static final String nodeDatacenter = "Data-Center";
/*      */   public static final String nodeVirtualization = "Virtualization";
/*      */   public static final String nodeUsers = "Users";
/*      */   public static final String nodeClouds = "Clouds";
/*      */   public static final String nodeMetamorphic = "Metamorphic testing";
/*  137 */   public static final Object[] columnNamesEnergyTable = { "#", "Energy state", "Value" };
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*  142 */   public static final Object[] columnNamesTenantsTable = { "Tenant", "# Instances" };
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*  147 */   public static final Object[] columnNamesVMsTable = { "Virtual Machine", "# Instances" };
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   public static final String dataCenterComputing = "Computing";
/*      */   
/*      */ 
/*      */ 
/*      */   public static final String dataCenterStorage = "Storage";
/*      */   
/*      */ 
/*      */ 
/*      */   public static final String dataCenterSwitch = "Switch";
/*      */   
/*      */ 
/*      */ 
/*      */   public static final String dataCenterRack = "Rack";
/*      */   
/*      */ 
/*      */ 
/*      */   public static final int dataCenterRegularConnection = 2;
/*      */   
/*      */ 
/*      */ 
/*      */   public static final int dataCenterRackConnection = 6;
/*      */   
/*      */ 
/*      */ 
/*      */   public static final String defaultHeaderForVMdefinition = "Definition of Virtual Machines";
/*      */   
/*      */ 
/*      */ 
/*      */   public static final String flagRackSwitchbandwidth = "// RackChannelBandwidth";
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   public static void energyTableModelToList(EnergyTableModel model, LinkedList<EnergyEntry> list)
/*      */   {
/*  187 */     list.clear();
/*      */     
/*  189 */     for (int currentRow = 0; currentRow < model.getRowCount(); currentRow++) {
/*  190 */       EnergyEntry entry = new EnergyEntry();
/*      */       
/*      */ 
/*  193 */       entry.setId(currentRow);
/*  194 */       entry.setName((String)model.getValueAt(currentRow, 1));
/*  195 */       entry.setValue((String)model.getValueAt(currentRow, 2));
/*      */       
/*      */ 
/*  198 */       list.add(entry);
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public static void userTableModelToList(UserTableModel model, LinkedList<UserEntry> list)
/*      */   {
/*  213 */     list.clear();
/*      */     
/*  215 */     for (int currentRow = 0; currentRow < model.getRowCount(); currentRow++) {
/*  216 */       UserEntry entry = new UserEntry();
/*      */       
/*      */ 
/*  219 */       entry.setName((String)model.getValueAt(currentRow, 0));
/*  220 */       entry.setInstances(Integer.parseInt((String)model.getValueAt(currentRow, 1)));
/*      */       
/*      */ 
/*  223 */       list.add(entry);
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public static void VMTableModelToList(UserTableModel model, LinkedList<VMEntry> list)
/*      */   {
/*  239 */     list.clear();
/*      */     
/*  241 */     for (int currentRow = 0; currentRow < model.getRowCount(); currentRow++) {
/*  242 */       VMEntry entry = new VMEntry();
/*      */       
/*      */ 
/*  245 */       entry.setName((String)model.getValueAt(currentRow, 0));
/*  246 */       entry.setInstances(Integer.parseInt((String)model.getValueAt(currentRow, 1)));
/*      */       
/*      */ 
/*  249 */       list.add(entry);
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public static void energyListToTable(LinkedList<EnergyEntry> list, EnergyTableModel model)
/*      */   {
/*  263 */     model.setColumnCount(3);
/*  264 */     model.setColumnIdentifiers(columnNamesEnergyTable);
/*      */     
/*  266 */     for (int currentRow = 0; currentRow < list.size(); currentRow++) {
/*  267 */       model.addRow(new Object[] { Integer.toString(((EnergyEntry)list.get(currentRow)).getId()), ((EnergyEntry)list.get(currentRow)).getName(), ((EnergyEntry)list.get(currentRow)).getValue() });
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public static void userListToTable(LinkedList<UserEntry> list, UserTableModel model)
/*      */   {
/*  283 */     model.setColumnCount(2);
/*  284 */     model.setColumnIdentifiers(columnNamesTenantsTable);
/*      */     
/*  286 */     for (int currentRow = 0; currentRow < list.size(); currentRow++) {
/*  287 */       model.addRow(new Object[] { ((UserEntry)list.get(currentRow)).getName(), Integer.toString(((UserEntry)list.get(currentRow)).getInstances()) });
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public static void VMListToTable(LinkedList<VMEntry> list, UserTableModel model)
/*      */   {
/*  302 */     model.setColumnCount(2);
/*  303 */     model.setColumnIdentifiers(columnNamesVMsTable);
/*      */     
/*  305 */     for (int currentRow = 0; currentRow < list.size(); currentRow++) {
/*  306 */       model.addRow(new Object[] { ((VMEntry)list.get(currentRow)).getName(), Integer.toString(((VMEntry)list.get(currentRow)).getInstances()) });
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public static void initEnergyTable(JTable table)
/*      */   {
/*  323 */     EnergyTableModel model = new EnergyTableModel();
/*  324 */     model.setColumnCount(3);
/*  325 */     model.setColumnIdentifiers(columnNamesEnergyTable);
/*      */     
/*      */ 
/*  328 */     for (int currentRow = 0; currentRow < 10; currentRow++) {
/*  329 */       model.addRow(new Object[] { Integer.toString(currentRow), "", "" });
/*      */     }
/*      */     
/*      */ 
/*      */ 
/*      */ 
/*  335 */     table.setModel(model);
/*      */     
/*      */ 
/*  338 */     TableColumnModel columnModel = table.getColumnModel();
/*  339 */     columnModel.getColumn(0).setWidth(50);
/*  340 */     columnModel.getColumn(0).setMaxWidth(50);
/*  341 */     columnModel.getColumn(0).setMinWidth(50);
/*  342 */     columnModel.getColumn(0).setResizable(false);
/*  343 */     columnModel.getColumn(1).setWidth(200);
/*  344 */     columnModel.getColumn(1).setMaxWidth(200);
/*  345 */     columnModel.getColumn(1).setMinWidth(200);
/*  346 */     columnModel.getColumn(1).setResizable(false);
/*  347 */     columnModel.getColumn(2).setResizable(false);
/*      */     
/*      */ 
/*  350 */     table.setShowGrid(true);
/*  351 */     table.updateUI();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public static void initTenantTable(JTable table)
/*      */   {
/*  367 */     UserTableModel model = new UserTableModel();
/*  368 */     model.setColumnCount(2);
/*  369 */     model.setColumnIdentifiers(columnNamesTenantsTable);
/*      */     
/*      */ 
/*  372 */     LinkedList<String> list = getComponentListFromDir(Configuration.REPOSITORY_USERS);
/*      */     
/*      */ 
/*  375 */     for (int currentRow = 0; currentRow < list.size(); currentRow++)
/*      */     {
/*      */ 
/*  378 */       int result = searchUserInTable((String)list.get(currentRow), table.getModel());
/*      */       
/*      */ 
/*  381 */       model.addRow(new Object[] { list.get(currentRow), Integer.toString(result) });
/*      */     }
/*      */     
/*      */ 
/*  385 */     table.setModel(model);
/*      */     
/*      */ 
/*  388 */     table.setShowGrid(true);
/*  389 */     table.updateUI();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public static void initVMTable(JTable table)
/*      */   {
/*  405 */     UserTableModel model = new UserTableModel();
/*  406 */     model.setColumnCount(2);
/*  407 */     model.setColumnIdentifiers(columnNamesVMsTable);
/*      */     
/*      */ 
/*  410 */     LinkedList<String> list = getComponentListFromDir(Configuration.REPOSITORY_VM);
/*      */     
/*      */ 
/*  413 */     for (int currentRow = 0; currentRow < list.size(); currentRow++)
/*      */     {
/*      */ 
/*  416 */       int result = searchVMInTable((String)list.get(currentRow), table.getModel());
/*      */       
/*      */ 
/*  419 */       model.addRow(new Object[] { list.get(currentRow), Integer.toString(result) });
/*      */     }
/*      */     
/*      */ 
/*  423 */     table.setModel(model);
/*      */     
/*      */ 
/*  426 */     table.setShowGrid(true);
/*  427 */     table.updateUI();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   private static int searchUserInTable(String user, TableModel model)
/*      */   {
/*  446 */     boolean found = false;
/*  447 */     int currentRow; int result = currentRow = 0;
/*      */     
/*      */ 
/*  450 */     while ((currentRow < model.getRowCount()) && (!found))
/*      */     {
/*      */ 
/*      */ 
/*  454 */       if ((String)model.getValueAt(currentRow, 0) == null) {
/*  455 */         currentRow++;
/*      */ 
/*      */ 
/*      */       }
/*  459 */       else if (((String)model.getValueAt(currentRow, 0)).equals(user)) {
/*  460 */         result = Integer.parseInt((String)model.getValueAt(currentRow, 1));
/*  461 */         found = true;
/*      */       }
/*      */       else {
/*  464 */         currentRow++;
/*      */       }
/*      */     }
/*  467 */     return result;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   private static int searchVMInTable(String vm, TableModel model)
/*      */   {
/*  485 */     boolean found = false;
/*  486 */     int currentRow; int result = currentRow = 0;
/*      */     
/*      */ 
/*  489 */     while ((currentRow < model.getRowCount()) && (!found))
/*      */     {
/*      */ 
/*      */ 
/*  493 */       if ((String)model.getValueAt(currentRow, 0) == null) {
/*  494 */         currentRow++;
/*      */ 
/*      */ 
/*      */       }
/*  498 */       else if (((String)model.getValueAt(currentRow, 0)).equals(vm)) {
/*  499 */         result = Integer.parseInt((String)model.getValueAt(currentRow, 1));
/*  500 */         found = true;
/*      */       }
/*      */       else {
/*  503 */         currentRow++;
/*      */       }
/*      */     }
/*  506 */     return result;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public static JLabel createLabel(ImageIcon labIcon, String name, Insets insets, MouseListener listener)
/*      */   {
/*  524 */     JLabel lab = new JLabel(name, labIcon, 0);
/*  525 */     lab.setVerticalTextPosition(3);
/*  526 */     lab.setHorizontalTextPosition(0);
/*      */     
/*      */ 
/*  529 */     addLabelProperties(lab, listener);
/*      */     
/*      */ 
/*  532 */     Dimension size = lab.getPreferredSize();
/*      */     
/*      */ 
/*  535 */     lab.setBounds(50 + insets.left, 50 + insets.top, size.width, size.height);
/*      */     
/*  537 */     lab.setLayout(null);
/*      */     
/*      */ 
/*  540 */     return lab;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public static void addLabelProperties(JLabel label, MouseListener listener)
/*      */   {
/*  553 */     label.addMouseMotionListener((MouseMotionListener)listener);
/*  554 */     label.addMouseListener(listener);
/*  555 */     Cursor moveCursor = new Cursor(13);
/*  556 */     label.setCursor(moveCursor);
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public static void initCombo(JComboBox combo, String defaultName)
/*      */   {
/*  570 */     boolean found = false;
/*  571 */     int i = 0;
/*      */     
/*      */ 
/*  574 */     while ((!found) && (i < combo.getItemCount()))
/*      */     {
/*  576 */       if (combo.getItemAt(i).toString().equals(defaultName)) {
/*  577 */         found = true;
/*  578 */         combo.setSelectedIndex(i);
/*      */       } else {
/*  580 */         i++;
/*      */       }
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public static void loadCombo(LinkedList<String> list, JComboBox combo)
/*      */   {
/*  595 */     combo.removeAllItems();
/*      */     
/*      */ 
/*  598 */     for (int currentComponent = 0; currentComponent < list.size(); currentComponent++) {
/*  599 */       combo.addItem(list.get(currentComponent));
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   public static void loadComboDataCenterUnit(LinkedList<DataCenterUnit> list, JComboBox combo)
/*      */   {
/*  608 */     combo.removeAllItems();
/*      */     
/*      */ 
/*  611 */     for (int currentComponent = 0; currentComponent < list.size(); currentComponent++) {
/*  612 */       combo.addItem(((DataCenterUnit)list.get(currentComponent)).getCompleteName());
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public static LinkedList<String> getComponentListFromDir(String path)
/*      */   {
/*  628 */     LinkedList<String> list = new LinkedList();
/*      */     
/*      */ 
/*      */     try
/*      */     {
/*  633 */       File initialDir = new File(path);
/*      */       
/*      */ 
/*  636 */       if (initialDir.isDirectory())
/*      */       {
/*      */ 
/*  639 */         File[] files = initialDir.listFiles();
/*      */         
/*      */ 
/*  642 */         for (int i = 0; i < files.length; i++)
/*      */         {
/*      */ 
/*  645 */           list.add(files[i].getName());
/*      */         }
/*      */       }
/*      */     } catch (Exception e) {
/*  649 */       e.printStackTrace();
/*      */     }
/*      */     
/*  652 */     return list;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public static void updateTreeNode(String path, DefaultMutableTreeNode root)
/*      */   {
/*      */     try
/*      */     {
/*  668 */       File initialDir = new File(path);
/*      */       
/*      */ 
/*  671 */       root.removeAllChildren();
/*      */       
/*      */ 
/*  674 */       if (initialDir.isDirectory())
/*      */       {
/*      */ 
/*  677 */         File[] files = initialDir.listFiles();
/*      */         
/*      */ 
/*  680 */         for (int i = 0; i < files.length; i++)
/*      */         {
/*      */ 
/*  683 */           DefaultMutableTreeNode node = new DefaultMutableTreeNode(files[i].getName());
/*  684 */           root.add(node);
/*      */         }
/*      */       }
/*      */     }
/*      */     catch (Exception e) {
/*  689 */       e.printStackTrace();
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public static boolean removeComponent(String path, String componentName)
/*      */   {
/*  707 */     boolean componentDeleted = false;
/*      */     
/*      */ 
/*  710 */     File componentToDelete = new File(path + File.separatorChar + componentName);
/*      */     
/*      */ 
/*  713 */     if (componentToDelete.exists())
/*      */     {
/*      */ 
/*  716 */       int response = JOptionPane.showConfirmDialog(null, "Are you sure to remove " + componentName + "?", "Confirm to remove", 2, 3);
/*      */       
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*  723 */       if (response == 0) {
/*  724 */         componentToDelete.delete();
/*  725 */         componentDeleted = true;
/*      */       }
/*      */     }
/*      */     else
/*      */     {
/*  730 */       JOptionPane.showMessageDialog(new JFrame(), "File of selected component does not exists.", "Error", 0);
/*      */     }
/*      */     
/*      */ 
/*      */ 
/*  735 */     return componentDeleted;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public static int getTabIndexFromName(JTabbedPane pane, String name)
/*      */   {
/*  751 */     int index = -1;
/*  752 */     int i = 0;
/*  753 */     boolean found = false;
/*      */     
/*  755 */     while ((i < pane.getTabCount()) && (!found))
/*      */     {
/*  757 */       if (pane.getTitleAt(i).equals(name)) {
/*  758 */         index = i;
/*  759 */         found = true;
/*      */       }
/*      */       else {
/*  762 */         i++;
/*      */       } }
/*  764 */     return index;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public static void showHelp(JPanel panel)
/*      */   {
/*  774 */     JOptionPane.showMessageDialog(panel, "The tree on the left shows every element in the simulation platform\nDouble click loads an element into the corresponding tab.\nRight click shows a popup menu with the corresponding actions.\nIn the data-center tab, only storage nodes, switches and racks can be added.\n", "Help", 1);
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public static LinkedList<String> getCommunicationLinkList()
/*      */   {
/*  792 */     LinkedList<String> list = new LinkedList();
/*      */     
/*      */ 
/*  795 */     list.add("Ethernet 10 Mbps");
/*  796 */     list.add("Ethernet 100 Mbps");
/*  797 */     list.add("Ethernet 1 Gbps");
/*  798 */     list.add("Ethernet 10 Gbps");
/*  799 */     list.add("Ethernet 40 Gbps");
/*  800 */     list.add("Ethernet 100 Gbps");
/*      */     
/*      */ 
/*  803 */     return list;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public static LinkedList<String> getCloudManagerList()
/*      */   {
/*  815 */     LinkedList<String> list = new LinkedList();
/*      */     
/*      */ 
/*  818 */     list.add("FCFS");
/*  819 */     list.add("Round-Robin");
/*  820 */     list.add("Random");
/*      */     
/*  822 */     return list;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public static LinkedList<String> getHypervisorList()
/*      */   {
/*  834 */     LinkedList<String> list = new LinkedList();
/*      */     
/*      */ 
/*  837 */     list.add("HypervisorNoOverhead");
/*  838 */     list.add("CitrixXenServer6_Linux");
/*  839 */     list.add("CitrixXenServer6_Windows");
/*  840 */     list.add("CitrixXenServer6_PV_Linux");
/*  841 */     list.add("HyperV_Win2008RP2SP1_Linux");
/*  842 */     list.add("HyperV_Win2008RP2SP1_Linux");
/*  843 */     list.add("RedHatEnterprise_Virtualization2_2_Linux.");
/*  844 */     list.add("RedHatEnterprise_Virtualization2_2_Windows");
/*  845 */     list.add("VMWareESXi5_VSphere5_Windows");
/*  846 */     list.add("VMWareESXi5_VSphere5_Linux");
/*      */     
/*  848 */     return list;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public static LinkedList<String> getDistributionList()
/*      */   {
/*  860 */     LinkedList<String> list = new LinkedList();
/*      */     
/*      */ 
/*  863 */     list.add("no_distribution");
/*  864 */     list.add("normal");
/*  865 */     list.add("exponential");
/*  866 */     list.add("uniform");
/*  867 */     list.add("truncnormal");
/*  868 */     list.add("student_t");
/*  869 */     list.add("cauchy");
/*  870 */     list.add("triang");
/*  871 */     list.add("lognormal");
/*  872 */     list.add("weibull");
/*  873 */     list.add("intuniform");
/*  874 */     list.add("bernoulli");
/*  875 */     list.add("pareto_shifted");
/*  876 */     list.add("binomial");
/*  877 */     list.add("geometric");
/*  878 */     list.add("negbinomial");
/*  879 */     list.add("poisson");
/*      */     
/*  881 */     return list;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public static CPU loadCPUobject(String cpuName)
/*      */   {
/*  898 */     CPU loadedCPU = null;
/*      */     
/*      */ 
/*      */ 
/*      */     try
/*      */     {
/*  904 */       File currentCPU = new File(Configuration.REPOSITORY_CPUS_FOLDER + File.separatorChar + cpuName);
/*      */       
/*      */ 
/*  907 */       if (currentCPU.exists())
/*      */       {
/*      */ 
/*  910 */         InputStream file = new FileInputStream(currentCPU);
/*  911 */         InputStream buffer = new BufferedInputStream(file);
/*  912 */         ObjectInput input = new ObjectInputStream(buffer);
/*      */         
/*      */ 
/*  915 */         loadedCPU = (CPU)input.readObject();
/*      */       }
/*      */       else
/*      */       {
/*  919 */         JOptionPane.showMessageDialog(new JFrame(), "File of selected CPU does not exists.", "Error", 0);
/*      */         
/*      */ 
/*  922 */         loadedCPU = null;
/*      */       }
/*      */     }
/*      */     catch (Exception e) {
/*  926 */       e.printStackTrace();
/*  927 */       loadedCPU = null;
/*      */     }
/*      */     
/*  930 */     return loadedCPU;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public static Disk loadDiskObject(String diskName)
/*      */   {
/*  947 */     Disk loadedDisk = null;
/*      */     
/*      */ 
/*      */     try
/*      */     {
/*  952 */       File currentDisk = new File(Configuration.REPOSITORY_DISKS_FOLDER + File.separatorChar + diskName);
/*      */       
/*      */ 
/*  955 */       if (currentDisk.exists())
/*      */       {
/*      */ 
/*  958 */         InputStream file = new FileInputStream(currentDisk);
/*  959 */         InputStream buffer = new BufferedInputStream(file);
/*  960 */         ObjectInput input = new ObjectInputStream(buffer);
/*      */         
/*      */ 
/*  963 */         loadedDisk = (Disk)input.readObject();
/*      */       }
/*      */       else
/*      */       {
/*  967 */         JOptionPane.showMessageDialog(new JFrame(), "File of selected disk does not exists.", "Error", 0);
/*      */         
/*      */ 
/*  970 */         loadedDisk = null;
/*      */       }
/*      */     }
/*      */     catch (Exception e) {
/*  974 */       e.printStackTrace();
/*  975 */       loadedDisk = null;
/*      */     }
/*      */     
/*      */ 
/*  979 */     return loadedDisk;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public static Memory loadMemoryObject(String memoryName)
/*      */   {
/*  996 */     Memory loadedMemory = null;
/*      */     
/*      */ 
/*      */     try
/*      */     {
/* 1001 */       File currentMemory = new File(Configuration.REPOSITORY_MEMORIES_FOLDER + File.separatorChar + memoryName);
/*      */       
/*      */ 
/* 1004 */       if (currentMemory.exists())
/*      */       {
/*      */ 
/* 1007 */         InputStream file = new FileInputStream(currentMemory);
/* 1008 */         InputStream buffer = new BufferedInputStream(file);
/* 1009 */         ObjectInput input = new ObjectInputStream(buffer);
/*      */         
/*      */ 
/* 1012 */         loadedMemory = (Memory)input.readObject();
/*      */       }
/*      */       else
/*      */       {
/* 1016 */         JOptionPane.showMessageDialog(new JFrame(), "File of selected Memory does not exists.", "Error", 0);
/*      */         
/*      */ 
/* 1019 */         loadedMemory = null;
/*      */       }
/*      */     }
/*      */     catch (Exception e) {
/* 1023 */       e.printStackTrace();
/* 1024 */       loadedMemory = null;
/*      */     }
/*      */     
/*      */ 
/* 1028 */     return loadedMemory;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public static PSU loadPSUobject(String psuName)
/*      */   {
/* 1045 */     PSU loadedPSU = null;
/*      */     
/*      */ 
/*      */     try
/*      */     {
/* 1050 */       File currentPSU = new File(Configuration.REPOSITORY_PSU_FOLDER + File.separatorChar + psuName);
/*      */       
/*      */ 
/* 1053 */       if (currentPSU.exists())
/*      */       {
/*      */ 
/* 1056 */         InputStream file = new FileInputStream(currentPSU);
/* 1057 */         InputStream buffer = new BufferedInputStream(file);
/* 1058 */         ObjectInput input = new ObjectInputStream(buffer);
/*      */         
/*      */ 
/* 1061 */         loadedPSU = (PSU)input.readObject();
/*      */       }
/*      */       else
/*      */       {
/* 1065 */         JOptionPane.showMessageDialog(new JFrame(), "File of selected PSU does not exists.", "Error", 0);
/*      */         
/*      */ 
/* 1068 */         loadedPSU = null;
/*      */       }
/*      */     }
/*      */     catch (Exception e) {
/* 1072 */       e.printStackTrace();
/* 1073 */       loadedPSU = null;
/*      */     }
/*      */     
/*      */ 
/* 1077 */     return loadedPSU;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public static Node loadNodeObject(boolean isStorage, String nodeName)
/*      */   {
/* 1095 */     Node loadedNode = null;
/*      */     try
/*      */     {
/*      */       File currentNode;
/* 1100 */       if (isStorage) {
/* 1101 */         currentNode = new File(Configuration.REPOSITORY_STORAGE_NODES_FOLDER + File.separatorChar + nodeName);
/*      */       } else {
/* 1103 */         currentNode = new File(Configuration.REPOSITORY_COMPUTING_NODES_FOLDER + File.separatorChar + nodeName);
/*      */       }
/*      */       
/*      */ 
/* 1107 */       if (currentNode.exists())
/*      */       {
/*      */ 
/* 1110 */         InputStream file = new FileInputStream(currentNode);
/* 1111 */         InputStream buffer = new BufferedInputStream(file);
/* 1112 */         ObjectInput input = new ObjectInputStream(buffer);
/*      */         
/*      */ 
/* 1115 */         loadedNode = (Node)input.readObject();
/*      */       }
/*      */       else
/*      */       {
/* 1119 */         JOptionPane.showMessageDialog(new JFrame(), "File of selected Node does not exists.", "Error", 0);
/*      */         
/*      */ 
/* 1122 */         loadedNode = null;
/*      */       }
/*      */     }
/*      */     catch (Exception e) {
/* 1126 */       e.printStackTrace();
/* 1127 */       loadedNode = null;
/*      */     }
/*      */     
/*      */ 
/* 1131 */     return loadedNode;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public static Switch loadSwitchObject(String switchName)
/*      */   {
/* 1148 */     Switch loadedSwitch = null;
/*      */     
/*      */ 
/*      */     try
/*      */     {
/* 1153 */       File currentSwitch = new File(Configuration.REPOSITORY_SWITCHES + File.separatorChar + switchName);
/*      */       
/*      */ 
/* 1156 */       if (currentSwitch.exists())
/*      */       {
/*      */ 
/* 1159 */         InputStream file = new FileInputStream(currentSwitch);
/* 1160 */         InputStream buffer = new BufferedInputStream(file);
/* 1161 */         ObjectInput input = new ObjectInputStream(buffer);
/*      */         
/*      */ 
/* 1164 */         loadedSwitch = (Switch)input.readObject();
/*      */       }
/*      */       else
/*      */       {
/* 1168 */         JOptionPane.showMessageDialog(new JFrame(), "File of selected Switch does not exists.", "Error", 0);
/*      */         
/*      */ 
/* 1171 */         loadedSwitch = null;
/*      */       }
/*      */     }
/*      */     catch (Exception e) {
/* 1175 */       e.printStackTrace();
/* 1176 */       loadedSwitch = null;
/*      */     }
/*      */     
/*      */ 
/* 1180 */     return loadedSwitch;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public static Rack loadRackObject(String rackName)
/*      */   {
/* 1197 */     Rack loadedRack = null;
/*      */     
/*      */ 
/*      */     try
/*      */     {
/* 1202 */       File currentRack = new File(Configuration.REPOSITORY_RACKS + File.separatorChar + rackName);
/*      */       
/*      */ 
/* 1205 */       if (currentRack.exists())
/*      */       {
/*      */ 
/* 1208 */         InputStream file = new FileInputStream(currentRack);
/* 1209 */         InputStream buffer = new BufferedInputStream(file);
/* 1210 */         ObjectInput input = new ObjectInputStream(buffer);
/*      */         
/*      */ 
/* 1213 */         loadedRack = (Rack)input.readObject();
/*      */       }
/*      */       else
/*      */       {
/* 1217 */         JOptionPane.showMessageDialog(new JFrame(), "File of selected Rack does not exists.", "Error", 0);
/*      */         
/*      */ 
/* 1220 */         loadedRack = null;
/*      */       }
/*      */     }
/*      */     catch (Exception e) {
/* 1224 */       e.printStackTrace();
/* 1225 */       loadedRack = null;
/*      */     }
/*      */     
/*      */ 
/* 1229 */     return loadedRack;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public static VirtualMachine loadVMObject(String vmName)
/*      */   {
/* 1246 */     VirtualMachine loadedVM = null;
/*      */     
/*      */ 
/*      */     try
/*      */     {
/* 1251 */       File currentVM = new File(Configuration.REPOSITORY_VM + File.separatorChar + vmName);
/*      */       
/*      */ 
/* 1254 */       if (currentVM.exists())
/*      */       {
/*      */ 
/* 1257 */         InputStream file = new FileInputStream(currentVM);
/* 1258 */         InputStream buffer = new BufferedInputStream(file);
/* 1259 */         ObjectInput input = new ObjectInputStream(buffer);
/*      */         
/*      */ 
/* 1262 */         loadedVM = (VirtualMachine)input.readObject();
/*      */       }
/*      */       else
/*      */       {
/* 1266 */         JOptionPane.showMessageDialog(new JFrame(), "File of selected Virtual Machine does not exists.", "Error", 0);
/*      */         
/*      */ 
/* 1269 */         loadedVM = null;
/*      */       }
/*      */     }
/*      */     catch (Exception e) {
/* 1273 */       e.printStackTrace();
/* 1274 */       loadedVM = null;
/*      */     }
/*      */     
/* 1277 */     return loadedVM;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public static User loadUserObject(String userName)
/*      */   {
/* 1294 */     User loadedUser = null;
/*      */     
/*      */ 
/*      */     try
/*      */     {
/* 1299 */       File currentUser = new File(Configuration.REPOSITORY_USERS + File.separatorChar + userName);
/*      */       
/*      */ 
/* 1302 */       if (currentUser.exists())
/*      */       {
/*      */ 
/* 1305 */         InputStream file = new FileInputStream(currentUser);
/* 1306 */         InputStream buffer = new BufferedInputStream(file);
/* 1307 */         ObjectInput input = new ObjectInputStream(buffer);
/*      */         
/*      */ 
/* 1310 */         loadedUser = (User)input.readObject();
/*      */       }
/*      */       else
/*      */       {
/* 1314 */         JOptionPane.showMessageDialog(new JFrame(), "File of selected User does not exists.", "Error", 0);
/*      */         
/*      */ 
/* 1317 */         loadedUser = null;
/*      */       }
/*      */     }
/*      */     catch (Exception e) {
/* 1321 */       e.printStackTrace();
/* 1322 */       loadedUser = null;
/*      */     }
/*      */     
/* 1325 */     return loadedUser;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public static DataCenter loadDataCenterObject(String dataCenterName)
/*      */   {
/* 1342 */     DataCenter loadedDataCenter = null;
/*      */     
/*      */ 
/*      */     try
/*      */     {
/* 1347 */       File currentDataCenter = new File(Configuration.REPOSITORY_DATA_CENTER + File.separatorChar + dataCenterName);
/*      */       
/*      */ 
/* 1350 */       if (currentDataCenter.exists())
/*      */       {
/*      */ 
/* 1353 */         InputStream file = new FileInputStream(currentDataCenter);
/* 1354 */         InputStream buffer = new BufferedInputStream(file);
/* 1355 */         ObjectInput input = new ObjectInputStream(buffer);
/*      */         
/*      */ 
/* 1358 */         loadedDataCenter = (DataCenter)input.readObject();
/*      */       }
/*      */       else
/*      */       {
/* 1362 */         JOptionPane.showMessageDialog(new JFrame(), "File of selected data center does not exists.", "Error", 0);
/*      */         
/*      */ 
/* 1365 */         loadedDataCenter = null;
/*      */       }
/*      */     }
/*      */     catch (Exception e) {
/* 1369 */       e.printStackTrace();
/* 1370 */       loadedDataCenter = null;
/*      */     }
/*      */     
/* 1373 */     return loadedDataCenter;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public static Cloud loadCloudObject(String cloudName)
/*      */   {
/* 1390 */     Cloud loadedCloud = null;
/*      */     
/*      */ 
/*      */     try
/*      */     {
/* 1395 */       File currentCloud = new File(Configuration.REPOSITORY_CLOUD + File.separatorChar + cloudName);
/*      */       
/*      */ 
/* 1398 */       if (currentCloud.exists())
/*      */       {
/*      */ 
/* 1401 */         InputStream file = new FileInputStream(currentCloud);
/* 1402 */         InputStream buffer = new BufferedInputStream(file);
/* 1403 */         ObjectInput input = new ObjectInputStream(buffer);
/*      */         
/*      */ 
/* 1406 */         loadedCloud = (Cloud)input.readObject();
/*      */       }
/*      */       else
/*      */       {
/* 1410 */         JOptionPane.showMessageDialog(new JFrame(), "File of selected cloud does not exists.", "Error", 0);
/*      */         
/*      */ 
/* 1413 */         loadedCloud = null;
/*      */       }
/*      */     }
/*      */     catch (Exception e) {
/* 1417 */       e.printStackTrace();
/* 1418 */       loadedCloud = null;
/*      */     }
/*      */     
/* 1421 */     return loadedCloud;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   private static boolean isThisVMInList(LinkedList<String> list, String vm)
/*      */   {
/* 1430 */     boolean found = false;
/* 1431 */     int i = 0;
/*      */     
/* 1433 */     while ((i < list.size()) && (!found))
/*      */     {
/* 1435 */       if (((String)list.get(i)).equals(vm)) {
/* 1436 */         found = true;
/*      */       } else {
/* 1438 */         i++;
/*      */       }
/*      */     }
/* 1441 */     return found;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public static LinkedList<String> getVMListInCloud(Cloud cloudModel)
/*      */   {
/* 1459 */     LinkedList<String> list = new LinkedList();
/* 1460 */     LinkedList<UserEntry> userList = cloudModel.getUserData();
/*      */     
/*      */ 
/* 1463 */     for (int currentUserIndex = 0; currentUserIndex < userList.size(); currentUserIndex++)
/*      */     {
/*      */ 
/* 1466 */       if (((UserEntry)userList.get(currentUserIndex)).getInstances() > 0)
/*      */       {
/*      */ 
/* 1469 */         User currentUser = loadUserObject(((UserEntry)userList.get(currentUserIndex)).getName());
/*      */         
/*      */ 
/* 1472 */         LinkedList<VMEntry> vmList = currentUser.getVMData();
/*      */         
/*      */ 
/* 1475 */         for (int currentVMIndex = 0; currentVMIndex < vmList.size(); currentVMIndex++)
/*      */         {
/*      */ 
/* 1478 */           if ((!isThisVMInList(list, ((VMEntry)vmList.get(currentVMIndex)).getName())) && (((VMEntry)vmList.get(currentVMIndex)).getInstances() > 0)) {
/* 1479 */             list.add(((VMEntry)vmList.get(currentVMIndex)).getName());
/*      */           }
/*      */         }
/*      */       }
/*      */     }
/* 1484 */     return list;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public static DataCenterUnit isDataCenterUnitInList(Iterator<DataCenterUnit> iterator, String element)
/*      */   {
/* 1500 */     boolean found = false;
/* 1501 */     DataCenterUnit unit = null;
/*      */     
/* 1503 */     while ((iterator.hasNext()) && (!found))
/*      */     {
/*      */ 
/* 1506 */       unit = (DataCenterUnit)iterator.next();
/*      */       
/* 1508 */       if (unit.getCompleteName().equals(element)) {
/* 1509 */         found = true;
/*      */       }
/*      */     }
/*      */     
/* 1513 */     if (!found) {
/* 1514 */       unit = null;
/*      */     }
/* 1516 */     return unit;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public static int getDataCenterUnitIndex(Iterator<DataCenterUnit> iterator, DataCenterUnit element)
/*      */   {
/* 1532 */     boolean found = false;
/* 1533 */     int index = 0;
/*      */     
/* 1535 */     while ((iterator.hasNext()) && (!found))
/*      */     {
/* 1537 */       if (((DataCenterUnit)iterator.next()).getCompleteName().equals(element.getCompleteName())) {
/* 1538 */         found = true;
/*      */       }
/*      */       else {
/* 1541 */         index++;
/*      */       }
/*      */     }
/* 1544 */     if (!found) {
/* 1545 */       index = -1;
/*      */     }
/* 1547 */     return index;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   public static boolean checkEnergyTable(EnergyTableModel model)
/*      */   {
/* 1555 */     boolean isOK = true;
/* 1556 */     int i = 0;
/*      */     
/*      */     try
/*      */     {
/* 1560 */       while ((i < model.getRowCount()) && (isOK))
/*      */       {
/*      */ 
/* 1563 */         if (model.getValueAt(i, 1).toString().length() <= 0) {
/* 1564 */           isOK = false;
/*      */         }
/*      */         
/* 1567 */         Double.parseDouble(model.getValueAt(i, 2).toString());
/*      */         
/*      */ 
/* 1570 */         i++;
/*      */       }
/*      */     }
/*      */     catch (Exception e) {
/* 1574 */       e.printStackTrace();
/* 1575 */       isOK = false;
/*      */     }
/*      */     
/* 1578 */     return isOK;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public static String energyListToString(LinkedList<EnergyEntry> energyList, String meter)
/*      */   {
/* 1587 */     String text = "";
/* 1588 */     text = text + meter + "numEnergyStates = " + (energyList.size() - 1) + "\n";
/* 1589 */     text = text + meter + "consumptionBase = " + ((EnergyEntry)energyList.get(0)).getValue() + "\n";
/*      */     
/* 1591 */     for (int i = 1; i < energyList.size(); i++)
/*      */     {
/* 1593 */       text = text + meter + "state[" + (i - 1) + "].stateName = \"" + ((EnergyEntry)energyList.get(i)).getName() + "\"\n";
/* 1594 */       text = text + meter + "state[" + (i - 1) + "].value = " + ((EnergyEntry)energyList.get(i)).getValue() + "\n";
/*      */     }
/*      */     
/*      */ 
/* 1598 */     return text;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public static boolean checkName(String name)
/*      */   {
/* 1614 */     boolean isOK = true;
/* 1615 */     int i = 0;
/*      */     
/*      */ 
/*      */ 
/* 1619 */     if (name.length() <= 0) {
/* 1620 */       isOK = false;
/*      */     }
/*      */     
/*      */ 
/* 1624 */     if ((isOK) && (!Character.isLetter(name.charAt(0)))) {
/* 1625 */       isOK = false;
/*      */     }
/*      */     
/*      */ 
/* 1629 */     while ((i < name.length()) && (isOK))
/*      */     {
/* 1631 */       if ((!Character.isDigit(name.charAt(i))) && (!Character.isLetter(name.charAt(i))) && (name.charAt(i) != '_') && (name.charAt(i) != '-'))
/*      */       {
/*      */ 
/*      */ 
/*      */ 
/* 1636 */         isOK = false;
/*      */       } else {
/* 1638 */         i++;
/*      */       }
/*      */     }
/*      */     
/* 1642 */     return isOK;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public static void showErrorMessage(String message, String title)
/*      */   {
/* 1653 */     JOptionPane.showMessageDialog(new JFrame(), message, title, 0);
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public static void printStringList(String description, LinkedList<String> list)
/*      */   {
/* 1665 */     System.out.print("Showing list: " + description + " -> ");
/*      */     
/* 1667 */     for (int i = 0; i < list.size(); i++) {
/* 1668 */       System.out.print((String)list.get(i) + " - ");
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public static String getBetterCPU()
/*      */     throws iCanCloudGUIException
/*      */   {
/* 1681 */     String cpuName = "";
/* 1682 */     int speed; int cores = speed = 0;
/* 1683 */     LinkedList<String> cpuList = getComponentListFromDir(Configuration.REPOSITORY_CPUS_FOLDER);
/*      */     
/* 1685 */     for (int i = 0; i < cpuList.size(); i++)
/*      */     {
/*      */ 
/* 1688 */       CPU currentCPU = loadCPUobject((String)cpuList.get(i));
/*      */       
/* 1690 */       if ((currentCPU.getNumCores() > cores) || ((currentCPU.getNumCores() == cores) && (currentCPU.getSpeed() > speed)))
/*      */       {
/*      */ 
/*      */ 
/* 1694 */         cores = currentCPU.getNumCores();
/* 1695 */         speed = currentCPU.getSpeed();
/* 1696 */         cpuName = currentCPU.getName();
/*      */       }
/*      */     }
/*      */     
/* 1700 */     if (cpuName.length() == 0) {
/* 1701 */       throw new iCanCloudGUIException("There is no CPUs in the repository!");
/*      */     }
/* 1703 */     return cpuName;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public static String getWorseCPU()
/*      */     throws iCanCloudGUIException
/*      */   {
/* 1715 */     String cpuName = "";
/* 1716 */     int speed; int cores = speed = 9999999;
/* 1717 */     LinkedList<String> cpuList = getComponentListFromDir(Configuration.REPOSITORY_CPUS_FOLDER);
/*      */     
/* 1719 */     for (int i = 0; i < cpuList.size(); i++)
/*      */     {
/*      */ 
/* 1722 */       CPU currentCPU = loadCPUobject((String)cpuList.get(i));
/*      */       
/* 1724 */       if ((currentCPU.getNumCores() < cores) || ((currentCPU.getNumCores() == cores) && (currentCPU.getSpeed() < speed)))
/*      */       {
/*      */ 
/*      */ 
/* 1728 */         cores = currentCPU.getNumCores();
/* 1729 */         speed = currentCPU.getSpeed();
/* 1730 */         cpuName = currentCPU.getName();
/*      */       }
/*      */     }
/*      */     
/* 1734 */     if (cpuName.length() == 0) {
/* 1735 */       throw new iCanCloudGUIException("There is no CPUs in the repository!");
/*      */     }
/* 1737 */     return cpuName;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public static String getBetterDisk()
/*      */     throws iCanCloudGUIException
/*      */   {
/* 1750 */     String diskName = "";
/* 1751 */     int size = 0;
/* 1752 */     double writeBandwidth; double readBandwidth = writeBandwidth = 0.0D;
/* 1753 */     LinkedList<String> diskList = getComponentListFromDir(Configuration.REPOSITORY_DISKS_FOLDER);
/*      */     
/* 1755 */     for (int i = 0; i < diskList.size(); i++)
/*      */     {
/*      */ 
/* 1758 */       Disk currentDisk = loadDiskObject((String)diskList.get(i));
/*      */       
/* 1760 */       if ((currentDisk.getSizePerDevice() > size) || ((currentDisk.getSizePerDevice() == size) && (currentDisk.getReadBandwidth() > readBandwidth)) || ((currentDisk.getSizePerDevice() == size) && (currentDisk.getReadBandwidth() == readBandwidth) && (currentDisk.getWriteBandwidth() > writeBandwidth)))
/*      */       {
/*      */ 
/*      */ 
/*      */ 
/* 1765 */         size = currentDisk.getSizePerDevice();
/* 1766 */         readBandwidth = currentDisk.getReadBandwidth();
/* 1767 */         writeBandwidth = currentDisk.getWriteBandwidth();
/* 1768 */         diskName = currentDisk.getName();
/*      */       }
/*      */     }
/*      */     
/* 1772 */     if (diskName.length() == 0) {
/* 1773 */       throw new iCanCloudGUIException("There is no Disks in the repository!");
/*      */     }
/* 1775 */     return diskName;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public static String getWorseDisk()
/*      */     throws iCanCloudGUIException
/*      */   {
/* 1788 */     String diskName = "";
/* 1789 */     int size = 9999999;
/* 1790 */     double writeBandwidth; double readBandwidth = writeBandwidth = 9999999.0D;
/* 1791 */     LinkedList<String> diskList = getComponentListFromDir(Configuration.REPOSITORY_DISKS_FOLDER);
/*      */     
/* 1793 */     for (int i = 0; i < diskList.size(); i++)
/*      */     {
/*      */ 
/* 1796 */       Disk currentDisk = loadDiskObject((String)diskList.get(i));
/*      */       
/* 1798 */       if ((currentDisk.getSizePerDevice() < size) || ((currentDisk.getSizePerDevice() == size) && (currentDisk.getReadBandwidth() < readBandwidth)) || ((currentDisk.getSizePerDevice() == size) && (currentDisk.getReadBandwidth() == readBandwidth) && (currentDisk.getWriteBandwidth() < writeBandwidth)))
/*      */       {
/*      */ 
/*      */ 
/*      */ 
/* 1803 */         size = currentDisk.getSizePerDevice();
/* 1804 */         readBandwidth = currentDisk.getReadBandwidth();
/* 1805 */         writeBandwidth = currentDisk.getWriteBandwidth();
/* 1806 */         diskName = currentDisk.getName();
/*      */       }
/*      */     }
/*      */     
/* 1810 */     if (diskName.length() == 0) {
/* 1811 */       throw new iCanCloudGUIException("There is no Disks in the repository!");
/*      */     }
/* 1813 */     return diskName;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public static String getBetterMemory()
/*      */     throws iCanCloudGUIException
/*      */   {
/* 1826 */     String memoryName = "";
/* 1827 */     int size = 0;
/* 1828 */     double writeLatency; double readLatency = writeLatency = 99999.0D;
/* 1829 */     LinkedList<String> memoryList = getComponentListFromDir(Configuration.REPOSITORY_MEMORIES_FOLDER);
/*      */     
/* 1831 */     for (int i = 0; i < memoryList.size(); i++)
/*      */     {
/*      */ 
/* 1834 */       Memory currentMemory = loadMemoryObject((String)memoryList.get(i));
/*      */       
/* 1836 */       if ((currentMemory.getMemorysize() > size) || ((currentMemory.getMemorysize() == size) && (currentMemory.getReadLatency() < readLatency)) || ((currentMemory.getMemorysize() == size) && (currentMemory.getReadLatency() == readLatency) && (currentMemory.getWriteLatency() < writeLatency)))
/*      */       {
/*      */ 
/*      */ 
/*      */ 
/* 1841 */         size = currentMemory.getMemorysize();
/* 1842 */         readLatency = currentMemory.getReadLatency();
/* 1843 */         writeLatency = currentMemory.getWriteLatency();
/* 1844 */         memoryName = currentMemory.getName();
/*      */       }
/*      */     }
/*      */     
/* 1848 */     if (memoryName.length() == 0) {
/* 1849 */       throw new iCanCloudGUIException("There is no Memories in the repository!");
/*      */     }
/* 1851 */     return memoryName;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public static String getWorseMemory()
/*      */     throws iCanCloudGUIException
/*      */   {
/* 1864 */     String memoryName = "";
/* 1865 */     int size = 9999999;
/* 1866 */     double writeLatency; double readLatency = writeLatency = 0.0D;
/* 1867 */     LinkedList<String> memoryList = getComponentListFromDir(Configuration.REPOSITORY_MEMORIES_FOLDER);
/*      */     
/* 1869 */     for (int i = 0; i < memoryList.size(); i++)
/*      */     {
/*      */ 
/* 1872 */       Memory currentMemory = loadMemoryObject((String)memoryList.get(i));
/*      */       
/* 1874 */       if ((currentMemory.getMemorysize() < size) || ((currentMemory.getMemorysize() == size) && (currentMemory.getReadLatency() > readLatency)) || ((currentMemory.getMemorysize() == size) && (currentMemory.getReadLatency() == readLatency) && (currentMemory.getWriteLatency() > writeLatency)))
/*      */       {
/*      */ 
/*      */ 
/*      */ 
/* 1879 */         size = currentMemory.getMemorysize();
/* 1880 */         readLatency = currentMemory.getReadLatency();
/* 1881 */         writeLatency = currentMemory.getWriteLatency();
/* 1882 */         memoryName = currentMemory.getName();
/*      */       }
/*      */     }
/*      */     
/* 1886 */     if (memoryName.length() == 0) {
/* 1887 */       throw new iCanCloudGUIException("There is no Memories in the repository!");
/*      */     }
/* 1889 */     return memoryName;
/*      */   }
/*      */ }


/* Location:              /home/chverma/Descargas/iCanCloudGUI/iCanCloudGUI.jar!/icancloudgui/Utils/Utils.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */