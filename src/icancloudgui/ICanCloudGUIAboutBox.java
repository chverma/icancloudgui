/*     */ package icancloudgui;
/*     */ 
/*     */ import javax.swing.GroupLayout.SequentialGroup;
/*     */ import javax.swing.JLabel;
/*     */ 
/*     */ public class ICanCloudGUIAboutBox extends javax.swing.JDialog
/*     */ {
/*     */   private javax.swing.JButton closeButton;
/*     */   
/*     */   public ICanCloudGUIAboutBox(java.awt.Frame parent)
/*     */   {
/*  12 */     super(parent);
/*  13 */     initComponents();
/*  14 */     getRootPane().setDefaultButton(this.closeButton);
/*     */   }
/*     */   
/*     */   @org.jdesktop.application.Action
/*  18 */   public void closeAboutBox() { dispose(); }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private void initComponents()
/*     */   {
/*  29 */     this.closeButton = new javax.swing.JButton();
/*  30 */     JLabel appTitleLabel = new JLabel();
/*  31 */     JLabel versionLabel = new JLabel();
/*  32 */     JLabel appVersionLabel = new JLabel();
/*  33 */     JLabel vendorLabel = new JLabel();
/*  34 */     JLabel appVendorLabel = new JLabel();
/*  35 */     JLabel homepageLabel = new JLabel();
/*  36 */     JLabel appHomepageLabel = new JLabel();
/*  37 */     JLabel appDescLabel = new JLabel();
/*  38 */     JLabel imageLabel = new JLabel();
/*     */     
/*  40 */     setDefaultCloseOperation(2);
/*  41 */     org.jdesktop.application.ResourceMap resourceMap = ((ICanCloudGUIApp)org.jdesktop.application.Application.getInstance(ICanCloudGUIApp.class)).getContext().getResourceMap(ICanCloudGUIAboutBox.class);
/*  42 */     setTitle(resourceMap.getString("title", new Object[0]));
/*  43 */     setModal(true);
/*  44 */     setName("aboutBox");
/*  45 */     setResizable(false);
/*     */     
/*  47 */     javax.swing.ActionMap actionMap = ((ICanCloudGUIApp)org.jdesktop.application.Application.getInstance(ICanCloudGUIApp.class)).getContext().getActionMap(ICanCloudGUIAboutBox.class, this);
/*  48 */     this.closeButton.setAction(actionMap.get("closeAboutBox"));
/*  49 */     this.closeButton.setName("closeButton");
/*     */     
/*  51 */     appTitleLabel.setFont(appTitleLabel.getFont().deriveFont(appTitleLabel.getFont().getStyle() | 0x1, appTitleLabel.getFont().getSize() + 4));
/*  52 */     appTitleLabel.setText(resourceMap.getString("Application.title", new Object[0]));
/*  53 */     appTitleLabel.setName("appTitleLabel");
/*     */     
/*  55 */     versionLabel.setFont(versionLabel.getFont().deriveFont(versionLabel.getFont().getStyle() | 0x1));
/*  56 */     versionLabel.setText(resourceMap.getString("versionLabel.text", new Object[0]));
/*  57 */     versionLabel.setName("versionLabel");
/*     */     
/*  59 */     appVersionLabel.setText(resourceMap.getString("Application.version", new Object[0]));
/*  60 */     appVersionLabel.setName("appVersionLabel");
/*     */     
/*  62 */     vendorLabel.setFont(vendorLabel.getFont().deriveFont(vendorLabel.getFont().getStyle() | 0x1));
/*  63 */     vendorLabel.setText(resourceMap.getString("vendorLabel.text", new Object[0]));
/*  64 */     vendorLabel.setName("vendorLabel");
/*     */     
/*  66 */     appVendorLabel.setText(resourceMap.getString("Application.vendor", new Object[0]));
/*  67 */     appVendorLabel.setName("appVendorLabel");
/*     */     
/*  69 */     homepageLabel.setFont(homepageLabel.getFont().deriveFont(homepageLabel.getFont().getStyle() | 0x1));
/*  70 */     homepageLabel.setText(resourceMap.getString("homepageLabel.text", new Object[0]));
/*  71 */     homepageLabel.setName("homepageLabel");
/*     */     
/*  73 */     appHomepageLabel.setText(resourceMap.getString("Application.homepage", new Object[0]));
/*  74 */     appHomepageLabel.setName("appHomepageLabel");
/*     */     
/*  76 */     appDescLabel.setText(resourceMap.getString("appDescLabel.text", new Object[0]));
/*  77 */     appDescLabel.setName("appDescLabel");
/*     */     
/*  79 */     imageLabel.setIcon(resourceMap.getIcon("imageLabel.icon"));
/*  80 */     imageLabel.setName("imageLabel");
/*     */     
/*  82 */     javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
/*  83 */     getContentPane().setLayout(layout);
/*  84 */     layout.setHorizontalGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING).addGroup(layout.createSequentialGroup().addComponent(imageLabel).addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING).addGroup(layout.createSequentialGroup().addGap(18, 18, 18).addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING).addGroup(javax.swing.GroupLayout.Alignment.LEADING, layout.createSequentialGroup().addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING).addComponent(versionLabel).addComponent(vendorLabel).addComponent(homepageLabel)).addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED).addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING).addComponent(appVersionLabel).addComponent(appVendorLabel).addComponent(appHomepageLabel))).addComponent(appTitleLabel, javax.swing.GroupLayout.Alignment.LEADING).addComponent(appDescLabel, javax.swing.GroupLayout.Alignment.LEADING, -1, 303, 32767))).addGroup(layout.createSequentialGroup().addGap(95, 95, 95).addComponent(this.closeButton))).addContainerGap()));
/*     */     
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 109 */     layout.setVerticalGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING).addComponent(imageLabel, -2, 217, 32767).addGroup(layout.createSequentialGroup().addContainerGap().addComponent(appTitleLabel).addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED).addComponent(appDescLabel, -2, -1, -2).addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED).addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE).addComponent(versionLabel).addComponent(appVersionLabel)).addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED).addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE).addComponent(vendorLabel).addComponent(appVendorLabel)).addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED).addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE).addComponent(homepageLabel).addComponent(appHomepageLabel)).addGap(18, 18, 18).addComponent(this.closeButton).addContainerGap(-1, 32767)));
/*     */     
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 134 */     pack();
/*     */   }
/*     */ }


/* Location:              /home/chverma/Descargas/iCanCloudGUI/iCanCloudGUI.jar!/icancloudgui/ICanCloudGUIAboutBox.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */