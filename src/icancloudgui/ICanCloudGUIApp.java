/*     */ package icancloudgui;
/*     */ 
/*     */ import icancloudgui.Utils.Configuration;
/*     */ import java.awt.Window;
/*     */ import java.io.File;
/*     */ import javax.swing.JFileChooser;
/*     */ import javax.swing.JOptionPane;
/*     */ import org.jdesktop.application.Application;
/*     */ import org.jdesktop.application.SingleFrameApplication;
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ public class ICanCloudGUIApp
/*     */   extends SingleFrameApplication
/*     */ {
/*     */   protected void startup()
/*     */   {
/*  24 */     checkFolders();
/*     */     
/*     */ 
/*  27 */     checkHomePath();
/*     */     
/*  29 */     show(new ICanCloudGUIView(this));
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   protected void configureWindow(Window root) {}
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public static ICanCloudGUIApp getApplication()
/*     */   {
/*  47 */     return (ICanCloudGUIApp)Application.getInstance(ICanCloudGUIApp.class);
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   public static void main(String[] args)
/*     */   {
/*  55 */     launch(ICanCloudGUIApp.class, args);
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private void checkHomePath()
/*     */   {
/*  73 */     boolean allOK = false;
/*  74 */     Configuration configFile = new Configuration();
/*  75 */     int counter = 3;
/*     */     
/*     */ 
/*  78 */     String homePath = configFile.getProperty("iCanCloudHome");
/*     */     
/*     */ 
/*  81 */     if ((homePath.length() == 0) || (!checkICanCloud_HOME(homePath)))
/*     */     {
/*     */ 
/*  84 */       JOptionPane.showMessageDialog(null, "Please, enter the iCanCloud home folder");
/*     */       
/*     */ 
/*  87 */       JFileChooser chooser = new JFileChooser();
/*  88 */       chooser.setDialogTitle("Choose the iCanCloud home path");
/*  89 */       chooser.setFileSelectionMode(1);
/*     */       
/*     */ 
/*  92 */       for (int i = 0; (i < counter) && (!allOK); i++)
/*     */       {
/*     */ 
/*  95 */         if (chooser.showOpenDialog(getApplication().getMainFrame()) == 0) {
/*  96 */           homePath = chooser.getSelectedFile().getAbsolutePath();
/*  97 */           allOK = checkICanCloud_HOME(homePath);
/*     */         }
/*     */         
/*     */ 
/* 101 */         if ((!allOK) && (i != counter - 1)) {
/* 102 */           JOptionPane.showMessageDialog(null, "Entered path is not a valid iCanCloud folder. Please, try again!");
/*     */         }
/*     */       }
/*     */       
/* 106 */       if (allOK)
/*     */       {
/* 108 */         configFile.setProperty("iCanCloudHome", homePath);
/*     */         
/*     */ 
/* 111 */         configFile.saveToFile();
/*     */       }
/*     */       else {
/* 114 */         JOptionPane.showMessageDialog(null, "Wrong path for iCanCloud folder. Check if you have installed iCanCloud under omnetpp folder!", "ERROR", 0);
/* 115 */         end();
/*     */       }
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private void checkFolders()
/*     */   {
/* 132 */     File file = new File(Configuration.CONFIG_FILE_FOLDER);
/*     */     
/* 134 */     if (!file.exists()) {
/* 135 */       file.mkdir();
/*     */     }
/*     */     
/*     */ 
/* 139 */     file = new File(Configuration.REPOSITORY_FOLDER);
/*     */     
/* 141 */     if (!file.exists()) {
/* 142 */       file.mkdir();
/*     */     }
/*     */     
/*     */ 
/* 146 */     file = new File(Configuration.REPOSITORY_HARDWARE_FOLDER);
/*     */     
/* 148 */     if (!file.exists()) {
/* 149 */       file.mkdir();
/*     */     }
/*     */     
/*     */ 
/* 153 */     file = new File(Configuration.REPOSITORY_CPUS_FOLDER);
/*     */     
/* 155 */     if (!file.exists()) {
/* 156 */       file.mkdir();
/*     */     }
/*     */     
/*     */ 
/* 160 */     file = new File(Configuration.REPOSITORY_DISKS_FOLDER);
/*     */     
/* 162 */     if (!file.exists()) {
/* 163 */       file.mkdir();
/*     */     }
/*     */     
/*     */ 
/* 167 */     file = new File(Configuration.REPOSITORY_MEMORIES_FOLDER);
/*     */     
/* 169 */     if (!file.exists()) {
/* 170 */       file.mkdir();
/*     */     }
/*     */     
/*     */ 
/* 174 */     file = new File(Configuration.REPOSITORY_PSU_FOLDER);
/*     */     
/* 176 */     if (!file.exists()) {
/* 177 */       file.mkdir();
/*     */     }
/*     */     
/*     */ 
/* 181 */     file = new File(Configuration.REPOSITORY_NODES_FOLDER);
/*     */     
/* 183 */     if (!file.exists()) {
/* 184 */       file.mkdir();
/*     */     }
/*     */     
/*     */ 
/* 188 */     file = new File(Configuration.REPOSITORY_COMPUTING_NODES_FOLDER);
/*     */     
/* 190 */     if (!file.exists()) {
/* 191 */       file.mkdir();
/*     */     }
/*     */     
/*     */ 
/* 195 */     file = new File(Configuration.REPOSITORY_STORAGE_NODES_FOLDER);
/*     */     
/* 197 */     if (!file.exists()) {
/* 198 */       file.mkdir();
/*     */     }
/*     */     
/*     */ 
/* 202 */     file = new File(Configuration.REPOSITORY_SWITCHES);
/*     */     
/* 204 */     if (!file.exists()) {
/* 205 */       file.mkdir();
/*     */     }
/*     */     
/*     */ 
/* 209 */     file = new File(Configuration.REPOSITORY_RACKS);
/*     */     
/* 211 */     if (!file.exists()) {
/* 212 */       file.mkdir();
/*     */     }
/*     */     
/*     */ 
/* 216 */     file = new File(Configuration.REPOSITORY_VM);
/*     */     
/* 218 */     if (!file.exists()) {
/* 219 */       file.mkdir();
/*     */     }
/*     */     
/*     */ 
/* 223 */     file = new File(Configuration.REPOSITORY_USERS);
/*     */     
/* 225 */     if (!file.exists()) {
/* 226 */       file.mkdir();
/*     */     }
/*     */     
/*     */ 
/* 230 */     file = new File(Configuration.REPOSITORY_DATA_CENTER);
/*     */     
/* 232 */     if (!file.exists()) {
/* 233 */       file.mkdir();
/*     */     }
/*     */     
/*     */ 
/* 237 */     file = new File(Configuration.REPOSITORY_CLOUD);
/*     */     
/* 239 */     if (!file.exists()) {
/* 240 */       file.mkdir();
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private boolean checkICanCloud_HOME(String path)
/*     */   {
/* 256 */     File file = new File(path + File.separatorChar + "src" + File.separatorChar + "run_iCanCloud");
/*     */     
/* 258 */     return file.exists();
/*     */   }
/*     */ }


/* Location:              /home/chverma/Descargas/iCanCloudGUI/iCanCloudGUI.jar!/icancloudgui/ICanCloudGUIApp.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */