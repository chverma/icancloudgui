/*     */ package icancloudgui.TabbedPanels;
/*     */ 
/*     */ import javax.swing.JLabel;
/*     */ 
/*     */ public class DiskPanel extends javax.swing.JPanel
/*     */ {
/*     */   icancloudgui.ICanCloudGUIView mainFrame;
/*     */   private JLabel buttonClear;
/*     */   private JLabel buttonHelp;
/*     */   private JLabel buttonResizeTable;
/*     */   private JLabel buttonSave;
/*     */   private javax.swing.JTable diskTable;
/*     */   private JLabel energyLabel;
/*     */   private JLabel labelComment;
/*     */   private JLabel labelMinSpeed;
/*     */   private JLabel labelName;
/*     */   private JLabel labelNumCores;
/*     */   private JLabel labelNumDevices;
/*     */   private JLabel labelResizeTable;
/*     */   private JLabel labelSpeed;
/*     */   private JLabel labelTick;
/*     */   private javax.swing.JScrollPane scrollTable;
/*     */   private javax.swing.JScrollPane scrollTextArea;
/*     */   private javax.swing.JTextArea textAreaComment;
/*     */   private javax.swing.JTextField textName;
/*     */   private javax.swing.JTextField textNewNumRows;
/*     */   private javax.swing.JTextField textNumDevices;
/*     */   private javax.swing.JTextField textReadBandwidth;
/*     */   private javax.swing.JTextField textSeekTime;
/*     */   private javax.swing.JTextField textSize;
/*     */   private javax.swing.JTextField textWriteBandwidth;
/*     */   
/*     */   public DiskPanel(icancloudgui.ICanCloudGUIView mainFrame) {
/*  34 */     initComponents();
/*  35 */     icancloudgui.Utils.Utils.initEnergyTable(this.diskTable);
/*  36 */     this.mainFrame = mainFrame;
/*     */     
/*     */ 
/*  39 */     this.labelResizeTable.setVisible(false);
/*  40 */     this.buttonResizeTable.setVisible(false);
/*  41 */     this.textNewNumRows.setVisible(false);
/*  42 */     initDefaultTableValues();
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public boolean checkValues()
/*     */   {
/*  55 */     boolean allOK = true;
/*     */     
/*     */ 
/*     */ 
/*  59 */     if (!icancloudgui.Utils.Utils.checkName(this.textName.getText()))
/*     */     {
/*  61 */       allOK = false;
/*  62 */       icancloudgui.Utils.Utils.showErrorMessage("Invalid name for this Disk.\nA name must start with a letter and can contain alphanumeric characters, '-' and '_'", "Wrong name!");
/*     */     }
/*     */     
/*     */ 
/*     */ 
/*     */     try
/*     */     {
/*  69 */       if (allOK) {
/*  70 */         Double.parseDouble(this.textReadBandwidth.getText());
/*     */       }
/*     */     }
/*     */     catch (Exception e) {
/*  74 */       allOK = false;
/*  75 */       icancloudgui.Utils.Utils.showErrorMessage("Wrong parameter for read bandwidth.\nThis parameter must be a double.\n", "Wrong parameter!");
/*     */     }
/*     */     
/*     */ 
/*     */ 
/*     */ 
/*     */     try
/*     */     {
/*  83 */       if (allOK) {
/*  84 */         Double.parseDouble(this.textWriteBandwidth.getText());
/*     */       }
/*     */     }
/*     */     catch (Exception e) {
/*  88 */       allOK = false;
/*  89 */       icancloudgui.Utils.Utils.showErrorMessage("Wrong parameter for write bandwidth.\nThis parameter must be a double.\n", "Wrong parameter!");
/*     */     }
/*     */     
/*     */ 
/*     */ 
/*     */     try
/*     */     {
/*  96 */       if (allOK) {
/*  97 */         Double.parseDouble(this.textSeekTime.getText());
/*     */       }
/*     */     }
/*     */     catch (Exception e) {
/* 101 */       allOK = false;
/* 102 */       icancloudgui.Utils.Utils.showErrorMessage("Wrong parameter for seek time.\nThis parameter must be a double.\n", "Wrong parameter!");
/*     */     }
/*     */     
/*     */ 
/*     */ 
/*     */     try
/*     */     {
/* 109 */       if (allOK) {
/* 110 */         Integer.parseInt(this.textSize.getText());
/*     */       }
/*     */     }
/*     */     catch (Exception e) {
/* 114 */       allOK = false;
/* 115 */       icancloudgui.Utils.Utils.showErrorMessage("Wrong parameter for disk size.\nThis parameter must be an integer.\n", "Wrong parameter!");
/*     */     }
/*     */     
/*     */ 
/*     */ 
/*     */     try
/*     */     {
/* 122 */       if (allOK) {
/* 123 */         Integer.parseInt(this.textSize.getText());
/*     */       }
/*     */     }
/*     */     catch (Exception e) {
/* 127 */       allOK = false;
/* 128 */       icancloudgui.Utils.Utils.showErrorMessage("Wrong parameter for number of devices.\nThis parameter must be an integer.\n", "Wrong parameter!");
/*     */     }
/*     */     
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 135 */     if ((allOK) && (!icancloudgui.Utils.Utils.checkEnergyTable((icancloudgui.Tables.EnergyTableModel)this.diskTable.getModel()))) {
/* 136 */       allOK = false;
/*     */       
/* 138 */       icancloudgui.Utils.Utils.showErrorMessage("Table of energy states contains wrong values.\nEnergy state must be a String and its value a double.\n", "Wrong parameter!");
/*     */     }
/*     */     
/*     */ 
/*     */ 
/*     */ 
/* 144 */     return allOK;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public icancloudgui.TabbedPanels.dataClasses.Disk panelToDiskObject()
/*     */   {
/* 158 */     icancloudgui.TabbedPanels.dataClasses.Disk currentDisk = new icancloudgui.TabbedPanels.dataClasses.Disk();
/* 159 */     java.util.LinkedList<icancloudgui.TabbedPanels.dataClasses.EnergyEntry> list = new java.util.LinkedList();
/*     */     
/*     */ 
/* 162 */     currentDisk.setName(this.textName.getText());
/* 163 */     currentDisk.setReadBandwidth(Double.parseDouble(this.textReadBandwidth.getText()));
/* 164 */     currentDisk.setWriteBandwidth(Double.parseDouble(this.textWriteBandwidth.getText()));
/* 165 */     currentDisk.setSeekTime(Double.parseDouble(this.textSeekTime.getText()));
/* 166 */     currentDisk.setSizePerDevice(Integer.parseInt(this.textSize.getText()));
/* 167 */     currentDisk.setNumStorageDevices(Integer.parseInt(this.textNumDevices.getText()));
/* 168 */     currentDisk.setComment(this.textAreaComment.getText());
/* 169 */     icancloudgui.Utils.Utils.energyTableModelToList((icancloudgui.Tables.EnergyTableModel)this.diskTable.getModel(), list);
/* 170 */     currentDisk.setEnergyData(list);
/*     */     
/* 172 */     return currentDisk;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void clear()
/*     */   {
/* 181 */     this.textName.setText("");
/* 182 */     this.textReadBandwidth.setText("");
/* 183 */     this.textWriteBandwidth.setText("");
/* 184 */     this.textSeekTime.setText("");
/* 185 */     this.textSize.setText("");
/* 186 */     this.textNumDevices.setText("");
/* 187 */     this.textAreaComment.setText("");
/* 188 */     icancloudgui.Utils.Utils.initEnergyTable(this.diskTable);
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private void loadDiskObjectInPanel(icancloudgui.TabbedPanels.dataClasses.Disk newDisk)
/*     */   {
/* 201 */     this.textName.setText(newDisk.getName());
/* 202 */     this.textReadBandwidth.setText(Double.toString(newDisk.getReadBandwidth()));
/* 203 */     this.textWriteBandwidth.setText(Double.toString(newDisk.getWriteBandwidth()));
/* 204 */     this.textSeekTime.setText(Double.toString(newDisk.getSeekTime()));
/* 205 */     this.textSize.setText(Integer.toString(newDisk.getSizePerDevice()));
/* 206 */     this.textNumDevices.setText(Integer.toString(newDisk.getNumStorageDevices()));
/* 207 */     this.textAreaComment.setText(newDisk.getComment());
/*     */     
/*     */ 
/* 210 */     icancloudgui.Tables.EnergyTableModel model = new icancloudgui.Tables.EnergyTableModel();
/* 211 */     icancloudgui.Utils.Utils.energyListToTable(newDisk.getEnergyData(), model);
/* 212 */     this.diskTable.setModel(model);
/* 213 */     this.diskTable.updateUI();
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void loadDiskInPanel(String diskName)
/*     */   {
/* 226 */     icancloudgui.TabbedPanels.dataClasses.Disk loadedDisk = icancloudgui.Utils.Utils.loadDiskObject(diskName);
/*     */     
/*     */ 
/* 229 */     if (loadedDisk != null) {
/* 230 */       loadDiskObjectInPanel(loadedDisk);
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private boolean writeDiskToFile()
/*     */   {
/* 250 */     boolean allOK = checkValues();
/*     */     
/*     */ 
/* 253 */     if (allOK)
/*     */     {
/*     */ 
/* 256 */       icancloudgui.TabbedPanels.dataClasses.Disk currentDisk = panelToDiskObject();
/*     */       
/*     */ 
/* 259 */       String newDiskFile = icancloudgui.Utils.Configuration.REPOSITORY_DISKS_FOLDER + java.io.File.separatorChar + currentDisk.getName();
/*     */       
/*     */ 
/*     */       try
/*     */       {
/* 264 */         if (new java.io.File(newDiskFile).exists())
/*     */         {
/*     */ 
/* 267 */           int response = javax.swing.JOptionPane.showConfirmDialog(null, "Overwrite existing disk?", "Confirm Overwrite", 2, 3);
/*     */           
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 274 */           if (response == 0)
/*     */           {
/*     */ 
/* 277 */             java.io.FileOutputStream fout = new java.io.FileOutputStream(newDiskFile);
/* 278 */             java.io.ObjectOutputStream oos = new java.io.ObjectOutputStream(fout);
/* 279 */             oos.writeObject(currentDisk);
/* 280 */             oos.close();
/* 281 */             allOK = true;
/*     */           }
/*     */           
/*     */ 
/*     */         }
/*     */         else
/*     */         {
/*     */ 
/* 289 */           java.io.FileOutputStream fout = new java.io.FileOutputStream(newDiskFile);
/* 290 */           java.io.ObjectOutputStream oos = new java.io.ObjectOutputStream(fout);
/* 291 */           oos.writeObject(currentDisk);
/* 292 */           oos.close();
/* 293 */           allOK = true;
/*     */         }
/*     */       }
/*     */       catch (Exception e) {
/* 297 */         e.printStackTrace();
/*     */       }
/*     */     }
/*     */     
/* 301 */     return allOK;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private void initDefaultTableValues()
/*     */   {
/* 314 */     icancloudgui.Tables.EnergyTableModel newModel = new icancloudgui.Tables.EnergyTableModel();
/* 315 */     newModel.setColumnCount(3);
/* 316 */     newModel.setColumnIdentifiers(icancloudgui.Utils.Utils.columnNamesEnergyTable);
/*     */     
/*     */ 
/* 319 */     newModel.addRow(new Object[] { Integer.toString(0), "W", "" });
/* 320 */     newModel.addRow(new Object[] { Integer.toString(1), "disk_off", "" });
/* 321 */     newModel.addRow(new Object[] { Integer.toString(2), "disk_IO", "" });
/* 322 */     newModel.addRow(new Object[] { Integer.toString(3), "disk_active", "" });
/* 323 */     newModel.addRow(new Object[] { Integer.toString(4), "disk_idle", "" });
/*     */     
/* 325 */     this.diskTable.setModel(newModel);
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private void initComponents()
/*     */   {
/* 340 */     this.labelName = new JLabel();
/* 341 */     this.labelNumCores = new JLabel();
/* 342 */     this.labelSpeed = new JLabel();
/* 343 */     this.labelMinSpeed = new JLabel();
/* 344 */     this.labelTick = new JLabel();
/* 345 */     this.textName = new javax.swing.JTextField();
/* 346 */     this.textReadBandwidth = new javax.swing.JTextField();
/* 347 */     this.textSeekTime = new javax.swing.JTextField();
/* 348 */     this.textWriteBandwidth = new javax.swing.JTextField();
/* 349 */     this.scrollTable = new javax.swing.JScrollPane();
/* 350 */     this.diskTable = new javax.swing.JTable();
/* 351 */     this.energyLabel = new JLabel();
/* 352 */     this.buttonResizeTable = new JLabel();
/* 353 */     this.textNewNumRows = new javax.swing.JTextField();
/* 354 */     this.labelResizeTable = new JLabel();
/* 355 */     this.textSize = new javax.swing.JTextField();
/* 356 */     this.textNumDevices = new javax.swing.JTextField();
/* 357 */     this.labelNumDevices = new JLabel();
/* 358 */     this.buttonHelp = new JLabel();
/* 359 */     this.buttonSave = new JLabel();
/* 360 */     this.labelComment = new JLabel();
/* 361 */     this.scrollTextArea = new javax.swing.JScrollPane();
/* 362 */     this.textAreaComment = new javax.swing.JTextArea();
/* 363 */     this.buttonClear = new JLabel();
/*     */     
/* 365 */     org.jdesktop.application.ResourceMap resourceMap = ((icancloudgui.ICanCloudGUIApp)org.jdesktop.application.Application.getInstance(icancloudgui.ICanCloudGUIApp.class)).getContext().getResourceMap(DiskPanel.class);
/* 366 */     setBackground(resourceMap.getColor("Form.background"));
/* 367 */     setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
/* 368 */     setMinimumSize(new java.awt.Dimension(782, 510));
/* 369 */     setName("Form");
/* 370 */     setPreferredSize(new java.awt.Dimension(500, 641));
/* 371 */     setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());
/*     */     
/* 373 */     this.labelName.setFont(resourceMap.getFont("labelMinSpeed.font"));
/* 374 */     this.labelName.setText(resourceMap.getString("labelName.text", new Object[0]));
/* 375 */     this.labelName.setName("labelName");
/* 376 */     add(this.labelName, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 35, -1, -1));
/*     */     
/* 378 */     this.labelNumCores.setFont(resourceMap.getFont("labelMinSpeed.font"));
/* 379 */     this.labelNumCores.setText(resourceMap.getString("labelNumCores.text", new Object[0]));
/* 380 */     this.labelNumCores.setName("labelNumCores");
/* 381 */     add(this.labelNumCores, new org.netbeans.lib.awtextra.AbsoluteConstraints(60, 70, -1, -1));
/*     */     
/* 383 */     this.labelSpeed.setFont(resourceMap.getFont("labelMinSpeed.font"));
/* 384 */     this.labelSpeed.setText(resourceMap.getString("labelSpeed.text", new Object[0]));
/* 385 */     this.labelSpeed.setName("labelSpeed");
/* 386 */     add(this.labelSpeed, new org.netbeans.lib.awtextra.AbsoluteConstraints(440, 70, -1, -1));
/*     */     
/* 388 */     this.labelMinSpeed.setFont(resourceMap.getFont("labelMinSpeed.font"));
/* 389 */     this.labelMinSpeed.setText(resourceMap.getString("labelMinSpeed.text", new Object[0]));
/* 390 */     this.labelMinSpeed.setName("labelMinSpeed");
/* 391 */     add(this.labelMinSpeed, new org.netbeans.lib.awtextra.AbsoluteConstraints(400, 105, -1, -1));
/*     */     
/* 393 */     this.labelTick.setFont(resourceMap.getFont("labelMinSpeed.font"));
/* 394 */     this.labelTick.setText(resourceMap.getString("labelTick.text", new Object[0]));
/* 395 */     this.labelTick.setName("labelTick");
/* 396 */     add(this.labelTick, new org.netbeans.lib.awtextra.AbsoluteConstraints(60, 105, -1, -1));
/*     */     
/* 398 */     this.textName.setText(resourceMap.getString("textName.text", new Object[0]));
/* 399 */     this.textName.setName("textName");
/* 400 */     add(this.textName, new org.netbeans.lib.awtextra.AbsoluteConstraints(50, 30, 300, -1));
/*     */     
/* 402 */     this.textReadBandwidth.setText(resourceMap.getString("textReadBandwidth.text", new Object[0]));
/* 403 */     this.textReadBandwidth.setName("textReadBandwidth");
/* 404 */     add(this.textReadBandwidth, new org.netbeans.lib.awtextra.AbsoluteConstraints(220, 65, 140, -1));
/*     */     
/* 406 */     this.textSeekTime.setText(resourceMap.getString("textSeekTime.text", new Object[0]));
/* 407 */     this.textSeekTime.setName("textSeekTime");
/* 408 */     add(this.textSeekTime, new org.netbeans.lib.awtextra.AbsoluteConstraints(535, 65, 140, -1));
/*     */     
/* 410 */     this.textWriteBandwidth.setText(resourceMap.getString("textWriteBandwidth.text", new Object[0]));
/* 411 */     this.textWriteBandwidth.setName("textWriteBandwidth");
/* 412 */     add(this.textWriteBandwidth, new org.netbeans.lib.awtextra.AbsoluteConstraints(220, 100, 140, -1));
/*     */     
/* 414 */     this.scrollTable.setName("scrollTable");
/*     */     
/* 416 */     this.diskTable.setModel(new javax.swing.table.DefaultTableModel(new Object[][] { { null, null, null }, { null, null, null }, { null, null, null }, { null, null, null }, { null, null, null }, { null, null, null }, { null, null, null }, { null, null, null }, { null, null, null }, { null, null, null } }, new String[] { "#", "Name", "Value" })
/*     */     {
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 433 */       Class[] types = { String.class, String.class, String.class };
/*     */       
/*     */ 
/* 436 */       boolean[] canEdit = { false, true, true };
/*     */       
/*     */ 
/*     */       public Class getColumnClass(int columnIndex)
/*     */       {
/* 441 */         return this.types[columnIndex];
/*     */       }
/*     */       
/*     */       public boolean isCellEditable(int rowIndex, int columnIndex) {
/* 445 */         return this.canEdit[columnIndex];
/*     */       }
/* 447 */     });
/* 448 */     this.diskTable.setGridColor(resourceMap.getColor("diskTable.gridColor"));
/* 449 */     this.diskTable.setName("diskTable");
/* 450 */     this.scrollTable.setViewportView(this.diskTable);
/* 451 */     this.diskTable.getColumnModel().getColumn(0).setHeaderValue(resourceMap.getString("diskTable.columnModel.title0", new Object[0]));
/* 452 */     this.diskTable.getColumnModel().getColumn(1).setHeaderValue(resourceMap.getString("diskTable.columnModel.title1", new Object[0]));
/* 453 */     this.diskTable.getColumnModel().getColumn(2).setHeaderValue(resourceMap.getString("diskTable.columnModel.title2", new Object[0]));
/*     */     
/* 455 */     add(this.scrollTable, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 210, 760, 207));
/*     */     
/* 457 */     this.energyLabel.setFont(resourceMap.getFont("labelMinSpeed.font"));
/* 458 */     this.energyLabel.setText(resourceMap.getString("energyLabel.text", new Object[0]));
/* 459 */     this.energyLabel.setName("energyLabel");
/* 460 */     add(this.energyLabel, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 185, -1, -1));
/*     */     
/* 462 */     this.buttonResizeTable.setIcon(resourceMap.getIcon("buttonResizeTable.icon"));
/* 463 */     this.buttonResizeTable.setText(resourceMap.getString("buttonResizeTable.text", new Object[0]));
/* 464 */     this.buttonResizeTable.setToolTipText(resourceMap.getString("buttonResizeTable.toolTipText", new Object[0]));
/* 465 */     this.buttonResizeTable.setName("buttonResizeTable");
/* 466 */     this.buttonResizeTable.addMouseListener(new java.awt.event.MouseAdapter() {
/*     */       public void mouseClicked(java.awt.event.MouseEvent evt) {
/* 468 */         DiskPanel.this.buttonResizeTableMouseClicked(evt);
/*     */       }
/* 470 */     });
/* 471 */     add(this.buttonResizeTable, new org.netbeans.lib.awtextra.AbsoluteConstraints(690, 180, -1, -1));
/*     */     
/* 473 */     this.textNewNumRows.setText(resourceMap.getString("textNewNumRows.text", new Object[0]));
/* 474 */     this.textNewNumRows.setName("textNewNumRows");
/* 475 */     add(this.textNewNumRows, new org.netbeans.lib.awtextra.AbsoluteConstraints(720, 180, 60, -1));
/*     */     
/* 477 */     this.labelResizeTable.setFont(resourceMap.getFont("labelMinSpeed.font"));
/* 478 */     this.labelResizeTable.setForeground(resourceMap.getColor("labelResizeTable.foreground"));
/* 479 */     this.labelResizeTable.setText(resourceMap.getString("labelResizeTable.text", new Object[0]));
/* 480 */     this.labelResizeTable.setName("labelResizeTable");
/* 481 */     add(this.labelResizeTable, new org.netbeans.lib.awtextra.AbsoluteConstraints(590, 183, -1, -1));
/*     */     
/* 483 */     this.textSize.setName("textSize");
/* 484 */     add(this.textSize, new org.netbeans.lib.awtextra.AbsoluteConstraints(535, 100, 140, -1));
/*     */     
/* 486 */     this.textNumDevices.setName("textNumDevices");
/* 487 */     add(this.textNumDevices, new org.netbeans.lib.awtextra.AbsoluteConstraints(220, 135, 140, -1));
/*     */     
/* 489 */     this.labelNumDevices.setFont(resourceMap.getFont("labelMinSpeed.font"));
/* 490 */     this.labelNumDevices.setText(resourceMap.getString("labelNumDevices.text", new Object[0]));
/* 491 */     this.labelNumDevices.setName("labelNumDevices");
/* 492 */     add(this.labelNumDevices, new org.netbeans.lib.awtextra.AbsoluteConstraints(90, 140, -1, -1));
/*     */     
/* 494 */     this.buttonHelp.setIcon(resourceMap.getIcon("buttonHelp.icon"));
/* 495 */     this.buttonHelp.setToolTipText(resourceMap.getString("buttonHelp.toolTipText", new Object[0]));
/* 496 */     this.buttonHelp.setName("buttonHelp");
/* 497 */     this.buttonHelp.addMouseListener(new java.awt.event.MouseAdapter() {
/*     */       public void mouseClicked(java.awt.event.MouseEvent evt) {
/* 499 */         DiskPanel.this.buttonHelpMouseClicked(evt);
/*     */       }
/* 501 */     });
/* 502 */     add(this.buttonHelp, new org.netbeans.lib.awtextra.AbsoluteConstraints(750, 20, -1, -1));
/*     */     
/* 504 */     this.buttonSave.setIcon(resourceMap.getIcon("buttonSave.icon"));
/* 505 */     this.buttonSave.setToolTipText(resourceMap.getString("buttonSave.toolTipText", new Object[0]));
/* 506 */     this.buttonSave.setName("buttonSave");
/* 507 */     this.buttonSave.addMouseListener(new java.awt.event.MouseAdapter() {
/*     */       public void mouseClicked(java.awt.event.MouseEvent evt) {
/* 509 */         DiskPanel.this.buttonSaveMouseClicked(evt);
/*     */       }
/* 511 */     });
/* 512 */     add(this.buttonSave, new org.netbeans.lib.awtextra.AbsoluteConstraints(710, 20, -1, -1));
/*     */     
/* 514 */     this.labelComment.setFont(resourceMap.getFont("labelMinSpeed.font"));
/* 515 */     this.labelComment.setText(resourceMap.getString("labelComment.text", new Object[0]));
/* 516 */     this.labelComment.setName("labelComment");
/* 517 */     add(this.labelComment, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 435, -1, -1));
/*     */     
/* 519 */     this.scrollTextArea.setName("scrollTextArea");
/*     */     
/* 521 */     this.textAreaComment.setColumns(20);
/* 522 */     this.textAreaComment.setRows(5);
/* 523 */     this.textAreaComment.setName("textAreaComment");
/* 524 */     this.scrollTextArea.setViewportView(this.textAreaComment);
/*     */     
/* 526 */     add(this.scrollTextArea, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 460, 760, 70));
/*     */     
/* 528 */     this.buttonClear.setIcon(resourceMap.getIcon("buttonClear.icon"));
/* 529 */     this.buttonClear.setToolTipText(resourceMap.getString("buttonClear.toolTipText", new Object[0]));
/* 530 */     this.buttonClear.setName("buttonClear");
/* 531 */     this.buttonClear.addMouseListener(new java.awt.event.MouseAdapter() {
/*     */       public void mouseClicked(java.awt.event.MouseEvent evt) {
/* 533 */         DiskPanel.this.buttonClearMouseClicked(evt);
/*     */       }
/* 535 */     });
/* 536 */     add(this.buttonClear, new org.netbeans.lib.awtextra.AbsoluteConstraints(670, 20, -1, -1));
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private void buttonResizeTableMouseClicked(java.awt.event.MouseEvent evt)
/*     */   {
/*     */     try
/*     */     {
/* 555 */       int newNumRows = Integer.parseInt(this.textNewNumRows.getText());
/*     */       
/*     */ 
/* 558 */       if (this.diskTable.getModel().getRowCount() == newNumRows)
/*     */       {
/* 560 */         javax.swing.JOptionPane.showMessageDialog(new javax.swing.JFrame(), "New number of rows is the same as the curent table", "Warning!", 2);
/*     */       }
/*     */       
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 567 */       if (newNumRows < this.diskTable.getModel().getRowCount())
/*     */       {
/*     */ 
/* 570 */         int response = javax.swing.JOptionPane.showConfirmDialog(null, "Are you sure of resizing this table?", "Confirm resize table", 2, 3);
/*     */         
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 576 */         if (response == 0)
/*     */         {
/*     */ 
/* 579 */           javax.swing.table.DefaultTableModel newModel = (javax.swing.table.DefaultTableModel)this.diskTable.getModel();
/* 580 */           int oldNumRows = this.diskTable.getModel().getRowCount();
/*     */           
/*     */ 
/* 583 */           for (int i = 0; i < oldNumRows - newNumRows; i++) {
/* 584 */             System.out.println(newModel.getRowCount());
/* 585 */             newModel.removeRow(newModel.getRowCount() - 1);
/*     */           }
/*     */           
/* 588 */           this.diskTable.setModel(newModel);
/*     */ 
/*     */         }
/*     */         
/*     */ 
/*     */       }
/*     */       else
/*     */       {
/*     */ 
/* 597 */         int response = javax.swing.JOptionPane.showConfirmDialog(null, "Are you sure of resizing this table?", "Confirm resize table", 2, 3);
/*     */         
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 603 */         if (response == 0)
/*     */         {
/*     */ 
/* 606 */           javax.swing.table.DefaultTableModel newModel = (javax.swing.table.DefaultTableModel)this.diskTable.getModel();
/* 607 */           int oldNumRows = this.diskTable.getModel().getRowCount();
/*     */           
/*     */ 
/* 610 */           for (int i = 0; i < newNumRows - oldNumRows; i++) {
/* 611 */             newModel.addRow(new Object[] { Integer.toString(oldNumRows + i), "", "" });
/*     */           }
/*     */           
/* 614 */           this.diskTable.setModel(newModel);
/*     */         }
/*     */       }
/*     */     }
/*     */     catch (Exception e)
/*     */     {
/* 620 */       javax.swing.JOptionPane.showMessageDialog(new javax.swing.JFrame(), "Wrong number, please enter a correct number value", "Error!", 2);
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private void buttonHelpMouseClicked(java.awt.event.MouseEvent evt)
/*     */   {
/* 634 */     icancloudgui.Utils.Utils.showHelp(this);
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private void buttonSaveMouseClicked(java.awt.event.MouseEvent evt)
/*     */   {
/* 647 */     boolean allOK = writeDiskToFile();
/*     */     
/*     */ 
/* 650 */     if (allOK) {
/* 651 */       this.mainFrame.updateTree();
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */   private void buttonClearMouseClicked(java.awt.event.MouseEvent evt)
/*     */   {
/* 659 */     int response = javax.swing.JOptionPane.showConfirmDialog(null, "Clear current Disk configuration?", "Confirm to clear this form", 2, 3);
/*     */     
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 666 */     if (response == 0) {
/* 667 */       clear();
/*     */     }
/*     */   }
/*     */ }


/* Location:              /home/chverma/Descargas/iCanCloudGUI/iCanCloudGUI.jar!/icancloudgui/TabbedPanels/DiskPanel.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */