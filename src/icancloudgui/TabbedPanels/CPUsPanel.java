/*     */ package icancloudgui.TabbedPanels;
/*     */ 
/*     */ import javax.swing.JLabel;
/*     */ import javax.swing.JTextField;
/*     */ 
/*     */ public class CPUsPanel extends javax.swing.JPanel
/*     */ {
/*     */   icancloudgui.ICanCloudGUIView mainFrame;
/*     */   private JLabel buttonClear;
/*     */   private JLabel buttonHelp;
/*     */   private JLabel buttonResizeTable;
/*     */   private JLabel buttonSave;
/*     */   private javax.swing.JCheckBox checkBoxIndependentCores;
/*     */   private javax.swing.JTable cpuTable;
/*     */   private JLabel labelComment;
/*     */   private JLabel labelEnergyParameters;
/*     */   private JLabel labelMinSpeed;
/*     */   private JLabel labelName;
/*     */   private JLabel labelNumCores;
/*     */   private JLabel labelResizeTable;
/*     */   private JLabel labelSpeed;
/*     */   private JLabel labelTick;
/*     */   private javax.swing.JScrollPane scrollTable;
/*     */   private javax.swing.JScrollPane scrollTextArea;
/*     */   private javax.swing.JTextArea textAreaComment;
/*     */   private JTextField textMinSpeed;
/*     */   private JTextField textName;
/*     */   private JTextField textNewNumRows;
/*     */   private JTextField textNumCores;
/*     */   private JTextField textSpeed;
/*     */   private JTextField textTick;
/*     */   
/*     */   public CPUsPanel(icancloudgui.ICanCloudGUIView mainFrame)
/*     */   {
/*  35 */     initComponents();
/*  36 */     icancloudgui.Utils.Utils.initEnergyTable(this.cpuTable);
/*  37 */     this.mainFrame = mainFrame;
/*     */     
/*     */ 
/*  40 */     this.labelResizeTable.setVisible(false);
/*  41 */     this.buttonResizeTable.setVisible(false);
/*  42 */     this.textNewNumRows.setVisible(false);
/*  43 */     initDefaultTableValues();
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public boolean checkValues()
/*     */   {
/*  57 */     boolean allOK = true;
/*     */     
/*     */ 
/*     */ 
/*  61 */     if (!icancloudgui.Utils.Utils.checkName(this.textName.getText()))
/*     */     {
/*  63 */       allOK = false;
/*  64 */       icancloudgui.Utils.Utils.showErrorMessage("Invalid name for this CPU.\nA name must start with a letter and can contain alphanumeric characters, '-' and '_'", "Wrong name!");
/*     */     }
/*     */     
/*     */ 
/*     */ 
/*     */     try
/*     */     {
/*  71 */       if (allOK) {
/*  72 */         Integer.parseInt(this.textNumCores.getText());
/*     */       }
/*     */     }
/*     */     catch (Exception e) {
/*  76 */       allOK = false;
/*  77 */       icancloudgui.Utils.Utils.showErrorMessage("Wrong parameter for number of cores.\nThis parameter must be an integer.\n", "Wrong parameter!");
/*     */     }
/*     */     
/*     */ 
/*     */ 
/*     */     try
/*     */     {
/*  84 */       if (allOK) {
/*  85 */         Double.parseDouble(this.textTick.getText());
/*     */       }
/*     */     }
/*     */     catch (Exception e) {
/*  89 */       allOK = false;
/*  90 */       icancloudgui.Utils.Utils.showErrorMessage("Wrong parameter for tick.\nThis parameter must be a double.\n", "Wrong parameter!");
/*     */     }
/*     */     
/*     */ 
/*     */ 
/*     */     try
/*     */     {
/*  97 */       if (allOK) {
/*  98 */         Integer.parseInt(this.textSpeed.getText());
/*     */       }
/*     */     }
/*     */     catch (Exception e) {
/* 102 */       allOK = false;
/* 103 */       icancloudgui.Utils.Utils.showErrorMessage("Wrong parameter for speed of the CPU.\nThis parameter must be an integer.\n", "Wrong parameter!");
/*     */     }
/*     */     
/*     */ 
/*     */ 
/*     */ 
/*     */     try
/*     */     {
/* 111 */       if (allOK) {
/* 112 */         Integer.parseInt(this.textMinSpeed.getText());
/*     */       }
/*     */     }
/*     */     catch (Exception e) {
/* 116 */       allOK = false;
/* 117 */       icancloudgui.Utils.Utils.showErrorMessage("Wrong parameter for minimum speed of the CPU.\nThis parameter must be an integer.\n", "Wrong parameter!");
/*     */     }
/*     */     
/*     */ 
/*     */ 
/*     */ 
/* 123 */     if ((allOK) && (!icancloudgui.Utils.Utils.checkEnergyTable((icancloudgui.Tables.EnergyTableModel)this.cpuTable.getModel()))) {
/* 124 */       allOK = false;
/*     */       
/* 126 */       icancloudgui.Utils.Utils.showErrorMessage("Table of energy states contains wrong values.\nEnergy state must be a String and its value a double.\n", "Wrong parameter!");
/*     */     }
/*     */     
/*     */ 
/*     */ 
/*     */ 
/* 132 */     return allOK;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public icancloudgui.TabbedPanels.dataClasses.CPU panelToCPUobject()
/*     */   {
/* 146 */     icancloudgui.TabbedPanels.dataClasses.CPU currentCPU = new icancloudgui.TabbedPanels.dataClasses.CPU();
/* 147 */     java.util.LinkedList<icancloudgui.TabbedPanels.dataClasses.EnergyEntry> list = new java.util.LinkedList();
/*     */     
/*     */ 
/* 150 */     currentCPU.setName(this.textName.getText());
/* 151 */     currentCPU.setNumCores(Integer.parseInt(this.textNumCores.getText()));
/* 152 */     currentCPU.setTick(Double.parseDouble(this.textTick.getText()));
/* 153 */     currentCPU.setSpeed(Integer.parseInt(this.textSpeed.getText()));
/* 154 */     currentCPU.setMinSpeed(Integer.parseInt(this.textMinSpeed.getText()));
/* 155 */     currentCPU.setIndependentCores(this.checkBoxIndependentCores.isSelected());
/* 156 */     currentCPU.setComment(this.textAreaComment.getText());
/* 157 */     icancloudgui.Utils.Utils.energyTableModelToList((icancloudgui.Tables.EnergyTableModel)this.cpuTable.getModel(), list);
/* 158 */     currentCPU.setEnergyData(list);
/*     */     
/* 160 */     return currentCPU;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   public void clear()
/*     */   {
/* 168 */     this.textName.setText("");
/* 169 */     this.textAreaComment.setText("");
/* 170 */     this.textNumCores.setText("");
/* 171 */     this.textTick.setText("");
/* 172 */     this.textSpeed.setText("");
/* 173 */     this.textMinSpeed.setText("");
/* 174 */     this.checkBoxIndependentCores.setSelected(false);
/* 175 */     this.textAreaComment.setText("");
/* 176 */     icancloudgui.Utils.Utils.initEnergyTable(this.cpuTable);
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private void loadCpuObjectInPanel(icancloudgui.TabbedPanels.dataClasses.CPU newCPU)
/*     */   {
/* 189 */     this.textName.setText(newCPU.getName());
/* 190 */     this.textNumCores.setText(Integer.toString(newCPU.getNumCores()));
/* 191 */     this.textTick.setText(Double.toString(newCPU.getTick()));
/* 192 */     this.textSpeed.setText(Integer.toString(newCPU.getSpeed()));
/* 193 */     this.textMinSpeed.setText(Integer.toString(newCPU.getMinSpeed()));
/* 194 */     this.checkBoxIndependentCores.setSelected(newCPU.isIndependentCores());
/* 195 */     this.textAreaComment.setText(newCPU.getComment());
/*     */     
/*     */ 
/* 198 */     icancloudgui.Tables.EnergyTableModel model = new icancloudgui.Tables.EnergyTableModel();
/* 199 */     icancloudgui.Utils.Utils.energyListToTable(newCPU.getEnergyData(), model);
/* 200 */     this.cpuTable.setModel(model);
/* 201 */     this.cpuTable.updateUI();
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void loadCpuInPanel(String cpuName)
/*     */   {
/* 214 */     icancloudgui.TabbedPanels.dataClasses.CPU loadedCPU = icancloudgui.Utils.Utils.loadCPUobject(cpuName);
/*     */     
/*     */ 
/* 217 */     if (loadedCPU != null) {
/* 218 */       loadCpuObjectInPanel(loadedCPU);
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private boolean writeCPUtoFile()
/*     */   {
/* 238 */     boolean allOK = checkValues();
/*     */     
/*     */ 
/* 241 */     if (allOK)
/*     */     {
/*     */ 
/* 244 */       icancloudgui.TabbedPanels.dataClasses.CPU currentCPU = panelToCPUobject();
/*     */       
/*     */ 
/* 247 */       String newCpuFile = icancloudgui.Utils.Configuration.REPOSITORY_CPUS_FOLDER + java.io.File.separatorChar + currentCPU.getName();
/*     */       
/*     */ 
/*     */       try
/*     */       {
/* 252 */         if (new java.io.File(newCpuFile).exists())
/*     */         {
/*     */ 
/* 255 */           int response = javax.swing.JOptionPane.showConfirmDialog(null, "Overwrite existing CPU?", "Confirm Overwrite", 2, 3);
/*     */           
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 262 */           if (response == 0)
/*     */           {
/*     */ 
/* 265 */             java.io.FileOutputStream fout = new java.io.FileOutputStream(newCpuFile);
/* 266 */             java.io.ObjectOutputStream oos = new java.io.ObjectOutputStream(fout);
/* 267 */             oos.writeObject(currentCPU);
/* 268 */             oos.close();
/* 269 */             allOK = true;
/*     */           }
/*     */           
/*     */ 
/*     */         }
/*     */         else
/*     */         {
/*     */ 
/* 277 */           java.io.FileOutputStream fout = new java.io.FileOutputStream(newCpuFile);
/* 278 */           java.io.ObjectOutputStream oos = new java.io.ObjectOutputStream(fout);
/* 279 */           oos.writeObject(currentCPU);
/* 280 */           oos.close();
/* 281 */           allOK = true;
/*     */         }
/*     */       }
/*     */       catch (Exception e) {
/* 285 */         e.printStackTrace();
/*     */       }
/*     */     }
/*     */     
/* 289 */     return allOK;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private void initDefaultTableValues()
/*     */   {
/* 303 */     icancloudgui.Tables.EnergyTableModel newModel = new icancloudgui.Tables.EnergyTableModel();
/* 304 */     newModel.setColumnCount(3);
/* 305 */     newModel.setColumnIdentifiers(icancloudgui.Utils.Utils.columnNamesEnergyTable);
/*     */     
/*     */ 
/* 308 */     newModel.addRow(new Object[] { Integer.toString(0), "W", "" });
/* 309 */     newModel.addRow(new Object[] { Integer.toString(1), "off", "" });
/* 310 */     newModel.addRow(new Object[] { Integer.toString(2), "c0_p11", "" });
/* 311 */     newModel.addRow(new Object[] { Integer.toString(3), "c0_p10", "" });
/* 312 */     newModel.addRow(new Object[] { Integer.toString(4), "c0_p9", "" });
/* 313 */     newModel.addRow(new Object[] { Integer.toString(5), "c0_p8", "" });
/* 314 */     newModel.addRow(new Object[] { Integer.toString(6), "c0_p7", "" });
/* 315 */     newModel.addRow(new Object[] { Integer.toString(7), "c0_p6", "" });
/* 316 */     newModel.addRow(new Object[] { Integer.toString(8), "c0_p5", "" });
/* 317 */     newModel.addRow(new Object[] { Integer.toString(9), "c0_p4", "" });
/* 318 */     newModel.addRow(new Object[] { Integer.toString(10), "c0_p3", "" });
/* 319 */     newModel.addRow(new Object[] { Integer.toString(11), "c0_p2", "" });
/* 320 */     newModel.addRow(new Object[] { Integer.toString(12), "c0_p1", "" });
/* 321 */     newModel.addRow(new Object[] { Integer.toString(13), "c0_p0", "" });
/* 322 */     newModel.addRow(new Object[] { Integer.toString(14), "c0_operating_state", "" });
/* 323 */     newModel.addRow(new Object[] { Integer.toString(15), "c1_halt", "" });
/* 324 */     newModel.addRow(new Object[] { Integer.toString(16), "c2_stop_grant", "" });
/* 325 */     newModel.addRow(new Object[] { Integer.toString(17), "c3_sleep", "" });
/* 326 */     newModel.addRow(new Object[] { Integer.toString(17), "c4_deeper_sleep", "" });
/* 327 */     newModel.addRow(new Object[] { Integer.toString(17), "c5_enhanced_deeper_sleep", "" });
/* 328 */     newModel.addRow(new Object[] { Integer.toString(17), "c6_deep_power_down", "" });
/*     */     
/* 330 */     this.cpuTable.setModel(newModel);
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private void initComponents()
/*     */   {
/* 342 */     this.labelName = new JLabel();
/* 343 */     this.labelNumCores = new JLabel();
/* 344 */     this.labelSpeed = new JLabel();
/* 345 */     this.labelMinSpeed = new JLabel();
/* 346 */     this.labelTick = new JLabel();
/* 347 */     this.textName = new JTextField();
/* 348 */     this.textNumCores = new JTextField();
/* 349 */     this.textSpeed = new JTextField();
/* 350 */     this.textTick = new JTextField();
/* 351 */     this.textMinSpeed = new JTextField();
/* 352 */     this.checkBoxIndependentCores = new javax.swing.JCheckBox();
/* 353 */     this.scrollTable = new javax.swing.JScrollPane();
/* 354 */     this.cpuTable = new javax.swing.JTable();
/* 355 */     this.labelEnergyParameters = new JLabel();
/* 356 */     this.buttonResizeTable = new JLabel();
/* 357 */     this.textNewNumRows = new JTextField();
/* 358 */     this.labelResizeTable = new JLabel();
/* 359 */     this.buttonSave = new JLabel();
/* 360 */     this.buttonHelp = new JLabel();
/* 361 */     this.scrollTextArea = new javax.swing.JScrollPane();
/* 362 */     this.textAreaComment = new javax.swing.JTextArea();
/* 363 */     this.labelComment = new JLabel();
/* 364 */     this.buttonClear = new JLabel();
/*     */     
/* 366 */     org.jdesktop.application.ResourceMap resourceMap = ((icancloudgui.ICanCloudGUIApp)org.jdesktop.application.Application.getInstance(icancloudgui.ICanCloudGUIApp.class)).getContext().getResourceMap(CPUsPanel.class);
/* 367 */     setBackground(resourceMap.getColor("Form.background"));
/* 368 */     setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
/* 369 */     setMinimumSize(new java.awt.Dimension(500, 510));
/* 370 */     setName("Form");
/* 371 */     setPreferredSize(new java.awt.Dimension(500, 641));
/* 372 */     setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());
/*     */     
/* 374 */     this.labelName.setFont(resourceMap.getFont("textMinSpeed.font"));
/* 375 */     this.labelName.setText(resourceMap.getString("labelName.text", new Object[0]));
/* 376 */     this.labelName.setName("labelName");
/* 377 */     add(this.labelName, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 35, -1, -1));
/*     */     
/* 379 */     this.labelNumCores.setFont(resourceMap.getFont("textMinSpeed.font"));
/* 380 */     this.labelNumCores.setText(resourceMap.getString("labelNumCores.text", new Object[0]));
/* 381 */     this.labelNumCores.setName("labelNumCores");
/* 382 */     add(this.labelNumCores, new org.netbeans.lib.awtextra.AbsoluteConstraints(60, 70, -1, -1));
/*     */     
/* 384 */     this.labelSpeed.setFont(resourceMap.getFont("textMinSpeed.font"));
/* 385 */     this.labelSpeed.setText(resourceMap.getString("labelSpeed.text", new Object[0]));
/* 386 */     this.labelSpeed.setName("labelSpeed");
/* 387 */     add(this.labelSpeed, new org.netbeans.lib.awtextra.AbsoluteConstraints(70, 105, -1, -1));
/*     */     
/* 389 */     this.labelMinSpeed.setFont(resourceMap.getFont("textMinSpeed.font"));
/* 390 */     this.labelMinSpeed.setText(resourceMap.getString("labelMinSpeed.text", new Object[0]));
/* 391 */     this.labelMinSpeed.setName("labelMinSpeed");
/* 392 */     add(this.labelMinSpeed, new org.netbeans.lib.awtextra.AbsoluteConstraints(360, 105, -1, -1));
/*     */     
/* 394 */     this.labelTick.setFont(resourceMap.getFont("textMinSpeed.font"));
/* 395 */     this.labelTick.setText(resourceMap.getString("labelTick.text", new Object[0]));
/* 396 */     this.labelTick.setName("labelTick");
/* 397 */     add(this.labelTick, new org.netbeans.lib.awtextra.AbsoluteConstraints(410, 70, -1, -1));
/*     */     
/* 399 */     this.textName.setFont(resourceMap.getFont("textMinSpeed.font"));
/* 400 */     this.textName.setText(resourceMap.getString("textName.text", new Object[0]));
/* 401 */     this.textName.setName("textName");
/* 402 */     add(this.textName, new org.netbeans.lib.awtextra.AbsoluteConstraints(50, 30, 270, 28));
/*     */     
/* 404 */     this.textNumCores.setFont(resourceMap.getFont("textMinSpeed.font"));
/* 405 */     this.textNumCores.setText(resourceMap.getString("textNumCores.text", new Object[0]));
/* 406 */     this.textNumCores.setName("textNumCores");
/* 407 */     add(this.textNumCores, new org.netbeans.lib.awtextra.AbsoluteConstraints(180, 65, 140, 28));
/*     */     
/* 409 */     this.textSpeed.setFont(resourceMap.getFont("textMinSpeed.font"));
/* 410 */     this.textSpeed.setText(resourceMap.getString("textSpeed.text", new Object[0]));
/* 411 */     this.textSpeed.setName("textSpeed");
/* 412 */     add(this.textSpeed, new org.netbeans.lib.awtextra.AbsoluteConstraints(180, 100, 140, 28));
/*     */     
/* 414 */     this.textTick.setFont(resourceMap.getFont("textMinSpeed.font"));
/* 415 */     this.textTick.setText(resourceMap.getString("textTick.text", new Object[0]));
/* 416 */     this.textTick.setName("textTick");
/* 417 */     add(this.textTick, new org.netbeans.lib.awtextra.AbsoluteConstraints(535, 65, 140, -1));
/*     */     
/* 419 */     this.textMinSpeed.setFont(resourceMap.getFont("textMinSpeed.font"));
/* 420 */     this.textMinSpeed.setText(resourceMap.getString("textMinSpeed.text", new Object[0]));
/* 421 */     this.textMinSpeed.setName("textMinSpeed");
/* 422 */     add(this.textMinSpeed, new org.netbeans.lib.awtextra.AbsoluteConstraints(535, 100, 140, -1));
/*     */     
/* 424 */     this.checkBoxIndependentCores.setFont(resourceMap.getFont("textMinSpeed.font"));
/* 425 */     this.checkBoxIndependentCores.setText(resourceMap.getString("checkBoxIndependentCores.text", new Object[0]));
/* 426 */     this.checkBoxIndependentCores.setHorizontalAlignment(2);
/* 427 */     this.checkBoxIndependentCores.setHorizontalTextPosition(2);
/* 428 */     this.checkBoxIndependentCores.setName("checkBoxIndependentCores");
/* 429 */     add(this.checkBoxIndependentCores, new org.netbeans.lib.awtextra.AbsoluteConstraints(370, 35, -1, -1));
/*     */     
/* 431 */     this.scrollTable.setName("scrollTable");
/*     */     
/* 433 */     this.cpuTable.setFont(resourceMap.getFont("textMinSpeed.font"));
/* 434 */     this.cpuTable.setModel(new javax.swing.table.DefaultTableModel(new Object[][] { { null, null, null }, { null, null, null }, { null, null, null }, { null, null, null }, { null, null, null }, { null, null, null }, { null, null, null }, { null, null, null }, { null, null, null }, { null, null, null } }, new String[] { "#", "Name", "Value" })
/*     */     {
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 451 */       Class[] types = { String.class, String.class, String.class };
/*     */       
/*     */ 
/* 454 */       boolean[] canEdit = { false, true, true };
/*     */       
/*     */ 
/*     */       public Class getColumnClass(int columnIndex)
/*     */       {
/* 459 */         return this.types[columnIndex];
/*     */       }
/*     */       
/*     */       public boolean isCellEditable(int rowIndex, int columnIndex) {
/* 463 */         return this.canEdit[columnIndex];
/*     */       }
/* 465 */     });
/* 466 */     this.cpuTable.setColumnSelectionAllowed(true);
/* 467 */     this.cpuTable.setGridColor(resourceMap.getColor("cpuTable.gridColor"));
/* 468 */     this.cpuTable.setName("cpuTable");
/* 469 */     this.cpuTable.setSelectionForeground(resourceMap.getColor("cpuTable.selectionForeground"));
/* 470 */     this.cpuTable.setShowGrid(true);
/* 471 */     this.scrollTable.setViewportView(this.cpuTable);
/* 472 */     this.cpuTable.getColumnModel().getSelectionModel().setSelectionMode(0);
/* 473 */     this.cpuTable.getColumnModel().getColumn(0).setResizable(false);
/* 474 */     this.cpuTable.getColumnModel().getColumn(0).setHeaderValue(resourceMap.getString("cpuTable.columnModel.title0", new Object[0]));
/* 475 */     this.cpuTable.getColumnModel().getColumn(1).setResizable(false);
/* 476 */     this.cpuTable.getColumnModel().getColumn(1).setHeaderValue(resourceMap.getString("cpuTable.columnModel.title1", new Object[0]));
/* 477 */     this.cpuTable.getColumnModel().getColumn(2).setResizable(false);
/* 478 */     this.cpuTable.getColumnModel().getColumn(2).setHeaderValue(resourceMap.getString("cpuTable.columnModel.title2", new Object[0]));
/*     */     
/* 480 */     add(this.scrollTable, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 190, 760, 180));
/*     */     
/* 482 */     this.labelEnergyParameters.setFont(resourceMap.getFont("checkBoxIndependentCores.font"));
/* 483 */     this.labelEnergyParameters.setText(resourceMap.getString("labelEnergyParameters.text", new Object[0]));
/* 484 */     this.labelEnergyParameters.setName("labelEnergyParameters");
/* 485 */     add(this.labelEnergyParameters, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 165, -1, -1));
/*     */     
/* 487 */     this.buttonResizeTable.setIcon(resourceMap.getIcon("buttonResizeTable.icon"));
/* 488 */     this.buttonResizeTable.setText(resourceMap.getString("buttonResizeTable.text", new Object[0]));
/* 489 */     this.buttonResizeTable.setToolTipText(resourceMap.getString("buttonResizeTable.toolTipText", new Object[0]));
/* 490 */     this.buttonResizeTable.setName("buttonResizeTable");
/* 491 */     this.buttonResizeTable.addMouseListener(new java.awt.event.MouseAdapter() {
/*     */       public void mouseClicked(java.awt.event.MouseEvent evt) {
/* 493 */         CPUsPanel.this.buttonResizeTableMouseClicked(evt);
/*     */       }
/* 495 */     });
/* 496 */     add(this.buttonResizeTable, new org.netbeans.lib.awtextra.AbsoluteConstraints(690, 162, -1, -1));
/*     */     
/* 498 */     this.textNewNumRows.setFont(resourceMap.getFont("textMinSpeed.font"));
/* 499 */     this.textNewNumRows.setText(resourceMap.getString("textNewNumRows.text", new Object[0]));
/* 500 */     this.textNewNumRows.setName("textNewNumRows");
/* 501 */     add(this.textNewNumRows, new org.netbeans.lib.awtextra.AbsoluteConstraints(722, 160, 60, -1));
/*     */     
/* 503 */     this.labelResizeTable.setFont(resourceMap.getFont("checkBoxIndependentCores.font"));
/* 504 */     this.labelResizeTable.setForeground(resourceMap.getColor("labelResizeTable.foreground"));
/* 505 */     this.labelResizeTable.setText(resourceMap.getString("labelResizeTable.text", new Object[0]));
/* 506 */     this.labelResizeTable.setName("labelResizeTable");
/* 507 */     add(this.labelResizeTable, new org.netbeans.lib.awtextra.AbsoluteConstraints(590, 165, -1, -1));
/*     */     
/* 509 */     this.buttonSave.setIcon(resourceMap.getIcon("buttonSave.icon"));
/* 510 */     this.buttonSave.setText(resourceMap.getString("buttonSave.text", new Object[0]));
/* 511 */     this.buttonSave.setToolTipText(resourceMap.getString("buttonSave.toolTipText", new Object[0]));
/* 512 */     this.buttonSave.setName("buttonSave");
/* 513 */     this.buttonSave.addMouseListener(new java.awt.event.MouseAdapter() {
/*     */       public void mouseClicked(java.awt.event.MouseEvent evt) {
/* 515 */         CPUsPanel.this.buttonSaveMouseClicked(evt);
/*     */       }
/* 517 */     });
/* 518 */     add(this.buttonSave, new org.netbeans.lib.awtextra.AbsoluteConstraints(710, 20, -1, -1));
/*     */     
/* 520 */     this.buttonHelp.setIcon(resourceMap.getIcon("buttonHelp.icon"));
/* 521 */     this.buttonHelp.setText(resourceMap.getString("buttonHelp.text", new Object[0]));
/* 522 */     this.buttonHelp.setToolTipText(resourceMap.getString("buttonHelp.toolTipText", new Object[0]));
/* 523 */     this.buttonHelp.setName("buttonHelp");
/* 524 */     this.buttonHelp.addMouseListener(new java.awt.event.MouseAdapter() {
/*     */       public void mouseClicked(java.awt.event.MouseEvent evt) {
/* 526 */         CPUsPanel.this.buttonHelpMouseClicked(evt);
/*     */       }
/* 528 */     });
/* 529 */     add(this.buttonHelp, new org.netbeans.lib.awtextra.AbsoluteConstraints(750, 20, -1, -1));
/*     */     
/* 531 */     this.scrollTextArea.setName("scrollTextArea");
/*     */     
/* 533 */     this.textAreaComment.setColumns(20);
/* 534 */     this.textAreaComment.setFont(resourceMap.getFont("textMinSpeed.font"));
/* 535 */     this.textAreaComment.setRows(5);
/* 536 */     this.textAreaComment.setName("textAreaComment");
/* 537 */     this.scrollTextArea.setViewportView(this.textAreaComment);
/*     */     
/* 539 */     add(this.scrollTextArea, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 430, 760, 80));
/*     */     
/* 541 */     this.labelComment.setFont(resourceMap.getFont("checkBoxIndependentCores.font"));
/* 542 */     this.labelComment.setText(resourceMap.getString("labelComment.text", new Object[0]));
/* 543 */     this.labelComment.setName("labelComment");
/* 544 */     add(this.labelComment, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 405, -1, -1));
/*     */     
/* 546 */     this.buttonClear.setIcon(resourceMap.getIcon("buttonClear.icon"));
/* 547 */     this.buttonClear.setToolTipText(resourceMap.getString("buttonClear.toolTipText", new Object[0]));
/* 548 */     this.buttonClear.setName("buttonClear");
/* 549 */     this.buttonClear.addMouseListener(new java.awt.event.MouseAdapter() {
/*     */       public void mouseClicked(java.awt.event.MouseEvent evt) {
/* 551 */         CPUsPanel.this.buttonClearMouseClicked(evt);
/*     */       }
/* 553 */     });
/* 554 */     add(this.buttonClear, new org.netbeans.lib.awtextra.AbsoluteConstraints(670, 20, -1, -1));
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private void buttonResizeTableMouseClicked(java.awt.event.MouseEvent evt)
/*     */   {
/*     */     try
/*     */     {
/* 573 */       int newNumRows = Integer.parseInt(this.textNewNumRows.getText());
/*     */       
/*     */ 
/* 576 */       if (this.cpuTable.getModel().getRowCount() == newNumRows)
/*     */       {
/* 578 */         javax.swing.JOptionPane.showMessageDialog(new javax.swing.JFrame(), "New number of rows is the same as the curent table", "Warning!", 2);
/*     */       }
/*     */       
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 585 */       if (newNumRows < this.cpuTable.getModel().getRowCount())
/*     */       {
/*     */ 
/* 588 */         int response = javax.swing.JOptionPane.showConfirmDialog(null, "Are you sure of resizing this table?", "Confirm resize table", 2, 3);
/*     */         
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 594 */         if (response == 0)
/*     */         {
/*     */ 
/* 597 */           javax.swing.table.DefaultTableModel newModel = (javax.swing.table.DefaultTableModel)this.cpuTable.getModel();
/* 598 */           int oldNumRows = this.cpuTable.getModel().getRowCount();
/*     */           
/*     */ 
/* 601 */           for (int i = 0; i < oldNumRows - newNumRows; i++) {
/* 602 */             System.out.println(newModel.getRowCount());
/* 603 */             newModel.removeRow(newModel.getRowCount() - 1);
/*     */           }
/*     */           
/* 606 */           this.cpuTable.setModel(newModel);
/*     */ 
/*     */         }
/*     */         
/*     */ 
/*     */       }
/*     */       else
/*     */       {
/*     */ 
/* 615 */         int response = javax.swing.JOptionPane.showConfirmDialog(null, "Are you sure of resizing this table?", "Confirm resize table", 2, 3);
/*     */         
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 621 */         if (response == 0)
/*     */         {
/*     */ 
/* 624 */           javax.swing.table.DefaultTableModel newModel = (javax.swing.table.DefaultTableModel)this.cpuTable.getModel();
/* 625 */           int oldNumRows = this.cpuTable.getModel().getRowCount();
/*     */           
/*     */ 
/* 628 */           for (int i = 0; i < newNumRows - oldNumRows; i++) {
/* 629 */             newModel.addRow(new Object[] { Integer.toString(oldNumRows + i), "", "" });
/*     */           }
/*     */           
/* 632 */           this.cpuTable.setModel(newModel);
/*     */         }
/*     */       }
/*     */     }
/*     */     catch (Exception e)
/*     */     {
/* 638 */       javax.swing.JOptionPane.showMessageDialog(new javax.swing.JFrame(), "Wrong number, please enter a correct number value", "Error!", 2);
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private void buttonSaveMouseClicked(java.awt.event.MouseEvent evt)
/*     */   {
/* 655 */     boolean allOK = writeCPUtoFile();
/*     */     
/*     */ 
/* 658 */     if (allOK) {
/* 659 */       this.mainFrame.updateTree();
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private void buttonHelpMouseClicked(java.awt.event.MouseEvent evt)
/*     */   {
/* 669 */     icancloudgui.Utils.Utils.showHelp(this);
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private void buttonClearMouseClicked(java.awt.event.MouseEvent evt)
/*     */   {
/* 678 */     int response = javax.swing.JOptionPane.showConfirmDialog(null, "Clear current CPU configuration?", "Confirm to clear this form", 2, 3);
/*     */     
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 685 */     if (response == 0) {
/* 686 */       clear();
/*     */     }
/*     */   }
/*     */ }


/* Location:              /home/chverma/Descargas/iCanCloudGUI/iCanCloudGUI.jar!/icancloudgui/TabbedPanels/CPUsPanel.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */