/*     */ package icancloudgui.TabbedPanels;
/*     */ 
/*     */ import icancloudgui.ICanCloudGUIView;
/*     */ import icancloudgui.TabbedPanels.dataClasses.Switch;
/*     */ import icancloudgui.Tables.EnergyTableModel;
/*     */ import icancloudgui.Utils.Utils;
/*     */ import java.awt.event.MouseEvent;
/*     */ import java.io.FileOutputStream;
/*     */ import java.io.ObjectOutputStream;
/*     */ import javax.swing.JCheckBox;
/*     */ import javax.swing.JComboBox;
/*     */ import javax.swing.JLabel;
/*     */ import javax.swing.JOptionPane;
/*     */ import javax.swing.JScrollPane;
/*     */ import javax.swing.JTable;
/*     */ import javax.swing.JTextArea;
/*     */ import javax.swing.JTextField;
/*     */ import javax.swing.table.DefaultTableModel;
/*     */ import javax.swing.table.TableModel;
/*     */ import org.jdesktop.application.ResourceMap;
/*     */ import org.netbeans.lib.awtextra.AbsoluteConstraints;
/*     */ 
/*     */ public class SwitchPanel extends javax.swing.JPanel
/*     */ {
/*     */   ICanCloudGUIView mainFrame;
/*  26 */   private final String QUEUE_QOS = "EtherQoSQueue";
/*  27 */   private final String QUEUE_DROP = "DropTailQueue";
/*  28 */   private final String RELAY_UNIT_NP = "MACRelayUnitNP";
/*  29 */   private final String RELAY_UNIT_PP = "MACRelayUnitPP";
/*  30 */   private final String MTU_DEFAULT = "1500";
/*     */   
/*     */   private JLabel buttonClear;
/*     */   private JLabel buttonHelp;
/*     */   private JLabel buttonResizeTable;
/*     */   private JLabel buttonSave;
/*     */   private JComboBox comboQueue;
/*     */   
/*     */   public SwitchPanel(ICanCloudGUIView mainFrame)
/*     */   {
/*  40 */     initComponents();
/*  41 */     Utils.initEnergyTable(this.switchTable);
/*  42 */     this.mainFrame = mainFrame;
/*  43 */     initPanel();
/*     */     
/*     */ 
/*  46 */     initDefaultTableValues();
/*     */     
/*     */ 
/*  49 */     this.labelResizeTable.setVisible(false);
/*  50 */     this.buttonResizeTable.setVisible(false);
/*  51 */     this.textNewNumRows.setVisible(false);
/*  52 */     this.energyLabel.setVisible(false);
/*  53 */     this.switchTable.setVisible(false);
/*  54 */     this.scrollTable.setVisible(false);
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   private void initPanel()
/*     */   {
/*  62 */     this.comboQueue.addItem("DropTailQueue");
/*  63 */     this.comboQueue.addItem("EtherQoSQueue");
/*     */     
/*     */ 
/*  66 */     this.comboRelayUnit.addItem("MACRelayUnitPP");
/*  67 */     this.comboRelayUnit.addItem("MACRelayUnitNP");
/*     */     
/*     */ 
/*  70 */     this.textMTU.setText("1500");
/*     */     
/*     */ 
/*  73 */     this.csmaSupport.setSelected(true);
/*     */     
/*     */ 
/*  76 */     this.dupplexMode.setSelected(true);
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public boolean checkValues()
/*     */   {
/*  89 */     boolean allOK = false;
/*     */     try
/*     */     {
/*  92 */       Integer.parseInt(this.textMTU.getText());
/*  93 */       allOK = true;
/*     */     }
/*     */     catch (Exception e) {
/*  96 */       e.printStackTrace();
/*  97 */       allOK = false;
/*     */     }
/*     */     
/* 100 */     return allOK;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private void initDefaultTableValues()
/*     */   {
/* 113 */     EnergyTableModel model = new EnergyTableModel();
/* 114 */     model.setColumnCount(3);
/* 115 */     model.setColumnIdentifiers(Utils.columnNamesEnergyTable);
/*     */     
/*     */ 
/* 118 */     model.addRow(new Object[] { Integer.toString(0), "W", "50.0" });
/*     */     
/*     */ 
/* 121 */     this.switchTable.setModel(model);
/*     */   }
/*     */   
/*     */ 
/*     */   private JComboBox comboRelayUnit;
/*     */   
/*     */   private JCheckBox csmaSupport;
/*     */   
/*     */   private JCheckBox dupplexMode;
/*     */   
/*     */   private JLabel energyLabel;
/*     */   private JLabel labelComment;
/*     */   private JLabel labelMTU;
/*     */   public Switch panelToSwitchObject()
/*     */   {
/* 136 */     Switch currentSwitch = new Switch();
/* 137 */     java.util.LinkedList<icancloudgui.TabbedPanels.dataClasses.EnergyEntry> list = new java.util.LinkedList();
/*     */     
/*     */ 
/* 140 */     currentSwitch.setName(this.textName.getText());
/* 141 */     currentSwitch.setComment(this.textAreaComment.getText());
/* 142 */     currentSwitch.setCsmaCdSupport(this.csmaSupport.isSelected());
/* 143 */     currentSwitch.setDuplexMode(this.dupplexMode.isSelected());
/* 144 */     currentSwitch.setQueueType(this.comboQueue.getSelectedItem().toString());
/* 145 */     currentSwitch.setRelayUnitType(this.comboRelayUnit.getSelectedItem().toString());
/* 146 */     currentSwitch.setMtu(Integer.parseInt(this.textMTU.getText()));
/* 147 */     Utils.energyTableModelToList((EnergyTableModel)this.switchTable.getModel(), list);
/* 148 */     currentSwitch.setEnergyData(list);
/*     */     
/* 150 */     return currentSwitch;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void clear()
/*     */   {
/* 160 */     this.textMTU.setText("1500");
/*     */     
/*     */ 
/* 163 */     this.csmaSupport.setSelected(true);
/*     */     
/*     */ 
/* 166 */     this.dupplexMode.setSelected(true);
/*     */     
/*     */ 
/* 169 */     this.textName.setText("");
/*     */     
/*     */ 
/* 172 */     this.textAreaComment.setText("");
/*     */     
/*     */ 
/* 175 */     Utils.initEnergyTable(this.switchTable);
/*     */     
/*     */ 
/* 178 */     if (this.comboQueue.getItemCount() > 0) {
/* 179 */       this.comboQueue.setSelectedIndex(0);
/*     */     }
/* 181 */     if (this.comboRelayUnit.getItemCount() > 0) {
/* 182 */       this.comboRelayUnit.setSelectedIndex(0);
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private void loadSwitchObjectInPanel(Switch newSwitch)
/*     */   {
/* 195 */     this.textName.setText(newSwitch.getName());
/* 196 */     this.textAreaComment.setText(newSwitch.getComment());
/* 197 */     this.textMTU.setText(Integer.toString(newSwitch.getMtu()));
/* 198 */     this.csmaSupport.setSelected(newSwitch.isCsmaCdSupport());
/* 199 */     this.dupplexMode.setSelected(newSwitch.isDuplexMode());
/* 200 */     Utils.initCombo(this.comboQueue, newSwitch.getQueueType());
/* 201 */     Utils.initCombo(this.comboRelayUnit, newSwitch.getRelayUnitType());
/*     */     
/*     */ 
/* 204 */     EnergyTableModel model = new EnergyTableModel();
/* 205 */     Utils.energyListToTable(newSwitch.getEnergyData(), model);
/* 206 */     this.switchTable.setModel(model);
/* 207 */     this.switchTable.updateUI();
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void loadSwitchInPanel(String switchName)
/*     */   {
/* 220 */     Switch loadedSwitch = Utils.loadSwitchObject(switchName);
/*     */     
/*     */ 
/* 223 */     if (loadedSwitch != null) {
/* 224 */       loadSwitchObjectInPanel(loadedSwitch);
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */   private JLabel labelName;
/*     */   
/*     */   private JLabel labelQueue;
/*     */   
/*     */   private JLabel labelRelayUnit;
/*     */   private JLabel labelResizeTable;
/*     */   private JScrollPane scrollTable;
/*     */   private JScrollPane scrollTextArea;
/*     */   private JTable switchTable;
/*     */   private JTextArea textAreaComment;
/*     */   private JTextField textMTU;
/*     */   private JTextField textName;
/*     */   private JTextField textNewNumRows;
/*     */   private boolean writeSwitchToFile()
/*     */   {
/* 244 */     boolean allOK = checkValues();
/*     */     
/*     */ 
/* 247 */     if (allOK)
/*     */     {
/*     */ 
/* 250 */       Switch currentSwitch = panelToSwitchObject();
/*     */       
/*     */ 
/* 253 */       String newSwitchFile = icancloudgui.Utils.Configuration.REPOSITORY_SWITCHES + java.io.File.separatorChar + currentSwitch.getName();
/*     */       
/*     */ 
/*     */       try
/*     */       {
/* 258 */         if (new java.io.File(newSwitchFile).exists())
/*     */         {
/*     */ 
/* 261 */           int response = JOptionPane.showConfirmDialog(null, "Overwrite existing Switch?", "Confirm Overwrite", 2, 3);
/*     */           
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 268 */           if (response == 0)
/*     */           {
/*     */ 
/* 271 */             FileOutputStream fout = new FileOutputStream(newSwitchFile);
/* 272 */             ObjectOutputStream oos = new ObjectOutputStream(fout);
/* 273 */             oos.writeObject(currentSwitch);
/* 274 */             oos.close();
/* 275 */             allOK = true;
/*     */           }
/*     */           
/*     */ 
/*     */         }
/*     */         else
/*     */         {
/*     */ 
/* 283 */           FileOutputStream fout = new FileOutputStream(newSwitchFile);
/* 284 */           ObjectOutputStream oos = new ObjectOutputStream(fout);
/* 285 */           oos.writeObject(currentSwitch);
/* 286 */           oos.close();
/* 287 */           allOK = true;
/*     */         }
/*     */       }
/*     */       catch (Exception e) {
/* 291 */         e.printStackTrace();
/*     */       }
/*     */       
/*     */ 
/*     */     }
/*     */     else
/*     */     {
/* 298 */       JOptionPane.showMessageDialog(new javax.swing.JFrame(), "Wrong values. Please, re-check", "Error", 0);
/*     */     }
/*     */     
/*     */ 
/* 302 */     return allOK;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private void initComponents()
/*     */   {
/* 315 */     this.labelName = new JLabel();
/* 316 */     this.labelQueue = new JLabel();
/* 317 */     this.labelRelayUnit = new JLabel();
/* 318 */     this.textName = new JTextField();
/* 319 */     this.scrollTable = new JScrollPane();
/* 320 */     this.switchTable = new JTable();
/* 321 */     this.energyLabel = new JLabel();
/* 322 */     this.buttonResizeTable = new JLabel();
/* 323 */     this.textNewNumRows = new JTextField();
/* 324 */     this.labelResizeTable = new JLabel();
/* 325 */     this.textMTU = new JTextField();
/* 326 */     this.labelMTU = new JLabel();
/* 327 */     this.buttonHelp = new JLabel();
/* 328 */     this.buttonSave = new JLabel();
/* 329 */     this.labelComment = new JLabel();
/* 330 */     this.scrollTextArea = new JScrollPane();
/* 331 */     this.textAreaComment = new JTextArea();
/* 332 */     this.csmaSupport = new JCheckBox();
/* 333 */     this.dupplexMode = new JCheckBox();
/* 334 */     this.comboQueue = new JComboBox();
/* 335 */     this.comboRelayUnit = new JComboBox();
/* 336 */     this.buttonClear = new JLabel();
/*     */     
/* 338 */     ResourceMap resourceMap = ((icancloudgui.ICanCloudGUIApp)org.jdesktop.application.Application.getInstance(icancloudgui.ICanCloudGUIApp.class)).getContext().getResourceMap(SwitchPanel.class);
/* 339 */     setBackground(resourceMap.getColor("Form.background"));
/* 340 */     setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
/* 341 */     setMinimumSize(new java.awt.Dimension(782, 510));
/* 342 */     setName("Form");
/* 343 */     setPreferredSize(new java.awt.Dimension(500, 641));
/* 344 */     setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());
/*     */     
/* 346 */     this.labelName.setFont(resourceMap.getFont("labelQueue.font"));
/* 347 */     this.labelName.setText(resourceMap.getString("labelName.text", new Object[0]));
/* 348 */     this.labelName.setName("labelName");
/* 349 */     add(this.labelName, new AbsoluteConstraints(10, 35, -1, -1));
/*     */     
/* 351 */     this.labelQueue.setFont(resourceMap.getFont("labelQueue.font"));
/* 352 */     this.labelQueue.setText(resourceMap.getString("labelQueue.text", new Object[0]));
/* 353 */     this.labelQueue.setName("labelQueue");
/* 354 */     add(this.labelQueue, new AbsoluteConstraints(60, 70, -1, -1));
/*     */     
/* 356 */     this.labelRelayUnit.setFont(resourceMap.getFont("labelQueue.font"));
/* 357 */     this.labelRelayUnit.setText(resourceMap.getString("labelRelayUnit.text", new Object[0]));
/* 358 */     this.labelRelayUnit.setName("labelRelayUnit");
/* 359 */     add(this.labelRelayUnit, new AbsoluteConstraints(30, 105, -1, -1));
/*     */     
/* 361 */     this.textName.setText(resourceMap.getString("textName.text", new Object[0]));
/* 362 */     this.textName.setName("textName");
/* 363 */     add(this.textName, new AbsoluteConstraints(50, 30, 300, -1));
/*     */     
/* 365 */     this.scrollTable.setName("scrollTable");
/*     */     
/* 367 */     this.switchTable.setModel(new DefaultTableModel(new Object[][] { { null, null, null }, { null, null, null }, { null, null, null }, { null, null, null }, { null, null, null }, { null, null, null }, { null, null, null }, { null, null, null }, { null, null, null }, { null, null, null } }, new String[] { "#", "Name", "Value" })
/*     */     {
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 384 */       Class[] types = { String.class, String.class, String.class };
/*     */       
/*     */ 
/* 387 */       boolean[] canEdit = { false, true, true };
/*     */       
/*     */ 
/*     */       public Class getColumnClass(int columnIndex)
/*     */       {
/* 392 */         return this.types[columnIndex];
/*     */       }
/*     */       
/*     */       public boolean isCellEditable(int rowIndex, int columnIndex) {
/* 396 */         return this.canEdit[columnIndex];
/*     */       }
/* 398 */     });
/* 399 */     this.switchTable.setGridColor(resourceMap.getColor("switchTable.gridColor"));
/* 400 */     this.switchTable.setName("switchTable");
/* 401 */     this.scrollTable.setViewportView(this.switchTable);
/* 402 */     this.switchTable.getColumnModel().getColumn(0).setHeaderValue(resourceMap.getString("switchTable.columnModel.title0", new Object[0]));
/* 403 */     this.switchTable.getColumnModel().getColumn(1).setHeaderValue(resourceMap.getString("switchTable.columnModel.title1", new Object[0]));
/* 404 */     this.switchTable.getColumnModel().getColumn(2).setHeaderValue(resourceMap.getString("switchTable.columnModel.title2", new Object[0]));
/*     */     
/* 406 */     add(this.scrollTable, new AbsoluteConstraints(20, 210, 760, 207));
/*     */     
/* 408 */     this.energyLabel.setFont(resourceMap.getFont("labelQueue.font"));
/* 409 */     this.energyLabel.setText(resourceMap.getString("energyLabel.text", new Object[0]));
/* 410 */     this.energyLabel.setName("energyLabel");
/* 411 */     add(this.energyLabel, new AbsoluteConstraints(13, 185, -1, -1));
/*     */     
/* 413 */     this.buttonResizeTable.setIcon(resourceMap.getIcon("buttonResizeTable.icon"));
/* 414 */     this.buttonResizeTable.setText(resourceMap.getString("buttonResizeTable.text", new Object[0]));
/* 415 */     this.buttonResizeTable.setToolTipText(resourceMap.getString("buttonResizeTable.toolTipText", new Object[0]));
/* 416 */     this.buttonResizeTable.setName("buttonResizeTable");
/* 417 */     this.buttonResizeTable.addMouseListener(new java.awt.event.MouseAdapter() {
/*     */       public void mouseClicked(MouseEvent evt) {
/* 419 */         SwitchPanel.this.buttonResizeTableMouseClicked(evt);
/*     */       }
/* 421 */     });
/* 422 */     add(this.buttonResizeTable, new AbsoluteConstraints(690, 180, -1, -1));
/*     */     
/* 424 */     this.textNewNumRows.setText(resourceMap.getString("textNewNumRows.text", new Object[0]));
/* 425 */     this.textNewNumRows.setName("textNewNumRows");
/* 426 */     add(this.textNewNumRows, new AbsoluteConstraints(720, 175, 60, -1));
/*     */     
/* 428 */     this.labelResizeTable.setFont(resourceMap.getFont("labelQueue.font"));
/* 429 */     this.labelResizeTable.setForeground(resourceMap.getColor("labelResizeTable.foreground"));
/* 430 */     this.labelResizeTable.setText(resourceMap.getString("labelResizeTable.text", new Object[0]));
/* 431 */     this.labelResizeTable.setName("labelResizeTable");
/* 432 */     add(this.labelResizeTable, new AbsoluteConstraints(590, 183, -1, -1));
/*     */     
/* 434 */     this.textMTU.setName("textMTU");
/* 435 */     add(this.textMTU, new AbsoluteConstraints(150, 135, 230, -1));
/*     */     
/* 437 */     this.labelMTU.setFont(resourceMap.getFont("labelQueue.font"));
/* 438 */     this.labelMTU.setText(resourceMap.getString("labelMTU.text", new Object[0]));
/* 439 */     this.labelMTU.setName("labelMTU");
/* 440 */     add(this.labelMTU, new AbsoluteConstraints(100, 140, -1, -1));
/*     */     
/* 442 */     this.buttonHelp.setIcon(resourceMap.getIcon("buttonHelp.icon"));
/* 443 */     this.buttonHelp.setToolTipText(resourceMap.getString("buttonHelp.toolTipText", new Object[0]));
/* 444 */     this.buttonHelp.setName("buttonHelp");
/* 445 */     this.buttonHelp.addMouseListener(new java.awt.event.MouseAdapter() {
/*     */       public void mouseClicked(MouseEvent evt) {
/* 447 */         SwitchPanel.this.buttonHelpMouseClicked(evt);
/*     */       }
/* 449 */     });
/* 450 */     add(this.buttonHelp, new AbsoluteConstraints(750, 20, -1, -1));
/*     */     
/* 452 */     this.buttonSave.setIcon(resourceMap.getIcon("buttonSave.icon"));
/* 453 */     this.buttonSave.setToolTipText(resourceMap.getString("buttonSave.toolTipText", new Object[0]));
/* 454 */     this.buttonSave.setName("buttonSave");
/* 455 */     this.buttonSave.addMouseListener(new java.awt.event.MouseAdapter() {
/*     */       public void mouseClicked(MouseEvent evt) {
/* 457 */         SwitchPanel.this.buttonSaveMouseClicked(evt);
/*     */       }
/* 459 */     });
/* 460 */     add(this.buttonSave, new AbsoluteConstraints(710, 20, -1, -1));
/*     */     
/* 462 */     this.labelComment.setFont(resourceMap.getFont("labelQueue.font"));
/* 463 */     this.labelComment.setText(resourceMap.getString("labelComment.text", new Object[0]));
/* 464 */     this.labelComment.setName("labelComment");
/* 465 */     add(this.labelComment, new AbsoluteConstraints(10, 435, -1, -1));
/*     */     
/* 467 */     this.scrollTextArea.setName("scrollTextArea");
/*     */     
/* 469 */     this.textAreaComment.setColumns(20);
/* 470 */     this.textAreaComment.setRows(5);
/* 471 */     this.textAreaComment.setName("textAreaComment");
/* 472 */     this.scrollTextArea.setViewportView(this.textAreaComment);
/*     */     
/* 474 */     add(this.scrollTextArea, new AbsoluteConstraints(20, 460, 760, 70));
/*     */     
/* 476 */     this.csmaSupport.setFont(resourceMap.getFont("labelQueue.font"));
/* 477 */     this.csmaSupport.setText(resourceMap.getString("csmaSupport.text", new Object[0]));
/* 478 */     this.csmaSupport.setHorizontalTextPosition(2);
/* 479 */     this.csmaSupport.setName("csmaSupport");
/* 480 */     add(this.csmaSupport, new AbsoluteConstraints(440, 80, -1, -1));
/*     */     
/* 482 */     this.dupplexMode.setFont(resourceMap.getFont("labelQueue.font"));
/* 483 */     this.dupplexMode.setText(resourceMap.getString("dupplexMode.text", new Object[0]));
/* 484 */     this.dupplexMode.setHorizontalTextPosition(2);
/* 485 */     this.dupplexMode.setName("dupplexMode");
/* 486 */     add(this.dupplexMode, new AbsoluteConstraints(440, 110, -1, -1));
/*     */     
/* 488 */     this.comboQueue.setName("comboQueue");
/* 489 */     add(this.comboQueue, new AbsoluteConstraints(150, 65, 230, -1));
/*     */     
/* 491 */     this.comboRelayUnit.setName("comboRelayUnit");
/* 492 */     add(this.comboRelayUnit, new AbsoluteConstraints(150, 100, 230, -1));
/*     */     
/* 494 */     this.buttonClear.setIcon(resourceMap.getIcon("buttonClear.icon"));
/* 495 */     this.buttonClear.setToolTipText(resourceMap.getString("buttonClear.toolTipText", new Object[0]));
/* 496 */     this.buttonClear.setName("buttonClear");
/* 497 */     this.buttonClear.addMouseListener(new java.awt.event.MouseAdapter() {
/*     */       public void mouseClicked(MouseEvent evt) {
/* 499 */         SwitchPanel.this.buttonClearMouseClicked(evt);
/*     */       }
/* 501 */     });
/* 502 */     add(this.buttonClear, new AbsoluteConstraints(670, 20, -1, -1));
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private void buttonResizeTableMouseClicked(MouseEvent evt)
/*     */   {
/*     */     try
/*     */     {
/* 520 */       int newNumRows = Integer.parseInt(this.textNewNumRows.getText());
/*     */       
/*     */ 
/* 523 */       if (this.switchTable.getModel().getRowCount() == newNumRows)
/*     */       {
/* 525 */         JOptionPane.showMessageDialog(new javax.swing.JFrame(), "New number of rows is the same as the curent table", "Warning!", 2);
/*     */       }
/*     */       
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 532 */       if (newNumRows < this.switchTable.getModel().getRowCount())
/*     */       {
/*     */ 
/* 535 */         int response = JOptionPane.showConfirmDialog(null, "Are you sure of resizing this table?", "Confirm resize table", 2, 3);
/*     */         
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 541 */         if (response == 0)
/*     */         {
/*     */ 
/* 544 */           DefaultTableModel newModel = (DefaultTableModel)this.switchTable.getModel();
/* 545 */           int oldNumRows = this.switchTable.getModel().getRowCount();
/*     */           
/*     */ 
/* 548 */           for (int i = 0; i < oldNumRows - newNumRows; i++) {
/* 549 */             System.out.println(newModel.getRowCount());
/* 550 */             newModel.removeRow(newModel.getRowCount() - 1);
/*     */           }
/*     */           
/* 553 */           this.switchTable.setModel(newModel);
/*     */ 
/*     */         }
/*     */         
/*     */ 
/*     */       }
/*     */       else
/*     */       {
/*     */ 
/* 562 */         int response = JOptionPane.showConfirmDialog(null, "Are you sure of resizing this table?", "Confirm resize table", 2, 3);
/*     */         
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 568 */         if (response == 0)
/*     */         {
/*     */ 
/* 571 */           DefaultTableModel newModel = (DefaultTableModel)this.switchTable.getModel();
/* 572 */           int oldNumRows = this.switchTable.getModel().getRowCount();
/*     */           
/*     */ 
/* 575 */           for (int i = 0; i < newNumRows - oldNumRows; i++) {
/* 576 */             newModel.addRow(new Object[] { Integer.toString(oldNumRows + i), "", "" });
/*     */           }
/*     */           
/* 579 */           this.switchTable.setModel(newModel);
/*     */         }
/*     */       }
/*     */     }
/*     */     catch (Exception e)
/*     */     {
/* 585 */       JOptionPane.showMessageDialog(new javax.swing.JFrame(), "Wrong number, please enter a correct number value", "Error!", 2);
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private void buttonHelpMouseClicked(MouseEvent evt)
/*     */   {
/* 599 */     Utils.showHelp(this);
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private void buttonSaveMouseClicked(MouseEvent evt)
/*     */   {
/* 612 */     boolean allOK = writeSwitchToFile();
/*     */     
/*     */ 
/* 615 */     if (allOK) {
/* 616 */       this.mainFrame.updateTree();
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */   private void buttonClearMouseClicked(MouseEvent evt)
/*     */   {
/* 624 */     int response = JOptionPane.showConfirmDialog(null, "Clear current Switch configuration?", "Confirm to clear this form", 2, 3);
/*     */     
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 631 */     if (response == 0) {
/* 632 */       clear();
/*     */     }
/*     */   }
/*     */ }


/* Location:              /home/chverma/Descargas/iCanCloudGUI/iCanCloudGUI.jar!/icancloudgui/TabbedPanels/SwitchPanel.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */