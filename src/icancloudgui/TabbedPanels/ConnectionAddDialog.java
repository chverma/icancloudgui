/*     */ package icancloudgui.TabbedPanels;
/*     */ 
/*     */ import java.awt.Container;
/*     */ import javax.swing.JLabel;
/*     */ import javax.swing.JTextField;
/*     */ import org.jdesktop.application.ResourceMap;
/*     */ import org.netbeans.lib.awtextra.AbsoluteConstraints;
/*     */ 
/*     */ public class ConnectionAddDialog extends javax.swing.JDialog
/*     */ {
/*     */   DataCenterPanel dataCenterPanel;
/*     */   private javax.swing.JButton buttonAddConnection;
/*     */   private javax.swing.JComboBox comboDestination;
/*     */   private javax.swing.JComboBox comboNetwork;
/*     */   private JLabel labelBer;
/*     */   private JLabel labelDelay;
/*     */   private JLabel labelDestination;
/*     */   private JLabel labelNetwork;
/*     */   private JLabel labelPer;
/*     */   private JLabel labelTitle;
/*     */   private JTextField textBer;
/*     */   private JTextField textDelay;
/*     */   private JTextField textPer;
/*     */   
/*     */   public ConnectionAddDialog(java.awt.Frame parent, DataCenterPanel main)
/*     */   {
/*  27 */     super(parent, true);
/*     */     
/*     */ 
/*     */ 
/*     */ 
/*  32 */     initComponents();
/*  33 */     this.dataCenterPanel = main;
/*     */     
/*     */ 
/*  36 */     if (this.comboNetwork.getItemCount() == 0) {
/*  37 */       java.util.LinkedList<String> list = icancloudgui.Utils.Utils.getCommunicationLinkList();
/*  38 */       icancloudgui.Utils.Utils.loadCombo(list, this.comboNetwork);
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public boolean checkValues()
/*     */   {
/*  52 */     boolean allOK = false;
/*     */     
/*     */     try
/*     */     {
/*  56 */       if (this.textDelay.getText().length() == 0) {
/*  57 */         allOK = false;
/*     */       }
/*  59 */       else if (this.textBer.getText().length() == 0) {
/*  60 */         allOK = false;
/*     */       }
/*  62 */       else if (this.textPer.getText().length() == 0) {
/*  63 */         allOK = false;
/*     */       }
/*     */       else
/*     */       {
/*  67 */         Double.parseDouble(this.textDelay.getText());
/*  68 */         Double.parseDouble(this.textBer.getText());
/*  69 */         Double.parseDouble(this.textPer.getText());
/*  70 */         allOK = true;
/*     */       }
/*     */     } catch (Exception e) {
/*  73 */       e.printStackTrace();
/*  74 */       allOK = false;
/*     */     }
/*     */     
/*  77 */     return allOK;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void clear()
/*     */   {
/*  86 */     this.textDelay.setText("");
/*  87 */     this.textBer.setText("");
/*  88 */     this.textPer.setText("");
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void loadComboDestination(java.util.LinkedList<icancloudgui.TabbedPanels.dataClasses.DataCenterUnit> list)
/*     */   {
/*  98 */     this.comboDestination.removeAllItems();
/*     */     
/*     */ 
/* 101 */     icancloudgui.Utils.Utils.loadComboDataCenterUnit(list, this.comboDestination);
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private void initComponents()
/*     */   {
/* 114 */     this.comboDestination = new javax.swing.JComboBox();
/* 115 */     this.comboNetwork = new javax.swing.JComboBox();
/* 116 */     this.buttonAddConnection = new javax.swing.JButton();
/* 117 */     this.textDelay = new JTextField();
/* 118 */     this.textBer = new JTextField();
/* 119 */     this.textPer = new JTextField();
/* 120 */     this.labelNetwork = new JLabel();
/* 121 */     this.labelDestination = new JLabel();
/* 122 */     this.labelTitle = new JLabel();
/* 123 */     this.labelBer = new JLabel();
/* 124 */     this.labelDelay = new JLabel();
/* 125 */     this.labelPer = new JLabel();
/*     */     
/* 127 */     setDefaultCloseOperation(2);
/* 128 */     ResourceMap resourceMap = ((icancloudgui.ICanCloudGUIApp)org.jdesktop.application.Application.getInstance(icancloudgui.ICanCloudGUIApp.class)).getContext().getResourceMap(ConnectionAddDialog.class);
/* 129 */     setBackground(resourceMap.getColor("Form.background"));
/* 130 */     setName("Form");
/* 131 */     getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());
/*     */     
/* 133 */     this.comboDestination.setName("comboDestination");
/* 134 */     getContentPane().add(this.comboDestination, new AbsoluteConstraints(210, 130, 300, -1));
/*     */     
/* 136 */     this.comboNetwork.setName("comboNetwork");
/* 137 */     getContentPane().add(this.comboNetwork, new AbsoluteConstraints(210, 190, 300, -1));
/*     */     
/* 139 */     this.buttonAddConnection.setForeground(resourceMap.getColor("buttonAddConnection.foreground"));
/* 140 */     this.buttonAddConnection.setText(resourceMap.getString("buttonAddConnection.text", new Object[0]));
/* 141 */     this.buttonAddConnection.setToolTipText(resourceMap.getString("buttonAddConnection.toolTipText", new Object[0]));
/* 142 */     this.buttonAddConnection.setName("buttonAddConnection");
/* 143 */     this.buttonAddConnection.addActionListener(new java.awt.event.ActionListener() {
/*     */       public void actionPerformed(java.awt.event.ActionEvent evt) {
/* 145 */         ConnectionAddDialog.this.buttonAddConnectionActionPerformed(evt);
/*     */       }
/* 147 */     });
/* 148 */     getContentPane().add(this.buttonAddConnection, new AbsoluteConstraints(210, 360, -1, -1));
/*     */     
/* 150 */     this.textDelay.setName("textDelay");
/* 151 */     getContentPane().add(this.textDelay, new AbsoluteConstraints(210, 235, 300, -1));
/*     */     
/* 153 */     this.textBer.setName("textBer");
/* 154 */     getContentPane().add(this.textBer, new AbsoluteConstraints(210, 275, 300, -1));
/*     */     
/* 156 */     this.textPer.setName("textPer");
/* 157 */     getContentPane().add(this.textPer, new AbsoluteConstraints(210, 315, 300, -1));
/*     */     
/* 159 */     this.labelNetwork.setFont(resourceMap.getFont("labelNetwork.font"));
/* 160 */     this.labelNetwork.setText(resourceMap.getString("labelNetwork.text", new Object[0]));
/* 161 */     this.labelNetwork.setName("labelNetwork");
/* 162 */     getContentPane().add(this.labelNetwork, new AbsoluteConstraints(80, 190, -1, -1));
/*     */     
/* 164 */     this.labelDestination.setFont(resourceMap.getFont("labelNetwork.font"));
/* 165 */     this.labelDestination.setText(resourceMap.getString("labelDestination.text", new Object[0]));
/* 166 */     this.labelDestination.setName("labelDestination");
/* 167 */     getContentPane().add(this.labelDestination, new AbsoluteConstraints(40, 130, -1, -1));
/*     */     
/* 169 */     this.labelTitle.setFont(resourceMap.getFont("labelTitle.font"));
/* 170 */     this.labelTitle.setText(resourceMap.getString("labelTitle.text", new Object[0]));
/* 171 */     this.labelTitle.setName("labelTitle");
/* 172 */     getContentPane().add(this.labelTitle, new AbsoluteConstraints(110, 30, -1, -1));
/*     */     
/* 174 */     this.labelBer.setFont(resourceMap.getFont("labelNetwork.font"));
/* 175 */     this.labelBer.setText(resourceMap.getString("labelBer.text", new Object[0]));
/* 176 */     this.labelBer.setToolTipText(resourceMap.getString("labelBer.toolTipText", new Object[0]));
/* 177 */     this.labelBer.setName("labelBer");
/* 178 */     getContentPane().add(this.labelBer, new AbsoluteConstraints(160, 280, -1, -1));
/*     */     
/* 180 */     this.labelDelay.setFont(resourceMap.getFont("labelNetwork.font"));
/* 181 */     this.labelDelay.setText(resourceMap.getString("labelDelay.text", new Object[0]));
/* 182 */     this.labelDelay.setToolTipText(resourceMap.getString("labelDelay.toolTipText", new Object[0]));
/* 183 */     this.labelDelay.setName("labelDelay");
/* 184 */     getContentPane().add(this.labelDelay, new AbsoluteConstraints(150, 240, -1, -1));
/*     */     
/* 186 */     this.labelPer.setFont(resourceMap.getFont("labelNetwork.font"));
/* 187 */     this.labelPer.setText(resourceMap.getString("labelPer.text", new Object[0]));
/* 188 */     this.labelPer.setToolTipText(resourceMap.getString("labelPer.toolTipText", new Object[0]));
/* 189 */     this.labelPer.setName("labelPer");
/* 190 */     getContentPane().add(this.labelPer, new AbsoluteConstraints(160, 320, -1, -1));
/*     */     
/* 192 */     pack();
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private void buttonAddConnectionActionPerformed(java.awt.event.ActionEvent evt)
/*     */   {
/* 205 */     boolean allOK = checkValues();
/*     */     
/*     */ 
/* 208 */     if (!allOK)
/*     */     {
/* 210 */       javax.swing.JOptionPane.showMessageDialog(new javax.swing.JFrame(), "Wrong values. Please, re-check", "Error", 0);
/*     */ 
/*     */ 
/*     */ 
/*     */     }
/*     */     else
/*     */     {
/*     */ 
/*     */ 
/* 219 */       this.dataCenterPanel.addNewConnection(this.comboDestination.getSelectedItem().toString(), this.comboNetwork.getSelectedItem().toString(), Double.parseDouble(this.textDelay.getText()), Double.parseDouble(this.textBer.getText()), Double.parseDouble(this.textPer.getText()));
/*     */       
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 227 */       setVisible(false);
/*     */     }
/*     */   }
/*     */ }


/* Location:              /home/chverma/Descargas/iCanCloudGUI/iCanCloudGUI.jar!/icancloudgui/TabbedPanels/ConnectionAddDialog.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */