/*      */ package icancloudgui.TabbedPanels;
/*      */ 
/*      */ import javax.swing.JTextField;
/*      */ 
/*      */ public class UsersPanel extends javax.swing.JPanel
/*      */ {
/*      */   icancloudgui.ICanCloudGUIView mainFrame;
/*      */   private javax.swing.JLabel buttonClear;
/*      */   private javax.swing.JCheckBox checkBoxCPU;
/*      */   private javax.swing.JCheckBox checkBoxHPC;
/*      */   private javax.swing.JCheckBox checkBoxServer;
/*      */   private javax.swing.JCheckBox checkBoxWorkersReadHPC;
/*      */   private javax.swing.JCheckBox checkBoxWorkersWriteHPC;
/*      */   private javax.swing.JScrollPane jScrollPane1;
/*      */   private javax.swing.JLabel labelComment;
/*      */   private javax.swing.JLabel labelDataCoordinadorHPC;
/*      */   private javax.swing.JLabel labelDataWorkersHPC;
/*      */   private javax.swing.JLabel labelHelp;
/*      */   private javax.swing.JLabel labelInput;
/*      */   private javax.swing.JLabel labelInstancesCPU;
/*      */   private javax.swing.JLabel labelInstancesHPC;
/*      */   private javax.swing.JLabel labelInstancesServer;
/*      */   private javax.swing.JLabel labelIterationsCPU;
/*      */   private javax.swing.JLabel labelIterationsHPC;
/*      */   private javax.swing.JLabel labelName;
/*      */   private javax.swing.JLabel labelNumProcessesHPC;
/*      */   private javax.swing.JLabel labelOutput;
/*      */   private javax.swing.JLabel labelProcessingCPU;
/*      */   private javax.swing.JLabel labelProcessingHPC;
/*      */   private javax.swing.JLabel labelProcessingServer;
/*      */   
/*      */   public UsersPanel(icancloudgui.ICanCloudGUIView mainFrame) {
/*   33 */     this.mainFrame = mainFrame;
/*      */     
/*   35 */     initComponents();
/*      */     
/*      */ 
/*   38 */     this.checkBoxServer.setSelected(true);
/*   39 */     this.checkBoxHPC.setSelected(true);
/*   40 */     this.checkBoxCPU.setSelected(true);
/*      */     
/*   42 */     icancloudgui.Utils.Utils.initVMTable(this.tableVM);
/*      */     
/*      */ 
/*   45 */     this.labelInstancesServer.setVisible(false);
/*   46 */     this.textInstancesServer.setVisible(false);
/*   47 */     this.textInstancesCPU.setVisible(false);
/*   48 */     this.labelInstancesCPU.setVisible(false);
/*   49 */     this.textInstancesHPC.setVisible(false);
/*   50 */     this.labelInstancesHPC.setVisible(false);
/*   51 */     this.labelNumProcessesHPC.setVisible(false);
/*   52 */     this.textNumProcessesHPC.setVisible(false);
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   private void checkApplications()
/*      */   {
/*   62 */     if (this.checkBoxServer.isSelected()) {
/*   63 */       this.textRequestServer.setEnabled(true);
/*   64 */       this.textProcessingServer.setEnabled(true);
/*   65 */       this.textInstancesServer.setEnabled(true);
/*      */ 
/*      */     }
/*      */     else
/*      */     {
/*   70 */       this.textRequestServer.setEnabled(false);
/*   71 */       this.textProcessingServer.setEnabled(false);
/*   72 */       this.textInstancesServer.setEnabled(false);
/*      */     }
/*      */     
/*      */ 
/*   76 */     if (this.checkBoxCPU.isSelected()) {
/*   77 */       this.textIterationsCPU.setEnabled(true);
/*   78 */       this.textProcessingCPU.setEnabled(true);
/*   79 */       this.textInstancesCPU.setEnabled(true);
/*      */ 
/*      */     }
/*      */     else
/*      */     {
/*   84 */       this.textIterationsCPU.setEnabled(false);
/*   85 */       this.textProcessingCPU.setEnabled(false);
/*   86 */       this.textInstancesCPU.setEnabled(false);
/*      */     }
/*      */     
/*      */ 
/*      */ 
/*   91 */     if (this.checkBoxHPC.isSelected()) {
/*   92 */       this.textWorkersHPC.setEnabled(true);
/*   93 */       this.textIterationsHPC.setEnabled(true);
/*   94 */       this.textDataWorkersHPC.setEnabled(true);
/*   95 */       this.textProcessingHPC.setEnabled(true);
/*   96 */       this.textDataCoordinadorHPC.setEnabled(true);
/*   97 */       this.textInstancesHPC.setEnabled(true);
/*   98 */       this.textNumProcessesHPC.setEnabled(true);
/*   99 */       this.checkBoxWorkersReadHPC.setEnabled(true);
/*  100 */       this.checkBoxWorkersWriteHPC.setEnabled(true);
/*      */ 
/*      */     }
/*      */     else
/*      */     {
/*  105 */       this.textWorkersHPC.setEnabled(false);
/*  106 */       this.textIterationsHPC.setEnabled(false);
/*  107 */       this.textDataWorkersHPC.setEnabled(false);
/*  108 */       this.textProcessingHPC.setEnabled(false);
/*  109 */       this.textDataCoordinadorHPC.setEnabled(false);
/*  110 */       this.textInstancesHPC.setEnabled(false);
/*  111 */       this.textNumProcessesHPC.setEnabled(false);
/*  112 */       this.checkBoxWorkersReadHPC.setEnabled(false);
/*  113 */       this.checkBoxWorkersWriteHPC.setEnabled(false);
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   public void reloadPanel()
/*      */   {
/*  122 */     icancloudgui.Utils.Utils.initVMTable(this.tableVM);
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void clear()
/*      */   {
/*  131 */     this.textName.setText("");
/*  132 */     this.textComment.setText("");
/*      */     
/*      */ 
/*  135 */     this.checkBoxServer.setSelected(true);
/*  136 */     this.textRequestServer.setText("");
/*  137 */     this.textProcessingServer.setText("");
/*  138 */     this.textInstancesServer.setText("");
/*      */     
/*      */ 
/*  141 */     this.checkBoxCPU.setSelected(true);
/*  142 */     this.textIterationsCPU.setText("");
/*  143 */     this.textProcessingCPU.setText("");
/*  144 */     this.textInstancesCPU.setText("");
/*  145 */     this.textInput.setText("");
/*  146 */     this.textOutput.setText("");
/*      */     
/*      */ 
/*  149 */     this.checkBoxHPC.setSelected(true);
/*  150 */     this.textWorkersHPC.setText("");
/*  151 */     this.textIterationsHPC.setText("");
/*  152 */     this.textDataWorkersHPC.setText("");
/*  153 */     this.textProcessingHPC.setText("");
/*  154 */     this.textDataCoordinadorHPC.setText("");
/*  155 */     this.textInstancesHPC.setText("");
/*  156 */     this.textNumProcessesHPC.setText("");
/*  157 */     this.checkBoxWorkersReadHPC.setSelected(false);
/*  158 */     this.checkBoxWorkersWriteHPC.setSelected(false);
/*      */     
/*  160 */     icancloudgui.Utils.Utils.initVMTable(this.tableVM);
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public boolean checkValues()
/*      */   {
/*  175 */     boolean allOK = true;
/*  176 */     int currentRow = 0;
/*      */     
/*      */ 
/*      */ 
/*  180 */     if (!icancloudgui.Utils.Utils.checkName(this.textName.getText()))
/*      */     {
/*  182 */       allOK = false;
/*  183 */       icancloudgui.Utils.Utils.showErrorMessage("Invalid name for this User.\nA name must start with a letter and can contain alphanumeric characters, '-' and '_'", "Wrong name!");
/*      */     }
/*      */     
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*  190 */     if ((this.checkBoxServer.isSelected()) && (allOK))
/*      */     {
/*      */ 
/*      */       try
/*      */       {
/*  195 */         if (allOK) {
/*  196 */           Integer.parseInt(this.textRequestServer.getText());
/*      */         }
/*      */       }
/*      */       catch (Exception e) {
/*  200 */         allOK = false;
/*  201 */         icancloudgui.Utils.Utils.showErrorMessage("Wrong parameter for number of requests (Server).\nThis parameter must be an integer.\n", "Wrong parameter!");
/*      */       }
/*      */       
/*      */ 
/*      */ 
/*      */       try
/*      */       {
/*  208 */         if (allOK) {
/*  209 */           Integer.parseInt(this.textProcessingServer.getText());
/*      */         }
/*      */       }
/*      */       catch (Exception e) {
/*  213 */         allOK = false;
/*  214 */         icancloudgui.Utils.Utils.showErrorMessage("Wrong parameter for processing per request (Server).\nThis parameter must be an integer.\n", "Wrong parameter!");
/*      */       }
/*      */       
/*      */ 
/*      */ 
/*      */       try
/*      */       {
/*  221 */         if (allOK) {
/*  222 */           Integer.parseInt(this.textInstancesServer.getText());
/*      */         }
/*      */       }
/*      */       catch (Exception e) {
/*  226 */         allOK = false;
/*  227 */         icancloudgui.Utils.Utils.showErrorMessage("Wrong parameter for number of instances (Server).\nThis parameter must be an integer.\n", "Wrong parameter!");
/*      */       }
/*      */     }
/*      */     
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*  236 */     if ((this.checkBoxCPU.isSelected()) && (allOK))
/*      */     {
/*      */       try
/*      */       {
/*  240 */         if (allOK) {
/*  241 */           Integer.parseInt(this.textIterationsCPU.getText());
/*      */         }
/*      */       }
/*      */       catch (Exception e) {
/*  245 */         allOK = false;
/*  246 */         icancloudgui.Utils.Utils.showErrorMessage("Wrong parameter for number of iterations (CPU-intensive).\nThis parameter must be an integer.\n", "Wrong parameter!");
/*      */       }
/*      */       
/*      */ 
/*      */ 
/*      */ 
/*      */       try
/*      */       {
/*  254 */         if (allOK) {
/*  255 */           Integer.parseInt(this.textProcessingCPU.getText());
/*      */         }
/*      */       }
/*      */       catch (Exception e) {
/*  259 */         allOK = false;
/*  260 */         icancloudgui.Utils.Utils.showErrorMessage("Wrong parameter for processing CPU (CPU-intensive).\nThis parameter must be an integer.\n", "Wrong parameter!");
/*      */       }
/*      */       
/*      */ 
/*      */ 
/*      */ 
/*      */       try
/*      */       {
/*  268 */         if (allOK) {
/*  269 */           Integer.parseInt(this.textInstancesCPU.getText());
/*      */         }
/*      */       }
/*      */       catch (Exception e) {
/*  273 */         allOK = false;
/*  274 */         icancloudgui.Utils.Utils.showErrorMessage("Wrong parameter for number of instances (CPU-intensive).\nThis parameter must be an integer.\n", "Wrong parameter!");
/*      */       }
/*      */       
/*      */ 
/*      */ 
/*      */ 
/*      */       try
/*      */       {
/*  282 */         if (allOK) {
/*  283 */           Integer.parseInt(this.textInput.getText());
/*      */         }
/*      */       }
/*      */       catch (Exception e) {
/*  287 */         allOK = false;
/*  288 */         icancloudgui.Utils.Utils.showErrorMessage("Wrong parameter for input size (CPU-intensive).\nThis parameter must be an integer.\n", "Wrong parameter!");
/*      */       }
/*      */       
/*      */ 
/*      */ 
/*      */       try
/*      */       {
/*  295 */         if (allOK) {
/*  296 */           Integer.parseInt(this.textOutput.getText());
/*      */         }
/*      */       }
/*      */       catch (Exception e) {
/*  300 */         allOK = false;
/*  301 */         icancloudgui.Utils.Utils.showErrorMessage("Wrong parameter for output size (CPU-intensive).\nThis parameter must be an integer.\n", "Wrong parameter!");
/*      */       }
/*      */     }
/*      */     
/*      */ 
/*      */ 
/*      */ 
/*  308 */     if (this.checkBoxHPC.isSelected())
/*      */     {
/*      */ 
/*      */       try
/*      */       {
/*  313 */         if (allOK) {
/*  314 */           Integer.parseInt(this.textWorkersHPC.getText());
/*      */         }
/*      */       }
/*      */       catch (Exception e) {
/*  318 */         allOK = false;
/*  319 */         icancloudgui.Utils.Utils.showErrorMessage("Wrong parameter for workers set (HPC app).\nThis parameter must be an integer.\n", "Wrong parameter!");
/*      */       }
/*      */       
/*      */ 
/*      */ 
/*      */ 
/*      */       try
/*      */       {
/*  327 */         if (allOK) {
/*  328 */           Integer.parseInt(this.textIterationsHPC.getText());
/*      */         }
/*      */       }
/*      */       catch (Exception e) {
/*  332 */         allOK = false;
/*  333 */         icancloudgui.Utils.Utils.showErrorMessage("Wrong parameter for number of iterations (HPC app).\nThis parameter must be an integer.\n", "Wrong parameter!");
/*      */       }
/*      */       
/*      */ 
/*      */ 
/*      */ 
/*      */       try
/*      */       {
/*  341 */         if (allOK) {
/*  342 */           Integer.parseInt(this.textDataWorkersHPC.getText());
/*      */         }
/*      */       }
/*      */       catch (Exception e) {
/*  346 */         allOK = false;
/*  347 */         icancloudgui.Utils.Utils.showErrorMessage("Wrong parameter for workers data (HPC app).\nThis parameter must be an integer.\n", "Wrong parameter!");
/*      */       }
/*      */       
/*      */ 
/*      */ 
/*      */ 
/*      */       try
/*      */       {
/*  355 */         if (allOK) {
/*  356 */           Integer.parseInt(this.textProcessingHPC.getText());
/*      */         }
/*      */       }
/*      */       catch (Exception e) {
/*  360 */         allOK = false;
/*  361 */         icancloudgui.Utils.Utils.showErrorMessage("Wrong parameter for processing (HPC app).\nThis parameter must be an integer.\n", "Wrong parameter!");
/*      */       }
/*      */       
/*      */ 
/*      */ 
/*      */ 
/*      */       try
/*      */       {
/*  369 */         if (allOK) {
/*  370 */           Integer.parseInt(this.textDataCoordinadorHPC.getText());
/*      */         }
/*      */       }
/*      */       catch (Exception e) {
/*  374 */         allOK = false;
/*  375 */         icancloudgui.Utils.Utils.showErrorMessage("Wrong parameter for coordinator data (HPC app).\nThis parameter must be an integer.\n", "Wrong parameter!");
/*      */       }
/*      */       
/*      */ 
/*      */ 
/*      */ 
/*      */       try
/*      */       {
/*  383 */         if (allOK) {
/*  384 */           Integer.parseInt(this.textInstancesHPC.getText());
/*      */         }
/*      */       }
/*      */       catch (Exception e) {
/*  388 */         allOK = false;
/*  389 */         icancloudgui.Utils.Utils.showErrorMessage("Wrong parameter for number of instances (HPC app).\nThis parameter must be an integer.\n", "Wrong parameter!");
/*      */       }
/*      */       
/*      */ 
/*      */ 
/*      */       try
/*      */       {
/*  396 */         if (allOK) {
/*  397 */           Integer.parseInt(this.textNumProcessesHPC.getText());
/*      */         }
/*      */       }
/*      */       catch (Exception e) {
/*  401 */         allOK = false;
/*  402 */         icancloudgui.Utils.Utils.showErrorMessage("Wrong parameter for number of processes (HPC app).\nThis parameter must be an integer.\n", "Wrong parameter!");
/*      */       }
/*      */     }
/*      */     
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */     try
/*      */     {
/*  413 */       while ((currentRow < this.tableVM.getModel().getRowCount()) && (allOK)) {
/*  414 */         Integer.parseInt((String)this.tableVM.getModel().getValueAt(currentRow, 1));
/*  415 */         currentRow++;
/*      */       }
/*      */       
/*      */     }
/*      */     catch (Exception e)
/*      */     {
/*  421 */       allOK = false;
/*  422 */       icancloudgui.Utils.Utils.showErrorMessage("Wrong parameter for VM instances.\nThis parameter must be an integer.\n", "Wrong parameter!");
/*      */     }
/*      */     
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*  429 */     return allOK;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public icancloudgui.TabbedPanels.dataClasses.User panelToUserObject()
/*      */   {
/*  443 */     icancloudgui.TabbedPanels.dataClasses.User currentUser = new icancloudgui.TabbedPanels.dataClasses.User();
/*  444 */     java.util.LinkedList<icancloudgui.TabbedPanels.dataClasses.VMEntry> list = new java.util.LinkedList();
/*      */     
/*      */ 
/*  447 */     currentUser.setName(this.textName.getText());
/*  448 */     currentUser.setComment(this.textComment.getText());
/*      */     
/*      */ 
/*  451 */     currentUser.setServerAppActive(this.checkBoxServer.isSelected());
/*  452 */     currentUser.setCpuAppActive(this.checkBoxCPU.isSelected());
/*  453 */     currentUser.setHpcAppActive(this.checkBoxHPC.isSelected());
/*      */     
/*      */ 
/*      */ 
/*  457 */     if (this.checkBoxServer.isSelected()) {
/*  458 */       currentUser.getServerApp().setInstances(Integer.parseInt(this.textInstancesServer.getText()));
/*  459 */       currentUser.getServerApp().setProcessing(Integer.parseInt(this.textProcessingServer.getText()));
/*  460 */       currentUser.getServerApp().setRequests(Integer.parseInt(this.textRequestServer.getText()));
/*      */     }
/*      */     
/*      */ 
/*  464 */     if (this.checkBoxCPU.isSelected()) {
/*  465 */       currentUser.getCpuApp().setInstances(Integer.parseInt(this.textInstancesCPU.getText()));
/*  466 */       currentUser.getCpuApp().setIterations(Integer.parseInt(this.textIterationsCPU.getText()));
/*  467 */       currentUser.getCpuApp().setProcessing(Integer.parseInt(this.textProcessingCPU.getText()));
/*  468 */       currentUser.getCpuApp().setInputSize(Integer.parseInt(this.textInput.getText()));
/*  469 */       currentUser.getCpuApp().setOutputSize(Integer.parseInt(this.textOutput.getText()));
/*      */     }
/*      */     
/*      */ 
/*  473 */     if (this.checkBoxHPC.isSelected()) {
/*  474 */       currentUser.getHpcApp().setWorkersSet(Integer.parseInt(this.textWorkersHPC.getText()));
/*  475 */       currentUser.getHpcApp().setIterations(Integer.parseInt(this.textIterationsHPC.getText()));
/*  476 */       currentUser.getHpcApp().setDataWorkers(Integer.parseInt(this.textDataWorkersHPC.getText()));
/*  477 */       currentUser.getHpcApp().setProcessing(Integer.parseInt(this.textProcessingHPC.getText()));
/*  478 */       currentUser.getHpcApp().setDataCoordinator(Integer.parseInt(this.textDataCoordinadorHPC.getText()));
/*  479 */       currentUser.getHpcApp().setWorkersRead(this.checkBoxWorkersReadHPC.isSelected());
/*  480 */       currentUser.getHpcApp().setWorkersWrite(this.checkBoxWorkersWriteHPC.isSelected());
/*  481 */       currentUser.getHpcApp().setInstances(Integer.parseInt(this.textInstancesHPC.getText()));
/*  482 */       currentUser.getHpcApp().setNumProcesses(Integer.parseInt(this.textNumProcessesHPC.getText()));
/*      */     }
/*      */     
/*  485 */     icancloudgui.Utils.Utils.VMTableModelToList((icancloudgui.Tables.UserTableModel)this.tableVM.getModel(), list);
/*  486 */     currentUser.setVMData(list);
/*      */     
/*  488 */     return currentUser;
/*      */   }
/*      */   
/*      */   private javax.swing.JLabel labelRequestServer;
/*      */   private javax.swing.JLabel labelWorkersHPC;
/*      */   private javax.swing.JPanel panelCPU;
/*      */   private javax.swing.JPanel panelHPC;
/*      */   private javax.swing.JPanel panelServer;
/*      */   private javax.swing.JLabel saveLabel;
/*      */   private javax.swing.JTable tableVM;
/*      */   private javax.swing.JScrollPane textArea;
/*      */   private javax.swing.JTextArea textComment;
/*      */   private JTextField textDataCoordinadorHPC;
/*      */   private JTextField textDataWorkersHPC;
/*      */   private JTextField textInput;
/*      */   
/*      */   private boolean writeUsertoFile()
/*      */   {
/*  506 */     boolean allOK = checkValues();
/*      */     
/*      */ 
/*  509 */     if (allOK)
/*      */     {
/*      */ 
/*  512 */       icancloudgui.TabbedPanels.dataClasses.User currentUser = panelToUserObject();
/*      */       
/*      */ 
/*  515 */       String newUserFile = icancloudgui.Utils.Configuration.REPOSITORY_USERS + java.io.File.separatorChar + currentUser.getName();
/*      */       
/*      */ 
/*      */       try
/*      */       {
/*  520 */         if (new java.io.File(newUserFile).exists())
/*      */         {
/*      */ 
/*  523 */           int response = javax.swing.JOptionPane.showConfirmDialog(null, "Overwrite existing User?", "Confirm Overwrite", 2, 3);
/*      */           
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*  530 */           if (response == 0)
/*      */           {
/*      */ 
/*  533 */             java.io.FileOutputStream fout = new java.io.FileOutputStream(newUserFile);
/*  534 */             java.io.ObjectOutputStream oos = new java.io.ObjectOutputStream(fout);
/*  535 */             oos.writeObject(currentUser);
/*  536 */             oos.close();
/*  537 */             allOK = true;
/*      */           }
/*      */           
/*      */ 
/*      */         }
/*      */         else
/*      */         {
/*      */ 
/*  545 */           java.io.FileOutputStream fout = new java.io.FileOutputStream(newUserFile);
/*  546 */           java.io.ObjectOutputStream oos = new java.io.ObjectOutputStream(fout);
/*  547 */           oos.writeObject(currentUser);
/*  548 */           oos.close();
/*  549 */           allOK = true;
/*      */         }
/*      */       }
/*      */       catch (Exception e) {
/*  553 */         e.printStackTrace();
/*      */       }
/*      */       
/*      */ 
/*      */     }
/*      */     else
/*      */     {
/*  560 */       javax.swing.JOptionPane.showMessageDialog(new javax.swing.JFrame(), "Wrong values. Please, re-check", "Error", 0);
/*      */     }
/*      */     
/*      */ 
/*  564 */     return allOK;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   private void loadUserObjectInPanel(icancloudgui.TabbedPanels.dataClasses.User newUser)
/*      */   {
/*  577 */     clear();
/*  578 */     this.textName.setText(newUser.getName());
/*  579 */     this.textComment.setText(newUser.getComment());
/*      */     
/*      */ 
/*  582 */     if (newUser.isServerAppActive()) {
/*  583 */       this.textRequestServer.setText(Integer.toString(newUser.getServerApp().getRequests()));
/*  584 */       this.textProcessingServer.setText(Integer.toString(newUser.getServerApp().getProcessing()));
/*  585 */       this.textInstancesServer.setText(Integer.toString(newUser.getServerApp().getInstances()));
/*  586 */       this.checkBoxServer.setSelected(newUser.isServerAppActive());
/*      */     }
/*      */     else {
/*  589 */       this.checkBoxServer.setSelected(false);
/*      */     }
/*      */     
/*      */ 
/*  593 */     if (newUser.isCpuAppActive()) {
/*  594 */       this.textIterationsCPU.setText(Integer.toString(newUser.getCpuApp().getIterations()));
/*  595 */       this.textProcessingCPU.setText(Integer.toString(newUser.getCpuApp().getProcessing()));
/*  596 */       this.textInstancesCPU.setText(Integer.toString(newUser.getCpuApp().getInstances()));
/*  597 */       this.checkBoxCPU.setSelected(newUser.isCpuAppActive());
/*  598 */       this.textInput.setText(Integer.toString(newUser.getCpuApp().getInputSize()));
/*  599 */       this.textOutput.setText(Integer.toString(newUser.getCpuApp().getOutputSize()));
/*      */     }
/*      */     else {
/*  602 */       this.checkBoxCPU.setSelected(false);
/*      */     }
/*      */     
/*      */ 
/*  606 */     if (newUser.isHpcAppActive()) {
/*  607 */       this.textWorkersHPC.setText(Integer.toString(newUser.getHpcApp().getWorkersSet()));
/*  608 */       this.textIterationsHPC.setText(Integer.toString(newUser.getHpcApp().getIterations()));
/*  609 */       this.textDataWorkersHPC.setText(Integer.toString(newUser.getHpcApp().getDataWorkers()));
/*  610 */       this.textProcessingHPC.setText(Integer.toString(newUser.getHpcApp().getProcessing()));
/*  611 */       this.textDataCoordinadorHPC.setText(Integer.toString(newUser.getHpcApp().getDataCoordinator()));
/*  612 */       this.textInstancesHPC.setText(Integer.toString(newUser.getHpcApp().getInstances()));
/*  613 */       this.textNumProcessesHPC.setText(Integer.toString(newUser.getHpcApp().getNumProcesses()));
/*  614 */       this.checkBoxWorkersReadHPC.setSelected(newUser.getHpcApp().isWorkersRead());
/*  615 */       this.checkBoxWorkersWriteHPC.setSelected(newUser.getHpcApp().isWorkersWrite());
/*  616 */       this.checkBoxHPC.setSelected(newUser.isHpcAppActive());
/*      */     }
/*      */     else {
/*  619 */       this.checkBoxHPC.setSelected(false);
/*      */     }
/*      */     
/*      */ 
/*  623 */     icancloudgui.Tables.UserTableModel model = new icancloudgui.Tables.UserTableModel();
/*  624 */     icancloudgui.Utils.Utils.VMListToTable(newUser.getVMData(), model);
/*  625 */     this.tableVM.setModel(model);
/*  626 */     this.tableVM.updateUI();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void loadUserInPanel(String userName)
/*      */   {
/*  639 */     icancloudgui.TabbedPanels.dataClasses.User loadedUser = icancloudgui.Utils.Utils.loadUserObject(userName);
/*      */     
/*      */ 
/*  642 */     if (loadedUser != null) {
/*  643 */       loadUserObjectInPanel(loadedUser);
/*  644 */       checkApplications(); } }
/*      */   
/*      */   private JTextField textInstancesCPU;
/*      */   private JTextField textInstancesHPC;
/*      */   private JTextField textInstancesServer;
/*      */   private JTextField textIterationsCPU;
/*      */   private JTextField textIterationsHPC;
/*      */   private JTextField textName;
/*      */   private JTextField textNumProcessesHPC;
/*      */   private JTextField textOutput;
/*      */   private JTextField textProcessingCPU;
/*      */   private JTextField textProcessingHPC;
/*      */   private JTextField textProcessingServer;
/*      */   private JTextField textRequestServer;
/*      */   private JTextField textWorkersHPC;
/*  659 */   private void initComponents() { this.labelName = new javax.swing.JLabel();
/*  660 */     this.textName = new JTextField();
/*  661 */     this.labelHelp = new javax.swing.JLabel();
/*  662 */     this.saveLabel = new javax.swing.JLabel();
/*  663 */     this.textArea = new javax.swing.JScrollPane();
/*  664 */     this.textComment = new javax.swing.JTextArea();
/*  665 */     this.labelComment = new javax.swing.JLabel();
/*  666 */     this.checkBoxHPC = new javax.swing.JCheckBox();
/*  667 */     this.panelHPC = new javax.swing.JPanel();
/*  668 */     this.labelWorkersHPC = new javax.swing.JLabel();
/*  669 */     this.labelProcessingHPC = new javax.swing.JLabel();
/*  670 */     this.labelIterationsHPC = new javax.swing.JLabel();
/*  671 */     this.labelDataCoordinadorHPC = new javax.swing.JLabel();
/*  672 */     this.labelDataWorkersHPC = new javax.swing.JLabel();
/*  673 */     this.checkBoxWorkersReadHPC = new javax.swing.JCheckBox();
/*  674 */     this.checkBoxWorkersWriteHPC = new javax.swing.JCheckBox();
/*  675 */     this.textIterationsHPC = new JTextField();
/*  676 */     this.textDataWorkersHPC = new JTextField();
/*  677 */     this.textProcessingHPC = new JTextField();
/*  678 */     this.textDataCoordinadorHPC = new JTextField();
/*  679 */     this.textWorkersHPC = new JTextField();
/*  680 */     this.labelInstancesHPC = new javax.swing.JLabel();
/*  681 */     this.textInstancesHPC = new JTextField();
/*  682 */     this.labelNumProcessesHPC = new javax.swing.JLabel();
/*  683 */     this.textNumProcessesHPC = new JTextField();
/*  684 */     this.checkBoxServer = new javax.swing.JCheckBox();
/*  685 */     this.panelServer = new javax.swing.JPanel();
/*  686 */     this.labelRequestServer = new javax.swing.JLabel();
/*  687 */     this.labelProcessingServer = new javax.swing.JLabel();
/*  688 */     this.textRequestServer = new JTextField();
/*  689 */     this.textProcessingServer = new JTextField();
/*  690 */     this.labelInstancesServer = new javax.swing.JLabel();
/*  691 */     this.textInstancesServer = new JTextField();
/*  692 */     this.checkBoxCPU = new javax.swing.JCheckBox();
/*  693 */     this.panelCPU = new javax.swing.JPanel();
/*  694 */     this.labelProcessingCPU = new javax.swing.JLabel();
/*  695 */     this.labelIterationsCPU = new javax.swing.JLabel();
/*  696 */     this.textIterationsCPU = new JTextField();
/*  697 */     this.textProcessingCPU = new JTextField();
/*  698 */     this.labelInstancesCPU = new javax.swing.JLabel();
/*  699 */     this.textInstancesCPU = new JTextField();
/*  700 */     this.labelInput = new javax.swing.JLabel();
/*  701 */     this.labelOutput = new javax.swing.JLabel();
/*  702 */     this.textInput = new JTextField();
/*  703 */     this.textOutput = new JTextField();
/*  704 */     this.buttonClear = new javax.swing.JLabel();
/*  705 */     this.jScrollPane1 = new javax.swing.JScrollPane();
/*  706 */     this.tableVM = new javax.swing.JTable();
/*      */     
/*  708 */     org.jdesktop.application.ResourceMap resourceMap = ((icancloudgui.ICanCloudGUIApp)org.jdesktop.application.Application.getInstance(icancloudgui.ICanCloudGUIApp.class)).getContext().getResourceMap(UsersPanel.class);
/*  709 */     setBackground(resourceMap.getColor("Form.background"));
/*  710 */     setName("Form");
/*  711 */     setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());
/*      */     
/*  713 */     this.labelName.setFont(resourceMap.getFont("labelComment.font"));
/*  714 */     this.labelName.setText(resourceMap.getString("labelName.text", new Object[0]));
/*  715 */     this.labelName.setName("labelName");
/*  716 */     add(this.labelName, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 40, -1, -1));
/*      */     
/*  718 */     this.textName.setText(resourceMap.getString("textName.text", new Object[0]));
/*  719 */     this.textName.setName("textName");
/*  720 */     add(this.textName, new org.netbeans.lib.awtextra.AbsoluteConstraints(66, 34, 260, -1));
/*      */     
/*  722 */     this.labelHelp.setIcon(resourceMap.getIcon("labelHelp.icon"));
/*  723 */     this.labelHelp.setText(resourceMap.getString("labelHelp.text", new Object[0]));
/*  724 */     this.labelHelp.setToolTipText(resourceMap.getString("labelHelp.toolTipText", new Object[0]));
/*  725 */     this.labelHelp.setName("labelHelp");
/*  726 */     this.labelHelp.addMouseListener(new java.awt.event.MouseAdapter() {
/*      */       public void mouseClicked(java.awt.event.MouseEvent evt) {
/*  728 */         UsersPanel.this.labelHelpMouseClicked(evt);
/*      */       }
/*  730 */     });
/*  731 */     add(this.labelHelp, new org.netbeans.lib.awtextra.AbsoluteConstraints(750, 20, -1, -1));
/*      */     
/*  733 */     this.saveLabel.setIcon(resourceMap.getIcon("saveLabel.icon"));
/*  734 */     this.saveLabel.setText(resourceMap.getString("saveLabel.text", new Object[0]));
/*  735 */     this.saveLabel.setToolTipText(resourceMap.getString("saveLabel.toolTipText", new Object[0]));
/*  736 */     this.saveLabel.setName("saveLabel");
/*  737 */     this.saveLabel.addMouseListener(new java.awt.event.MouseAdapter() {
/*      */       public void mouseClicked(java.awt.event.MouseEvent evt) {
/*  739 */         UsersPanel.this.saveLabelMouseClicked(evt);
/*      */       }
/*  741 */     });
/*  742 */     add(this.saveLabel, new org.netbeans.lib.awtextra.AbsoluteConstraints(710, 20, -1, -1));
/*      */     
/*  744 */     this.textArea.setName("textArea");
/*      */     
/*  746 */     this.textComment.setColumns(20);
/*  747 */     this.textComment.setRows(5);
/*  748 */     this.textComment.setName("textComment");
/*  749 */     this.textArea.setViewportView(this.textComment);
/*      */     
/*  751 */     add(this.textArea, new org.netbeans.lib.awtextra.AbsoluteConstraints(420, 450, 360, 90));
/*      */     
/*  753 */     this.labelComment.setFont(resourceMap.getFont("labelComment.font"));
/*  754 */     this.labelComment.setText(resourceMap.getString("labelComment.text", new Object[0]));
/*  755 */     this.labelComment.setName("labelComment");
/*  756 */     add(this.labelComment, new org.netbeans.lib.awtextra.AbsoluteConstraints(420, 430, -1, -1));
/*      */     
/*  758 */     this.checkBoxHPC.setBackground(resourceMap.getColor("checkBoxHPC.background"));
/*  759 */     this.checkBoxHPC.setFont(resourceMap.getFont("labelComment.font"));
/*  760 */     this.checkBoxHPC.setText(resourceMap.getString("checkBoxHPC.text", new Object[0]));
/*  761 */     this.checkBoxHPC.setHorizontalTextPosition(2);
/*  762 */     this.checkBoxHPC.setName("checkBoxHPC");
/*  763 */     this.checkBoxHPC.setOpaque(true);
/*  764 */     this.checkBoxHPC.addItemListener(new java.awt.event.ItemListener() {
/*      */       public void itemStateChanged(java.awt.event.ItemEvent evt) {
/*  766 */         UsersPanel.this.checkBoxHPCItemStateChanged(evt);
/*      */       }
/*  768 */     });
/*  769 */     add(this.checkBoxHPC, new org.netbeans.lib.awtextra.AbsoluteConstraints(430, 80, -1, -1));
/*      */     
/*  771 */     this.panelHPC.setBackground(resourceMap.getColor("panelHPC.background"));
/*  772 */     this.panelHPC.setBorder(javax.swing.BorderFactory.createEtchedBorder());
/*  773 */     this.panelHPC.setName("panelHPC");
/*  774 */     this.panelHPC.setOpaque(false);
/*  775 */     this.panelHPC.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());
/*      */     
/*  777 */     this.labelWorkersHPC.setFont(resourceMap.getFont("labelComment.font"));
/*  778 */     this.labelWorkersHPC.setText(resourceMap.getString("labelWorkersHPC.text", new Object[0]));
/*  779 */     this.labelWorkersHPC.setName("labelWorkersHPC");
/*  780 */     this.panelHPC.add(this.labelWorkersHPC, new org.netbeans.lib.awtextra.AbsoluteConstraints(100, 30, -1, -1));
/*      */     
/*  782 */     this.labelProcessingHPC.setFont(resourceMap.getFont("labelComment.font"));
/*  783 */     this.labelProcessingHPC.setText(resourceMap.getString("labelProcessingHPC.text", new Object[0]));
/*  784 */     this.labelProcessingHPC.setName("labelProcessingHPC");
/*  785 */     this.panelHPC.add(this.labelProcessingHPC, new org.netbeans.lib.awtextra.AbsoluteConstraints(65, 120, -1, 20));
/*      */     
/*  787 */     this.labelIterationsHPC.setFont(resourceMap.getFont("labelComment.font"));
/*  788 */     this.labelIterationsHPC.setText(resourceMap.getString("labelIterationsHPC.text", new Object[0]));
/*  789 */     this.labelIterationsHPC.setName("labelIterationsHPC");
/*  790 */     this.panelHPC.add(this.labelIterationsHPC, new org.netbeans.lib.awtextra.AbsoluteConstraints(100, 60, -1, 20));
/*      */     
/*  792 */     this.labelDataCoordinadorHPC.setFont(resourceMap.getFont("labelComment.font"));
/*  793 */     this.labelDataCoordinadorHPC.setText(resourceMap.getString("labelDataCoordinadorHPC.text", new Object[0]));
/*  794 */     this.labelDataCoordinadorHPC.setName("labelDataCoordinadorHPC");
/*  795 */     this.panelHPC.add(this.labelDataCoordinadorHPC, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 150, -1, 20));
/*      */     
/*  797 */     this.labelDataWorkersHPC.setFont(resourceMap.getFont("labelComment.font"));
/*  798 */     this.labelDataWorkersHPC.setText(resourceMap.getString("labelDataWorkersHPC.text", new Object[0]));
/*  799 */     this.labelDataWorkersHPC.setName("labelDataWorkersHPC");
/*  800 */     this.panelHPC.add(this.labelDataWorkersHPC, new org.netbeans.lib.awtextra.AbsoluteConstraints(25, 90, -1, 20));
/*      */     
/*  802 */     this.checkBoxWorkersReadHPC.setFont(resourceMap.getFont("labelComment.font"));
/*  803 */     this.checkBoxWorkersReadHPC.setText(resourceMap.getString("checkBoxWorkersReadHPC.text", new Object[0]));
/*  804 */     this.checkBoxWorkersReadHPC.setHorizontalTextPosition(2);
/*  805 */     this.checkBoxWorkersReadHPC.setName("checkBoxWorkersReadHPC");
/*  806 */     this.panelHPC.add(this.checkBoxWorkersReadHPC, new org.netbeans.lib.awtextra.AbsoluteConstraints(40, 180, -1, -1));
/*      */     
/*  808 */     this.checkBoxWorkersWriteHPC.setFont(resourceMap.getFont("labelComment.font"));
/*  809 */     this.checkBoxWorkersWriteHPC.setText(resourceMap.getString("checkBoxWorkersWriteHPC.text", new Object[0]));
/*  810 */     this.checkBoxWorkersWriteHPC.setHorizontalTextPosition(2);
/*  811 */     this.checkBoxWorkersWriteHPC.setName("checkBoxWorkersWriteHPC");
/*  812 */     this.panelHPC.add(this.checkBoxWorkersWriteHPC, new org.netbeans.lib.awtextra.AbsoluteConstraints(40, 210, -1, -1));
/*      */     
/*  814 */     this.textIterationsHPC.setText(resourceMap.getString("textIterationsHPC.text", new Object[0]));
/*  815 */     this.textIterationsHPC.setName("textIterationsHPC");
/*  816 */     this.panelHPC.add(this.textIterationsHPC, new org.netbeans.lib.awtextra.AbsoluteConstraints(190, 55, 140, -1));
/*      */     
/*  818 */     this.textDataWorkersHPC.setFont(resourceMap.getFont("textDataWorkersHPC.font"));
/*  819 */     this.textDataWorkersHPC.setText(resourceMap.getString("textDataWorkersHPC.text", new Object[0]));
/*  820 */     this.textDataWorkersHPC.setName("textDataWorkersHPC");
/*  821 */     this.panelHPC.add(this.textDataWorkersHPC, new org.netbeans.lib.awtextra.AbsoluteConstraints(190, 85, 140, -1));
/*      */     
/*  823 */     this.textProcessingHPC.setText(resourceMap.getString("textProcessingHPC.text", new Object[0]));
/*  824 */     this.textProcessingHPC.setName("textProcessingHPC");
/*  825 */     this.panelHPC.add(this.textProcessingHPC, new org.netbeans.lib.awtextra.AbsoluteConstraints(190, 115, 140, -1));
/*      */     
/*  827 */     this.textDataCoordinadorHPC.setText(resourceMap.getString("textDataCoordinadorHPC.text", new Object[0]));
/*  828 */     this.textDataCoordinadorHPC.setName("textDataCoordinadorHPC");
/*  829 */     this.panelHPC.add(this.textDataCoordinadorHPC, new org.netbeans.lib.awtextra.AbsoluteConstraints(190, 145, 140, -1));
/*      */     
/*  831 */     this.textWorkersHPC.setText(resourceMap.getString("textWorkersHPC.text", new Object[0]));
/*  832 */     this.textWorkersHPC.setName("textWorkersHPC");
/*  833 */     this.panelHPC.add(this.textWorkersHPC, new org.netbeans.lib.awtextra.AbsoluteConstraints(190, 25, 140, -1));
/*      */     
/*  835 */     this.labelInstancesHPC.setFont(resourceMap.getFont("labelComment.font"));
/*  836 */     this.labelInstancesHPC.setText(resourceMap.getString("labelInstancesHPC.text", new Object[0]));
/*  837 */     this.labelInstancesHPC.setName("labelInstancesHPC");
/*  838 */     this.panelHPC.add(this.labelInstancesHPC, new org.netbeans.lib.awtextra.AbsoluteConstraints(73, 240, -1, -1));
/*      */     
/*  840 */     this.textInstancesHPC.setText(resourceMap.getString("textInstancesHPC.text", new Object[0]));
/*  841 */     this.textInstancesHPC.setName("textInstancesHPC");
/*  842 */     this.panelHPC.add(this.textInstancesHPC, new org.netbeans.lib.awtextra.AbsoluteConstraints(190, 235, 140, -1));
/*      */     
/*  844 */     this.labelNumProcessesHPC.setFont(resourceMap.getFont("labelComment.font"));
/*  845 */     this.labelNumProcessesHPC.setText(resourceMap.getString("labelNumProcessesHPC.text", new Object[0]));
/*  846 */     this.labelNumProcessesHPC.setName("labelNumProcessesHPC");
/*  847 */     this.panelHPC.add(this.labelNumProcessesHPC, new org.netbeans.lib.awtextra.AbsoluteConstraints(40, 270, -1, -1));
/*      */     
/*  849 */     this.textNumProcessesHPC.setText(resourceMap.getString("textNumProcessesHPC.text", new Object[0]));
/*  850 */     this.textNumProcessesHPC.setName("textNumProcessesHPC");
/*  851 */     this.panelHPC.add(this.textNumProcessesHPC, new org.netbeans.lib.awtextra.AbsoluteConstraints(190, 265, 140, -1));
/*      */     
/*  853 */     add(this.panelHPC, new org.netbeans.lib.awtextra.AbsoluteConstraints(420, 90, 360, 310));
/*      */     
/*  855 */     this.checkBoxServer.setBackground(resourceMap.getColor("checkBoxServer.background"));
/*  856 */     this.checkBoxServer.setFont(resourceMap.getFont("labelComment.font"));
/*  857 */     this.checkBoxServer.setText(resourceMap.getString("checkBoxServer.text", new Object[0]));
/*  858 */     this.checkBoxServer.setHorizontalTextPosition(2);
/*  859 */     this.checkBoxServer.setName("checkBoxServer");
/*  860 */     this.checkBoxServer.setOpaque(true);
/*  861 */     this.checkBoxServer.addItemListener(new java.awt.event.ItemListener() {
/*      */       public void itemStateChanged(java.awt.event.ItemEvent evt) {
/*  863 */         UsersPanel.this.checkBoxServerItemStateChanged(evt);
/*      */       }
/*  865 */     });
/*  866 */     add(this.checkBoxServer, new org.netbeans.lib.awtextra.AbsoluteConstraints(40, 80, -1, -1));
/*      */     
/*  868 */     this.panelServer.setBackground(resourceMap.getColor("panelServer.background"));
/*  869 */     this.panelServer.setBorder(javax.swing.BorderFactory.createEtchedBorder());
/*  870 */     this.panelServer.setName("panelServer");
/*  871 */     this.panelServer.setOpaque(false);
/*  872 */     this.panelServer.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());
/*      */     
/*  874 */     this.labelRequestServer.setFont(resourceMap.getFont("labelComment.font"));
/*  875 */     this.labelRequestServer.setText(resourceMap.getString("labelRequestServer.text", new Object[0]));
/*  876 */     this.labelRequestServer.setName("labelRequestServer");
/*  877 */     this.panelServer.add(this.labelRequestServer, new org.netbeans.lib.awtextra.AbsoluteConstraints(90, 30, -1, -1));
/*      */     
/*  879 */     this.labelProcessingServer.setFont(resourceMap.getFont("labelComment.font"));
/*  880 */     this.labelProcessingServer.setText(resourceMap.getString("labelProcessingServer.text", new Object[0]));
/*  881 */     this.labelProcessingServer.setName("labelProcessingServer");
/*  882 */     this.panelServer.add(this.labelProcessingServer, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 60, -1, 20));
/*      */     
/*  884 */     this.textRequestServer.setText(resourceMap.getString("textRequestServer.text", new Object[0]));
/*  885 */     this.textRequestServer.setName("textRequestServer");
/*  886 */     this.panelServer.add(this.textRequestServer, new org.netbeans.lib.awtextra.AbsoluteConstraints(190, 25, 140, -1));
/*      */     
/*  888 */     this.textProcessingServer.setText(resourceMap.getString("textProcessingServer.text", new Object[0]));
/*  889 */     this.textProcessingServer.setName("textProcessingServer");
/*  890 */     this.panelServer.add(this.textProcessingServer, new org.netbeans.lib.awtextra.AbsoluteConstraints(190, 55, 140, -1));
/*      */     
/*  892 */     this.labelInstancesServer.setFont(resourceMap.getFont("labelComment.font"));
/*  893 */     this.labelInstancesServer.setText(resourceMap.getString("labelInstancesServer.text", new Object[0]));
/*  894 */     this.labelInstancesServer.setName("labelInstancesServer");
/*  895 */     this.panelServer.add(this.labelInstancesServer, new org.netbeans.lib.awtextra.AbsoluteConstraints(60, 90, -1, -1));
/*      */     
/*  897 */     this.textInstancesServer.setText(resourceMap.getString("textInstancesServer.text", new Object[0]));
/*  898 */     this.textInstancesServer.setName("textInstancesServer");
/*  899 */     this.panelServer.add(this.textInstancesServer, new org.netbeans.lib.awtextra.AbsoluteConstraints(190, 85, 140, -1));
/*      */     
/*  901 */     add(this.panelServer, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 90, 350, 130));
/*      */     
/*  903 */     this.checkBoxCPU.setBackground(resourceMap.getColor("checkBoxCPU.background"));
/*  904 */     this.checkBoxCPU.setFont(resourceMap.getFont("labelComment.font"));
/*  905 */     this.checkBoxCPU.setText(resourceMap.getString("checkBoxCPU.text", new Object[0]));
/*  906 */     this.checkBoxCPU.setHorizontalTextPosition(2);
/*  907 */     this.checkBoxCPU.setName("checkBoxCPU");
/*  908 */     this.checkBoxCPU.setOpaque(true);
/*  909 */     this.checkBoxCPU.addItemListener(new java.awt.event.ItemListener() {
/*      */       public void itemStateChanged(java.awt.event.ItemEvent evt) {
/*  911 */         UsersPanel.this.checkBoxCPUItemStateChanged(evt);
/*      */       }
/*  913 */     });
/*  914 */     add(this.checkBoxCPU, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 240, -1, -1));
/*      */     
/*  916 */     this.panelCPU.setBackground(resourceMap.getColor("panelCPU.background"));
/*  917 */     this.panelCPU.setBorder(javax.swing.BorderFactory.createEtchedBorder());
/*  918 */     this.panelCPU.setName("panelCPU");
/*  919 */     this.panelCPU.setOpaque(false);
/*  920 */     this.panelCPU.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());
/*      */     
/*  922 */     this.labelProcessingCPU.setFont(resourceMap.getFont("labelComment.font"));
/*  923 */     this.labelProcessingCPU.setText(resourceMap.getString("labelProcessingCPU.text", new Object[0]));
/*  924 */     this.labelProcessingCPU.setName("labelProcessingCPU");
/*  925 */     this.panelCPU.add(this.labelProcessingCPU, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 60, -1, 20));
/*      */     
/*  927 */     this.labelIterationsCPU.setFont(resourceMap.getFont("labelComment.font"));
/*  928 */     this.labelIterationsCPU.setText(resourceMap.getString("labelIterationsCPU.text", new Object[0]));
/*  929 */     this.labelIterationsCPU.setName("labelIterationsCPU");
/*  930 */     this.panelCPU.add(this.labelIterationsCPU, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 30, -1, -1));
/*      */     
/*  932 */     this.textIterationsCPU.setText(resourceMap.getString("textIterationsCPU.text", new Object[0]));
/*  933 */     this.textIterationsCPU.setName("textIterationsCPU");
/*  934 */     this.panelCPU.add(this.textIterationsCPU, new org.netbeans.lib.awtextra.AbsoluteConstraints(95, 25, 50, -1));
/*      */     
/*  936 */     this.textProcessingCPU.setText(resourceMap.getString("textProcessingCPU.text", new Object[0]));
/*  937 */     this.textProcessingCPU.setName("textProcessingCPU");
/*  938 */     this.panelCPU.add(this.textProcessingCPU, new org.netbeans.lib.awtextra.AbsoluteConstraints(200, 55, 130, -1));
/*      */     
/*  940 */     this.labelInstancesCPU.setFont(resourceMap.getFont("labelComment.font"));
/*  941 */     this.labelInstancesCPU.setText(resourceMap.getString("labelInstancesCPU.text", new Object[0]));
/*  942 */     this.labelInstancesCPU.setName("labelInstancesCPU");
/*  943 */     this.panelCPU.add(this.labelInstancesCPU, new org.netbeans.lib.awtextra.AbsoluteConstraints(170, 30, -1, -1));
/*      */     
/*  945 */     this.textInstancesCPU.setText(resourceMap.getString("textInstancesCPU.text", new Object[0]));
/*  946 */     this.textInstancesCPU.setName("textInstancesCPU");
/*  947 */     this.panelCPU.add(this.textInstancesCPU, new org.netbeans.lib.awtextra.AbsoluteConstraints(280, 25, 50, -1));
/*      */     
/*  949 */     this.labelInput.setText(resourceMap.getString("labelInput.text", new Object[0]));
/*  950 */     this.labelInput.setName("labelInput");
/*  951 */     this.panelCPU.add(this.labelInput, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 95, -1, -1));
/*      */     
/*  953 */     this.labelOutput.setText(resourceMap.getString("labelOutput.text", new Object[0]));
/*  954 */     this.labelOutput.setName("labelOutput");
/*  955 */     this.panelCPU.add(this.labelOutput, new org.netbeans.lib.awtextra.AbsoluteConstraints(200, 95, -1, -1));
/*      */     
/*  957 */     this.textInput.setText(resourceMap.getString("textInput.text", new Object[0]));
/*  958 */     this.textInput.setName("textInput");
/*  959 */     this.panelCPU.add(this.textInput, new org.netbeans.lib.awtextra.AbsoluteConstraints(95, 90, 50, -1));
/*      */     
/*  961 */     this.textOutput.setText(resourceMap.getString("textOutput.text", new Object[0]));
/*  962 */     this.textOutput.setName("textOutput");
/*  963 */     this.panelCPU.add(this.textOutput, new org.netbeans.lib.awtextra.AbsoluteConstraints(280, 90, 50, -1));
/*      */     
/*  965 */     add(this.panelCPU, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 250, 350, 130));
/*      */     
/*  967 */     this.buttonClear.setIcon(resourceMap.getIcon("buttonClear.icon"));
/*  968 */     this.buttonClear.setToolTipText(resourceMap.getString("buttonClear.toolTipText", new Object[0]));
/*  969 */     this.buttonClear.setName("buttonClear");
/*  970 */     this.buttonClear.addMouseListener(new java.awt.event.MouseAdapter() {
/*      */       public void mouseClicked(java.awt.event.MouseEvent evt) {
/*  972 */         UsersPanel.this.buttonClearMouseClicked(evt);
/*      */       }
/*  974 */     });
/*  975 */     add(this.buttonClear, new org.netbeans.lib.awtextra.AbsoluteConstraints(670, 20, -1, -1));
/*      */     
/*  977 */     this.jScrollPane1.setName("jScrollPane1");
/*      */     
/*  979 */     this.tableVM.setModel(new javax.swing.table.DefaultTableModel(new Object[][] { { null, null }, { null, null }, { null, null }, { null, null } }, new String[] { "Virtual Machine", "Instances" })
/*      */     {
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*  990 */       Class[] types = { String.class, String.class };
/*      */       
/*      */ 
/*      */       public Class getColumnClass(int columnIndex)
/*      */       {
/*  995 */         return this.types[columnIndex];
/*      */       }
/*  997 */     });
/*  998 */     this.tableVM.setGridColor(resourceMap.getColor("tableVM.gridColor"));
/*  999 */     this.tableVM.setName("tableVM");
/* 1000 */     this.jScrollPane1.setViewportView(this.tableVM);
/* 1001 */     this.tableVM.getColumnModel().getColumn(0).setHeaderValue(resourceMap.getString("tableVM.columnModel.title0", new Object[0]));
/* 1002 */     this.tableVM.getColumnModel().getColumn(1).setHeaderValue(resourceMap.getString("tableVM.columnModel.title1", new Object[0]));
/*      */     
/* 1004 */     add(this.jScrollPane1, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 400, 350, 140));
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   private void labelHelpMouseClicked(java.awt.event.MouseEvent evt)
/*      */   {
/* 1015 */     icancloudgui.Utils.Utils.showHelp(this);
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   private void saveLabelMouseClicked(java.awt.event.MouseEvent evt)
/*      */   {
/* 1028 */     boolean allOK = writeUsertoFile();
/*      */     
/*      */ 
/* 1031 */     if (allOK) {
/* 1032 */       this.mainFrame.updateTree();
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */   private void buttonClearMouseClicked(java.awt.event.MouseEvent evt)
/*      */   {
/* 1040 */     int response = javax.swing.JOptionPane.showConfirmDialog(null, "Clear current User configuration?", "Confirm to clear this form", 2, 3);
/*      */     
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/* 1047 */     if (response == 0) {
/* 1048 */       clear();
/*      */     }
/*      */   }
/*      */   
/*      */   private void checkBoxHPCItemStateChanged(java.awt.event.ItemEvent evt) {
/* 1053 */     checkApplications();
/*      */   }
/*      */   
/*      */   private void checkBoxServerItemStateChanged(java.awt.event.ItemEvent evt) {
/* 1057 */     checkApplications();
/*      */   }
/*      */   
/*      */   private void checkBoxCPUItemStateChanged(java.awt.event.ItemEvent evt) {
/* 1061 */     checkApplications();
/*      */   }
/*      */ }


/* Location:              /home/chverma/Descargas/iCanCloudGUI/iCanCloudGUI.jar!/icancloudgui/TabbedPanels/UsersPanel.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */