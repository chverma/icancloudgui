/*     */ package icancloudgui.TabbedPanels;
/*     */ 
/*     */ import icancloudgui.TabbedPanels.dataClasses.PSU;
/*     */ import java.awt.event.MouseEvent;
/*     */ import javax.swing.JLabel;
/*     */ import javax.swing.JTable;
/*     */ import javax.swing.JTextField;
/*     */ import org.jdesktop.application.ResourceMap;
/*     */ 
/*     */ public class PSUsPanel extends javax.swing.JPanel
/*     */ {
/*     */   icancloudgui.ICanCloudGUIView mainFrame;
/*     */   private JTable PSUsTable;
/*     */   private JLabel buttonClear;
/*     */   private JLabel buttonHelp;
/*     */   private JLabel buttonResize;
/*     */   private JLabel buttonSave;
/*     */   private JLabel labelComment;
/*     */   private JLabel labelEnergy;
/*     */   private JLabel labelName;
/*     */   private JLabel labelResize;
/*     */   private JLabel labelScale;
/*     */   private JLabel labelWattage;
/*     */   private javax.swing.JScrollPane scrollTable;
/*     */   private javax.swing.JScrollPane scrollTextArea;
/*     */   private javax.swing.JTextArea textAreaComment;
/*     */   private JTextField textName;
/*     */   private JTextField textNewNumRows;
/*     */   private JTextField textScale;
/*     */   private JTextField textWattage;
/*     */   
/*     */   public PSUsPanel(icancloudgui.ICanCloudGUIView mainFrame)
/*     */   {
/*  34 */     initComponents();
/*  35 */     icancloudgui.Utils.Utils.initEnergyTable(this.PSUsTable);
/*  36 */     this.mainFrame = mainFrame;
/*     */     
/*     */ 
/*  39 */     initDefaultTableValues();
/*     */     
/*     */ 
/*  42 */     this.labelResize.setVisible(false);
/*  43 */     this.buttonResize.setVisible(false);
/*  44 */     this.textNewNumRows.setVisible(false);
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public boolean checkValues()
/*     */   {
/*  57 */     boolean allOK = true;
/*     */     
/*     */ 
/*  60 */     if (!icancloudgui.Utils.Utils.checkName(this.textName.getText()))
/*     */     {
/*  62 */       allOK = false;
/*  63 */       icancloudgui.Utils.Utils.showErrorMessage("Invalid name for this PSU.\nA name must start with a letter and can contain alphanumeric characters, '-' and '_'", "Wrong name!");
/*     */     }
/*     */     
/*     */ 
/*     */ 
/*     */     try
/*     */     {
/*  70 */       if (allOK) {
/*  71 */         Double.parseDouble(this.textWattage.getText());
/*     */       }
/*     */     }
/*     */     catch (Exception e) {
/*  75 */       allOK = false;
/*  76 */       icancloudgui.Utils.Utils.showErrorMessage("Wrong parameter for wattage.\nThis parameter must be a double.\n", "Wrong parameter!");
/*     */     }
/*     */     
/*     */ 
/*     */ 
/*     */     try
/*     */     {
/*  83 */       if (allOK) {
/*  84 */         Double.parseDouble(this.textScale.getText());
/*     */       }
/*     */     }
/*     */     catch (Exception e) {
/*  88 */       allOK = false;
/*  89 */       icancloudgui.Utils.Utils.showErrorMessage("Wrong parameter for scale.\nThis parameter must be a double.\n", "Wrong parameter!");
/*     */     }
/*     */     
/*     */ 
/*     */ 
/*     */ 
/*  95 */     if ((allOK) && (!icancloudgui.Utils.Utils.checkEnergyTable((icancloudgui.Tables.EnergyTableModel)this.PSUsTable.getModel()))) {
/*  96 */       allOK = false;
/*     */       
/*  98 */       icancloudgui.Utils.Utils.showErrorMessage("Table of energy states contains wrong values.\nEnergy state must be a String and its value a double.\n", "Wrong parameter!");
/*     */     }
/*     */     
/*     */ 
/*     */ 
/* 103 */     return allOK;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public PSU panelToPsuObject()
/*     */   {
/* 117 */     PSU currentPSU = new PSU();
/* 118 */     java.util.LinkedList<icancloudgui.TabbedPanels.dataClasses.EnergyEntry> list = new java.util.LinkedList();
/*     */     
/*     */ 
/* 121 */     currentPSU.setName(this.textName.getText());
/* 122 */     currentPSU.setWattage(Double.parseDouble(this.textWattage.getText()));
/* 123 */     currentPSU.setScale(Double.parseDouble(this.textScale.getText()));
/* 124 */     currentPSU.setComment(this.textAreaComment.getText());
/* 125 */     icancloudgui.Utils.Utils.energyTableModelToList((icancloudgui.Tables.EnergyTableModel)this.PSUsTable.getModel(), list);
/* 126 */     currentPSU.setEnergyData(list);
/*     */     
/* 128 */     return currentPSU;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   public void clear()
/*     */   {
/* 136 */     this.textName.setText("");
/* 137 */     this.textWattage.setText("");
/* 138 */     this.textScale.setText("");
/* 139 */     this.textAreaComment.setText("");
/* 140 */     icancloudgui.Utils.Utils.initEnergyTable(this.PSUsTable);
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private void loadPsuObjectInPanel(PSU newPSU)
/*     */   {
/* 153 */     this.textName.setText(newPSU.getName());
/* 154 */     this.textWattage.setText(Double.toString(newPSU.getWattage()));
/* 155 */     this.textScale.setText(Double.toString(newPSU.getScale()));
/* 156 */     this.textAreaComment.setText(newPSU.getComment());
/*     */     
/*     */ 
/* 159 */     icancloudgui.Tables.EnergyTableModel model = new icancloudgui.Tables.EnergyTableModel();
/* 160 */     icancloudgui.Utils.Utils.energyListToTable(newPSU.getEnergyData(), model);
/* 161 */     this.PSUsTable.setModel(model);
/* 162 */     this.PSUsTable.updateUI();
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void loadPSUInPanel(String psuName)
/*     */   {
/* 175 */     PSU loadedPSU = icancloudgui.Utils.Utils.loadPSUobject(psuName);
/*     */     
/*     */ 
/* 178 */     if (loadedPSU != null) {
/* 179 */       loadPsuObjectInPanel(loadedPSU);
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private boolean writePSUToFile()
/*     */   {
/* 199 */     boolean allOK = checkValues();
/*     */     
/*     */ 
/* 202 */     if (allOK)
/*     */     {
/*     */ 
/* 205 */       PSU currentPSU = panelToPsuObject();
/*     */       
/*     */ 
/* 208 */       String newPsuFile = icancloudgui.Utils.Configuration.REPOSITORY_PSU_FOLDER + java.io.File.separatorChar + currentPSU.getName();
/*     */       
/*     */ 
/*     */       try
/*     */       {
/* 213 */         if (new java.io.File(newPsuFile).exists())
/*     */         {
/*     */ 
/* 216 */           int response = javax.swing.JOptionPane.showConfirmDialog(null, "Overwrite existing PSU?", "Confirm Overwrite", 2, 3);
/*     */           
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 223 */           if (response == 0)
/*     */           {
/*     */ 
/* 226 */             java.io.FileOutputStream fout = new java.io.FileOutputStream(newPsuFile);
/* 227 */             java.io.ObjectOutputStream oos = new java.io.ObjectOutputStream(fout);
/* 228 */             oos.writeObject(currentPSU);
/* 229 */             oos.close();
/* 230 */             allOK = true;
/*     */           }
/*     */           
/*     */ 
/*     */         }
/*     */         else
/*     */         {
/*     */ 
/* 238 */           java.io.FileOutputStream fout = new java.io.FileOutputStream(newPsuFile);
/* 239 */           java.io.ObjectOutputStream oos = new java.io.ObjectOutputStream(fout);
/* 240 */           oos.writeObject(currentPSU);
/* 241 */           oos.close();
/* 242 */           allOK = true;
/*     */         }
/*     */       }
/*     */       catch (Exception e) {
/* 246 */         e.printStackTrace();
/*     */       }
/*     */     }
/*     */     
/* 250 */     return allOK;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private void initDefaultTableValues()
/*     */   {
/* 263 */     icancloudgui.Tables.EnergyTableModel newModel = new icancloudgui.Tables.EnergyTableModel();
/* 264 */     newModel.setColumnCount(3);
/* 265 */     newModel.setColumnIdentifiers(icancloudgui.Utils.Utils.columnNamesEnergyTable);
/*     */     
/*     */ 
/* 268 */     newModel.addRow(new Object[] { Integer.toString(0), "psu_20", "" });
/* 269 */     newModel.addRow(new Object[] { Integer.toString(1), "psu_50", "" });
/* 270 */     newModel.addRow(new Object[] { Integer.toString(2), "psu_100", "" });
/*     */     
/* 272 */     this.PSUsTable.setModel(newModel);
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private void initComponents()
/*     */   {
/* 285 */     this.labelName = new JLabel();
/* 286 */     this.labelWattage = new JLabel();
/* 287 */     this.labelScale = new JLabel();
/* 288 */     this.textName = new JTextField();
/* 289 */     this.textWattage = new JTextField();
/* 290 */     this.textScale = new JTextField();
/* 291 */     this.scrollTable = new javax.swing.JScrollPane();
/* 292 */     this.PSUsTable = new JTable();
/* 293 */     this.labelEnergy = new JLabel();
/* 294 */     this.buttonResize = new JLabel();
/* 295 */     this.textNewNumRows = new JTextField();
/* 296 */     this.labelResize = new JLabel();
/* 297 */     this.buttonSave = new JLabel();
/* 298 */     this.buttonHelp = new JLabel();
/* 299 */     this.scrollTextArea = new javax.swing.JScrollPane();
/* 300 */     this.textAreaComment = new javax.swing.JTextArea();
/* 301 */     this.labelComment = new JLabel();
/* 302 */     this.buttonClear = new JLabel();
/*     */     
/* 304 */     ResourceMap resourceMap = ((icancloudgui.ICanCloudGUIApp)org.jdesktop.application.Application.getInstance(icancloudgui.ICanCloudGUIApp.class)).getContext().getResourceMap(PSUsPanel.class);
/* 305 */     setBackground(resourceMap.getColor("Form.background"));
/* 306 */     setName("Form");
/* 307 */     setPreferredSize(new java.awt.Dimension(500, 641));
/* 308 */     setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());
/*     */     
/* 310 */     this.labelName.setFont(resourceMap.getFont("labelEnergy.font"));
/* 311 */     this.labelName.setText(resourceMap.getString("labelName.text", new Object[0]));
/* 312 */     this.labelName.setName("labelName");
/* 313 */     add(this.labelName, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 35, -1, -1));
/*     */     
/* 315 */     this.labelWattage.setFont(resourceMap.getFont("labelEnergy.font"));
/* 316 */     this.labelWattage.setText(resourceMap.getString("labelWattage.text", new Object[0]));
/* 317 */     this.labelWattage.setName("labelWattage");
/* 318 */     add(this.labelWattage, new org.netbeans.lib.awtextra.AbsoluteConstraints(60, 70, -1, -1));
/*     */     
/* 320 */     this.labelScale.setFont(resourceMap.getFont("labelEnergy.font"));
/* 321 */     this.labelScale.setText(resourceMap.getString("labelScale.text", new Object[0]));
/* 322 */     this.labelScale.setName("labelScale");
/* 323 */     add(this.labelScale, new org.netbeans.lib.awtextra.AbsoluteConstraints(80, 105, -1, -1));
/*     */     
/* 325 */     this.textName.setText(resourceMap.getString("textName.text", new Object[0]));
/* 326 */     this.textName.setName("textName");
/* 327 */     add(this.textName, new org.netbeans.lib.awtextra.AbsoluteConstraints(50, 30, 350, -1));
/*     */     
/* 329 */     this.textWattage.setText(resourceMap.getString("textWattage.text", new Object[0]));
/* 330 */     this.textWattage.setName("textWattage");
/* 331 */     add(this.textWattage, new org.netbeans.lib.awtextra.AbsoluteConstraints(120, 65, 280, -1));
/*     */     
/* 333 */     this.textScale.setText(resourceMap.getString("textScale.text", new Object[0]));
/* 334 */     this.textScale.setName("textScale");
/* 335 */     add(this.textScale, new org.netbeans.lib.awtextra.AbsoluteConstraints(120, 100, 280, -1));
/*     */     
/* 337 */     this.scrollTable.setName("scrollTable");
/*     */     
/* 339 */     this.PSUsTable.setModel(new javax.swing.table.DefaultTableModel(new Object[][] { { null, null, null }, { null, null, null }, { null, null, null }, { null, null, null }, { null, null, null }, { null, null, null }, { null, null, null }, { null, null, null }, { null, null, null }, { null, null, null } }, new String[] { "#", "Name", "Value" })
/*     */     {
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 356 */       Class[] types = { String.class, String.class, String.class };
/*     */       
/*     */ 
/* 359 */       boolean[] canEdit = { false, true, true };
/*     */       
/*     */ 
/*     */       public Class getColumnClass(int columnIndex)
/*     */       {
/* 364 */         return this.types[columnIndex];
/*     */       }
/*     */       
/*     */       public boolean isCellEditable(int rowIndex, int columnIndex) {
/* 368 */         return this.canEdit[columnIndex];
/*     */       }
/* 370 */     });
/* 371 */     this.PSUsTable.setGridColor(resourceMap.getColor("PSUsTable.gridColor"));
/* 372 */     this.PSUsTable.setName("PSUsTable");
/* 373 */     this.scrollTable.setViewportView(this.PSUsTable);
/* 374 */     this.PSUsTable.getColumnModel().getColumn(0).setHeaderValue(resourceMap.getString("PSUsTable.columnModel.title0", new Object[0]));
/* 375 */     this.PSUsTable.getColumnModel().getColumn(1).setHeaderValue(resourceMap.getString("PSUsTable.columnModel.title1", new Object[0]));
/* 376 */     this.PSUsTable.getColumnModel().getColumn(2).setHeaderValue(resourceMap.getString("PSUsTable.columnModel.title2", new Object[0]));
/*     */     
/* 378 */     add(this.scrollTable, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 180, 760, 207));
/*     */     
/* 380 */     this.labelEnergy.setFont(resourceMap.getFont("labelEnergy.font"));
/* 381 */     this.labelEnergy.setText(resourceMap.getString("labelEnergy.text", new Object[0]));
/* 382 */     this.labelEnergy.setName("labelEnergy");
/* 383 */     add(this.labelEnergy, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 155, -1, -1));
/*     */     
/* 385 */     this.buttonResize.setIcon(resourceMap.getIcon("buttonResize.icon"));
/* 386 */     this.buttonResize.setText(resourceMap.getString("buttonResize.text", new Object[0]));
/* 387 */     this.buttonResize.setToolTipText(resourceMap.getString("buttonResize.toolTipText", new Object[0]));
/* 388 */     this.buttonResize.setName("buttonResize");
/* 389 */     this.buttonResize.addMouseListener(new java.awt.event.MouseAdapter() {
/*     */       public void mouseClicked(MouseEvent evt) {
/* 391 */         PSUsPanel.this.buttonResizeMouseClicked(evt);
/*     */       }
/* 393 */     });
/* 394 */     add(this.buttonResize, new org.netbeans.lib.awtextra.AbsoluteConstraints(690, 150, -1, -1));
/*     */     
/* 396 */     this.textNewNumRows.setText(resourceMap.getString("textNewNumRows.text", new Object[0]));
/* 397 */     this.textNewNumRows.setName("textNewNumRows");
/* 398 */     add(this.textNewNumRows, new org.netbeans.lib.awtextra.AbsoluteConstraints(721, 145, 60, -1));
/*     */     
/* 400 */     this.labelResize.setFont(resourceMap.getFont("labelEnergy.font"));
/* 401 */     this.labelResize.setForeground(resourceMap.getColor("labelResize.foreground"));
/* 402 */     this.labelResize.setText(resourceMap.getString("labelResize.text", new Object[0]));
/* 403 */     this.labelResize.setName("labelResize");
/* 404 */     add(this.labelResize, new org.netbeans.lib.awtextra.AbsoluteConstraints(600, 150, -1, -1));
/*     */     
/* 406 */     this.buttonSave.setIcon(resourceMap.getIcon("buttonSave.icon"));
/* 407 */     this.buttonSave.setToolTipText(resourceMap.getString("buttonSave.toolTipText", new Object[0]));
/* 408 */     this.buttonSave.setName("buttonSave");
/* 409 */     this.buttonSave.addMouseListener(new java.awt.event.MouseAdapter() {
/*     */       public void mouseClicked(MouseEvent evt) {
/* 411 */         PSUsPanel.this.buttonSaveMouseClicked(evt);
/*     */       }
/* 413 */     });
/* 414 */     add(this.buttonSave, new org.netbeans.lib.awtextra.AbsoluteConstraints(710, 20, -1, -1));
/*     */     
/* 416 */     this.buttonHelp.setIcon(resourceMap.getIcon("buttonHelp.icon"));
/* 417 */     this.buttonHelp.setToolTipText(resourceMap.getString("buttonHelp.toolTipText", new Object[0]));
/* 418 */     this.buttonHelp.setName("buttonHelp");
/* 419 */     this.buttonHelp.addMouseListener(new java.awt.event.MouseAdapter() {
/*     */       public void mouseClicked(MouseEvent evt) {
/* 421 */         PSUsPanel.this.buttonHelpMouseClicked(evt);
/*     */       }
/* 423 */     });
/* 424 */     add(this.buttonHelp, new org.netbeans.lib.awtextra.AbsoluteConstraints(750, 20, -1, -1));
/*     */     
/* 426 */     this.scrollTextArea.setName("scrollTextArea");
/*     */     
/* 428 */     this.textAreaComment.setColumns(20);
/* 429 */     this.textAreaComment.setRows(5);
/* 430 */     this.textAreaComment.setName("textAreaComment");
/* 431 */     this.scrollTextArea.setViewportView(this.textAreaComment);
/*     */     
/* 433 */     add(this.scrollTextArea, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 430, 760, 90));
/*     */     
/* 435 */     this.labelComment.setFont(resourceMap.getFont("labelEnergy.font"));
/* 436 */     this.labelComment.setText(resourceMap.getString("labelComment.text", new Object[0]));
/* 437 */     this.labelComment.setName("labelComment");
/* 438 */     add(this.labelComment, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 405, -1, -1));
/*     */     
/* 440 */     this.buttonClear.setIcon(resourceMap.getIcon("buttonClear.icon"));
/* 441 */     this.buttonClear.setToolTipText(resourceMap.getString("buttonClear.toolTipText", new Object[0]));
/* 442 */     this.buttonClear.setName("buttonClear");
/* 443 */     this.buttonClear.addMouseListener(new java.awt.event.MouseAdapter() {
/*     */       public void mouseClicked(MouseEvent evt) {
/* 445 */         PSUsPanel.this.buttonClearMouseClicked(evt);
/*     */       }
/* 447 */     });
/* 448 */     add(this.buttonClear, new org.netbeans.lib.awtextra.AbsoluteConstraints(670, 20, -1, -1));
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private void buttonResizeMouseClicked(MouseEvent evt)
/*     */   {
/*     */     try
/*     */     {
/* 466 */       int newNumRows = Integer.parseInt(this.textNewNumRows.getText());
/*     */       
/*     */ 
/* 469 */       if (this.PSUsTable.getModel().getRowCount() == newNumRows)
/*     */       {
/* 471 */         javax.swing.JOptionPane.showMessageDialog(new javax.swing.JFrame(), "New number of rows is the same as the curent table", "Warning!", 2);
/*     */       }
/*     */       
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 478 */       if (newNumRows < this.PSUsTable.getModel().getRowCount())
/*     */       {
/*     */ 
/* 481 */         int response = javax.swing.JOptionPane.showConfirmDialog(null, "Are you sure of resizing this table?", "Confirm resize table", 2, 3);
/*     */         
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 487 */         if (response == 0)
/*     */         {
/*     */ 
/* 490 */           javax.swing.table.DefaultTableModel newModel = (javax.swing.table.DefaultTableModel)this.PSUsTable.getModel();
/* 491 */           int oldNumRows = this.PSUsTable.getModel().getRowCount();
/*     */           
/*     */ 
/* 494 */           for (int i = 0; i < oldNumRows - newNumRows; i++) {
/* 495 */             System.out.println(newModel.getRowCount());
/* 496 */             newModel.removeRow(newModel.getRowCount() - 1);
/*     */           }
/*     */           
/* 499 */           this.PSUsTable.setModel(newModel);
/*     */ 
/*     */         }
/*     */         
/*     */ 
/*     */       }
/*     */       else
/*     */       {
/*     */ 
/* 508 */         int response = javax.swing.JOptionPane.showConfirmDialog(null, "Are you sure of resizing this table?", "Confirm resize table", 2, 3);
/*     */         
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 514 */         if (response == 0)
/*     */         {
/*     */ 
/* 517 */           javax.swing.table.DefaultTableModel newModel = (javax.swing.table.DefaultTableModel)this.PSUsTable.getModel();
/* 518 */           int oldNumRows = this.PSUsTable.getModel().getRowCount();
/*     */           
/*     */ 
/* 521 */           for (int i = 0; i < newNumRows - oldNumRows; i++) {
/* 522 */             newModel.addRow(new Object[] { Integer.toString(oldNumRows + i), "", "" });
/*     */           }
/*     */           
/* 525 */           this.PSUsTable.setModel(newModel);
/*     */         }
/*     */       }
/*     */     }
/*     */     catch (Exception e)
/*     */     {
/* 531 */       javax.swing.JOptionPane.showMessageDialog(new javax.swing.JFrame(), "Wrong number, please enter a correct number value", "Error!", 2);
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private void buttonSaveMouseClicked(MouseEvent evt)
/*     */   {
/* 548 */     boolean allOK = writePSUToFile();
/*     */     
/*     */ 
/* 551 */     if (allOK) {
/* 552 */       this.mainFrame.updateTree();
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private void buttonHelpMouseClicked(MouseEvent evt)
/*     */   {
/* 562 */     icancloudgui.Utils.Utils.showHelp(this);
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private void buttonClearMouseClicked(MouseEvent evt)
/*     */   {
/* 571 */     int response = javax.swing.JOptionPane.showConfirmDialog(null, "Clear current PSU configuration?", "Confirm to clear this form", 2, 3);
/*     */     
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 578 */     if (response == 0) {
/* 579 */       clear();
/*     */     }
/*     */   }
/*     */ }


/* Location:              /home/chverma/Descargas/iCanCloudGUI/iCanCloudGUI.jar!/icancloudgui/TabbedPanels/PSUsPanel.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */