/*     */ package icancloudgui.TabbedPanels.dataClasses;
/*     */ 
/*     */ import icancloudgui.TabbedPanels.DataCenterPanel;
/*     */ import java.awt.Insets;
/*     */ import java.awt.event.MouseListener;
/*     */ import java.io.Serializable;
/*     */ import java.util.Iterator;
/*     */ import java.util.LinkedList;
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ public class DataCenter
/*     */   implements Serializable
/*     */ {
/*     */   private LinkedList<DataCenterUnit> computingNodeList;
/*     */   private LinkedList<DataCenterUnit> storageNodeList;
/*     */   private LinkedList<DataCenterUnit> switchesList;
/*     */   private LinkedList<DataCenterUnit> racksList;
/*     */   private LinkedList<Connection> connectionsList;
/*     */   private String name;
/*     */   int currentElement;
/*     */   
/*     */   public DataCenter()
/*     */   {
/*  32 */     this.computingNodeList = new LinkedList();
/*  33 */     this.storageNodeList = new LinkedList();
/*  34 */     this.switchesList = new LinkedList();
/*  35 */     this.racksList = new LinkedList();
/*  36 */     this.connectionsList = new LinkedList();
/*  37 */     this.currentElement = 0;
/*  38 */     this.name = "";
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public DataCenterUnit addElement(String type, String name, Insets insets, MouseListener listener)
/*     */   {
/*  55 */     DataCenterUnit unit = new DataCenterUnit(type, name, insets, listener, this.currentElement);
/*  56 */     this.currentElement += 1;
/*     */     
/*  58 */     if (type.equals("Computing")) {
/*  59 */       this.computingNodeList.add(unit);
/*     */     }
/*  61 */     else if (type.equals("Storage")) {
/*  62 */       this.storageNodeList.add(unit);
/*     */     }
/*  64 */     else if (type.equals("Switch")) {
/*  65 */       this.switchesList.add(unit);
/*     */     }
/*  67 */     else if (type.equals("Rack")) {
/*  68 */       this.racksList.add(unit);
/*     */     }
/*  70 */     return unit;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void addConnection(Connection newConnection)
/*     */   {
/*  79 */     this.connectionsList.add(newConnection);
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public DataCenterUnit getElementByCompleteName(String completeName)
/*     */   {
/*  95 */     boolean found = false;
/*  96 */     Iterator iterator = null;
/*  97 */     DataCenterUnit currentElement = null;
/*     */     
/*     */ 
/* 100 */     if (completeName.startsWith("Computing")) {
/* 101 */       iterator = this.computingNodeList.iterator();
/*     */     }
/* 103 */     else if (completeName.startsWith("Storage")) {
/* 104 */       iterator = this.storageNodeList.iterator();
/*     */     }
/* 106 */     else if (completeName.startsWith("Switch")) {
/* 107 */       iterator = this.switchesList.iterator();
/*     */     }
/* 109 */     else if (completeName.startsWith("Rack")) {
/* 110 */       iterator = this.racksList.iterator();
/*     */     }
/*     */     
/*     */ 
/* 114 */     while ((iterator.hasNext()) && (!found))
/*     */     {
/*     */ 
/* 117 */       currentElement = (DataCenterUnit)iterator.next();
/*     */       
/*     */ 
/* 120 */       if (currentElement.getCompleteName().equals(completeName)) {
/* 121 */         found = true;
/*     */       }
/*     */       else {
/* 124 */         currentElement = null;
/*     */       }
/*     */     }
/* 127 */     return currentElement;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void removeElementByCompleteName(String completeName)
/*     */   {
/* 143 */     boolean found = false;
/* 144 */     Iterator iterator = null;
/* 145 */     DataCenterUnit currentElement = null;
/*     */     
/*     */ 
/* 148 */     if (completeName.startsWith("Computing")) {
/* 149 */       iterator = this.computingNodeList.iterator();
/*     */     }
/* 151 */     else if (completeName.startsWith("Storage")) {
/* 152 */       iterator = this.storageNodeList.iterator();
/*     */     }
/* 154 */     else if (completeName.startsWith("Switch")) {
/* 155 */       iterator = this.switchesList.iterator();
/*     */     }
/* 157 */     else if (completeName.startsWith("Rack")) {
/* 158 */       iterator = this.racksList.iterator();
/*     */     }
/*     */     
/*     */ 
/* 162 */     while ((iterator.hasNext()) && (!found))
/*     */     {
/*     */ 
/* 165 */       currentElement = (DataCenterUnit)iterator.next();
/*     */       
/*     */ 
/* 168 */       if (currentElement.getCompleteName().equals(completeName)) {
/* 169 */         found = true;
/* 170 */         iterator.remove();
/*     */       }
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void removeInvolvedConnection(String element)
/*     */   {
/* 187 */     boolean found = false;
/* 188 */     Iterator iterator = null;
/* 189 */     Connection currentElement = null;
/*     */     
/*     */ 
/* 192 */     iterator = this.connectionsList.iterator();
/*     */     
/*     */ 
/* 195 */     while ((iterator.hasNext()) && (!found))
/*     */     {
/*     */ 
/* 198 */       currentElement = (Connection)iterator.next();
/*     */       
/*     */ 
/* 201 */       if ((currentElement.getSource().equals(element)) || (currentElement.getDestination().equals(element))) {
/* 202 */         found = true;
/* 203 */         iterator.remove();
/*     */       }
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void removeConnection(String source, String destination)
/*     */   {
/* 221 */     boolean found = false;
/* 222 */     Iterator iterator = null;
/* 223 */     Connection currentElement = null;
/*     */     
/*     */ 
/* 226 */     iterator = this.connectionsList.iterator();
/*     */     
/*     */ 
/* 229 */     while ((iterator.hasNext()) && (!found))
/*     */     {
/*     */ 
/* 232 */       currentElement = (Connection)iterator.next();
/*     */       
/*     */ 
/* 235 */       if (((currentElement.getSource().equals(source)) && (currentElement.getDestination().equals(destination))) || ((currentElement.getSource().equals(destination)) && (currentElement.getDestination().equals(source))))
/*     */       {
/*     */ 
/* 238 */         found = true;
/*     */         
/*     */ 
/* 241 */         iterator.remove();
/*     */       }
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public boolean existsCurrentConnection(String source, String destination)
/*     */   {
/* 260 */     boolean found = false;
/* 261 */     Iterator iterator = null;
/* 262 */     Connection currentElement = null;
/*     */     
/*     */ 
/* 265 */     iterator = this.connectionsList.iterator();
/*     */     
/*     */ 
/* 268 */     while ((iterator.hasNext()) && (!found))
/*     */     {
/*     */ 
/* 271 */       currentElement = (Connection)iterator.next();
/*     */       
/*     */ 
/* 274 */       if (((currentElement.getSource().equals(source)) && (currentElement.getDestination().equals(destination))) || ((currentElement.getSource().equals(destination)) && (currentElement.getDestination().equals(source))))
/*     */       {
/*     */ 
/* 277 */         found = true;
/*     */       }
/*     */     }
/* 280 */     return found;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public LinkedList<String> getConnectionsLinkedTo(String source)
/*     */   {
/* 297 */     LinkedList<String> list = new LinkedList();
/*     */     
/*     */ 
/* 300 */     Iterator iterator = this.connectionsList.iterator();
/*     */     
/*     */ 
/* 303 */     while (iterator.hasNext())
/*     */     {
/*     */ 
/* 306 */       Connection currentConnection = (Connection)iterator.next();
/*     */       
/*     */ 
/* 309 */       if (currentConnection.getSource().equals(source)) {
/* 310 */         list.add(currentConnection.getDestination());
/*     */ 
/*     */ 
/*     */       }
/* 314 */       else if (currentConnection.getDestination().equals(source)) {
/* 315 */         list.add(currentConnection.getSource());
/*     */       }
/*     */     }
/*     */     
/* 319 */     return list;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public Iterator<Connection> getConnectionListIterator()
/*     */   {
/* 328 */     return this.connectionsList.iterator();
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   public Iterator<DataCenterUnit> getRackList()
/*     */   {
/* 336 */     return this.racksList.iterator();
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   public Iterator<DataCenterUnit> getComputingNodeList()
/*     */   {
/* 344 */     return this.computingNodeList.iterator();
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   public Iterator<DataCenterUnit> getStorageNodeList()
/*     */   {
/* 352 */     return this.storageNodeList.iterator();
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   public Iterator<DataCenterUnit> getSwitchesList()
/*     */   {
/* 360 */     return this.switchesList.iterator();
/*     */   }
/*     */   
/*     */   public int getNumRacks() {
/* 364 */     return this.racksList.size();
/*     */   }
/*     */   
/*     */   public int getNumComputingNodes() {
/* 368 */     return this.computingNodeList.size();
/*     */   }
/*     */   
/*     */   public int getNumStorageNodes() {
/* 372 */     return this.storageNodeList.size();
/*     */   }
/*     */   
/*     */   public String getName() {
/* 376 */     return this.name;
/*     */   }
/*     */   
/*     */   public void setName(String newName) {
/* 380 */     this.name = newName;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public LinkedList<DataCenterUnit> getPosibleDestinations(String source)
/*     */   {
/* 394 */     LinkedList<DataCenterUnit> list = new LinkedList();
/*     */     
/*     */ 
/* 397 */     if ((source.startsWith("Computing")) || (source.startsWith("Storage")) || (source.startsWith("Rack")))
/*     */     {
/*     */ 
/*     */ 
/* 401 */       list.addAll(this.switchesList);
/*     */ 
/*     */     }
/*     */     else
/*     */     {
/* 406 */       list.addAll(removeSourceElement(this.switchesList, source));
/* 407 */       list.addAll(this.computingNodeList);
/* 408 */       list.addAll(this.storageNodeList);
/* 409 */       list.addAll(this.racksList);
/*     */     }
/*     */     
/* 412 */     return list;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void saveAllElementCoordinates()
/*     */   {
/* 424 */     Iterator iterator = null;
/*     */     
/*     */ 
/* 427 */     iterator = this.computingNodeList.iterator();
/*     */     
/* 429 */     while (iterator.hasNext()) {
/* 430 */       ((DataCenterUnit)iterator.next()).saveCoordinates();
/*     */     }
/*     */     
/*     */ 
/* 434 */     iterator = this.storageNodeList.iterator();
/*     */     
/* 436 */     while (iterator.hasNext()) {
/* 437 */       ((DataCenterUnit)iterator.next()).saveCoordinates();
/*     */     }
/*     */     
/*     */ 
/* 441 */     iterator = this.switchesList.iterator();
/*     */     
/* 443 */     while (iterator.hasNext()) {
/* 444 */       ((DataCenterUnit)iterator.next()).saveCoordinates();
/*     */     }
/*     */     
/* 447 */     iterator = this.racksList.iterator();
/*     */     
/* 449 */     while (iterator.hasNext()) {
/* 450 */       ((DataCenterUnit)iterator.next()).saveCoordinates();
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void loadAllElementCoordinates(Insets insets, MouseListener listener)
/*     */   {
/* 464 */     Iterator iterator = null;
/*     */     
/*     */ 
/* 467 */     iterator = this.computingNodeList.iterator();
/*     */     
/* 469 */     while (iterator.hasNext()) {
/* 470 */       ((DataCenterUnit)iterator.next()).loadIconCoordinates(insets, listener);
/*     */     }
/*     */     
/*     */ 
/* 474 */     iterator = this.storageNodeList.iterator();
/*     */     
/* 476 */     while (iterator.hasNext()) {
/* 477 */       ((DataCenterUnit)iterator.next()).loadIconCoordinates(insets, listener);
/*     */     }
/*     */     
/*     */ 
/* 481 */     iterator = this.switchesList.iterator();
/*     */     
/* 483 */     while (iterator.hasNext()) {
/* 484 */       ((DataCenterUnit)iterator.next()).loadIconCoordinates(insets, listener);
/*     */     }
/*     */     
/* 487 */     iterator = this.racksList.iterator();
/*     */     
/* 489 */     while (iterator.hasNext()) {
/* 490 */       ((DataCenterUnit)iterator.next()).loadIconCoordinates(insets, listener);
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void loadIcons(DataCenterPanel dataCenterPanel)
/*     */   {
/* 503 */     Iterator iterator = null;
/*     */     
/*     */ 
/* 506 */     iterator = this.computingNodeList.iterator();
/*     */     
/* 508 */     while (iterator.hasNext()) {
/* 509 */       dataCenterPanel.add(((DataCenterUnit)iterator.next()).getLabel());
/*     */     }
/*     */     
/* 512 */     iterator = this.storageNodeList.iterator();
/*     */     
/* 514 */     while (iterator.hasNext()) {
/* 515 */       dataCenterPanel.add(((DataCenterUnit)iterator.next()).getLabel());
/*     */     }
/*     */     
/*     */ 
/* 519 */     iterator = this.switchesList.iterator();
/*     */     
/* 521 */     while (iterator.hasNext()) {
/* 522 */       dataCenterPanel.add(((DataCenterUnit)iterator.next()).getLabel());
/*     */     }
/*     */     
/* 525 */     iterator = this.racksList.iterator();
/*     */     
/* 527 */     while (iterator.hasNext()) {
/* 528 */       dataCenterPanel.add(((DataCenterUnit)iterator.next()).getLabel());
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private LinkedList<DataCenterUnit> removeSourceElement(LinkedList<DataCenterUnit> destinationList, String source)
/*     */   {
/* 549 */     LinkedList<DataCenterUnit> list = new LinkedList();
/* 550 */     Iterator iterator = destinationList.iterator();
/*     */     
/*     */ 
/* 553 */     while (iterator.hasNext())
/*     */     {
/*     */ 
/* 556 */       DataCenterUnit currentElement = (DataCenterUnit)iterator.next();
/*     */       
/*     */ 
/* 559 */       if (!currentElement.getCompleteName().equals(source)) {
/* 560 */         list.add(currentElement);
/*     */       }
/*     */     }
/*     */     
/* 564 */     return list;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String connectionsToString()
/*     */   {
/* 576 */     Iterator iterator = this.connectionsList.iterator();
/* 577 */     String text = "";
/* 578 */     int index = 0;
/*     */     
/*     */ 
/* 581 */     while (iterator.hasNext())
/*     */     {
/*     */ 
/* 584 */       Connection currentConnection = (Connection)iterator.next();
/*     */       
/*     */ 
/* 587 */       text = text + "Connection [" + index + "]:" + currentConnection.toString() + "\n";
/*     */       
/*     */ 
/* 590 */       index++;
/*     */     }
/*     */     
/* 593 */     return text;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public int getNumComputingInstances(String computingInstance)
/*     */   {
/* 604 */     int numInstances = 0;
/* 605 */     Iterator iterator = null;
/* 606 */     iterator = this.computingNodeList.iterator();
/*     */     
/*     */ 
/* 609 */     while (iterator.hasNext())
/*     */     {
/*     */ 
/* 612 */       DataCenterUnit currentUnit = (DataCenterUnit)iterator.next();
/*     */       
/*     */ 
/* 615 */       if (currentUnit.getName().equals(computingInstance)) {
/* 616 */         numInstances++;
/*     */       }
/*     */     }
/*     */     
/* 620 */     return numInstances;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public int getNumStorageInstances(String storageInstance)
/*     */   {
/* 630 */     int numInstances = 0;
/* 631 */     Iterator iterator = null;
/* 632 */     iterator = this.storageNodeList.iterator();
/*     */     
/*     */ 
/* 635 */     while (iterator.hasNext())
/*     */     {
/*     */ 
/* 638 */       DataCenterUnit currentUnit = (DataCenterUnit)iterator.next();
/*     */       
/*     */ 
/* 641 */       if (currentUnit.getName().equals(storageInstance)) {
/* 642 */         numInstances++;
/*     */       }
/*     */     }
/*     */     
/* 646 */     return numInstances;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public int getNumRackInstances(String rackInstance)
/*     */   {
/* 656 */     int numInstances = 0;
/* 657 */     Iterator iterator = null;
/* 658 */     DataCenterUnit currentUnit = null;
/* 659 */     iterator = this.racksList.iterator();
/*     */     
/*     */ 
/* 662 */     while (iterator.hasNext())
/*     */     {
/*     */ 
/* 665 */       currentUnit = (DataCenterUnit)iterator.next();
/*     */       
/*     */ 
/* 668 */       if (currentUnit.getName().equals(rackInstance)) {
/* 669 */         numInstances++;
/*     */       }
/*     */     }
/*     */     
/* 673 */     return numInstances;
/*     */   }
/*     */ }


/* Location:              /home/chverma/Descargas/iCanCloudGUI/iCanCloudGUI.jar!/icancloudgui/TabbedPanels/dataClasses/DataCenter.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */