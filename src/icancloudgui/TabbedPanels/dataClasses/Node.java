/*     */ package icancloudgui.TabbedPanels.dataClasses;
/*     */ 
/*     */ import icancloudgui.Utils.Utils;
/*     */ import java.io.Serializable;
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ public class Node
/*     */   implements Serializable
/*     */ {
/*     */   private String name;
/*     */   private String cpu;
/*     */   private String memory;
/*     */   private String disk;
/*     */   private String psu;
/*     */   private String hypervisor;
/*     */   private String comment;
/*     */   private boolean isStorage;
/*     */   private int numDisks;
/*     */   
/*     */   public Node()
/*     */   {
/*  28 */     this.name = "";
/*  29 */     this.cpu = "";
/*  30 */     this.disk = "";
/*  31 */     this.memory = "";
/*  32 */     this.psu = "";
/*  33 */     this.hypervisor = "";
/*  34 */     this.comment = "";
/*  35 */     this.isStorage = false;
/*  36 */     this.numDisks = 0;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String toString()
/*     */   {
/*  53 */     String text = "";
/*     */     
/*     */ 
/*  56 */     CPU cpuObject = Utils.loadCPUobject(this.cpu);
/*  57 */     Memory memoryObject = Utils.loadMemoryObject(this.memory);
/*  58 */     Disk diskObject = Utils.loadDiskObject(this.disk);
/*  59 */     PSU psuObject = Utils.loadPSUobject(this.psu);
/*     */     
/*     */ 
/*  62 */     if (this.isStorage) {
/*  63 */       text = this.name + " (Storage)\n";
/*     */     } else {
/*  65 */       text = this.name + " (Computing)\n\n";
/*     */     }
/*     */     
/*  68 */     text = text + " - " + cpuObject.toString() + "\n";
/*  69 */     text = text + " - " + memoryObject.toString() + "\n";
/*  70 */     text = text + " - " + Integer.toString(this.numDisks) + "x " + diskObject.toString() + "\n";
/*  71 */     text = text + " - " + psuObject.toString() + "\n";
/*  72 */     text = text + " - Hypervisor: " + this.hypervisor + "\n";
/*     */     
/*  74 */     return text;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getHypervisor()
/*     */   {
/*  83 */     return this.hypervisor;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setHypervisor(String hypervisor)
/*     */   {
/*  91 */     this.hypervisor = hypervisor;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getComment()
/*     */   {
/*  99 */     return this.comment;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setComment(String comment)
/*     */   {
/* 107 */     this.comment = comment;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getCpu()
/*     */   {
/* 115 */     return this.cpu;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setCpu(String cpu)
/*     */   {
/* 123 */     this.cpu = cpu;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getDisk()
/*     */   {
/* 131 */     return this.disk;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setDisk(String disk)
/*     */   {
/* 139 */     this.disk = disk;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   public boolean isIsStorage()
/*     */   {
/* 147 */     return this.isStorage;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setIsStorage(boolean isStorage)
/*     */   {
/* 155 */     this.isStorage = isStorage;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getMemory()
/*     */   {
/* 163 */     return this.memory;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setMemory(String memory)
/*     */   {
/* 171 */     this.memory = memory;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getName()
/*     */   {
/* 179 */     return this.name;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setName(String name)
/*     */   {
/* 187 */     this.name = name;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   public int getNumDisks()
/*     */   {
/* 195 */     return this.numDisks;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setNumDisks(int numDisks)
/*     */   {
/* 203 */     this.numDisks = numDisks;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getPsu()
/*     */   {
/* 211 */     return this.psu;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setPsu(String psu)
/*     */   {
/* 219 */     this.psu = psu;
/*     */   }
/*     */ }


/* Location:              /home/chverma/Descargas/iCanCloudGUI/iCanCloudGUI.jar!/icancloudgui/TabbedPanels/dataClasses/Node.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */