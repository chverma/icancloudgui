/*     */ package icancloudgui.TabbedPanels.dataClasses;
/*     */ 
/*     */ import java.io.Serializable;
/*     */ import java.util.LinkedList;
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ public class PSU
/*     */   implements Serializable
/*     */ {
/*     */   private String name;
/*     */   private double wattage;
/*     */   private double scale;
/*     */   private String comment;
/*     */   private LinkedList<EnergyEntry> energyData;
/*     */   
/*     */   public PSU()
/*     */   {
/*  24 */     this.name = "";
/*  25 */     this.wattage = 0.0D;
/*  26 */     this.scale = 0.0D;
/*  27 */     this.comment = "";
/*  28 */     this.energyData = new LinkedList();
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String toString()
/*     */   {
/*  40 */     String text = "PSU: " + this.name + "\n" + "     * Wattage: " + Double.toString(this.wattage) + " W\n" + "     * Scale: " + Double.toString(this.scale) + "\n";
/*     */     
/*     */ 
/*     */ 
/*  44 */     return text;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getComment()
/*     */   {
/*  53 */     return this.comment;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setComment(String comment)
/*     */   {
/*  61 */     this.comment = comment;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   public LinkedList<EnergyEntry> getEnergyData()
/*     */   {
/*  69 */     return this.energyData;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setEnergyData(LinkedList<EnergyEntry> energyData)
/*     */   {
/*  77 */     this.energyData = energyData;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getName()
/*     */   {
/*  85 */     return this.name;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setName(String name)
/*     */   {
/*  93 */     this.name = name;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public double getScale()
/*     */   {
/* 102 */     return this.scale;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setScale(double scale)
/*     */   {
/* 110 */     this.scale = scale;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   public double getWattage()
/*     */   {
/* 118 */     return this.wattage;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setWattage(double wattage)
/*     */   {
/* 126 */     this.wattage = wattage;
/*     */   }
/*     */ }


/* Location:              /home/chverma/Descargas/iCanCloudGUI/iCanCloudGUI.jar!/icancloudgui/TabbedPanels/dataClasses/PSU.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */