/*     */ package icancloudgui.TabbedPanels.dataClasses;
/*     */ 
/*     */ import java.io.Serializable;
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ public class VirtualMachine
/*     */   implements Serializable
/*     */ {
/*     */   private String name;
/*     */   private double memory;
/*     */   private int disk;
/*     */   private int cpuCores;
/*     */   private String comment;
/*     */   
/*     */   public VirtualMachine()
/*     */   {
/*  21 */     this.name = "";
/*  22 */     this.memory = 0.0D;
/*  23 */     this.disk = 0;
/*  24 */     this.cpuCores = 0;
/*  25 */     this.comment = "";
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String toString()
/*     */   {
/*  37 */     String text = "VM: " + this.name + "\n" + "     * CPU: " + Integer.toString(this.cpuCores) + " cores\n" + "     * Memory: " + Double.toString(this.memory) + " GB\n" + "     * Disk: " + Integer.toString(this.disk) + " GB\n";
/*     */     
/*     */ 
/*     */ 
/*     */ 
/*  42 */     return text;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getComment()
/*     */   {
/*  51 */     return this.comment;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setComment(String comment)
/*     */   {
/*  59 */     this.comment = comment;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getName()
/*     */   {
/*  67 */     return this.name;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setName(String name)
/*     */   {
/*  75 */     this.name = name;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   public int getDisk()
/*     */   {
/*  83 */     return this.disk;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setDisk(int disk)
/*     */   {
/*  91 */     this.disk = disk;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   public int getCpuCores()
/*     */   {
/*  99 */     return this.cpuCores;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setCpuCores(int cpuCores)
/*     */   {
/* 107 */     this.cpuCores = cpuCores;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   public double getMemory()
/*     */   {
/* 115 */     return this.memory;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setMemory(double memory)
/*     */   {
/* 123 */     this.memory = memory;
/*     */   }
/*     */ }


/* Location:              /home/chverma/Descargas/iCanCloudGUI/iCanCloudGUI.jar!/icancloudgui/TabbedPanels/dataClasses/VirtualMachine.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */