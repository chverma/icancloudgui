/*     */ package icancloudgui.TabbedPanels.dataClasses;
/*     */ 
/*     */ import java.io.Serializable;
/*     */ import java.util.LinkedList;
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ public class Cloud
/*     */   implements Serializable
/*     */ {
/*     */   private String name;
/*     */   private String dataCenter;
/*     */   private String cloudManager;
/*     */   private int preLoadedTenants;
/*     */   private int tenantsAfterStart;
/*     */   private String distribution;
/*     */   private double parameterX;
/*     */   private double parameterY;
/*     */   private double parameterZ;
/*     */   private double simTime;
/*     */   private String comment;
/*     */   private LinkedList<UserEntry> userData;
/*     */   private int numberOfVMsPerNode;
/*     */   private String hypevisorName;
/*     */   
/*     */   public Cloud()
/*     */   {
/*  35 */     this.name = "";
/*  36 */     this.dataCenter = "";
/*  37 */     this.cloudManager = "";
/*  38 */     this.preLoadedTenants = 0;
/*  39 */     this.tenantsAfterStart = 0;
/*  40 */     this.distribution = "";
/*  41 */     this.parameterX = -1.0D;
/*  42 */     this.parameterY = -1.0D;
/*  43 */     this.parameterZ = -1.0D;
/*  44 */     this.simTime = 0.0D;
/*  45 */     this.comment = "";
/*  46 */     this.userData = null;
/*     */     
/*  48 */     this.numberOfVMsPerNode = 0;
/*  49 */     this.hypevisorName = "";
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public int getNumVMsPerNode()
/*     */   {
/*  58 */     return this.numberOfVMsPerNode;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setNumVMsPerNode(int maxVMsPerNode)
/*     */   {
/*  66 */     this.numberOfVMsPerNode = maxVMsPerNode;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getHypervisor()
/*     */   {
/*  74 */     return this.hypevisorName;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setHypervisor(String hypevisorName)
/*     */   {
/*  82 */     this.hypevisorName = hypevisorName;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getCloudManager()
/*     */   {
/*  91 */     return this.cloudManager;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setCloudManager(String cloudManager)
/*     */   {
/*  99 */     this.cloudManager = cloudManager;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getComment()
/*     */   {
/* 107 */     return this.comment;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setComment(String comment)
/*     */   {
/* 115 */     this.comment = comment;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getDataCenter()
/*     */   {
/* 123 */     return this.dataCenter;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setDataCenter(String dataCenter)
/*     */   {
/* 131 */     this.dataCenter = dataCenter;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getDistribution()
/*     */   {
/* 139 */     return this.distribution;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setDistribution(String distribution)
/*     */   {
/* 147 */     this.distribution = distribution;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getName()
/*     */   {
/* 155 */     return this.name;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setName(String name)
/*     */   {
/* 163 */     this.name = name;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   public double getParameterX()
/*     */   {
/* 171 */     return this.parameterX;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setParameterX(double parameterX)
/*     */   {
/* 179 */     this.parameterX = parameterX;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   public double getParameterY()
/*     */   {
/* 187 */     return this.parameterY;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setParameterY(double parameterY)
/*     */   {
/* 195 */     this.parameterY = parameterY;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   public double getParameterZ()
/*     */   {
/* 203 */     return this.parameterZ;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setParameterZ(double parameterZ)
/*     */   {
/* 211 */     this.parameterZ = parameterZ;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   public int getPreLoadedTenants()
/*     */   {
/* 219 */     return this.preLoadedTenants;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setPreLoadedTenants(int preLoadedTenants)
/*     */   {
/* 227 */     this.preLoadedTenants = preLoadedTenants;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   public double getSimTime()
/*     */   {
/* 235 */     return this.simTime;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setSimTime(double simTime)
/*     */   {
/* 243 */     this.simTime = simTime;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public int getRepetitions()
/*     */   {
/* 252 */     return 0;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setRepetitions(int repetitions) {}
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public int getTenantsAfterStart()
/*     */   {
/* 268 */     return this.tenantsAfterStart;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setTenantsAfterStart(int tenantsAfterStart)
/*     */   {
/* 276 */     this.tenantsAfterStart = tenantsAfterStart;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   public LinkedList<UserEntry> getUserData()
/*     */   {
/* 284 */     return this.userData;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setUserData(LinkedList<UserEntry> userData)
/*     */   {
/* 292 */     this.userData = userData;
/*     */   }
/*     */   
/*     */   public int getNumberOfUsers()
/*     */   {
/* 297 */     return this.userData.size();
/*     */   }
/*     */ }


/* Location:              /home/chverma/Descargas/iCanCloudGUI/iCanCloudGUI.jar!/icancloudgui/TabbedPanels/dataClasses/Cloud.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */