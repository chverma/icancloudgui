/*     */ package icancloudgui.TabbedPanels.dataClasses;
/*     */ 
/*     */ import java.io.Serializable;
/*     */ import java.util.LinkedList;
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ public class Switch
/*     */   implements Serializable
/*     */ {
/*     */   private String name;
/*     */   private String comment;
/*     */   private String queueType;
/*     */   private String relayUnitType;
/*     */   private boolean csmaCdSupport;
/*     */   private boolean duplexMode;
/*     */   private int mtu;
/*     */   private LinkedList<EnergyEntry> energyData;
/*     */   
/*     */   public Switch()
/*     */   {
/*  26 */     this.name = "";
/*  27 */     this.comment = "";
/*  28 */     this.queueType = "";
/*  29 */     this.relayUnitType = "";
/*  30 */     this.csmaCdSupport = true;
/*  31 */     this.duplexMode = true;
/*  32 */     this.mtu = 1500;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */   public String toString()
/*     */   {
/*     */     String csmacdSupport;
/*     */     
/*     */ 
/*     */     
/*     */ 
/*  45 */     if (this.csmaCdSupport) {
/*  46 */       csmacdSupport = "yes";
/*     */     } else
/*  48 */       csmacdSupport = "no";
/*     */     String duplexSupport;
              if (this.duplexMode) {
/*  51 */       duplexSupport = "yes";
/*     */     } else {
/*  53 */       duplexSupport = "no";
/*     */     }
/*  55 */     String text = "Switch: " + this.name + "\n";
/*  56 */     text = text + " - Queue: " + this.queueType + "\n" + " - Relay Unit Type: " + this.relayUnitType + "\n" + " - CSMA/CD support: " + csmacdSupport + "\n" + " - Duplex Mode: " + duplexSupport + "\n";
/*     */     
/*     */ 
/*     */ 
/*     */ 
/*  61 */     return text;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getComment()
/*     */   {
/*  69 */     return this.comment;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setComment(String comment)
/*     */   {
/*  77 */     this.comment = comment;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   public LinkedList<EnergyEntry> getEnergyData()
/*     */   {
/*  85 */     return this.energyData;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setEnergyData(LinkedList<EnergyEntry> energyData)
/*     */   {
/*  93 */     this.energyData = energyData;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getName()
/*     */   {
/* 101 */     return this.name;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setName(String name)
/*     */   {
/* 109 */     this.name = name;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   public boolean isCsmaCdSupport()
/*     */   {
/* 117 */     return this.csmaCdSupport;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setCsmaCdSupport(boolean csmaCdSupport)
/*     */   {
/* 125 */     this.csmaCdSupport = csmaCdSupport;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   public boolean isDuplexMode()
/*     */   {
/* 133 */     return this.duplexMode;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setDuplexMode(boolean duplexMode)
/*     */   {
/* 141 */     this.duplexMode = duplexMode;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   public int getMtu()
/*     */   {
/* 149 */     return this.mtu;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setMtu(int mtu)
/*     */   {
/* 157 */     this.mtu = mtu;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getQueueType()
/*     */   {
/* 165 */     return this.queueType;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setQueueType(String queueType)
/*     */   {
/* 173 */     this.queueType = queueType;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getRelayUnitType()
/*     */   {
/* 181 */     return this.relayUnitType;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setRelayUnitType(String relayUnitType)
/*     */   {
/* 189 */     this.relayUnitType = relayUnitType;
/*     */   }
/*     */ }


/* Location:              /home/chverma/Descargas/iCanCloudGUI/iCanCloudGUI.jar!/icancloudgui/TabbedPanels/dataClasses/Switch.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */