/*     */ package icancloudgui.TabbedPanels.dataClasses;
/*     */ 
/*     */ import java.io.Serializable;
/*     */ import java.util.LinkedList;
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ public class Memory
/*     */   implements Serializable
/*     */ {
/*     */   private String name;
/*     */   private double readLatency;
/*     */   private double writeLatency;
/*     */   private double searchLatency;
/*     */   private int memorysize;
/*     */   private int blockSize;
/*     */   private int numDramChips;
/*     */   private int numModules;
/*     */   private String comment;
/*     */   private LinkedList<EnergyEntry> energyData;
/*     */   
/*     */   public Memory()
/*     */   {
/*  28 */     this.name = "";
/*  29 */     this.readLatency = 0.0D;
/*  30 */     this.writeLatency = 0.0D;
/*  31 */     this.searchLatency = 0.0D;
/*  32 */     this.memorysize = 0;
/*  33 */     this.blockSize = 0;
/*  34 */     this.numDramChips = 0;
/*  35 */     this.numModules = 0;
/*  36 */     this.comment = "";
/*  37 */     this.energyData = new LinkedList();
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String toString()
/*     */   {
/*  49 */     String text = "Memory: " + this.name + " of " + Integer.toString(this.memorysize) + " GB\n" + "     * " + Double.toString(this.readLatency) + " Mbps/read, " + Double.toString(this.writeLatency) + " Mbps/write\n" + "     * " + "search Time " + Double.toString(this.searchLatency) + " secs, Block size " + Integer.toString(this.blockSize) + " KB\n" + "     * " + Integer.toString(this.numModules) + " modules, " + Integer.toString(this.numDramChips) + " DRAM chips\n";
/*     */     
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*  55 */     return text;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getComment()
/*     */   {
/*  64 */     return this.comment;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setComment(String comment)
/*     */   {
/*  72 */     this.comment = comment;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   public LinkedList<EnergyEntry> getEnergyData()
/*     */   {
/*  80 */     return this.energyData;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setEnergyData(LinkedList<EnergyEntry> energyData)
/*     */   {
/*  88 */     this.energyData = energyData;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getName()
/*     */   {
/*  96 */     return this.name;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setName(String name)
/*     */   {
/* 104 */     this.name = name;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   public int getBlockSize()
/*     */   {
/* 112 */     return this.blockSize;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setBlockSize(int blockSize)
/*     */   {
/* 120 */     this.blockSize = blockSize;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   public int getMemorysize()
/*     */   {
/* 128 */     return this.memorysize;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setMemorysize(int memorysize)
/*     */   {
/* 136 */     this.memorysize = memorysize;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   public int getNumDramChips()
/*     */   {
/* 144 */     return this.numDramChips;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setNumDramChips(int numDramChips)
/*     */   {
/* 152 */     this.numDramChips = numDramChips;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   public int getNumModules()
/*     */   {
/* 160 */     return this.numModules;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setNumModules(int numModules)
/*     */   {
/* 168 */     this.numModules = numModules;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   public double getReadLatency()
/*     */   {
/* 176 */     return this.readLatency;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setReadLatency(double readLatency)
/*     */   {
/* 184 */     this.readLatency = readLatency;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   public double getSearchLatency()
/*     */   {
/* 192 */     return this.searchLatency;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setSearchLatency(double searchLatency)
/*     */   {
/* 200 */     this.searchLatency = searchLatency;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   public double getWriteLatency()
/*     */   {
/* 208 */     return this.writeLatency;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setWriteLatency(double writeLatency)
/*     */   {
/* 216 */     this.writeLatency = writeLatency;
/*     */   }
/*     */ }


/* Location:              /home/chverma/Descargas/iCanCloudGUI/iCanCloudGUI.jar!/icancloudgui/TabbedPanels/dataClasses/Memory.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */