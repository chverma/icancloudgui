/*    */ package icancloudgui.TabbedPanels.dataClasses.Applications;
/*    */ 
/*    */ import java.io.Serializable;
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ public class ServerApp
/*    */   implements Serializable
/*    */ {
/*    */   private int instances;
/*    */   private int processing;
/*    */   private int requests;
/*    */   
/*    */   public ServerApp()
/*    */   {
/* 19 */     this.instances = 0;
/* 20 */     this.processing = 0;
/* 21 */     this.requests = 0;
/*    */   }
/*    */   
/*    */ 
/*    */ 
/*    */ 
/*    */   public int getInstances()
/*    */   {
/* 29 */     return this.instances;
/*    */   }
/*    */   
/*    */ 
/*    */ 
/*    */ 
/*    */   public void setInstances(int instances)
/*    */   {
/* 37 */     this.instances = instances;
/*    */   }
/*    */   
/*    */ 
/*    */ 
/*    */ 
/*    */   public int getProcessing()
/*    */   {
/* 45 */     return this.processing;
/*    */   }
/*    */   
/*    */ 
/*    */ 
/*    */ 
/*    */   public void setProcessing(int processing)
/*    */   {
/* 53 */     this.processing = processing;
/*    */   }
/*    */   
/*    */ 
/*    */ 
/*    */ 
/*    */   public int getRequests()
/*    */   {
/* 61 */     return this.requests;
/*    */   }
/*    */   
/*    */ 
/*    */ 
/*    */ 
/*    */   public void setRequests(int requests)
/*    */   {
/* 69 */     this.requests = requests;
/*    */   }
/*    */ }


/* Location:              /home/chverma/Descargas/iCanCloudGUI/iCanCloudGUI.jar!/icancloudgui/TabbedPanels/dataClasses/Applications/ServerApp.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */