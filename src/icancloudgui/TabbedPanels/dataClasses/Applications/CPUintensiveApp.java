/*    */ package icancloudgui.TabbedPanels.dataClasses.Applications;
/*    */ 
/*    */ import java.io.Serializable;
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ public class CPUintensiveApp
/*    */   implements Serializable
/*    */ {
/*    */   private int instances;
/*    */   private int processing;
/*    */   private int iterations;
/*    */   private int inputSize;
/*    */   private int outputSize;
/*    */   
/*    */   public CPUintensiveApp()
/*    */   {
/* 22 */     this.instances = 0;
/* 23 */     this.processing = 0;
/* 24 */     this.iterations = 0;
/*    */   }
/*    */   
/*    */ 
/*    */ 
/*    */ 
/*    */   public int getInstances()
/*    */   {
/* 32 */     return this.instances;
/*    */   }
/*    */   
/*    */ 
/*    */ 
/*    */ 
/*    */   public void setInstances(int instances)
/*    */   {
/* 40 */     this.instances = instances;
/*    */   }
/*    */   
/*    */ 
/*    */ 
/*    */ 
/*    */   public int getProcessing()
/*    */   {
/* 48 */     return this.processing;
/*    */   }
/*    */   
/*    */ 
/*    */ 
/*    */ 
/*    */   public void setProcessing(int processing)
/*    */   {
/* 56 */     this.processing = processing;
/*    */   }
/*    */   
/*    */ 
/*    */ 
/*    */ 
/*    */   public int getIterations()
/*    */   {
/* 64 */     return this.iterations;
/*    */   }
/*    */   
/*    */ 
/*    */ 
/*    */ 
/*    */   public void setIterations(int iterations)
/*    */   {
/* 72 */     this.iterations = iterations;
/*    */   }
/*    */   
/*    */   public int getInputSize() {
/* 76 */     return this.inputSize;
/*    */   }
/*    */   
/*    */   public void setInputSize(int inputSize) {
/* 80 */     this.inputSize = inputSize;
/*    */   }
/*    */   
/*    */   public int getOutputSize() {
/* 84 */     return this.outputSize;
/*    */   }
/*    */   
/*    */   public void setOutputSize(int outputSize) {
/* 88 */     this.outputSize = outputSize;
/*    */   }
/*    */ }


/* Location:              /home/chverma/Descargas/iCanCloudGUI/iCanCloudGUI.jar!/icancloudgui/TabbedPanels/dataClasses/Applications/CPUintensiveApp.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */