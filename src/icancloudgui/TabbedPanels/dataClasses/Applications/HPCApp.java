/*     */ package icancloudgui.TabbedPanels.dataClasses.Applications;
/*     */ 
/*     */ import java.io.Serializable;
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ public class HPCApp
/*     */   implements Serializable
/*     */ {
/*     */   private int instances;
/*     */   private int processing;
/*     */   private int iterations;
/*     */   private int workersSet;
/*     */   private int dataWorkers;
/*     */   private int dataCoordinator;
/*     */   private boolean workersRead;
/*     */   private boolean workersWrite;
/*     */   private int numProcesses;
/*     */   
/*     */   public HPCApp()
/*     */   {
/*  26 */     this.instances = 0;
/*  27 */     this.processing = 0;
/*  28 */     this.iterations = 0;
/*  29 */     this.workersSet = 0;
/*  30 */     this.dataWorkers = 0;
/*  31 */     this.dataCoordinator = 0;
/*  32 */     this.workersRead = false;
/*  33 */     this.workersWrite = false;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   public int getInstances()
/*     */   {
/*  41 */     return this.instances;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setInstances(int instances)
/*     */   {
/*  49 */     this.instances = instances;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   public int getProcessing()
/*     */   {
/*  57 */     return this.processing;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setProcessing(int processing)
/*     */   {
/*  65 */     this.processing = processing;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   public int getIterations()
/*     */   {
/*  73 */     return this.iterations;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setIterations(int iterations)
/*     */   {
/*  81 */     this.iterations = iterations;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   public int getDataCoordinator()
/*     */   {
/*  89 */     return this.dataCoordinator;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setDataCoordinator(int dataCoordinator)
/*     */   {
/*  97 */     this.dataCoordinator = dataCoordinator;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   public int getDataWorkers()
/*     */   {
/* 105 */     return this.dataWorkers;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setDataWorkers(int dataWorkers)
/*     */   {
/* 113 */     this.dataWorkers = dataWorkers;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   public boolean isWorkersRead()
/*     */   {
/* 121 */     return this.workersRead;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setWorkersRead(boolean workersRead)
/*     */   {
/* 129 */     this.workersRead = workersRead;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   public int getWorkersSet()
/*     */   {
/* 137 */     return this.workersSet;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setWorkersSet(int workersSet)
/*     */   {
/* 145 */     this.workersSet = workersSet;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   public boolean isWorkersWrite()
/*     */   {
/* 153 */     return this.workersWrite;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setWorkersWrite(boolean workersWrite)
/*     */   {
/* 161 */     this.workersWrite = workersWrite;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   public int getNumProcesses()
/*     */   {
/* 169 */     return this.numProcesses;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setNumProcesses(int numProcesses)
/*     */   {
/* 177 */     this.numProcesses = numProcesses;
/*     */   }
/*     */ }


/* Location:              /home/chverma/Descargas/iCanCloudGUI/iCanCloudGUI.jar!/icancloudgui/TabbedPanels/dataClasses/Applications/HPCApp.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */