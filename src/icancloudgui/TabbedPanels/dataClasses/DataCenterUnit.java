/*     */ package icancloudgui.TabbedPanels.dataClasses;
/*     */ 
/*     */ import icancloudgui.Utils.Utils;
/*     */ import java.awt.Insets;
/*     */ import java.awt.Point;
/*     */ import java.awt.event.MouseListener;
/*     */ import java.io.Serializable;
/*     */ import java.net.URL;
/*     */ import javax.swing.ImageIcon;
/*     */ import javax.swing.JLabel;
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ public class DataCenterUnit
/*     */   implements Serializable
/*     */ {
/*     */   private transient JLabel elementLabel;
/*     */   private String elementName;
/*     */   private String elementType;
/*     */   private String completeName;
/*     */   private Point point;
/*     */   
/*     */   public DataCenterUnit(String type, String name, Insets insets, MouseListener listener, int number)
/*     */   {
/*  43 */     this.elementType = type;
/*  44 */     this.elementName = name;
/*  45 */     this.completeName = (type + " (" + name + ")[" + Integer.toString(number) + "]");
/*  46 */     this.elementLabel = null;
/*  47 */     URL url = null;
/*     */     
/*     */ 
/*  50 */     if (type.equals("Computing")) {
/*  51 */       url = getClass().getResource("resources/computing.png");
/*     */     }
/*  53 */     else if (type.equals("Storage")) {
/*  54 */       url = getClass().getResource("resources/storage.png");
/*     */     }
/*  56 */     else if (type.equals("Switch")) {
/*  57 */       url = getClass().getResource("resources/switch.png");
/*     */     }
/*  59 */     else if (type.equals("Rack")) {
/*  60 */       url = getClass().getResource("resources/rack.png");
/*     */     }
/*     */     
/*     */ 
/*  64 */     ImageIcon icon = new ImageIcon(url);
/*  65 */     this.elementLabel = Utils.createLabel(icon, this.completeName, insets, listener);
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void saveCoordinates()
/*     */   {
/*  74 */     this.point = new Point(this.elementLabel.getX(), this.elementLabel.getY());
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void loadIconCoordinates(Insets insets, MouseListener listener)
/*     */   {
/*  89 */     URL url = null;
/*     */     
/*     */ 
/*  92 */     if (this.elementType.equals("Computing")) {
/*  93 */       url = getClass().getResource("resources/computing.png");
/*     */     }
/*  95 */     else if (this.elementType.equals("Storage")) {
/*  96 */       url = getClass().getResource("resources/storage.png");
/*     */     }
/*  98 */     else if (this.elementType.equals("Switch")) {
/*  99 */       url = getClass().getResource("resources/switch.png");
/*     */     }
/* 101 */     else if (this.elementType.equals("Rack")) {
/* 102 */       url = getClass().getResource("resources/rack.png");
/*     */     }
/*     */     
/* 105 */     ImageIcon icon = new ImageIcon(url);
/* 106 */     this.elementLabel = Utils.createLabel(icon, this.completeName, insets, listener);
/* 107 */     this.elementLabel.setLocation(this.point);
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getName()
/*     */   {
/* 115 */     return this.elementName;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getCompleteName()
/*     */   {
/* 123 */     return this.completeName;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getType()
/*     */   {
/* 131 */     return this.elementType;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   public JLabel getLabel()
/*     */   {
/* 139 */     return this.elementLabel;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getIcon()
/*     */   {
/* 148 */     return this.elementLabel.getIcon().toString();
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   public int getX()
/*     */   {
/* 156 */     return this.elementLabel.getX();
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   public int getY()
/*     */   {
/* 164 */     return this.elementLabel.getY();
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public int getWidth()
/*     */   {
/* 173 */     return this.elementLabel.getWidth();
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public int getHeight()
/*     */   {
/* 182 */     return this.elementLabel.getHeight();
/*     */   }
/*     */ }


/* Location:              /home/chverma/Descargas/iCanCloudGUI/iCanCloudGUI.jar!/icancloudgui/TabbedPanels/dataClasses/DataCenterUnit.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */