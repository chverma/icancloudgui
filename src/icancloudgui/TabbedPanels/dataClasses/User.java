/*     */ package icancloudgui.TabbedPanels.dataClasses;
/*     */ 
/*     */ import icancloudgui.TabbedPanels.dataClasses.Applications.CPUintensiveApp;
/*     */ import icancloudgui.TabbedPanels.dataClasses.Applications.HPCApp;
/*     */ import icancloudgui.TabbedPanels.dataClasses.Applications.ServerApp;
/*     */ import java.io.Serializable;
/*     */ import java.util.LinkedList;
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ public class User
/*     */   implements Serializable
/*     */ {
/*     */   private String name;
/*     */   private String comment;
/*     */   private boolean serverAppActive;
/*     */   private boolean cpuAppActive;
/*     */   private boolean hpcAppActive;
/*     */   private ServerApp serverApp;
/*     */   public HPCApp hpcApp;
/*     */   private CPUintensiveApp cpuApp;
/*     */   private LinkedList<VMEntry> VMData;
/*     */   
/*     */   public User()
/*     */   {
/*  31 */     this.name = "";
/*  32 */     this.comment = "";
/*  33 */     this.serverAppActive = false;
/*  34 */     this.cpuAppActive = false;
/*  35 */     this.hpcAppActive = false;
/*     */     
/*  37 */     this.serverApp = new ServerApp();
/*  38 */     this.hpcApp = new HPCApp();
/*  39 */     this.cpuApp = new CPUintensiveApp();
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getComment()
/*     */   {
/*  47 */     return this.comment;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setComment(String comment)
/*     */   {
/*  55 */     this.comment = comment;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getName()
/*     */   {
/*  63 */     return this.name;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setName(String name)
/*     */   {
/*  71 */     this.name = name;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   public CPUintensiveApp getCpuApp()
/*     */   {
/*  79 */     return this.cpuApp;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setCpuApp(CPUintensiveApp cpuApp)
/*     */   {
/*  87 */     this.cpuApp = cpuApp;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   public boolean isCpuAppActive()
/*     */   {
/*  95 */     return this.cpuAppActive;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setCpuAppActive(boolean cpuAppActive)
/*     */   {
/* 103 */     this.cpuAppActive = cpuAppActive;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   public HPCApp getHpcApp()
/*     */   {
/* 111 */     return this.hpcApp;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setHpcApp(HPCApp hpcApp)
/*     */   {
/* 119 */     this.hpcApp = hpcApp;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   public boolean isHpcAppActive()
/*     */   {
/* 127 */     return this.hpcAppActive;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setHpcAppActive(boolean hpcAppActive)
/*     */   {
/* 135 */     this.hpcAppActive = hpcAppActive;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   public ServerApp getServerApp()
/*     */   {
/* 143 */     return this.serverApp;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setServerApp(ServerApp serverApp)
/*     */   {
/* 151 */     this.serverApp = serverApp;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   public boolean isServerAppActive()
/*     */   {
/* 159 */     return this.serverAppActive;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setServerAppActive(boolean serverAppActive)
/*     */   {
/* 167 */     this.serverAppActive = serverAppActive;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   public LinkedList<VMEntry> getVMData()
/*     */   {
/* 175 */     return this.VMData;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setVMData(LinkedList<VMEntry> VMData)
/*     */   {
/* 183 */     this.VMData = VMData;
/*     */   }
/*     */ }


/* Location:              /home/chverma/Descargas/iCanCloudGUI/iCanCloudGUI.jar!/icancloudgui/TabbedPanels/dataClasses/User.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */