/*    */ package icancloudgui.TabbedPanels.dataClasses;
/*    */ 
/*    */ import java.io.Serializable;
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ public class VMEntry
/*    */   implements Serializable
/*    */ {
/*    */   private String name;
/*    */   private int instances;
/*    */   
/*    */   public VMEntry()
/*    */   {
/* 19 */     this.name = "";
/* 20 */     this.instances = 0;
/*    */   }
/*    */   
/*    */ 
/*    */ 
/*    */ 
/*    */   public int getInstances()
/*    */   {
/* 28 */     return this.instances;
/*    */   }
/*    */   
/*    */ 
/*    */ 
/*    */ 
/*    */   public void setInstances(int instances)
/*    */   {
/* 36 */     this.instances = instances;
/*    */   }
/*    */   
/*    */ 
/*    */ 
/*    */ 
/*    */   public String getName()
/*    */   {
/* 44 */     return this.name;
/*    */   }
/*    */   
/*    */ 
/*    */ 
/*    */ 
/*    */   public void setName(String name)
/*    */   {
/* 52 */     this.name = name;
/*    */   }
/*    */ }


/* Location:              /home/chverma/Descargas/iCanCloudGUI/iCanCloudGUI.jar!/icancloudgui/TabbedPanels/dataClasses/VMEntry.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */