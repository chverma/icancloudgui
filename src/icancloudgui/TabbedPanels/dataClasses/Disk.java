/*     */ package icancloudgui.TabbedPanels.dataClasses;
/*     */ 
/*     */ import java.io.Serializable;
/*     */ import java.util.LinkedList;
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ public class Disk
/*     */   implements Serializable
/*     */ {
/*     */   private String name;
/*     */   private int numStorageDevices;
/*     */   private int sizePerDevice;
/*     */   private double readBandwidth;
/*     */   private double writeBandwidth;
/*     */   private double seekTime;
/*     */   private String comment;
/*     */   private LinkedList<EnergyEntry> energyData;
/*     */   
/*     */   public Disk()
/*     */   {
/*  27 */     this.name = "";
/*  28 */     this.numStorageDevices = 0;
/*  29 */     this.sizePerDevice = 0;
/*  30 */     this.readBandwidth = 0.0D;
/*  31 */     this.writeBandwidth = 0.0D;
/*  32 */     this.seekTime = 0.0D;
/*  33 */     this.comment = "";
/*  34 */     this.energyData = new LinkedList();
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String toString()
/*     */   {
/*  47 */     String text = "Disk: " + this.name + "\n" + "     * " + Integer.toString(this.numStorageDevices) + " disk(s) of " + Integer.toString(this.sizePerDevice) + " MB\n" + "     * " + Double.toString(this.readBandwidth) + " Mbps/read, " + Double.toString(this.writeBandwidth) + " Mbps/write\n" + "     * " + "seekTime " + Double.toString(this.seekTime) + " secs.\n";
/*     */     
/*     */ 
/*     */ 
/*     */ 
/*  52 */     return text;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getComment()
/*     */   {
/*  61 */     return this.comment;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setComment(String comment)
/*     */   {
/*  69 */     this.comment = comment;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   public LinkedList<EnergyEntry> getEnergyData()
/*     */   {
/*  77 */     return this.energyData;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setEnergyData(LinkedList<EnergyEntry> energyData)
/*     */   {
/*  85 */     this.energyData = energyData;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getName()
/*     */   {
/*  93 */     return this.name;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setName(String name)
/*     */   {
/* 101 */     this.name = name;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   public int getNumStorageDevices()
/*     */   {
/* 109 */     return this.numStorageDevices;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setNumStorageDevices(int numStorageDevices)
/*     */   {
/* 117 */     this.numStorageDevices = numStorageDevices;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   public double getReadBandwidth()
/*     */   {
/* 125 */     return this.readBandwidth;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setReadBandwidth(double readBandwidth)
/*     */   {
/* 133 */     this.readBandwidth = readBandwidth;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   public double getSeekTime()
/*     */   {
/* 141 */     return this.seekTime;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setSeekTime(double seekTime)
/*     */   {
/* 149 */     this.seekTime = seekTime;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   public int getSizePerDevice()
/*     */   {
/* 157 */     return this.sizePerDevice;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setSizePerDevice(int sizePerDevice)
/*     */   {
/* 165 */     this.sizePerDevice = sizePerDevice;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   public double getWriteBandwidth()
/*     */   {
/* 173 */     return this.writeBandwidth;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setWriteBandwidth(double writeBandwidth)
/*     */   {
/* 181 */     this.writeBandwidth = writeBandwidth;
/*     */   }
/*     */ }


/* Location:              /home/chverma/Descargas/iCanCloudGUI/iCanCloudGUI.jar!/icancloudgui/TabbedPanels/dataClasses/Disk.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */