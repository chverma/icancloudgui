/*     */ package icancloudgui.TabbedPanels.dataClasses;
/*     */ 
/*     */ import java.io.Serializable;
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ public class Rack
/*     */   implements Serializable
/*     */ {
/*     */   private String name;
/*     */   private boolean isStorage;
/*     */   private String nodeType;
/*     */   private String switchType;
/*     */   private int numNodes;
/*     */   private String commLink;
/*     */   private Double ber;
/*     */   private Double per;
/*     */   private Double delay;
/*     */   private String comment;
/*     */   
/*     */   public Rack()
/*     */   {
/*  26 */     this.name = "";
/*  27 */     this.nodeType = "";
/*  28 */     this.switchType = "";
/*  29 */     this.numNodes = 0;
/*  30 */     this.commLink = "";
/*  31 */     this.ber = Double.valueOf(0.0D);
/*  32 */     this.per = Double.valueOf(0.0D);
/*  33 */     this.delay = Double.valueOf(0.0D);
/*  34 */     this.comment = "";
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getShortDescription()
/*     */   {
/*  42 */     String text = "";
/*     */     
/*     */ 
/*  45 */     text = text + "   - " + this.name;
/*     */     
/*  47 */     if (this.isStorage) {
/*  48 */       text = text + " (Storage), ";
/*     */     } else {
/*  50 */       text = text + " (Computing), ";
/*     */     }
/*     */     
/*  53 */     text = text + Integer.toString(this.numNodes) + "x nodes " + this.nodeType;
/*     */     
/*  55 */     return text;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getComment()
/*     */   {
/*  64 */     return this.comment;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setComment(String comment)
/*     */   {
/*  72 */     this.comment = comment;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getName()
/*     */   {
/*  80 */     return this.name;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setName(String name)
/*     */   {
/*  88 */     this.name = name;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   public boolean isIsStorage()
/*     */   {
/*  96 */     return this.isStorage;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setIsStorage(boolean isStorage)
/*     */   {
/* 104 */     this.isStorage = isStorage;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   public Double getBer()
/*     */   {
/* 112 */     return this.ber;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setBer(Double ber)
/*     */   {
/* 120 */     this.ber = ber;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getCommLink()
/*     */   {
/* 128 */     return this.commLink;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setCommLink(String commLink)
/*     */   {
/* 136 */     this.commLink = commLink;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   public Double getPer()
/*     */   {
/* 144 */     return this.per;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setPer(Double per)
/*     */   {
/* 152 */     this.per = per;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   public Double getDelay()
/*     */   {
/* 160 */     return this.delay;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setDelay(Double delay)
/*     */   {
/* 168 */     this.delay = delay;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getNodeType()
/*     */   {
/* 177 */     return this.nodeType;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setNodeType(String nodeType)
/*     */   {
/* 185 */     this.nodeType = nodeType;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   public int getNumNodes()
/*     */   {
/* 193 */     return this.numNodes;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setNumNodes(int numNodes)
/*     */   {
/* 201 */     this.numNodes = numNodes;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getSwitchType()
/*     */   {
/* 209 */     return this.switchType;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setSwitchType(String switchType)
/*     */   {
/* 217 */     this.switchType = switchType;
/*     */   }
/*     */ }


/* Location:              /home/chverma/Descargas/iCanCloudGUI/iCanCloudGUI.jar!/icancloudgui/TabbedPanels/dataClasses/Rack.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */