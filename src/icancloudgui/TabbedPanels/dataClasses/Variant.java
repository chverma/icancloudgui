/*     */ package icancloudgui.TabbedPanels.dataClasses;
/*     */ 
/*     */ 
/*     */ public class Variant
/*     */ {
/*     */   private String cpuEnergy;
/*     */   
/*     */   private int cpuSpeed;
/*     */   
/*     */   private int cpuMinSpeed;
/*     */   
/*     */   private int cpuNumCores;
/*     */   
/*     */   private String diskEnergy;
/*     */   
/*     */   private int diskSize;
/*     */   
/*     */   private double diskReadBandwidth;
/*     */   
/*     */   private double diskWriteBandwidth;
/*     */   
/*     */   private String memoryEnergy;
/*     */   
/*     */   private int memorySize;
/*     */   
/*     */   private double memoryReadLatency;
/*     */   
/*     */   private double memoryWriteLatency;
/*     */   
/*     */   private String switchBandwidth;
/*     */   
/*     */   private int numStorageServers;
/*     */   
/*     */   public Variant()
/*     */   {
/*  36 */     this.cpuEnergy = "";
/*  37 */     this.cpuSpeed = 0;
/*  38 */     this.cpuMinSpeed = 0;
/*  39 */     this.cpuNumCores = 0;
/*  40 */     this.diskEnergy = "";
/*  41 */     this.diskSize = 0;
/*  42 */     this.diskReadBandwidth = 0.0D;
/*  43 */     this.diskWriteBandwidth = 0.0D;
/*  44 */     this.memoryEnergy = "";
/*  45 */     this.memorySize = 0;
/*  46 */     this.memoryReadLatency = 0.0D;
/*  47 */     this.memoryWriteLatency = 0.0D;
/*  48 */     this.switchBandwidth = "";
/*  49 */     this.numStorageServers = 0;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String toString()
/*     */   {
/*  61 */     String text = "";
/*     */     
/*  63 */     text = text + "- CPU: " + this.cpuNumCores + " - " + this.cpuSpeed + " - " + this.cpuMinSpeed + " -> " + this.cpuEnergy + "\n";
/*  64 */     text = text + "- Disk: " + this.diskSize + " - " + this.diskReadBandwidth + " - " + this.diskWriteBandwidth + " -> " + this.diskEnergy + "\n";
/*  65 */     text = text + "- Memory: " + this.memorySize + " - " + this.memoryReadLatency + " - " + this.memoryWriteLatency + " -> " + this.memoryEnergy + "\n";
/*  66 */     text = text + "- Switch bandwidth: " + this.switchBandwidth + "\n\n";
/*     */     
/*     */ 
/*  69 */     return text;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getCpuEnergy()
/*     */   {
/*  77 */     return this.cpuEnergy;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setCpuEnergy(String cpuEnergy)
/*     */   {
/*  85 */     this.cpuEnergy = cpuEnergy;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   public int getCpuMinSpeed()
/*     */   {
/*  93 */     return this.cpuMinSpeed;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setCpuMinSpeed(int cpuMinSpeed)
/*     */   {
/* 101 */     this.cpuMinSpeed = cpuMinSpeed;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   public int getCpuSpeed()
/*     */   {
/* 109 */     return this.cpuSpeed;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setCpuSpeed(int cpuSpeed)
/*     */   {
/* 117 */     this.cpuSpeed = cpuSpeed;
/*     */   }
/*     */   
/*     */   public int getCpuNumCores() {
/* 121 */     return this.cpuNumCores;
/*     */   }
/*     */   
/*     */   public void setCpuNumCores(int cpuNumCores) {
/* 125 */     this.cpuNumCores = cpuNumCores;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getDiskEnergy()
/*     */   {
/* 133 */     return this.diskEnergy;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setDiskEnergy(String diskEnergy)
/*     */   {
/* 141 */     this.diskEnergy = diskEnergy;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   public int getDiskSize()
/*     */   {
/* 149 */     return this.diskSize;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setDiskSize(int diskSize)
/*     */   {
/* 157 */     this.diskSize = diskSize;
/*     */   }
/*     */   
/*     */   public double getDiskReadBandwidth() {
/* 161 */     return this.diskReadBandwidth;
/*     */   }
/*     */   
/*     */   public void setDiskReadBandwidth(double diskReadBandwidth) {
/* 165 */     this.diskReadBandwidth = diskReadBandwidth;
/*     */   }
/*     */   
/*     */   public double getDiskWriteBandwidth() {
/* 169 */     return this.diskWriteBandwidth;
/*     */   }
/*     */   
/*     */   public void setDiskWriteBandwidth(double diskWriteBandwidth) {
/* 173 */     this.diskWriteBandwidth = diskWriteBandwidth;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getMemoryEnergy()
/*     */   {
/* 183 */     return this.memoryEnergy;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setMemoryEnergy(String memoryEnergy)
/*     */   {
/* 191 */     this.memoryEnergy = memoryEnergy;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   public int getMemorySize()
/*     */   {
/* 199 */     return this.memorySize;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setMemorySize(int memorySize)
/*     */   {
/* 207 */     this.memorySize = memorySize;
/*     */   }
/*     */   
/*     */   public double getMemoryReadLatency() {
/* 211 */     return this.memoryReadLatency;
/*     */   }
/*     */   
/*     */   public void setMemoryReadLatency(double memoryReadLatency) {
/* 215 */     this.memoryReadLatency = memoryReadLatency;
/*     */   }
/*     */   
/*     */   public double getMemoryWriteLatency() {
/* 219 */     return this.memoryWriteLatency;
/*     */   }
/*     */   
/*     */   public void setMemoryWriteLatency(double memoryWriteLatency) {
/* 223 */     this.memoryWriteLatency = memoryWriteLatency;
/*     */   }
/*     */   
/*     */   public String getSwitchBandwidth() {
/* 227 */     return this.switchBandwidth;
/*     */   }
/*     */   
/*     */   public void setSwitchBandwidth(String switchBandwidth) {
/* 231 */     this.switchBandwidth = switchBandwidth;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   public int getNumStorageServers()
/*     */   {
/* 239 */     return this.numStorageServers;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setNumStorageServers(int numStorageServers)
/*     */   {
/* 247 */     this.numStorageServers = numStorageServers;
/*     */   }
/*     */ }


/* Location:              /home/chverma/Descargas/iCanCloudGUI/iCanCloudGUI.jar!/icancloudgui/TabbedPanels/dataClasses/Variant.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */