/*     */ package icancloudgui.TabbedPanels.dataClasses;
/*     */ 
/*     */ import java.io.Serializable;
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ public class Connection
/*     */   implements Serializable
/*     */ {
/*     */   private String sourceName;
/*     */   private String destinationName;
/*     */   private String communicationLink;
/*     */   private double delay;
/*     */   private double per;
/*     */   private double ber;
/*     */   
/*     */   public String getCommunicationLink()
/*     */   {
/*  26 */     return this.communicationLink;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setCommunicationLink(String communicationLink)
/*     */   {
/*  34 */     this.communicationLink = communicationLink;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getDestination()
/*     */   {
/*  42 */     return this.destinationName;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setDestination(String destinationName)
/*     */   {
/*  50 */     this.destinationName = destinationName;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getSource()
/*     */   {
/*  58 */     return this.sourceName;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setSource(String sourceName)
/*     */   {
/*  66 */     this.sourceName = sourceName;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   public double getBer()
/*     */   {
/*  74 */     return this.ber;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setBer(double ber)
/*     */   {
/*  82 */     this.ber = ber;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   public double getDelay()
/*     */   {
/*  90 */     return this.delay;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setDelay(double delay)
/*     */   {
/*  98 */     this.delay = delay;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   public double getPer()
/*     */   {
/* 106 */     return this.per;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setPer(double per)
/*     */   {
/* 114 */     this.per = per;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */   public String toString()
/*     */   {
/* 121 */     String text = "Source: " + this.sourceName + " - Destination: " + this.destinationName;
/*     */     
/* 123 */     return text;
/*     */   }
/*     */ }


/* Location:              /home/chverma/Descargas/iCanCloudGUI/iCanCloudGUI.jar!/icancloudgui/TabbedPanels/dataClasses/Connection.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */