/*     */ package icancloudgui.TabbedPanels.dataClasses;
/*     */ 
/*     */ import java.io.Serializable;
/*     */ import java.util.LinkedList;
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ public class CPU
/*     */   implements Serializable
/*     */ {
/*     */   private String name;
/*     */   private int numCores;
/*     */   private boolean independentCores;
/*     */   private double tick;
/*     */   private int speed;
/*     */   private int minSpeed;
/*     */   private String comment;
/*     */   private LinkedList<EnergyEntry> energyData;
/*     */   
/*     */   public CPU()
/*     */   {
/*  26 */     this.name = "";
/*  27 */     this.numCores = 0;
/*  28 */     this.independentCores = true;
/*  29 */     this.tick = 0.0D;
/*  30 */     this.speed = 0;
/*  31 */     this.minSpeed = 0;
/*  32 */     this.comment = "";
/*  33 */     this.energyData = new LinkedList();
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   public String toString()
/*     */   {
/*     */     String independentCoresText;
/*     */     
/*     */     
/*     */ 
/*  47 */     if (this.independentCores) {
/*  48 */       independentCoresText = "independent cores";
/*     */     } else {
/*  50 */       independentCoresText = "non-independent cores";
/*     */     }
/*  52 */     String text = "CPU: " + this.name + "\n" + "     * " + Integer.toString(this.numCores) + " " + independentCoresText + "\n" + "     * " + Integer.toString(this.speed) + " MIPS \n" + "     * " + "tick " + Double.toString(this.tick) + " sec.\n";
/*     */     
/*     */ 
/*     */ 
/*     */ 
/*  57 */     return text;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getComment()
/*     */   {
/*  65 */     return this.comment;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setComment(String comment)
/*     */   {
/*  73 */     this.comment = comment;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   public LinkedList<EnergyEntry> getEnergyData()
/*     */   {
/*  81 */     return this.energyData;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setEnergyData(LinkedList<EnergyEntry> energyData)
/*     */   {
/*  89 */     this.energyData = energyData;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getName()
/*     */   {
/*  97 */     return this.name;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setName(String name)
/*     */   {
/* 105 */     this.name = name;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   public boolean isIndependentCores()
/*     */   {
/* 113 */     return this.independentCores;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setIndependentCores(boolean independentCores)
/*     */   {
/* 122 */     this.independentCores = independentCores;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   public int getMinSpeed()
/*     */   {
/* 130 */     return this.minSpeed;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setMinSpeed(int minSpeed)
/*     */   {
/* 138 */     this.minSpeed = minSpeed;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   public int getNumCores()
/*     */   {
/* 146 */     return this.numCores;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setNumCores(int numCores)
/*     */   {
/* 154 */     this.numCores = numCores;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   public int getSpeed()
/*     */   {
/* 162 */     return this.speed;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setSpeed(int speed)
/*     */   {
/* 170 */     this.speed = speed;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   public double getTick()
/*     */   {
/* 178 */     return this.tick;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setTick(double tick)
/*     */   {
/* 186 */     this.tick = tick;
/*     */   }
/*     */ }


/* Location:              /home/chverma/Descargas/iCanCloudGUI/iCanCloudGUI.jar!/icancloudgui/TabbedPanels/dataClasses/CPU.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */