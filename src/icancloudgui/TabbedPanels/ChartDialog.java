/*     */ package icancloudgui.TabbedPanels;
/*     */ 
/*     */ import icancloudgui.Parser.Graphics;
/*     */ import icancloudgui.Parser.LoadData;
/*     */ import icancloudgui.Parser.data.TimeStampBlock;
/*     */ import java.awt.Container;
/*     */ import java.awt.event.ActionEvent;
/*     */ import java.awt.event.MouseEvent;
/*     */ import java.io.File;
/*     */ import java.util.LinkedList;
/*     */ import javax.swing.JButton;
/*     */ import javax.swing.JCheckBox;
/*     */ import javax.swing.JFileChooser;
/*     */ import javax.swing.JLabel;
/*     */ import javax.swing.JScrollPane;
/*     */ import javax.swing.JTextArea;
/*     */ import javax.swing.JTextField;
/*     */ import org.jdesktop.application.ResourceMap;
/*     */ import org.netbeans.lib.awtextra.AbsoluteConstraints;
/*     */ 
/*     */ public class ChartDialog extends javax.swing.JDialog
/*     */ {
/*     */   private LoadData loadData;
/*     */   private Graphics graphics;
/*     */   private File dir;
/*     */   private icancloudgui.Parser.ParseUtils utils;
/*     */   private JButton buttonEnergyFile;
/*     */   private JButton buttonGenerateCharts;
/*     */   private JCheckBox checkBoxTestFile;
/*     */   private JCheckBox checkBoxVerbose;
/*     */   private JLabel labelEnergyFile;
/*     */   private JTextField textEnergyFile;
/*     */   private JTextArea textNote;
/*     */   private JScrollPane textNoteScroll;
/*     */   
/*     */   public ChartDialog(java.awt.Frame parent)
/*     */   {
/*  38 */     super(parent, true);
/*  39 */     initComponents();
/*     */     
/*  41 */     this.loadData = new LoadData();
/*  42 */     this.graphics = new Graphics();
/*  43 */     this.utils = new icancloudgui.Parser.ParseUtils();
/*     */     
/*  45 */     this.textNoteScroll.setVerticalScrollBarPolicy(21);
/*  46 */     this.textNoteScroll.setHorizontalScrollBarPolicy(31);
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private void initComponents()
/*     */   {
/*  58 */     this.labelEnergyFile = new JLabel();
/*  59 */     this.textEnergyFile = new JTextField();
/*  60 */     this.buttonEnergyFile = new JButton();
/*  61 */     this.checkBoxTestFile = new JCheckBox();
/*  62 */     this.checkBoxVerbose = new JCheckBox();
/*  63 */     this.buttonGenerateCharts = new JButton();
/*  64 */     this.textNoteScroll = new JScrollPane();
/*  65 */     this.textNote = new JTextArea();
/*     */     
/*  67 */     setDefaultCloseOperation(2);
/*  68 */     setName("Form");
/*  69 */     getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());
/*     */     
/*  71 */     ResourceMap resourceMap = ((icancloudgui.ICanCloudGUIApp)org.jdesktop.application.Application.getInstance(icancloudgui.ICanCloudGUIApp.class)).getContext().getResourceMap(ChartDialog.class);
/*  72 */     this.labelEnergyFile.setText(resourceMap.getString("labelEnergyFile.text", new Object[0]));
/*  73 */     this.labelEnergyFile.setName("labelEnergyFile");
/*  74 */     getContentPane().add(this.labelEnergyFile, new AbsoluteConstraints(20, 25, -1, -1));
/*     */     
/*  76 */     this.textEnergyFile.setName("textEnergyFile");
/*  77 */     getContentPane().add(this.textEnergyFile, new AbsoluteConstraints(100, 20, 270, -1));
/*     */     
/*  79 */     this.buttonEnergyFile.setText(resourceMap.getString("buttonEnergyFile.text", new Object[0]));
/*  80 */     this.buttonEnergyFile.setName("buttonEnergyFile");
/*  81 */     this.buttonEnergyFile.addActionListener(new java.awt.event.ActionListener() {
/*     */       public void actionPerformed(ActionEvent evt) {
/*  83 */         ChartDialog.this.buttonEnergyFileActionPerformed(evt);
/*     */       }
/*  85 */     });
/*  86 */     getContentPane().add(this.buttonEnergyFile, new AbsoluteConstraints(380, 20, -1, -1));
/*     */     
/*  88 */     this.checkBoxTestFile.setText(resourceMap.getString("checkBoxTestFile.text", new Object[0]));
/*  89 */     this.checkBoxTestFile.setName("checkBoxTestFile");
/*  90 */     getContentPane().add(this.checkBoxTestFile, new AbsoluteConstraints(20, 140, -1, -1));
/*     */     
/*  92 */     this.checkBoxVerbose.setText(resourceMap.getString("checkBoxVerbose.text", new Object[0]));
/*  93 */     this.checkBoxVerbose.setName("checkBoxVerbose");
/*  94 */     getContentPane().add(this.checkBoxVerbose, new AbsoluteConstraints(20, 180, -1, -1));
/*     */     
/*  96 */     this.buttonGenerateCharts.setText(resourceMap.getString("buttonGenerateCharts.text", new Object[0]));
/*  97 */     this.buttonGenerateCharts.setName("buttonGenerateCharts");
/*  98 */     this.buttonGenerateCharts.addMouseListener(new java.awt.event.MouseAdapter() {
/*     */       public void mouseClicked(MouseEvent evt) {
/* 100 */         ChartDialog.this.buttonGenerateChartsMouseClicked(evt);
/*     */       }
/* 102 */     });
/* 103 */     getContentPane().add(this.buttonGenerateCharts, new AbsoluteConstraints(200, 230, -1, -1));
/*     */     
/* 105 */     this.textNoteScroll.setBorder(javax.swing.BorderFactory.createEtchedBorder());
/* 106 */     this.textNoteScroll.setHorizontalScrollBarPolicy(31);
/* 107 */     this.textNoteScroll.setVerticalScrollBarPolicy(21);
/* 108 */     this.textNoteScroll.setName("textNoteScroll");
/*     */     
/* 110 */     this.textNote.setBackground(resourceMap.getColor("textNote.background"));
/* 111 */     this.textNote.setColumns(20);
/* 112 */     this.textNote.setEditable(false);
/* 113 */     this.textNote.setRows(5);
/* 114 */     this.textNote.setText(resourceMap.getString("textNote.text", new Object[0]));
/* 115 */     this.textNote.setAutoscrolls(false);
/* 116 */     this.textNote.setName("textNote");
/* 117 */     this.textNoteScroll.setViewportView(this.textNote);
/*     */     
/* 119 */     getContentPane().add(this.textNoteScroll, new AbsoluteConstraints(20, 60, 510, 60));
/*     */     
/* 121 */     pack();
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private void buttonEnergyFileActionPerformed(ActionEvent evt)
/*     */   {
/* 130 */     icancloudgui.Utils.Configuration configFile = new icancloudgui.Utils.Configuration();
/*     */     
/*     */ 
/* 133 */     JFileChooser chooser = new JFileChooser();
/* 134 */     chooser.setDialogTitle("Choose the energy file");
/* 135 */     chooser.setFileSelectionMode(0);
/* 136 */     chooser.addChoosableFileFilter(new javax.swing.filechooser.FileNameExtensionFilter("Energy Files", new String[] { "txt" }));
/* 137 */     chooser.setCurrentDirectory(new File(configFile.getProperty("iCanCloudHome") + File.separatorChar + "simulations"));
/*     */     
/*     */ 
/* 140 */     if (chooser.showOpenDialog(this) == 0) {
/* 141 */       this.textEnergyFile.setText(chooser.getSelectedFile().getAbsolutePath());
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private void buttonGenerateChartsMouseClicked(MouseEvent evt)
/*     */   {
/* 152 */     File energyFile = new File(this.textEnergyFile.getText());
/*     */     
/*     */ 
/*     */ 
/* 156 */     if (!energyFile.exists()) {
/* 157 */       icancloudgui.Utils.Utils.showErrorMessage("Energy file and Users file not found, please check!", "File not found!");
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */     }
/* 163 */     else if (energyFile.exists())
/*     */     {
/*     */ 
/* 166 */       File targetFolder = new File(energyFile.getParentFile().getAbsolutePath() + File.separatorChar + "charts");
/*     */       
/* 168 */       if (!targetFolder.exists()) {
/* 169 */         targetFolder.mkdir();
/*     */       }
/*     */       
/* 172 */       this.loadData.loadEnergyValues(energyFile.getAbsolutePath());
/*     */       
/*     */ 
/* 175 */       if (this.checkBoxVerbose.isSelected()) {
/* 176 */         this.loadData.showEnergyValues();
/*     */       }
/*     */       
/*     */ 
/* 180 */       System.out.println("Generating scripts...");
/*     */       
/*     */ 
/* 183 */       if (this.checkBoxTestFile.isSelected()) {
/* 184 */         this.loadData.writeEnergyValues(energyFile.getName(), targetFolder.getAbsolutePath());
/*     */       }
/*     */       
/*     */ 
/* 188 */       this.utils.generateLaunchScript(targetFolder.getAbsolutePath());
/*     */       
/*     */ 
/* 191 */       if (this.loadData.getEnergyList().size() > 1)
/*     */       {
/*     */ 
/* 194 */         this.graphics.generateEnergyContinuousDataCenter(this.loadData.getEnergyList(), targetFolder.getAbsolutePath());
/*     */         
/*     */ 
/* 197 */         this.graphics.generateEnergyDataCenter3D(this.loadData.getEnergyList(), targetFolder.getAbsolutePath());
/*     */         
/*     */ 
/* 200 */         this.graphics.generateEnergyDataCenter3DAggregated(this.loadData.getEnergyList(), targetFolder.getAbsolutePath());
/*     */         
/*     */ 
/* 203 */         this.graphics.generateEnergyTotal(this.loadData.getEnergyList(), targetFolder.getAbsolutePath());
/*     */         
/*     */ 
/* 206 */         this.graphics.generateEnergyContinuousNode(this.loadData.getEnergyList(), targetFolder.getAbsolutePath(), ((TimeStampBlock)this.loadData.getEnergyList().get(this.loadData.getEnergyList().size() - 1)).getMinConsumptionNodeIndex(), "min");
/* 207 */         this.graphics.generateEnergyContinuousNode(this.loadData.getEnergyList(), targetFolder.getAbsolutePath(), ((TimeStampBlock)this.loadData.getEnergyList().get(this.loadData.getEnergyList().size() - 1)).getMaxConsumptionNodeIndex(), "max");
/* 208 */         this.graphics.generateEnergyContinuousNode(this.loadData.getEnergyList(), targetFolder.getAbsolutePath(), ((TimeStampBlock)this.loadData.getEnergyList().get(this.loadData.getEnergyList().size() - 1)).getAverageConsumptionNodeIndex(), "avg");
/*     */ 
/*     */ 
/*     */       }
/*     */       else
/*     */       {
/*     */ 
/* 215 */         this.graphics.generateEnergyTotal(this.loadData.getEnergyList(), targetFolder.getAbsolutePath());
/*     */       }
/*     */       
/*     */ 
/* 219 */       System.out.println("Done!");
/*     */     }
/*     */   }
/*     */ }


/* Location:              /home/chverma/Descargas/iCanCloudGUI/iCanCloudGUI.jar!/icancloudgui/TabbedPanels/ChartDialog.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */