/*     */ package icancloudgui.TabbedPanels;
/*     */ 
/*     */ import icancloudgui.TabbedPanels.dataClasses.Memory;
/*     */ import icancloudgui.Tables.EnergyTableModel;
/*     */ import icancloudgui.Utils.Utils;
/*     */ import java.awt.event.MouseEvent;
/*     */ import java.io.ObjectOutputStream;
/*     */ import javax.swing.JLabel;
/*     */ import javax.swing.JTable;
/*     */ import javax.swing.JTextField;
/*     */ import javax.swing.table.DefaultTableModel;
/*     */ import org.jdesktop.application.ResourceMap;
/*     */ import org.netbeans.lib.awtextra.AbsoluteConstraints;
/*     */ 
/*     */ public class MemoryPanel extends javax.swing.JPanel
/*     */ {
/*     */   icancloudgui.ICanCloudGUIView mainFrame;
/*     */   private JLabel buttonClear;
/*     */   private JLabel buttonHelp;
/*     */   private JLabel buttonResizeTable;
/*     */   private JLabel buttonSave;
/*     */   private JLabel cpuEnergyLabel;
/*     */   private JLabel jLabel2;
/*     */   private JLabel jLabel3;
/*     */   private JLabel jLabel4;
/*     */   private javax.swing.JScrollPane jScrollPane1;
/*     */   private JLabel labelComment;
/*     */   private JLabel labelMinSpeed;
/*     */   private JLabel labelName;
/*     */   private JLabel labelNumCores;
/*     */   
/*     */   public MemoryPanel(icancloudgui.ICanCloudGUIView mainFrame)
/*     */   {
/*  34 */     initComponents();
/*  35 */     Utils.initEnergyTable(this.memoryTable);
/*  36 */     this.mainFrame = mainFrame;
/*     */     
/*     */ 
/*  39 */     this.labelResizeTable.setVisible(false);
/*  40 */     this.buttonResizeTable.setVisible(false);
/*  41 */     this.textNewNumRows.setVisible(false);
/*  42 */     initDefaultTableValues();
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public boolean checkValues()
/*     */   {
/*  56 */     boolean allOK = true;
/*     */     
/*     */ 
/*     */ 
/*  60 */     if (!Utils.checkName(this.textName.getText()))
/*     */     {
/*  62 */       allOK = false;
/*  63 */       Utils.showErrorMessage("Invalid name for this Memory.\nA name must start with a letter and can contain alphanumeric characters, '-' and '_'", "Wrong name!");
/*     */     }
/*     */     
/*     */ 
/*     */ 
/*     */ 
/*     */     try
/*     */     {
/*  71 */       if (allOK) {
/*  72 */         Double.parseDouble(this.textReadLatency.getText());
/*     */       }
/*     */     }
/*     */     catch (Exception e) {
/*  76 */       allOK = false;
/*  77 */       Utils.showErrorMessage("Wrong parameter for read latency.\nThis parameter must be a double.\n", "Wrong parameter!");
/*     */     }
/*     */     
/*     */ 
/*     */ 
/*     */ 
/*     */     try
/*     */     {
/*  85 */       if (allOK) {
/*  86 */         Double.parseDouble(this.textWriteLatency.getText());
/*     */       }
/*     */     }
/*     */     catch (Exception e) {
/*  90 */       allOK = false;
/*  91 */       Utils.showErrorMessage("Wrong parameter for write latency.\nThis parameter must be a double.\n", "Wrong parameter!");
/*     */     }
/*     */     
/*     */ 
/*     */ 
/*     */ 
/*     */     try
/*     */     {
/*  99 */       if (allOK) {
/* 100 */         Double.parseDouble(this.textSearchLatency.getText());
/*     */       }
/*     */     }
/*     */     catch (Exception e) {
/* 104 */       allOK = false;
/* 105 */       Utils.showErrorMessage("Wrong parameter for search latency.\nThis parameter must be a double.\n", "Wrong parameter!");
/*     */     }
/*     */     
/*     */ 
/*     */ 
/*     */ 
/*     */     try
/*     */     {
/* 113 */       if (allOK) {
/* 114 */         Integer.parseInt(this.textSize.getText());
/*     */       }
/*     */     }
/*     */     catch (Exception e) {
/* 118 */       allOK = false;
/* 119 */       Utils.showErrorMessage("Wrong parameter for the memory size.\nThis parameter must be an integer.\n", "Wrong parameter!");
/*     */     }
/*     */     
/*     */ 
/*     */ 
/*     */ 
/*     */     try
/*     */     {
/* 127 */       if (allOK) {
/* 128 */         Integer.parseInt(this.textBlockSize.getText());
/*     */       }
/*     */     }
/*     */     catch (Exception e) {
/* 132 */       allOK = false;
/* 133 */       Utils.showErrorMessage("Wrong parameter for the block size.\nThis parameter must be an integer.\n", "Wrong parameter!");
/*     */     }
/*     */     
/*     */ 
/*     */ 
/*     */ 
/*     */     try
/*     */     {
/* 141 */       if (allOK) {
/* 142 */         Integer.parseInt(this.textDramChips.getText());
/*     */       }
/*     */     }
/*     */     catch (Exception e) {
/* 146 */       allOK = false;
/* 147 */       Utils.showErrorMessage("Wrong parameter for the number of Dram chips.\nThis parameter must be an integer.\n", "Wrong parameter!");
/*     */     }
/*     */     
/*     */ 
/*     */ 
/*     */     try
/*     */     {
/* 154 */       if (allOK) {
/* 155 */         Integer.parseInt(this.textNumModules.getText());
/*     */       }
/*     */     }
/*     */     catch (Exception e) {
/* 159 */       allOK = false;
/* 160 */       Utils.showErrorMessage("Wrong parameter for the number of modules.\nThis parameter must be an integer.\n", "Wrong parameter!");
/*     */     }
/*     */     
/*     */ 
/*     */ 
/*     */ 
/* 166 */     if ((allOK) && (!Utils.checkEnergyTable((EnergyTableModel)this.memoryTable.getModel()))) {
/* 167 */       allOK = false;
/*     */       
/* 169 */       Utils.showErrorMessage("Table of energy states contains wrong values.\nEnergy state must be a String and its value a double.\n", "Wrong parameter!");
/*     */     }
/*     */     
/*     */ 
/*     */ 
/*     */ 
/* 175 */     return allOK;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public Memory panelToMemoryObject()
/*     */   {
/* 189 */     Memory currentMemory = new Memory();
/* 190 */     java.util.LinkedList<icancloudgui.TabbedPanels.dataClasses.EnergyEntry> list = new java.util.LinkedList();
/*     */     
/*     */ 
/* 193 */     currentMemory.setName(this.textName.getText());
/* 194 */     currentMemory.setReadLatency(Double.parseDouble(this.textReadLatency.getText()));
/* 195 */     currentMemory.setWriteLatency(Double.parseDouble(this.textWriteLatency.getText()));
/* 196 */     currentMemory.setSearchLatency(Double.parseDouble(this.textSearchLatency.getText()));
/* 197 */     currentMemory.setMemorysize(Integer.parseInt(this.textSize.getText()));
/* 198 */     currentMemory.setBlockSize(Integer.parseInt(this.textBlockSize.getText()));
/* 199 */     currentMemory.setNumDramChips(Integer.parseInt(this.textDramChips.getText()));
/* 200 */     currentMemory.setNumModules(Integer.parseInt(this.textNumModules.getText()));
/* 201 */     currentMemory.setComment(this.textAreaComment.getText());
/* 202 */     Utils.energyTableModelToList((EnergyTableModel)this.memoryTable.getModel(), list);
/* 203 */     currentMemory.setEnergyData(list);
/*     */     
/* 205 */     return currentMemory;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void clear()
/*     */   {
/* 214 */     this.textName.setText("");
/* 215 */     this.textReadLatency.setText("");
/* 216 */     this.textWriteLatency.setText("");
/* 217 */     this.textSearchLatency.setText("");
/* 218 */     this.textSize.setText("");
/* 219 */     this.textBlockSize.setText("");
/* 220 */     this.textDramChips.setText("");
/* 221 */     this.textNumModules.setText("");
/* 222 */     this.textAreaComment.setText("");
/* 223 */     Utils.initEnergyTable(this.memoryTable);
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private void loadMemoryObjectInPanel(Memory newMemory)
/*     */   {
/* 236 */     this.textName.setText(newMemory.getName());
/* 237 */     this.textReadLatency.setText(Double.toString(newMemory.getReadLatency()));
/* 238 */     this.textWriteLatency.setText(Double.toString(newMemory.getWriteLatency()));
/* 239 */     this.textSearchLatency.setText(Double.toString(newMemory.getSearchLatency()));
/* 240 */     this.textSize.setText(Integer.toString(newMemory.getMemorysize()));
/* 241 */     this.textBlockSize.setText(Integer.toString(newMemory.getBlockSize()));
/* 242 */     this.textDramChips.setText(Integer.toString(newMemory.getNumDramChips()));
/* 243 */     this.textNumModules.setText(Integer.toString(newMemory.getNumModules()));
/* 244 */     this.textAreaComment.setText(newMemory.getComment());
/*     */     
/*     */ 
/* 247 */     EnergyTableModel model = new EnergyTableModel();
/* 248 */     Utils.energyListToTable(newMemory.getEnergyData(), model);
/* 249 */     this.memoryTable.setModel(model);
/* 250 */     this.memoryTable.updateUI();
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void loadMemoryInPanel(String memoryName)
/*     */   {
/* 263 */     Memory loadedMemory = Utils.loadMemoryObject(memoryName);
/*     */     
/*     */ 
/* 266 */     if (loadedMemory != null) {
/* 267 */       loadMemoryObjectInPanel(loadedMemory);
/*     */     }
/*     */   }
/*     */   
/*     */   private JLabel labelResizeTable;
/*     */   private JLabel labelSpeed;
/*     */   private JLabel labelTick;
/*     */   private JTable memoryTable;
/*     */   private javax.swing.JScrollPane scrollTextArea;
/*     */   private javax.swing.JTextArea textAreaComment;
/*     */   private JTextField textBlockSize;
/*     */   private JTextField textDramChips;
/*     */   private JTextField textName;
/*     */   private JTextField textNewNumRows;
/*     */   private JTextField textNumModules;
/*     */   private JTextField textReadLatency;
/*     */   private JTextField textSearchLatency;
/*     */   private JTextField textSize;
/*     */   private JTextField textWriteLatency;
/*     */   private boolean writeMemoryToFile() {
/* 287 */     boolean allOK = checkValues();
/*     */     
/*     */ 
/* 290 */     if (allOK)
/*     */     {
/*     */ 
/* 293 */       Memory currentMemory = panelToMemoryObject();
/*     */       
/*     */ 
/* 296 */       String newMemoryFile = icancloudgui.Utils.Configuration.REPOSITORY_MEMORIES_FOLDER + java.io.File.separatorChar + currentMemory.getName();
/*     */       
/*     */ 
/*     */       try
/*     */       {
/* 301 */         if (new java.io.File(newMemoryFile).exists())
/*     */         {
/*     */ 
/* 304 */           int response = javax.swing.JOptionPane.showConfirmDialog(null, "Overwrite existing memory?", "Confirm Overwrite", 2, 3);
/*     */           
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 311 */           if (response == 0)
/*     */           {
/*     */ 
/* 314 */             java.io.FileOutputStream fout = new java.io.FileOutputStream(newMemoryFile);
/* 315 */             ObjectOutputStream oos = new ObjectOutputStream(fout);
/* 316 */             oos.writeObject(currentMemory);
/* 317 */             oos.close();
/* 318 */             allOK = true;
/*     */           }
/*     */           
/*     */ 
/*     */         }
/*     */         else
/*     */         {
/*     */ 
/* 326 */           java.io.FileOutputStream fout = new java.io.FileOutputStream(newMemoryFile);
/* 327 */           ObjectOutputStream oos = new ObjectOutputStream(fout);
/* 328 */           oos.writeObject(currentMemory);
/* 329 */           oos.close();
/* 330 */           allOK = true;
/*     */         }
/*     */       }
/*     */       catch (Exception e) {
/* 334 */         e.printStackTrace();
/*     */       }
/*     */     }
/*     */     
/* 338 */     return allOK;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private void initDefaultTableValues()
/*     */   {
/* 351 */     EnergyTableModel newModel = new EnergyTableModel();
/* 352 */     newModel.setColumnCount(3);
/* 353 */     newModel.setColumnIdentifiers(Utils.columnNamesEnergyTable);
/*     */     
/*     */ 
/* 356 */     newModel.addRow(new Object[] { Integer.toString(0), "A", "" });
/* 357 */     newModel.addRow(new Object[] { Integer.toString(1), "memory_off", "" });
/* 358 */     newModel.addRow(new Object[] { Integer.toString(2), "memory_idle", "" });
/* 359 */     newModel.addRow(new Object[] { Integer.toString(3), "memory_read", "" });
/* 360 */     newModel.addRow(new Object[] { Integer.toString(4), "memory_write", "" });
/* 361 */     newModel.addRow(new Object[] { Integer.toString(5), "memory_search", "" });
/*     */     
/* 363 */     this.memoryTable.setModel(newModel);
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private void initComponents()
/*     */   {
/* 377 */     this.labelName = new JLabel();
/* 378 */     this.labelNumCores = new JLabel();
/* 379 */     this.labelSpeed = new JLabel();
/* 380 */     this.labelMinSpeed = new JLabel();
/* 381 */     this.labelTick = new JLabel();
/* 382 */     this.textName = new JTextField();
/* 383 */     this.textReadLatency = new JTextField();
/* 384 */     this.textSearchLatency = new JTextField();
/* 385 */     this.textWriteLatency = new JTextField();
/* 386 */     this.textDramChips = new JTextField();
/* 387 */     this.jScrollPane1 = new javax.swing.JScrollPane();
/* 388 */     this.memoryTable = new JTable();
/* 389 */     this.cpuEnergyLabel = new JLabel();
/* 390 */     this.buttonResizeTable = new JLabel();
/* 391 */     this.textNewNumRows = new JTextField();
/* 392 */     this.labelResizeTable = new JLabel();
/* 393 */     this.textSize = new JTextField();
/* 394 */     this.textBlockSize = new JTextField();
/* 395 */     this.jLabel2 = new JLabel();
/* 396 */     this.jLabel3 = new JLabel();
/* 397 */     this.jLabel4 = new JLabel();
/* 398 */     this.textNumModules = new JTextField();
/* 399 */     this.buttonSave = new JLabel();
/* 400 */     this.buttonHelp = new JLabel();
/* 401 */     this.scrollTextArea = new javax.swing.JScrollPane();
/* 402 */     this.textAreaComment = new javax.swing.JTextArea();
/* 403 */     this.labelComment = new JLabel();
/* 404 */     this.buttonClear = new JLabel();
/*     */     
/* 406 */     ResourceMap resourceMap = ((icancloudgui.ICanCloudGUIApp)org.jdesktop.application.Application.getInstance(icancloudgui.ICanCloudGUIApp.class)).getContext().getResourceMap(MemoryPanel.class);
/* 407 */     setBackground(resourceMap.getColor("Form.background"));
/* 408 */     setName("Form");
/* 409 */     setPreferredSize(new java.awt.Dimension(500, 641));
/* 410 */     setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());
/*     */     
/* 412 */     this.labelName.setFont(resourceMap.getFont("labelMinSpeed.font"));
/* 413 */     this.labelName.setText(resourceMap.getString("labelName.text", new Object[0]));
/* 414 */     this.labelName.setName("labelName");
/* 415 */     add(this.labelName, new AbsoluteConstraints(10, 35, -1, -1));
/*     */     
/* 417 */     this.labelNumCores.setFont(resourceMap.getFont("labelMinSpeed.font"));
/* 418 */     this.labelNumCores.setText(resourceMap.getString("labelNumCores.text", new Object[0]));
/* 419 */     this.labelNumCores.setName("labelNumCores");
/* 420 */     add(this.labelNumCores, new AbsoluteConstraints(60, 70, -1, -1));
/*     */     
/* 422 */     this.labelSpeed.setFont(resourceMap.getFont("labelMinSpeed.font"));
/* 423 */     this.labelSpeed.setText(resourceMap.getString("labelSpeed.text", new Object[0]));
/* 424 */     this.labelSpeed.setName("labelSpeed");
/* 425 */     add(this.labelSpeed, new AbsoluteConstraints(51, 140, -1, -1));
/*     */     
/* 427 */     this.labelMinSpeed.setFont(resourceMap.getFont("labelMinSpeed.font"));
/* 428 */     this.labelMinSpeed.setText(resourceMap.getString("labelMinSpeed.text", new Object[0]));
/* 429 */     this.labelMinSpeed.setName("labelMinSpeed");
/* 430 */     add(this.labelMinSpeed, new AbsoluteConstraints(100, 175, -1, -1));
/*     */     
/* 432 */     this.labelTick.setFont(resourceMap.getFont("labelMinSpeed.font"));
/* 433 */     this.labelTick.setText(resourceMap.getString("labelTick.text", new Object[0]));
/* 434 */     this.labelTick.setName("labelTick");
/* 435 */     add(this.labelTick, new AbsoluteConstraints(59, 105, -1, -1));
/*     */     
/* 437 */     this.textName.setText(resourceMap.getString("textName.text", new Object[0]));
/* 438 */     this.textName.setName("textName");
/* 439 */     add(this.textName, new AbsoluteConstraints(50, 30, 260, -1));
/*     */     
/* 441 */     this.textReadLatency.setText(resourceMap.getString("textReadLatency.text", new Object[0]));
/* 442 */     this.textReadLatency.setName("textReadLatency");
/* 443 */     add(this.textReadLatency, new AbsoluteConstraints(170, 65, 140, -1));
/*     */     
/* 445 */     this.textSearchLatency.setText(resourceMap.getString("textSearchLatency.text", new Object[0]));
/* 446 */     this.textSearchLatency.setName("textSearchLatency");
/* 447 */     add(this.textSearchLatency, new AbsoluteConstraints(170, 135, 140, -1));
/*     */     
/* 449 */     this.textWriteLatency.setText(resourceMap.getString("textWriteLatency.text", new Object[0]));
/* 450 */     this.textWriteLatency.setName("textWriteLatency");
/* 451 */     add(this.textWriteLatency, new AbsoluteConstraints(170, 100, 140, -1));
/*     */     
/* 453 */     this.textDramChips.setText(resourceMap.getString("textDramChips.text", new Object[0]));
/* 454 */     this.textDramChips.setName("textDramChips");
/* 455 */     add(this.textDramChips, new AbsoluteConstraints(530, 100, 140, -1));
/*     */     
/* 457 */     this.jScrollPane1.setName("jScrollPane1");
/*     */     
/* 459 */     this.memoryTable.setModel(new DefaultTableModel(new Object[][] { { null, null, null }, { null, null, null }, { null, null, null }, { null, null, null }, { null, null, null }, { null, null, null }, { null, null, null }, { null, null, null }, { null, null, null }, { null, null, null } }, new String[] { "#", "Name", "Value" })
/*     */     {
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 476 */       Class[] types = { String.class, String.class, String.class };
/*     */       
/*     */ 
/* 479 */       boolean[] canEdit = { false, true, true };
/*     */       
/*     */ 
/*     */       public Class getColumnClass(int columnIndex)
/*     */       {
/* 484 */         return this.types[columnIndex];
/*     */       }
/*     */       
/*     */       public boolean isCellEditable(int rowIndex, int columnIndex) {
/* 488 */         return this.canEdit[columnIndex];
/*     */       }
/* 490 */     });
/* 491 */     this.memoryTable.setGridColor(resourceMap.getColor("memoryTable.gridColor"));
/* 492 */     this.memoryTable.setName("memoryTable");
/* 493 */     this.jScrollPane1.setViewportView(this.memoryTable);
/* 494 */     this.memoryTable.getColumnModel().getColumn(0).setHeaderValue(resourceMap.getString("memoryTable.columnModel.title0", new Object[0]));
/* 495 */     this.memoryTable.getColumnModel().getColumn(1).setHeaderValue(resourceMap.getString("memoryTable.columnModel.title1", new Object[0]));
/* 496 */     this.memoryTable.getColumnModel().getColumn(2).setHeaderValue(resourceMap.getString("memoryTable.columnModel.title2", new Object[0]));
/*     */     
/* 498 */     add(this.jScrollPane1, new AbsoluteConstraints(20, 260, 760, 180));
/*     */     
/* 500 */     this.cpuEnergyLabel.setFont(resourceMap.getFont("labelMinSpeed.font"));
/* 501 */     this.cpuEnergyLabel.setText(resourceMap.getString("cpuEnergyLabel.text", new Object[0]));
/* 502 */     this.cpuEnergyLabel.setName("cpuEnergyLabel");
/* 503 */     add(this.cpuEnergyLabel, new AbsoluteConstraints(10, 230, -1, -1));
/*     */     
/* 505 */     this.buttonResizeTable.setIcon(resourceMap.getIcon("buttonResizeTable.icon"));
/* 506 */     this.buttonResizeTable.setText(resourceMap.getString("buttonResizeTable.text", new Object[0]));
/* 507 */     this.buttonResizeTable.setToolTipText(resourceMap.getString("buttonResizeTable.toolTipText", new Object[0]));
/* 508 */     this.buttonResizeTable.setName("buttonResizeTable");
/* 509 */     this.buttonResizeTable.addMouseListener(new java.awt.event.MouseAdapter() {
/*     */       public void mouseClicked(MouseEvent evt) {
/* 511 */         MemoryPanel.this.buttonResizeTableMouseClicked(evt);
/*     */       }
/* 513 */     });
/* 514 */     add(this.buttonResizeTable, new AbsoluteConstraints(690, 230, -1, -1));
/*     */     
/* 516 */     this.textNewNumRows.setText(resourceMap.getString("textNewNumRows.text", new Object[0]));
/* 517 */     this.textNewNumRows.setName("textNewNumRows");
/* 518 */     add(this.textNewNumRows, new AbsoluteConstraints(720, 225, 60, -1));
/*     */     
/* 520 */     this.labelResizeTable.setFont(resourceMap.getFont("labelMinSpeed.font"));
/* 521 */     this.labelResizeTable.setForeground(resourceMap.getColor("labelResizeTable.foreground"));
/* 522 */     this.labelResizeTable.setText(resourceMap.getString("labelResizeTable.text", new Object[0]));
/* 523 */     this.labelResizeTable.setName("labelResizeTable");
/* 524 */     add(this.labelResizeTable, new AbsoluteConstraints(600, 230, -1, -1));
/*     */     
/* 526 */     this.textSize.setName("textSize");
/* 527 */     add(this.textSize, new AbsoluteConstraints(170, 170, 140, -1));
/*     */     
/* 529 */     this.textBlockSize.setName("textBlockSize");
/* 530 */     add(this.textBlockSize, new AbsoluteConstraints(530, 65, 140, -1));
/*     */     
/* 532 */     this.jLabel2.setFont(resourceMap.getFont("labelMinSpeed.font"));
/* 533 */     this.jLabel2.setText(resourceMap.getString("jLabel2.text", new Object[0]));
/* 534 */     this.jLabel2.setName("jLabel2");
/* 535 */     add(this.jLabel2, new AbsoluteConstraints(430, 70, -1, -1));
/*     */     
/* 537 */     this.jLabel3.setFont(resourceMap.getFont("labelMinSpeed.font"));
/* 538 */     this.jLabel3.setText(resourceMap.getString("jLabel3.text", new Object[0]));
/* 539 */     this.jLabel3.setName("jLabel3");
/* 540 */     add(this.jLabel3, new AbsoluteConstraints(370, 105, -1, -1));
/*     */     
/* 542 */     this.jLabel4.setFont(resourceMap.getFont("labelMinSpeed.font"));
/* 543 */     this.jLabel4.setText(resourceMap.getString("jLabel4.text", new Object[0]));
/* 544 */     this.jLabel4.setName("jLabel4");
/* 545 */     add(this.jLabel4, new AbsoluteConstraints(393, 140, -1, 20));
/*     */     
/* 547 */     this.textNumModules.setName("textNumModules");
/* 548 */     add(this.textNumModules, new AbsoluteConstraints(530, 135, 140, -1));
/*     */     
/* 550 */     this.buttonSave.setIcon(resourceMap.getIcon("buttonSave.icon"));
/* 551 */     this.buttonSave.setToolTipText(resourceMap.getString("buttonSave.toolTipText", new Object[0]));
/* 552 */     this.buttonSave.setName("buttonSave");
/* 553 */     this.buttonSave.addMouseListener(new java.awt.event.MouseAdapter() {
/*     */       public void mouseClicked(MouseEvent evt) {
/* 555 */         MemoryPanel.this.buttonSaveMouseClicked(evt);
/*     */       }
/* 557 */     });
/* 558 */     add(this.buttonSave, new AbsoluteConstraints(710, 20, -1, -1));
/*     */     
/* 560 */     this.buttonHelp.setIcon(resourceMap.getIcon("buttonHelp.icon"));
/* 561 */     this.buttonHelp.setToolTipText(resourceMap.getString("buttonHelp.toolTipText", new Object[0]));
/* 562 */     this.buttonHelp.setName("buttonHelp");
/* 563 */     this.buttonHelp.addMouseListener(new java.awt.event.MouseAdapter() {
/*     */       public void mouseClicked(MouseEvent evt) {
/* 565 */         MemoryPanel.this.buttonHelpMouseClicked(evt);
/*     */       }
/* 567 */     });
/* 568 */     add(this.buttonHelp, new AbsoluteConstraints(750, 20, -1, -1));
/*     */     
/* 570 */     this.scrollTextArea.setName("scrollTextArea");
/*     */     
/* 572 */     this.textAreaComment.setColumns(20);
/* 573 */     this.textAreaComment.setRows(5);
/* 574 */     this.textAreaComment.setName("textAreaComment");
/* 575 */     this.scrollTextArea.setViewportView(this.textAreaComment);
/*     */     
/* 577 */     add(this.scrollTextArea, new AbsoluteConstraints(20, 480, 760, 60));
/*     */     
/* 579 */     this.labelComment.setFont(resourceMap.getFont("labelMinSpeed.font"));
/* 580 */     this.labelComment.setText(resourceMap.getString("labelComment.text", new Object[0]));
/* 581 */     this.labelComment.setName("labelComment");
/* 582 */     add(this.labelComment, new AbsoluteConstraints(10, 455, -1, -1));
/*     */     
/* 584 */     this.buttonClear.setIcon(resourceMap.getIcon("buttonClear.icon"));
/* 585 */     this.buttonClear.setToolTipText(resourceMap.getString("buttonClear.toolTipText", new Object[0]));
/* 586 */     this.buttonClear.setName("buttonClear");
/* 587 */     this.buttonClear.addMouseListener(new java.awt.event.MouseAdapter() {
/*     */       public void mouseClicked(MouseEvent evt) {
/* 589 */         MemoryPanel.this.buttonClearMouseClicked(evt);
/*     */       }
/* 591 */     });
/* 592 */     add(this.buttonClear, new AbsoluteConstraints(670, 20, -1, -1));
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private void buttonResizeTableMouseClicked(MouseEvent evt)
/*     */   {
/*     */     try
/*     */     {
/* 610 */       int newNumRows = Integer.parseInt(this.textNewNumRows.getText());
/*     */       
/*     */ 
/* 613 */       if (this.memoryTable.getModel().getRowCount() == newNumRows)
/*     */       {
/* 615 */         javax.swing.JOptionPane.showMessageDialog(new javax.swing.JFrame(), "New number of rows is the same as the curent table", "Warning!", 2);
/*     */       }
/*     */       
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 622 */       if (newNumRows < this.memoryTable.getModel().getRowCount())
/*     */       {
/*     */ 
/* 625 */         int response = javax.swing.JOptionPane.showConfirmDialog(null, "Are you sure of resizing this table?", "Confirm resize table", 2, 3);
/*     */         
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 631 */         if (response == 0)
/*     */         {
/*     */ 
/* 634 */           DefaultTableModel newModel = (DefaultTableModel)this.memoryTable.getModel();
/* 635 */           int oldNumRows = this.memoryTable.getModel().getRowCount();
/*     */           
/*     */ 
/* 638 */           for (int i = 0; i < oldNumRows - newNumRows; i++) {
/* 639 */             System.out.println(newModel.getRowCount());
/* 640 */             newModel.removeRow(newModel.getRowCount() - 1);
/*     */           }
/*     */           
/* 643 */           this.memoryTable.setModel(newModel);
/*     */ 
/*     */         }
/*     */         
/*     */ 
/*     */       }
/*     */       else
/*     */       {
/*     */ 
/* 652 */         int response = javax.swing.JOptionPane.showConfirmDialog(null, "Are you sure of resizing this table?", "Confirm resize table", 2, 3);
/*     */         
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 658 */         if (response == 0)
/*     */         {
/*     */ 
/* 661 */           DefaultTableModel newModel = (DefaultTableModel)this.memoryTable.getModel();
/* 662 */           int oldNumRows = this.memoryTable.getModel().getRowCount();
/*     */           
/*     */ 
/* 665 */           for (int i = 0; i < newNumRows - oldNumRows; i++) {
/* 666 */             newModel.addRow(new Object[] { Integer.toString(oldNumRows + i), "", "" });
/*     */           }
/*     */           
/* 669 */           this.memoryTable.setModel(newModel);
/*     */         }
/*     */       }
/*     */     }
/*     */     catch (Exception e)
/*     */     {
/* 675 */       javax.swing.JOptionPane.showMessageDialog(new javax.swing.JFrame(), "Wrong number, please enter a correct number value", "Error!", 2);
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private void buttonSaveMouseClicked(MouseEvent evt)
/*     */   {
/* 692 */     boolean allOK = writeMemoryToFile();
/*     */     
/*     */ 
/* 695 */     if (allOK) {
/* 696 */       this.mainFrame.updateTree();
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private void buttonHelpMouseClicked(MouseEvent evt)
/*     */   {
/* 706 */     Utils.showHelp(this);
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private void buttonClearMouseClicked(MouseEvent evt)
/*     */   {
/* 715 */     int response = javax.swing.JOptionPane.showConfirmDialog(null, "Clear current Memory configuration?", "Confirm to clear this form", 2, 3);
/*     */     
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 722 */     if (response == 0) {
/* 723 */       clear();
/*     */     }
/*     */   }
/*     */ }


/* Location:              /home/chverma/Descargas/iCanCloudGUI/iCanCloudGUI.jar!/icancloudgui/TabbedPanels/MemoryPanel.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */