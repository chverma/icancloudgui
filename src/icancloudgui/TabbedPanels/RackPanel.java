/*     */ package icancloudgui.TabbedPanels;
/*     */ 
/*     */ import icancloudgui.TabbedPanels.dataClasses.Rack;
/*     */ import icancloudgui.Utils.Utils;
/*     */ import java.awt.event.ItemEvent;
/*     */ import javax.swing.JComboBox;
/*     */ import javax.swing.JLabel;
/*     */ import javax.swing.JTextArea;
/*     */ import javax.swing.JTextField;
/*     */ import org.jdesktop.application.ResourceMap;
/*     */ import org.netbeans.lib.awtextra.AbsoluteConstraints;
/*     */ 
/*     */ public class RackPanel extends javax.swing.JPanel
/*     */ {
/*     */   icancloudgui.ICanCloudGUIView mainFrame;
/*     */   private JLabel buttonClear;
/*     */   private JComboBox comboCommLink;
/*     */   private JComboBox comboNodeType;
/*     */   private JComboBox comboSwitchType;
/*     */   private javax.swing.JCheckBox isStorageCheckBox;
/*     */   private javax.swing.JScrollPane jScrollPane1;
/*     */   private javax.swing.JScrollPane jScrollPane2;
/*     */   private JLabel labelBer;
/*     */   private JLabel labelCommLink;
/*     */   private JLabel labelComment;
/*     */   private JLabel labelDelay;
/*     */   private JLabel labelDescription;
/*     */   
/*     */   public RackPanel(icancloudgui.ICanCloudGUIView mainFrame)
/*     */   {
/*  31 */     this.mainFrame = mainFrame;
/*     */     
/*  33 */     initComponents();
/*     */     
/*  35 */     reloadPanel();
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void reloadPanel()
/*     */   {
/*  49 */     String currentElement = null;
/*  50 */     boolean isStorage = false;
/*     */     
/*  52 */     if (this.comboNodeType.getItemCount() > 0) {
/*  53 */       currentElement = this.comboNodeType.getSelectedItem().toString();
/*  54 */       isStorage = this.isStorageCheckBox.isSelected();
/*     */     }
/*     */     
/*     */     java.util.LinkedList<String> list;
/*  58 */     if (isStorage) {
/*  59 */       list = Utils.getComponentListFromDir(icancloudgui.Utils.Configuration.REPOSITORY_STORAGE_NODES_FOLDER);
/*     */     } else {
/*  61 */       list = Utils.getComponentListFromDir(icancloudgui.Utils.Configuration.REPOSITORY_COMPUTING_NODES_FOLDER);
/*     */     }
/*  63 */     Utils.loadCombo(list, this.comboNodeType);
/*     */     
/*  65 */     if (currentElement != null) {
/*  66 */       Utils.initCombo(this.comboNodeType, currentElement);
/*     */     }
/*     */     
/*     */ 
/*  70 */     currentElement = null;
/*     */     
/*  72 */     if (this.comboSwitchType.getItemCount() > 0) {
/*  73 */       currentElement = this.comboSwitchType.getSelectedItem().toString();
/*     */     }
/*  75 */     list = Utils.getComponentListFromDir(icancloudgui.Utils.Configuration.REPOSITORY_SWITCHES);
/*  76 */     Utils.loadCombo(list, this.comboSwitchType);
/*     */     
/*  78 */     if (currentElement != null) {
/*  79 */       Utils.initCombo(this.comboSwitchType, currentElement);
/*     */     }
/*     */     
/*     */ 
/*  83 */     currentElement = null;
/*     */     
/*     */ 
/*  86 */     if (this.comboCommLink.getItemCount() == 0) {
/*  87 */       list = Utils.getCommunicationLinkList();
/*  88 */       Utils.loadCombo(list, this.comboCommLink);
/*     */ 
/*     */     }
/*     */     else
/*     */     {
/*  93 */       currentElement = this.comboCommLink.getSelectedItem().toString();
/*     */     }
/*     */     
/*  96 */     if (currentElement != null) {
/*  97 */       Utils.initCombo(this.comboCommLink, currentElement);
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */   public void clear()
/*     */   {
/* 105 */     this.textName.setText("");
/* 106 */     this.textComment.setText("");
/* 107 */     this.isStorageCheckBox.setSelected(false);
/* 108 */     this.textNumNodes.setText("");
/* 109 */     this.textDelay.setText("");
/* 110 */     this.textBer.setText("");
/* 111 */     this.textPer.setText("");
/*     */     
/*     */ 
/* 114 */     if (this.comboNodeType.getItemCount() > 0) {
/* 115 */       this.comboNodeType.setSelectedIndex(0);
/*     */     }
/* 117 */     if (this.comboSwitchType.getItemCount() > 0) {
/* 118 */       this.comboSwitchType.setSelectedIndex(0);
/*     */     }
/* 120 */     if (this.comboCommLink.getItemCount() > 0) {
/* 121 */       this.comboCommLink.setSelectedIndex(0);
/*     */     }
/* 123 */     showDescription();
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void showDescription()
/*     */   {
/* 136 */     String name = "";
/* 137 */     String text = "";
/*     */     
/*     */ 
/*     */ 
/* 141 */     if ((this.comboNodeType.getItemCount() > 0) && (this.comboCommLink.getItemCount() > 0) && (this.comboSwitchType.getItemCount() > 0) && (this.textNumNodes.getText().length() > 0) && (this.textBer.getText().length() > 0) && (this.textPer.getText().length() > 0) && (this.textDelay.getText().length() > 0))
/*     */     {
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 147 */       if (this.textName.getText().length() == 0) {
/* 148 */         name = "ENTER A FANCY NAME HERE!";
/*     */       }
/*     */       else {
/* 151 */         name = this.textName.getText();
/*     */       }
/*     */       
/* 154 */       text = "Configuration of rack: " + name + "\n\n";
/*     */       
/*     */ 
/* 157 */       if (this.isStorageCheckBox.isSelected()) {
/* 158 */         text = text + this.textNumNodes.getText() + "x " + Utils.loadNodeObject(true, this.comboNodeType.getSelectedItem().toString()).toString() + "\n";
/*     */       } else {
/* 160 */         text = text + this.textNumNodes.getText() + "x " + Utils.loadNodeObject(false, this.comboNodeType.getSelectedItem().toString()).toString() + "\n";
/*     */       }
/*     */       
/*     */ 
/* 164 */       text = text + Utils.loadSwitchObject(this.comboSwitchType.getSelectedItem().toString()).toString() + "\n";
/*     */       
/*     */ 
/* 167 */       text = text + "Communication link: " + this.comboCommLink.getSelectedItem().toString() + "\n";
/*     */       
/*     */ 
/*     */ 
/* 171 */       text = text + " - Delay: " + this.textDelay.getText() + "\n" + " - Ber: " + this.textBer.getText() + "\n" + " - Per: " + this.textPer.getText() + "\n";
/*     */ 
/*     */ 
/*     */     }
/*     */     else
/*     */     {
/*     */ 
/* 178 */       text = "Please, check that each component is properly configured!";
/*     */     }
/*     */     
/*     */ 
/* 182 */     this.textDescription.setText(text);
/* 183 */     this.textDescription.updateUI();
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public boolean checkValues()
/*     */   {
/* 196 */     boolean allOK = true;
/*     */     
/*     */ 
/*     */ 
/* 200 */     if (!Utils.checkName(this.textName.getText()))
/*     */     {
/* 202 */       allOK = false;
/* 203 */       Utils.showErrorMessage("Invalid name for this rack.\nA name must start with a letter and can contain alphanumeric characters, '-' and '_'", "Wrong name!");
/*     */     }
/*     */     
/*     */ 
/*     */ 
/*     */ 
/* 209 */     if ((allOK) && (this.comboNodeType.getItemCount() <= 0)) {
/* 210 */       allOK = false;
/* 211 */       Utils.showErrorMessage("Invalid node for this rack.\nA name must start with a letter and can contain alphanumeric characters, '-' and '_'", "Wrong name!");
/*     */     }
/*     */     
/*     */ 
/*     */ 
/*     */ 
/* 217 */     if ((allOK) && (this.comboSwitchType.getItemCount() <= 0)) {
/* 218 */       allOK = false;
/* 219 */       Utils.showErrorMessage("Invalid switch for this rack.\nA name must start with a letter and can contain alphanumeric characters, '-' and '_'", "Wrong name!");
/*     */     }
/*     */     
/*     */ 
/*     */ 
/*     */ 
/* 225 */     if ((allOK) && (this.comboCommLink.getItemCount() <= 0)) {
/* 226 */       allOK = false;
/* 227 */       Utils.showErrorMessage("Invalid communication link for this rack.\nA name must start with a letter and can contain alphanumeric characters, '-' and '_'", "Wrong name!");
/*     */     }
/*     */     
/*     */ 
/*     */ 
/*     */ 
/*     */     try
/*     */     {
/* 235 */       if (allOK) {
/* 236 */         Integer.parseInt(this.textNumNodes.getText());
/*     */       }
/*     */     }
/*     */     catch (Exception e) {
/* 240 */       allOK = false;
/* 241 */       Utils.showErrorMessage("Wrong parameter for number of nodes.\nThis parameter must be an integer.\n", "Wrong parameter!");
/*     */     }
/*     */     
/*     */ 
/*     */ 
/*     */     try
/*     */     {
/* 248 */       if (allOK) {
/* 249 */         Double.parseDouble(this.textBer.getText());
/*     */       }
/*     */     }
/*     */     catch (Exception e) {
/* 253 */       allOK = false;
/* 254 */       Utils.showErrorMessage("Wrong parameter for bet (bit error rate).\nThis parameter must be a double.\n", "Wrong parameter!");
/*     */     }
/*     */     
/*     */ 
/*     */ 
/*     */     try
/*     */     {
/* 261 */       if (allOK) {
/* 262 */         Double.parseDouble(this.textPer.getText());
/*     */       }
/*     */     }
/*     */     catch (Exception e) {
/* 266 */       allOK = false;
/* 267 */       Utils.showErrorMessage("Wrong parameter for per (packet error rate).\nThis parameter must be a double.\n", "Wrong parameter!");
/*     */     }
/*     */     
/*     */ 
/*     */ 
/*     */     try
/*     */     {
/* 274 */       if (allOK) {
/* 275 */         Double.parseDouble(this.textDelay.getText());
/*     */       }
/*     */     }
/*     */     catch (Exception e) {
/* 279 */       allOK = false;
/* 280 */       Utils.showErrorMessage("Wrong parameter for delay.\nThis parameter must be a double.\n", "Wrong parameter!");
/*     */     }
/*     */     
/*     */ 
/*     */ 
/* 285 */     return allOK;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public Rack panelToRackObject()
/*     */   {
/* 298 */     Rack currentRack = new Rack();
/*     */     
/*     */ 
/* 301 */     currentRack.setName(this.textName.getText());
/* 302 */     currentRack.setComment(this.textComment.getText());
/* 303 */     currentRack.setIsStorage(this.isStorageCheckBox.isSelected());
/* 304 */     currentRack.setNodeType(this.comboNodeType.getSelectedItem().toString());
/* 305 */     currentRack.setSwitchType(this.comboSwitchType.getSelectedItem().toString());
/* 306 */     currentRack.setCommLink(this.comboCommLink.getSelectedItem().toString());
/* 307 */     currentRack.setNumNodes(Integer.parseInt(this.textNumNodes.getText()));
/* 308 */     currentRack.setBer(Double.valueOf(Double.parseDouble(this.textBer.getText())));
/* 309 */     currentRack.setPer(Double.valueOf(Double.parseDouble(this.textPer.getText())));
/* 310 */     currentRack.setDelay(Double.valueOf(Double.parseDouble(this.textDelay.getText())));
/*     */     
/* 312 */     return currentRack;
/*     */   }
/*     */   
/*     */   private JLabel labelHelp;
/*     */   private JLabel labelName;
/*     */   private JLabel labelNodeType;
/*     */   private JLabel labelNumNodes;
/*     */   private JLabel labelPer;
/*     */   private JLabel labelSwitchType;
/*     */   private JLabel saveLabel;
/*     */   private JTextField textBer;
/*     */   private JTextArea textComment;
/*     */   private JTextField textDelay;
/*     */   private JTextArea textDescription;
/*     */   private JTextField textName;
/*     */   private JTextField textNumNodes;
/*     */   private JTextField textPer;
/*     */   private boolean writeRacktoFile() {
/* 330 */     boolean allOK = checkValues();
/*     */     
/*     */ 
/* 333 */     if (allOK)
/*     */     {
/*     */ 
/* 336 */       Rack currentRack = panelToRackObject();
/*     */       
/*     */ 
/* 339 */       String newRackFile = icancloudgui.Utils.Configuration.REPOSITORY_RACKS + java.io.File.separatorChar + currentRack.getName();
/*     */       
/*     */ 
/*     */       try
/*     */       {
/* 344 */         if (new java.io.File(newRackFile).exists())
/*     */         {
/*     */ 
/* 347 */           int response = javax.swing.JOptionPane.showConfirmDialog(null, "Overwrite existing Rack?", "Confirm Overwrite", 2, 3);
/*     */           
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 354 */           if (response == 0)
/*     */           {
/*     */ 
/* 357 */             java.io.FileOutputStream fout = new java.io.FileOutputStream(newRackFile);
/* 358 */             java.io.ObjectOutputStream oos = new java.io.ObjectOutputStream(fout);
/* 359 */             oos.writeObject(currentRack);
/* 360 */             oos.close();
/* 361 */             allOK = true;
/*     */           }
/*     */           
/*     */ 
/*     */         }
/*     */         else
/*     */         {
/*     */ 
/* 369 */           java.io.FileOutputStream fout = new java.io.FileOutputStream(newRackFile);
/* 370 */           java.io.ObjectOutputStream oos = new java.io.ObjectOutputStream(fout);
/* 371 */           oos.writeObject(currentRack);
/* 372 */           oos.close();
/* 373 */           allOK = true;
/*     */         }
/*     */       }
/*     */       catch (Exception e) {
/* 377 */         e.printStackTrace();
/*     */       }
/*     */       
/*     */ 
/*     */     }
/*     */     else
/*     */     {
/* 384 */       javax.swing.JOptionPane.showMessageDialog(new javax.swing.JFrame(), "Wrong values. Please, re-check", "Error", 0);
/*     */     }
/*     */     
/*     */ 
/* 388 */     return allOK;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private void loadRackObjectInPanel(Rack newRack)
/*     */   {
/* 399 */     this.textName.setText(newRack.getName());
/* 400 */     this.textComment.setText(newRack.getComment());
/* 401 */     this.isStorageCheckBox.setSelected(newRack.isIsStorage());
/* 402 */     Utils.initCombo(this.comboNodeType, newRack.getNodeType());
/* 403 */     Utils.initCombo(this.comboSwitchType, newRack.getSwitchType());
/* 404 */     Utils.initCombo(this.comboCommLink, newRack.getCommLink());
/* 405 */     this.textNumNodes.setText(Integer.toString(newRack.getNumNodes()));
/* 406 */     this.textBer.setText(Double.toString(newRack.getBer().doubleValue()));
/* 407 */     this.textPer.setText(Double.toString(newRack.getPer().doubleValue()));
/* 408 */     this.textDelay.setText(Double.toString(newRack.getDelay().doubleValue()));
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void loadRackInPanel(String rackName)
/*     */   {
/* 421 */     Rack loadedRack = Utils.loadRackObject(rackName);
/*     */     
/*     */ 
/* 424 */     if (loadedRack != null) {
/* 425 */       loadRackObjectInPanel(loadedRack);
/* 426 */       showDescription();
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private void initComponents()
/*     */   {
/* 441 */     this.comboNodeType = new JComboBox();
/* 442 */     this.comboSwitchType = new JComboBox();
/* 443 */     this.comboCommLink = new JComboBox();
/* 444 */     this.labelSwitchType = new JLabel();
/* 445 */     this.labelCommLink = new JLabel();
/* 446 */     this.labelName = new JLabel();
/* 447 */     this.textName = new JTextField();
/* 448 */     this.labelNumNodes = new JLabel();
/* 449 */     this.textNumNodes = new JTextField();
/* 450 */     this.labelNodeType = new JLabel();
/* 451 */     this.jScrollPane1 = new javax.swing.JScrollPane();
/* 452 */     this.textDescription = new JTextArea();
/* 453 */     this.labelHelp = new JLabel();
/* 454 */     this.saveLabel = new JLabel();
/* 455 */     this.labelDescription = new JLabel();
/* 456 */     this.jScrollPane2 = new javax.swing.JScrollPane();
/* 457 */     this.textComment = new JTextArea();
/* 458 */     this.labelComment = new JLabel();
/* 459 */     this.labelDelay = new JLabel();
/* 460 */     this.textDelay = new JTextField();
/* 461 */     this.labelBer = new JLabel();
/* 462 */     this.textBer = new JTextField();
/* 463 */     this.labelPer = new JLabel();
/* 464 */     this.textPer = new JTextField();
/* 465 */     this.isStorageCheckBox = new javax.swing.JCheckBox();
/* 466 */     this.buttonClear = new JLabel();
/*     */     
/* 468 */     ResourceMap resourceMap = ((icancloudgui.ICanCloudGUIApp)org.jdesktop.application.Application.getInstance(icancloudgui.ICanCloudGUIApp.class)).getContext().getResourceMap(RackPanel.class);
/* 469 */     setBackground(resourceMap.getColor("Form.background"));
/* 470 */     setName("Form");
/* 471 */     setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());
/*     */     
/* 473 */     this.comboNodeType.setName("comboNodeType");
/* 474 */     this.comboNodeType.addItemListener(new java.awt.event.ItemListener() {
/*     */       public void itemStateChanged(ItemEvent evt) {
/* 476 */         RackPanel.this.comboNodeTypeItemStateChanged(evt);
/*     */       }
/* 478 */     });
/* 479 */     add(this.comboNodeType, new AbsoluteConstraints(100, 115, 230, -1));
/*     */     
/* 481 */     this.comboSwitchType.setName("comboSwitchType");
/* 482 */     this.comboSwitchType.addItemListener(new java.awt.event.ItemListener() {
/*     */       public void itemStateChanged(ItemEvent evt) {
/* 484 */         RackPanel.this.comboSwitchTypeItemStateChanged(evt);
/*     */       }
/* 486 */     });
/* 487 */     add(this.comboSwitchType, new AbsoluteConstraints(100, 195, 230, -1));
/*     */     
/* 489 */     this.comboCommLink.setName("comboCommLink");
/* 490 */     this.comboCommLink.addItemListener(new java.awt.event.ItemListener() {
/*     */       public void itemStateChanged(ItemEvent evt) {
/* 492 */         RackPanel.this.comboCommLinkItemStateChanged(evt);
/*     */       }
/* 494 */     });
/* 495 */     add(this.comboCommLink, new AbsoluteConstraints(100, 235, 230, -1));
/*     */     
/* 497 */     this.labelSwitchType.setFont(resourceMap.getFont("labelSwitchType.font"));
/* 498 */     this.labelSwitchType.setText(resourceMap.getString("labelSwitchType.text", new Object[0]));
/* 499 */     this.labelSwitchType.setName("labelSwitchType");
/* 500 */     add(this.labelSwitchType, new AbsoluteConstraints(20, 200, -1, -1));
/*     */     
/* 502 */     this.labelCommLink.setFont(resourceMap.getFont("labelSwitchType.font"));
/* 503 */     this.labelCommLink.setText(resourceMap.getString("labelCommLink.text", new Object[0]));
/* 504 */     this.labelCommLink.setName("labelCommLink");
/* 505 */     add(this.labelCommLink, new AbsoluteConstraints(15, 240, -1, -1));
/*     */     
/* 507 */     this.labelName.setFont(resourceMap.getFont("labelSwitchType.font"));
/* 508 */     this.labelName.setText(resourceMap.getString("labelName.text", new Object[0]));
/* 509 */     this.labelName.setName("labelName");
/* 510 */     add(this.labelName, new AbsoluteConstraints(20, 40, -1, -1));
/*     */     
/* 512 */     this.textName.setText(resourceMap.getString("textName.text", new Object[0]));
/* 513 */     this.textName.setName("textName");
/* 514 */     add(this.textName, new AbsoluteConstraints(66, 34, 260, -1));
/*     */     
/* 516 */     this.labelNumNodes.setFont(resourceMap.getFont("labelSwitchType.font"));
/* 517 */     this.labelNumNodes.setText(resourceMap.getString("labelNumNodes.text", new Object[0]));
/* 518 */     this.labelNumNodes.setName("labelNumNodes");
/* 519 */     add(this.labelNumNodes, new AbsoluteConstraints(40, 160, -1, -1));
/*     */     
/* 521 */     this.textNumNodes.setText(resourceMap.getString("textNumNodes.text", new Object[0]));
/* 522 */     this.textNumNodes.setName("textNumNodes");
/* 523 */     add(this.textNumNodes, new AbsoluteConstraints(100, 155, 230, -1));
/*     */     
/* 525 */     this.labelNodeType.setFont(resourceMap.getFont("labelSwitchType.font"));
/* 526 */     this.labelNodeType.setText(resourceMap.getString("labelNodeType.text", new Object[0]));
/* 527 */     this.labelNodeType.setName("labelNodeType");
/* 528 */     add(this.labelNodeType, new AbsoluteConstraints(30, 120, -1, -1));
/*     */     
/* 530 */     this.jScrollPane1.setName("jScrollPane1");
/*     */     
/* 532 */     this.textDescription.setBackground(resourceMap.getColor("textDescription.background"));
/* 533 */     this.textDescription.setColumns(20);
/* 534 */     this.textDescription.setForeground(resourceMap.getColor("textDescription.foreground"));
/* 535 */     this.textDescription.setRows(5);
/* 536 */     this.textDescription.setTabSize(4);
/* 537 */     this.textDescription.setName("textDescription");
/* 538 */     this.jScrollPane1.setViewportView(this.textDescription);
/*     */     
/* 540 */     add(this.jScrollPane1, new AbsoluteConstraints(360, 70, 420, 440));
/*     */     
/* 542 */     this.labelHelp.setIcon(resourceMap.getIcon("labelHelp.icon"));
/* 543 */     this.labelHelp.setText(resourceMap.getString("labelHelp.text", new Object[0]));
/* 544 */     this.labelHelp.setToolTipText(resourceMap.getString("labelHelp.toolTipText", new Object[0]));
/* 545 */     this.labelHelp.setName("labelHelp");
/* 546 */     this.labelHelp.addMouseListener(new java.awt.event.MouseAdapter() {
/*     */       public void mouseClicked(java.awt.event.MouseEvent evt) {
/* 548 */         RackPanel.this.labelHelpMouseClicked(evt);
/*     */       }
/* 550 */     });
/* 551 */     add(this.labelHelp, new AbsoluteConstraints(750, 20, -1, -1));
/*     */     
/* 553 */     this.saveLabel.setIcon(resourceMap.getIcon("saveLabel.icon"));
/* 554 */     this.saveLabel.setText(resourceMap.getString("saveLabel.text", new Object[0]));
/* 555 */     this.saveLabel.setToolTipText(resourceMap.getString("saveLabel.toolTipText", new Object[0]));
/* 556 */     this.saveLabel.setName("saveLabel");
/* 557 */     this.saveLabel.addMouseListener(new java.awt.event.MouseAdapter() {
/*     */       public void mouseClicked(java.awt.event.MouseEvent evt) {
/* 559 */         RackPanel.this.saveLabelMouseClicked(evt);
/*     */       }
/* 561 */     });
/* 562 */     add(this.saveLabel, new AbsoluteConstraints(710, 20, -1, -1));
/*     */     
/* 564 */     this.labelDescription.setFont(resourceMap.getFont("labelSwitchType.font"));
/* 565 */     this.labelDescription.setText(resourceMap.getString("labelDescription.text", new Object[0]));
/* 566 */     this.labelDescription.setName("labelDescription");
/* 567 */     add(this.labelDescription, new AbsoluteConstraints(380, 48, -1, -1));
/*     */     
/* 569 */     this.jScrollPane2.setName("jScrollPane2");
/*     */     
/* 571 */     this.textComment.setColumns(20);
/* 572 */     this.textComment.setRows(5);
/* 573 */     this.textComment.setName("textComment");
/* 574 */     this.jScrollPane2.setViewportView(this.textComment);
/*     */     
/* 576 */     add(this.jScrollPane2, new AbsoluteConstraints(20, 430, 310, -1));
/*     */     
/* 578 */     this.labelComment.setFont(resourceMap.getFont("labelSwitchType.font"));
/* 579 */     this.labelComment.setText(resourceMap.getString("labelComment.text", new Object[0]));
/* 580 */     this.labelComment.setName("labelComment");
/* 581 */     add(this.labelComment, new AbsoluteConstraints(20, 400, -1, -1));
/*     */     
/* 583 */     this.labelDelay.setFont(resourceMap.getFont("labelSwitchType.font"));
/* 584 */     this.labelDelay.setText(resourceMap.getString("labelDelay.text", new Object[0]));
/* 585 */     this.labelDelay.setToolTipText(resourceMap.getString("labelDelay.toolTipText", new Object[0]));
/* 586 */     this.labelDelay.setName("labelDelay");
/* 587 */     add(this.labelDelay, new AbsoluteConstraints(55, 280, -1, -1));
/*     */     
/* 589 */     this.textDelay.setName("textDelay");
/* 590 */     add(this.textDelay, new AbsoluteConstraints(100, 275, 230, -1));
/*     */     
/* 592 */     this.labelBer.setFont(resourceMap.getFont("labelSwitchType.font"));
/* 593 */     this.labelBer.setText(resourceMap.getString("labelBer.text", new Object[0]));
/* 594 */     this.labelBer.setToolTipText(resourceMap.getString("labelBer.toolTipText", new Object[0]));
/* 595 */     this.labelBer.setName("labelBer");
/* 596 */     add(this.labelBer, new AbsoluteConstraints(70, 320, -1, -1));
/*     */     
/* 598 */     this.textBer.setName("textBer");
/* 599 */     add(this.textBer, new AbsoluteConstraints(100, 315, 230, -1));
/*     */     
/* 601 */     this.labelPer.setFont(resourceMap.getFont("labelSwitchType.font"));
/* 602 */     this.labelPer.setText(resourceMap.getString("labelPer.text", new Object[0]));
/* 603 */     this.labelPer.setToolTipText(resourceMap.getString("labelPer.toolTipText", new Object[0]));
/* 604 */     this.labelPer.setName("labelPer");
/* 605 */     add(this.labelPer, new AbsoluteConstraints(70, 360, -1, -1));
/*     */     
/* 607 */     this.textPer.setName("textPer");
/* 608 */     add(this.textPer, new AbsoluteConstraints(100, 355, 230, -1));
/*     */     
/* 610 */     this.isStorageCheckBox.setFont(resourceMap.getFont("labelSwitchType.font"));
/* 611 */     this.isStorageCheckBox.setText(resourceMap.getString("isStorageCheckBox.text", new Object[0]));
/* 612 */     this.isStorageCheckBox.setHorizontalTextPosition(2);
/* 613 */     this.isStorageCheckBox.setName("isStorageCheckBox");
/* 614 */     this.isStorageCheckBox.addItemListener(new java.awt.event.ItemListener() {
/*     */       public void itemStateChanged(ItemEvent evt) {
/* 616 */         RackPanel.this.isStorageCheckBoxItemStateChanged(evt);
/*     */       }
/* 618 */     });
/* 619 */     add(this.isStorageCheckBox, new AbsoluteConstraints(20, 80, -1, -1));
/*     */     
/* 621 */     this.buttonClear.setIcon(resourceMap.getIcon("buttonClear.icon"));
/* 622 */     this.buttonClear.setToolTipText(resourceMap.getString("buttonClear.toolTipText", new Object[0]));
/* 623 */     this.buttonClear.setName("buttonClear");
/* 624 */     this.buttonClear.addMouseListener(new java.awt.event.MouseAdapter() {
/*     */       public void mouseClicked(java.awt.event.MouseEvent evt) {
/* 626 */         RackPanel.this.buttonClearMouseClicked(evt);
/*     */       }
/* 628 */     });
/* 629 */     add(this.buttonClear, new AbsoluteConstraints(670, 20, -1, -1));
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private void labelHelpMouseClicked(java.awt.event.MouseEvent evt)
/*     */   {
/* 641 */     Utils.showHelp(this);
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private void saveLabelMouseClicked(java.awt.event.MouseEvent evt)
/*     */   {
/* 654 */     boolean allOK = writeRacktoFile();
/*     */     
/*     */ 
/* 657 */     if (allOK) {
/* 658 */       this.mainFrame.updateTree();
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private void buttonClearMouseClicked(java.awt.event.MouseEvent evt)
/*     */   {
/* 671 */     int response = javax.swing.JOptionPane.showConfirmDialog(null, "Clear current Rack configuration?", "Confirm to clear this form", 2, 3);
/*     */     
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 678 */     if (response == 0) {
/* 679 */       clear();
/*     */     }
/*     */   }
/*     */   
/*     */   private void comboNodeTypeItemStateChanged(ItemEvent evt) {
/* 684 */     showDescription();
/*     */   }
/*     */   
/*     */   private void comboSwitchTypeItemStateChanged(ItemEvent evt) {
/* 688 */     showDescription();
/*     */   }
/*     */   
/*     */   private void comboCommLinkItemStateChanged(ItemEvent evt) {
/* 692 */     showDescription();
/*     */   }
/*     */   
/*     */   private void isStorageCheckBoxItemStateChanged(ItemEvent evt) {
/* 696 */     reloadPanel();
/* 697 */     showDescription();
/*     */   }
/*     */ }


/* Location:              /home/chverma/Descargas/iCanCloudGUI/iCanCloudGUI.jar!/icancloudgui/TabbedPanels/RackPanel.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */