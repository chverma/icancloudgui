/*     */ package icancloudgui.TabbedPanels;
/*     */ 
/*     */ import icancloudgui.TabbedPanels.dataClasses.VirtualMachine;
/*     */ import java.awt.event.MouseEvent;
/*     */ import javax.swing.JLabel;
/*     */ import javax.swing.JTextField;
/*     */ import org.jdesktop.application.ResourceMap;
/*     */ import org.netbeans.lib.awtextra.AbsoluteConstraints;
/*     */ 
/*     */ public class VirtualizationPanel extends javax.swing.JPanel
/*     */ {
/*     */   icancloudgui.ICanCloudGUIView mainFrame;
/*     */   private JLabel buttonClear;
/*     */   private JLabel buttonHelp;
/*     */   private JLabel buttonSave;
/*     */   private JLabel labelCPU;
/*     */   private JLabel labelComment;
/*     */   private JLabel labelDisk;
/*     */   private JLabel labelMemory;
/*     */   private JLabel labelName;
/*     */   private javax.swing.JScrollPane scrollTextArea;
/*     */   private javax.swing.JTextArea textAreaComment;
/*     */   private JTextField textCPU;
/*     */   private JTextField textDisk;
/*     */   private JTextField textMemory;
/*     */   private JTextField textName;
/*     */   
/*     */   public VirtualizationPanel(icancloudgui.ICanCloudGUIView mainFrame)
/*     */   {
/*  30 */     initComponents();
/*  31 */     this.mainFrame = mainFrame;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public boolean checkValues()
/*     */   {
/*  44 */     boolean allOK = true;
/*     */     
/*     */ 
/*     */ 
/*  48 */     if (!icancloudgui.Utils.Utils.checkName(this.textName.getText()))
/*     */     {
/*  50 */       allOK = false;
/*  51 */       icancloudgui.Utils.Utils.showErrorMessage("Invalid name for this VM.\nA name must start with a letter and can contain alphanumeric characters, '-' and '_'", "Wrong name!");
/*     */     }
/*     */     
/*     */ 
/*     */ 
/*     */     try
/*     */     {
/*  58 */       if (allOK) {
/*  59 */         Integer.parseInt(this.textCPU.getText());
/*     */       }
/*     */     }
/*     */     catch (Exception e) {
/*  63 */       allOK = false;
/*  64 */       icancloudgui.Utils.Utils.showErrorMessage("Wrong parameter for number of cores.\nThis parameter must be an integer.\n", "Wrong parameter!");
/*     */     }
/*     */     
/*     */ 
/*     */ 
/*     */ 
/*     */     try
/*     */     {
/*  72 */       if (allOK) {
/*  73 */         Integer.parseInt(this.textDisk.getText());
/*     */       }
/*     */     }
/*     */     catch (Exception e) {
/*  77 */       allOK = false;
/*  78 */       icancloudgui.Utils.Utils.showErrorMessage("Wrong parameter for disk size.\nThis parameter must be an integer.\n", "Wrong parameter!");
/*     */     }
/*     */     
/*     */ 
/*     */ 
/*     */ 
/*     */     try
/*     */     {
/*  86 */       if (allOK) {
/*  87 */         Double.parseDouble(this.textMemory.getText());
/*     */       }
/*     */     }
/*     */     catch (Exception e) {
/*  91 */       allOK = false;
/*  92 */       icancloudgui.Utils.Utils.showErrorMessage("Wrong parameter for memory size.\nThis parameter must be a double.\n", "Wrong parameter!");
/*     */     }
/*     */     
/*     */ 
/*     */ 
/*  97 */     return allOK;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public VirtualMachine panelToVMObject()
/*     */   {
/* 110 */     VirtualMachine currentVM = new VirtualMachine();
/*     */     
/*     */ 
/* 113 */     currentVM.setName(this.textName.getText());
/* 114 */     currentVM.setCpuCores(Integer.parseInt(this.textCPU.getText()));
/* 115 */     currentVM.setMemory(Double.parseDouble(this.textMemory.getText()));
/* 116 */     currentVM.setDisk(Integer.parseInt(this.textDisk.getText()));
/* 117 */     currentVM.setComment(this.textAreaComment.getText());
/*     */     
/* 119 */     return currentVM;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */   public void clear()
/*     */   {
/* 126 */     this.textName.setText("");
/* 127 */     this.textCPU.setText("");
/* 128 */     this.textMemory.setText("");
/* 129 */     this.textDisk.setText("");
/* 130 */     this.textAreaComment.setText("");
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private void loadVMObjectInPanel(VirtualMachine newVM)
/*     */   {
/* 141 */     this.textName.setText(newVM.getName());
/* 142 */     this.textCPU.setText(Integer.toString(newVM.getCpuCores()));
/* 143 */     this.textMemory.setText(Double.toString(newVM.getMemory()));
/* 144 */     this.textDisk.setText(Integer.toString(newVM.getDisk()));
/* 145 */     this.textAreaComment.setText(newVM.getComment());
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void loadVMInPanel(String vmName)
/*     */   {
/* 158 */     VirtualMachine loadedVM = icancloudgui.Utils.Utils.loadVMObject(vmName);
/*     */     
/*     */ 
/* 161 */     if (loadedVM != null) {
/* 162 */       loadVMObjectInPanel(loadedVM);
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private boolean writeVMToFile()
/*     */   {
/* 182 */     boolean allOK = checkValues();
/*     */     
/*     */ 
/* 185 */     if (allOK)
/*     */     {
/*     */ 
/* 188 */       VirtualMachine currentVM = panelToVMObject();
/*     */       
/*     */ 
/* 191 */       String newVMFile = icancloudgui.Utils.Configuration.REPOSITORY_VM + java.io.File.separatorChar + currentVM.getName();
/*     */       
/*     */ 
/*     */       try
/*     */       {
/* 196 */         if (new java.io.File(newVMFile).exists())
/*     */         {
/*     */ 
/* 199 */           int response = javax.swing.JOptionPane.showConfirmDialog(null, "Overwrite existing Virtual Machine?", "Confirm Overwrite", 2, 3);
/*     */           
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 206 */           if (response == 0)
/*     */           {
/*     */ 
/* 209 */             java.io.FileOutputStream fout = new java.io.FileOutputStream(newVMFile);
/* 210 */             java.io.ObjectOutputStream oos = new java.io.ObjectOutputStream(fout);
/* 211 */             oos.writeObject(currentVM);
/* 212 */             oos.close();
/* 213 */             allOK = true;
/*     */           }
/*     */           
/*     */ 
/*     */         }
/*     */         else
/*     */         {
/*     */ 
/* 221 */           java.io.FileOutputStream fout = new java.io.FileOutputStream(newVMFile);
/* 222 */           java.io.ObjectOutputStream oos = new java.io.ObjectOutputStream(fout);
/* 223 */           oos.writeObject(currentVM);
/* 224 */           oos.close();
/* 225 */           allOK = true;
/*     */         }
/*     */       }
/*     */       catch (Exception e) {
/* 229 */         e.printStackTrace();
/*     */       }
/*     */       
/*     */ 
/*     */     }
/*     */     else
/*     */     {
/* 236 */       javax.swing.JOptionPane.showMessageDialog(new javax.swing.JFrame(), "Wrong values. Please, re-check", "Error", 0);
/*     */     }
/*     */     
/*     */ 
/* 240 */     return allOK;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private void initComponents()
/*     */   {
/* 253 */     this.labelName = new JLabel();
/* 254 */     this.labelCPU = new JLabel();
/* 255 */     this.labelDisk = new JLabel();
/* 256 */     this.textName = new JTextField();
/* 257 */     this.textCPU = new JTextField();
/* 258 */     this.textDisk = new JTextField();
/* 259 */     this.buttonSave = new JLabel();
/* 260 */     this.buttonHelp = new JLabel();
/* 261 */     this.scrollTextArea = new javax.swing.JScrollPane();
/* 262 */     this.textAreaComment = new javax.swing.JTextArea();
/* 263 */     this.labelComment = new JLabel();
/* 264 */     this.labelMemory = new JLabel();
/* 265 */     this.textMemory = new JTextField();
/* 266 */     this.buttonClear = new JLabel();
/*     */     
/* 268 */     ResourceMap resourceMap = ((icancloudgui.ICanCloudGUIApp)org.jdesktop.application.Application.getInstance(icancloudgui.ICanCloudGUIApp.class)).getContext().getResourceMap(VirtualizationPanel.class);
/* 269 */     setBackground(resourceMap.getColor("Form.background"));
/* 270 */     setName("Form");
/* 271 */     setPreferredSize(new java.awt.Dimension(500, 641));
/* 272 */     setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());
/*     */     
/* 274 */     this.labelName.setFont(resourceMap.getFont("labelDisk.font"));
/* 275 */     this.labelName.setText(resourceMap.getString("labelName.text", new Object[0]));
/* 276 */     this.labelName.setName("labelName");
/* 277 */     add(this.labelName, new AbsoluteConstraints(10, 35, -1, -1));
/*     */     
/* 279 */     this.labelCPU.setFont(resourceMap.getFont("labelDisk.font"));
/* 280 */     this.labelCPU.setText(resourceMap.getString("labelCPU.text", new Object[0]));
/* 281 */     this.labelCPU.setName("labelCPU");
/* 282 */     add(this.labelCPU, new AbsoluteConstraints(120, 70, -1, -1));
/*     */     
/* 284 */     this.labelDisk.setFont(resourceMap.getFont("labelDisk.font"));
/* 285 */     this.labelDisk.setText(resourceMap.getString("labelDisk.text", new Object[0]));
/* 286 */     this.labelDisk.setName("labelDisk");
/* 287 */     add(this.labelDisk, new AbsoluteConstraints(80, 140, -1, -1));
/*     */     
/* 289 */     this.textName.setText(resourceMap.getString("textName.text", new Object[0]));
/* 290 */     this.textName.setName("textName");
/* 291 */     add(this.textName, new AbsoluteConstraints(50, 30, 350, 28));
/*     */     
/* 293 */     this.textCPU.setText(resourceMap.getString("textCPU.text", new Object[0]));
/* 294 */     this.textCPU.setName("textCPU");
/* 295 */     add(this.textCPU, new AbsoluteConstraints(200, 65, 200, 28));
/*     */     
/* 297 */     this.textDisk.setText(resourceMap.getString("textDisk.text", new Object[0]));
/* 298 */     this.textDisk.setName("textDisk");
/* 299 */     add(this.textDisk, new AbsoluteConstraints(200, 135, 200, 28));
/*     */     
/* 301 */     this.buttonSave.setIcon(resourceMap.getIcon("buttonSave.icon"));
/* 302 */     this.buttonSave.setToolTipText(resourceMap.getString("buttonSave.toolTipText", new Object[0]));
/* 303 */     this.buttonSave.setName("buttonSave");
/* 304 */     this.buttonSave.addMouseListener(new java.awt.event.MouseAdapter() {
/*     */       public void mouseClicked(MouseEvent evt) {
/* 306 */         VirtualizationPanel.this.buttonSaveMouseClicked(evt);
/*     */       }
/* 308 */     });
/* 309 */     add(this.buttonSave, new AbsoluteConstraints(710, 20, -1, -1));
/*     */     
/* 311 */     this.buttonHelp.setIcon(resourceMap.getIcon("buttonHelp.icon"));
/* 312 */     this.buttonHelp.setToolTipText(resourceMap.getString("buttonHelp.toolTipText", new Object[0]));
/* 313 */     this.buttonHelp.setName("buttonHelp");
/* 314 */     this.buttonHelp.addMouseListener(new java.awt.event.MouseAdapter() {
/*     */       public void mouseClicked(MouseEvent evt) {
/* 316 */         VirtualizationPanel.this.buttonHelpMouseClicked(evt);
/*     */       }
/* 318 */     });
/* 319 */     add(this.buttonHelp, new AbsoluteConstraints(750, 20, -1, -1));
/*     */     
/* 321 */     this.scrollTextArea.setName("scrollTextArea");
/*     */     
/* 323 */     this.textAreaComment.setColumns(20);
/* 324 */     this.textAreaComment.setRows(5);
/* 325 */     this.textAreaComment.setName("textAreaComment");
/* 326 */     this.scrollTextArea.setViewportView(this.textAreaComment);
/*     */     
/* 328 */     add(this.scrollTextArea, new AbsoluteConstraints(20, 260, 760, 90));
/*     */     
/* 330 */     this.labelComment.setFont(resourceMap.getFont("labelDisk.font"));
/* 331 */     this.labelComment.setText(resourceMap.getString("labelComment.text", new Object[0]));
/* 332 */     this.labelComment.setName("labelComment");
/* 333 */     add(this.labelComment, new AbsoluteConstraints(10, 230, -1, -1));
/*     */     
/* 335 */     this.labelMemory.setFont(resourceMap.getFont("labelDisk.font"));
/* 336 */     this.labelMemory.setText(resourceMap.getString("labelMemory.text", new Object[0]));
/* 337 */     this.labelMemory.setName("labelMemory");
/* 338 */     add(this.labelMemory, new AbsoluteConstraints(50, 105, -1, -1));
/*     */     
/* 340 */     this.textMemory.setName("textMemory");
/* 341 */     add(this.textMemory, new AbsoluteConstraints(200, 100, 200, 28));
/*     */     
/* 343 */     this.buttonClear.setIcon(resourceMap.getIcon("buttonClear.icon"));
/* 344 */     this.buttonClear.setToolTipText(resourceMap.getString("buttonClear.toolTipText", new Object[0]));
/* 345 */     this.buttonClear.setName("buttonClear");
/* 346 */     this.buttonClear.addMouseListener(new java.awt.event.MouseAdapter() {
/*     */       public void mouseClicked(MouseEvent evt) {
/* 348 */         VirtualizationPanel.this.buttonClearMouseClicked(evt);
/*     */       }
/* 350 */     });
/* 351 */     add(this.buttonClear, new AbsoluteConstraints(670, 20, -1, -1));
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private void buttonSaveMouseClicked(MouseEvent evt)
/*     */   {
/* 369 */     boolean allOK = writeVMToFile();
/*     */     
/*     */ 
/* 372 */     if (allOK) {
/* 373 */       this.mainFrame.updateTree();
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private void buttonHelpMouseClicked(MouseEvent evt)
/*     */   {
/* 383 */     icancloudgui.Utils.Utils.showHelp(this);
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private void buttonClearMouseClicked(MouseEvent evt)
/*     */   {
/* 392 */     int response = javax.swing.JOptionPane.showConfirmDialog(null, "Clear current Virtual Machine configuration?", "Confirm to clear this form", 2, 3);
/*     */     
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 399 */     if (response == 0) {
/* 400 */       clear();
/*     */     }
/*     */   }
/*     */ }


/* Location:              /home/chverma/Descargas/iCanCloudGUI/iCanCloudGUI.jar!/icancloudgui/TabbedPanels/VirtualizationPanel.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */