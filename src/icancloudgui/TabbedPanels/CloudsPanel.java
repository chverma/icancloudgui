/*      */ package icancloudgui.TabbedPanels;
/*      */ 
/*      */ import icancloudgui.TabbedPanels.dataClasses.Cloud;
/*      */ import icancloudgui.Utils.Utils;
/*      */ import java.awt.event.MouseEvent;
/*      */ import javax.swing.JComboBox;
/*      */ import javax.swing.JLabel;
/*      */ import javax.swing.JTable;
/*      */ import javax.swing.JTextArea;
/*      */ import javax.swing.JTextField;
/*      */ import org.jdesktop.application.ResourceMap;
/*      */ import org.netbeans.lib.awtextra.AbsoluteConstraints;
/*      */ 
/*      */ public class CloudsPanel extends javax.swing.JPanel
/*      */ {
/*      */   icancloudgui.ICanCloudGUIView mainFrame;
/*      */   ChartDialog charts;
/*      */   private JLabel buttonClear;
/*      */   private JLabel buttonGenerate;
/*      */   private JComboBox comboCloudManager;
/*      */   private JComboBox comboDataCenter;
/*      */   private JComboBox comboDistribution;
/*      */   private JTextField hoursRepetition;
/*      */   private JComboBox hypervisorComboBox;
/*      */   private JLabel jLabel1;
/*      */   private JLabel jLabel2;
/*      */   private javax.swing.JPanel jPanel1;
/*      */   private javax.swing.JScrollPane jScrollPane1;
/*      */   private javax.swing.JScrollPane jScrollPane2;
/*      */   private javax.swing.JScrollPane jScrollPane3;
/*      */   private JLabel labelCloudManager1;
/*      */   private JLabel labelCloudName;
/*      */   private JLabel labelComment;
/*      */   private JLabel labelConfiguration;
/*      */   private JLabel labelDataCenter;
/*      */   
/*      */   public CloudsPanel(icancloudgui.ICanCloudGUIView mainFrame)
/*      */   {
/*   39 */     this.mainFrame = mainFrame;
/*   40 */     initComponents();
/*      */     
/*      */ 
/*   43 */     this.charts = new ChartDialog((java.awt.Frame)getTopLevelAncestor());
/*   44 */     this.charts.setPreferredSize(new java.awt.Dimension(540, 290));
/*   45 */     this.charts.setMinimumSize(new java.awt.Dimension(540, 290));
/*   46 */     this.charts.setMaximumSize(new java.awt.Dimension(540, 290));
/*   47 */     this.charts.setResizable(false);
/*   48 */     this.charts.setVisible(false);
/*      */     
/*      */ 
/*   51 */     Utils.initTenantTable(this.tableTenants);
/*   52 */     Utils.loadCombo(Utils.getComponentListFromDir(icancloudgui.Utils.Configuration.REPOSITORY_DATA_CENTER), this.comboDataCenter);
/*      */     
/*      */ 
/*   55 */     reloadPanel();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void clear()
/*      */   {
/*   64 */     Utils.initTenantTable(this.tableTenants);
/*      */     
/*      */ 
/*   67 */     this.textCloudName.setText("");
/*   68 */     this.textPreLoadTenants1.setText("");
/*   69 */     this.textSimTime.setText("");
/*   70 */     this.hoursRepetition.setText("");
/*   71 */     this.textComment.setText("");
/*   72 */     this.textParamX.setText("");
/*   73 */     this.textParamY.setText("");
/*   74 */     this.textParamZ.setText("");
/*   75 */     this.textMaxVMsPerNode.setText("");
/*      */     
/*      */ 
/*   78 */     if (this.comboDataCenter.getItemCount() > 0) {
/*   79 */       this.comboDataCenter.setSelectedIndex(0);
/*      */     }
/*   81 */     if (this.comboCloudManager.getItemCount() > 0) {
/*   82 */       this.comboCloudManager.setSelectedIndex(0);
/*      */     }
/*   84 */     if (this.comboDistribution.getItemCount() > 0) {
/*   85 */       this.comboDistribution.setSelectedIndex(0);
/*      */     }
/*   87 */     if (this.hypervisorComboBox.getItemCount() > 0) {
/*   88 */       this.hypervisorComboBox.setSelectedIndex(0);
/*      */     }
/*   90 */     showDescription();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   private void showDescription()
/*      */   {
/*      */     String rackName;
/*      */     
/*      */ 
/*      */ 
/*      */     String text;
/*      */     
/*      */ 
/*      */ 
/*  107 */     String name = text = rackName = "";
/*  108 */     int numInstances = 0;
/*      */     
/*      */ 
/*  111 */     if ((this.comboDataCenter.getItemCount() > 0) && (this.comboCloudManager.getItemCount() > 0) && (this.comboDistribution.getItemCount() > 0))
/*      */     {
/*      */ 
/*      */ 
/*      */ 
/*  116 */       if (this.textCloudName.getText().length() == 0) {
/*  117 */         name = "ENTER A FANCY NAME HERE!";
/*      */       }
/*      */       else {
/*  120 */         name = this.textCloudName.getText();
/*      */       }
/*      */       
/*      */ 
/*  124 */       icancloudgui.TabbedPanels.dataClasses.DataCenter dataCenterObject = Utils.loadDataCenterObject(this.comboDataCenter.getSelectedItem().toString());
/*      */       
/*      */ 
/*  127 */       text = text + " Description of Data-Center\n ----------------------\n\n";
/*      */       
/*      */ 
/*      */ 
/*  131 */       text = text + " Number of racks: " + Integer.toString(dataCenterObject.getNumRacks()) + "\n\n";
/*      */       
/*      */ 
/*  134 */       java.util.Iterator<icancloudgui.TabbedPanels.dataClasses.DataCenterUnit> iterator = dataCenterObject.getRackList();
/*      */       
/*      */ 
/*  137 */       while (iterator.hasNext())
/*      */       {
/*  139 */         rackName = ((icancloudgui.TabbedPanels.dataClasses.DataCenterUnit)iterator.next()).getName();
/*      */         
/*      */ 
/*  142 */         numInstances = dataCenterObject.getNumRackInstances(rackName);
/*      */         
/*  144 */         if (numInstances > 0)
/*      */         {
/*      */ 
/*  147 */           icancloudgui.TabbedPanels.dataClasses.Rack rackObject = Utils.loadRackObject(rackName);
/*  148 */           text = text + rackObject.getShortDescription();
/*      */         }
/*      */       }
/*      */       
/*  152 */       text = text + "\n\n";
/*      */       
/*      */ 
/*  155 */       text = text + " Number of nodes (computing): " + Integer.toString(dataCenterObject.getNumComputingNodes()) + "\n\n";
/*      */       
/*      */ 
/*  158 */       java.util.LinkedList<String> list = Utils.getComponentListFromDir(icancloudgui.Utils.Configuration.REPOSITORY_COMPUTING_NODES_FOLDER);
/*      */       
/*  160 */       for (int i = 0; i < list.size(); i++)
/*      */       {
/*      */ 
/*  163 */         numInstances = dataCenterObject.getNumComputingInstances((String)list.get(i));
/*      */         
/*  165 */         if (numInstances > 0) {
/*  166 */           text = text + "   - " + Integer.toString(numInstances) + "x " + (String)list.get(i) + "\n";
/*      */         }
/*      */       }
/*      */       
/*  170 */       text = text + "\n";
/*      */       
/*      */ 
/*  173 */       text = text + " Number of nodes (storage): " + Integer.toString(dataCenterObject.getNumStorageNodes()) + "\n\n";
/*      */       
/*      */ 
/*  176 */       list = Utils.getComponentListFromDir(icancloudgui.Utils.Configuration.REPOSITORY_STORAGE_NODES_FOLDER);
/*      */       
/*  178 */       for (int i = 0; i < list.size(); i++)
/*      */       {
/*      */ 
/*  181 */         numInstances = dataCenterObject.getNumStorageInstances((String)list.get(i));
/*      */         
/*  183 */         if (numInstances > 0) {
/*  184 */           text = text + "   - " + Integer.toString(numInstances) + "x " + (String)list.get(i) + "\n";
/*      */         }
/*      */       }
/*      */     }
/*      */     
/*  189 */     this.textConfiguration.setText(text);
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void reloadPanel()
/*      */   {
/*  203 */     Utils.initTenantTable(this.tableTenants);
/*      */     
/*      */ 
/*  206 */     String currentElement = null;
/*      */     
/*  208 */     if (this.comboDataCenter.getItemCount() > 0) {
/*  209 */       currentElement = this.comboDataCenter.getSelectedItem().toString();
/*      */     }
/*      */     
/*      */ 
/*  213 */     java.util.LinkedList<String> list = Utils.getComponentListFromDir(icancloudgui.Utils.Configuration.REPOSITORY_DATA_CENTER);
/*      */     
/*      */ 
/*  216 */     Utils.loadCombo(list, this.comboDataCenter);
/*      */     
/*  218 */     if (currentElement != null) {
/*  219 */       Utils.initCombo(this.comboDataCenter, currentElement);
/*      */     }
/*      */     
/*  222 */     if (this.comboCloudManager.getItemCount() == 0) {
/*  223 */       list = Utils.getCloudManagerList();
/*  224 */       Utils.loadCombo(list, this.comboCloudManager);
/*      */ 
/*      */     }
/*      */     else
/*      */     {
/*  229 */       currentElement = this.comboCloudManager.getSelectedItem().toString();
/*      */     }
/*      */     
/*      */ 
/*  233 */     if (this.hypervisorComboBox.getItemCount() == 0) {
/*  234 */       list = Utils.getHypervisorList();
/*  235 */       Utils.loadCombo(list, this.hypervisorComboBox);
/*      */     }
/*      */     
/*  238 */     if (currentElement != null) {
/*  239 */       Utils.initCombo(this.comboCloudManager, currentElement);
/*      */     }
/*      */     
/*  242 */     if (this.comboDistribution.getItemCount() == 0) {
/*  243 */       list = Utils.getDistributionList();
/*  244 */       Utils.loadCombo(list, this.comboDistribution);
/*      */     }
/*      */     else
/*      */     {
/*  248 */       currentElement = this.comboDistribution.getSelectedItem().toString();
/*      */     }
/*      */     
/*  251 */     if (currentElement != null) {
/*  252 */       Utils.initCombo(this.comboDistribution, currentElement);
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */   private void updateDistributionParameters()
/*      */   {
/*  259 */     if (this.comboDistribution.getSelectedItem().toString().equals("no_distribution"))
/*      */     {
/*  261 */       this.labelParamX.setText("Unused parameter");
/*  262 */       this.labelParamY.setText("Unused parameter");
/*  263 */       this.labelParamZ.setText("Unused parameter");
/*      */       
/*  265 */       this.textParamX.setEnabled(false);
/*  266 */       this.textParamY.setEnabled(false);
/*  267 */       this.textParamZ.setEnabled(false);
/*      */       
/*  269 */       this.textParamX.setText("");
/*  270 */       this.textParamY.setText("");
/*  271 */       this.textParamZ.setText("");
/*      */ 
/*      */ 
/*      */ 
/*      */     }
/*  276 */     else if (this.comboDistribution.getSelectedItem().toString().equals("normal"))
/*      */     {
/*  278 */       this.labelParamX.setText("Mean");
/*  279 */       this.labelParamY.setText("Standard deviation");
/*  280 */       this.labelParamZ.setText("Unused parameter");
/*      */       
/*  282 */       this.textParamX.setEnabled(true);
/*  283 */       this.textParamY.setEnabled(true);
/*  284 */       this.textParamZ.setEnabled(false);
/*      */ 
/*      */ 
/*      */     }
/*  288 */     else if (this.comboDistribution.getSelectedItem().toString().equals("exponential"))
/*      */     {
/*  290 */       this.labelParamX.setText("Mean");
/*  291 */       this.labelParamY.setText("Unused parameter");
/*  292 */       this.labelParamZ.setText("Unused parameter");
/*      */       
/*  294 */       this.textParamX.setEnabled(true);
/*  295 */       this.textParamY.setEnabled(false);
/*  296 */       this.textParamZ.setEnabled(false);
/*      */ 
/*      */ 
/*      */     }
/*  300 */     else if (this.comboDistribution.getSelectedItem().toString().equals("uniform"))
/*      */     {
/*  302 */       this.labelParamX.setText("Begin of the interval");
/*  303 */       this.labelParamY.setText("End of the interval");
/*  304 */       this.labelParamZ.setText("Unused parameter");
/*      */       
/*  306 */       this.textParamX.setEnabled(true);
/*  307 */       this.textParamY.setEnabled(true);
/*  308 */       this.textParamZ.setEnabled(false);
/*      */ 
/*      */ 
/*      */     }
/*  312 */     else if (this.comboDistribution.getSelectedItem().toString().equals("truncnormal"))
/*      */     {
/*  314 */       this.labelParamX.setText("Mean");
/*  315 */       this.labelParamY.setText("Standard deviation");
/*  316 */       this.labelParamZ.setText("Unused parameter");
/*      */       
/*  318 */       this.textParamX.setEnabled(true);
/*  319 */       this.textParamY.setEnabled(true);
/*  320 */       this.textParamZ.setEnabled(false);
/*      */ 
/*      */ 
/*      */     }
/*  324 */     else if (this.comboDistribution.getSelectedItem().toString().equals("student_t"))
/*      */     {
/*  326 */       this.labelParamX.setText("Degrees of freedom");
/*  327 */       this.labelParamY.setText("Standard deviation");
/*  328 */       this.labelParamZ.setText("Unused parameter");
/*      */       
/*  330 */       this.textParamX.setEnabled(true);
/*  331 */       this.textParamY.setEnabled(true);
/*  332 */       this.textParamZ.setEnabled(false);
/*      */ 
/*      */ 
/*      */     }
/*  336 */     else if (this.comboDistribution.getSelectedItem().toString().equals("cauchy"))
/*      */     {
/*  338 */       this.labelParamX.setText("Begin of segment");
/*  339 */       this.labelParamY.setText("End of segment");
/*  340 */       this.labelParamZ.setText("Unused parameter");
/*      */       
/*  342 */       this.textParamX.setEnabled(true);
/*  343 */       this.textParamY.setEnabled(true);
/*  344 */       this.textParamZ.setEnabled(false);
/*      */ 
/*      */ 
/*      */     }
/*  348 */     else if (this.comboDistribution.getSelectedItem().toString().equals("triang"))
/*      */     {
/*  350 */       this.labelParamX.setText("1st param");
/*  351 */       this.labelParamY.setText("2nd param");
/*  352 */       this.labelParamZ.setText("3rd param");
/*      */       
/*  354 */       this.textParamX.setEnabled(true);
/*  355 */       this.textParamY.setEnabled(true);
/*  356 */       this.textParamZ.setEnabled(true);
/*      */ 
/*      */ 
/*      */     }
/*  360 */     else if (this.comboDistribution.getSelectedItem().toString().equals("lognormal"))
/*      */     {
/*  362 */       this.labelParamX.setText("Scale (mean)");
/*  363 */       this.labelParamY.setText("Shape (std dev)");
/*  364 */       this.labelParamZ.setText("Unused parameter");
/*      */       
/*  366 */       this.textParamX.setEnabled(true);
/*  367 */       this.textParamY.setEnabled(true);
/*  368 */       this.textParamZ.setEnabled(false);
/*      */ 
/*      */ 
/*      */     }
/*  372 */     else if (this.comboDistribution.getSelectedItem().toString().equals("weibull"))
/*      */     {
/*  374 */       this.labelParamX.setText("Scale");
/*  375 */       this.labelParamY.setText("Shape");
/*  376 */       this.labelParamZ.setText("Unused parameter");
/*      */       
/*  378 */       this.textParamX.setEnabled(true);
/*  379 */       this.textParamY.setEnabled(true);
/*  380 */       this.textParamZ.setEnabled(false);
/*      */ 
/*      */ 
/*      */     }
/*  384 */     else if (this.comboDistribution.getSelectedItem().toString().equals("intuniform"))
/*      */     {
/*  386 */       this.labelParamX.setText("Begin of segment");
/*  387 */       this.labelParamY.setText("End of segment");
/*  388 */       this.labelParamZ.setText("Unused parameter");
/*      */       
/*  390 */       this.textParamX.setEnabled(true);
/*  391 */       this.textParamY.setEnabled(true);
/*  392 */       this.textParamZ.setEnabled(false);
/*      */ 
/*      */ 
/*      */ 
/*      */     }
/*  397 */     else if (this.comboDistribution.getSelectedItem().toString().equals("pareto_shifted"))
/*      */     {
/*  399 */       this.labelParamX.setText("a");
/*  400 */       this.labelParamY.setText("b");
/*  401 */       this.labelParamZ.setText("left-shift");
/*      */       
/*  403 */       this.textParamX.setEnabled(true);
/*  404 */       this.textParamY.setEnabled(true);
/*  405 */       this.textParamZ.setEnabled(false);
/*      */ 
/*      */ 
/*      */     }
/*  409 */     else if (this.comboDistribution.getSelectedItem().toString().equals("binomial"))
/*      */     {
/*  411 */       this.labelParamX.setText("n");
/*  412 */       this.labelParamY.setText("p");
/*  413 */       this.labelParamZ.setText("Unused parameter");
/*      */       
/*  415 */       this.textParamX.setEnabled(true);
/*  416 */       this.textParamY.setEnabled(true);
/*  417 */       this.textParamZ.setEnabled(false);
/*      */ 
/*      */ 
/*      */     }
/*  421 */     else if (this.comboDistribution.getSelectedItem().toString().equals("bernoulli"))
/*      */     {
/*  423 */       this.labelParamX.setText("Independent trials prob");
/*  424 */       this.labelParamZ.setText("Unused parameter");
/*  425 */       this.labelParamZ.setText("Unused parameter");
/*      */       
/*  427 */       this.textParamX.setEnabled(true);
/*  428 */       this.textParamY.setEnabled(false);
/*  429 */       this.textParamZ.setEnabled(false);
/*      */ 
/*      */ 
/*      */     }
/*  433 */     else if (this.comboDistribution.getSelectedItem().toString().equals("geometric"))
/*      */     {
/*  435 */       this.labelParamX.setText("Independent trials prob");
/*  436 */       this.labelParamY.setText("Unused parameter");
/*  437 */       this.labelParamZ.setText("Unused parameter");
/*      */       
/*  439 */       this.textParamX.setEnabled(true);
/*  440 */       this.textParamY.setEnabled(false);
/*  441 */       this.textParamZ.setEnabled(false);
/*      */ 
/*      */ 
/*      */     }
/*  445 */     else if (this.comboDistribution.getSelectedItem().toString().equals("negbinomial"))
/*      */     {
/*  447 */       this.labelParamX.setText("n");
/*  448 */       this.labelParamY.setText("p");
/*  449 */       this.labelParamZ.setText("Unused parameter");
/*      */       
/*  451 */       this.textParamX.setEnabled(true);
/*  452 */       this.textParamY.setEnabled(true);
/*  453 */       this.textParamZ.setEnabled(false);
/*      */ 
/*      */ 
/*      */     }
/*  457 */     else if (this.comboDistribution.getSelectedItem().toString().equals("poisson")) {
/*  458 */       this.labelParamX.setText("Mean");
/*  459 */       this.labelParamY.setText("Unused parameter");
/*  460 */       this.labelParamZ.setText("Unused parameter");
/*      */       
/*  462 */       this.textParamX.setEnabled(true);
/*  463 */       this.textParamY.setEnabled(false);
/*  464 */       this.textParamZ.setEnabled(false);
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public boolean checkValues()
/*      */   {
/*  478 */     boolean allOK = true;
/*  479 */     int currentRow = 0;
/*      */     
/*      */ 
/*      */ 
/*  483 */     if (!Utils.checkName(this.textCloudName.getText()))
/*      */     {
/*  485 */       allOK = false;
/*  486 */       Utils.showErrorMessage("Invalid name for this cloud.\nA name must start with a letter and can contain alphanumeric characters, '-' and '_'", "Wrong name!");
/*      */     }
/*      */     
/*      */ 
/*      */ 
/*      */ 
/*  492 */     if ((allOK) && (this.comboDataCenter.getItemCount() <= 0)) {
/*  493 */       allOK = false;
/*      */       
/*  495 */       Utils.showErrorMessage("Invalid data-center for this cloud.\nA name must start with a letter and can contain alphanumeric characters, '-' and '_'", "Wrong name!");
/*      */     }
/*      */     
/*      */ 
/*      */ 
/*      */ 
/*  501 */     if ((allOK) && (this.comboCloudManager.getItemCount() <= 0)) {
/*  502 */       allOK = false;
/*      */       
/*  504 */       Utils.showErrorMessage("Invalid cloud manager for this cloud.\nA name must start with a letter and can contain alphanumeric characters, '-' and '_'", "Wrong name!");
/*      */     }
/*      */     
/*      */ 
/*      */ 
/*      */ 
/*      */     try
/*      */     {
/*  512 */       if (allOK) {
/*  513 */         Integer.parseInt(this.textMaxVMsPerNode.getText());
/*      */       }
/*  515 */       if ((allOK) && (Integer.parseInt(this.textMaxVMsPerNode.getText()) <= 0)) {
/*  516 */         allOK = false;
/*      */         
/*  518 */         Utils.showErrorMessage("Invalid number of VMs per node. It must be > 0.\nA name must start with a letter and can contain alphanumeric characters, '-' and '_'", "Wrong name!");
/*      */       }
/*      */       
/*      */ 
/*      */     }
/*      */     catch (Exception e)
/*      */     {
/*  525 */       allOK = false;
/*  526 */       Utils.showErrorMessage("Wrong parameter for maximum number of VMs per node.\nThis parameter must be an integer.\n", "Wrong parameter!");
/*      */     }
/*      */     
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*  534 */     if ((allOK) && (this.comboDistribution.getItemCount() <= 0)) {
/*  535 */       allOK = false;
/*      */       
/*  537 */       Utils.showErrorMessage("Invalid distribution for this cloud.\nA name must start with a letter and can contain alphanumeric characters, '-' and '_'", "Wrong name!");
/*      */     }
/*      */     
/*      */ 
/*      */ 
/*      */     try
/*      */     {
/*  544 */       if (allOK) {
/*  545 */         Integer.parseInt(this.textPreLoadTenants1.getText());
/*      */       }
/*      */     }
/*      */     catch (Exception e) {
/*  549 */       allOK = false;
/*  550 */       Utils.showErrorMessage("Wrong parameter for pre-loaded tenants.\nThis parameter must be an integer.\n", "Wrong parameter!");
/*      */     }
/*      */     
/*      */ 
/*      */ 
/*      */     try
/*      */     {
/*  557 */       if (allOK) {
/*  558 */         Double.parseDouble(this.textSimTime.getText());
/*      */       }
/*      */     }
/*      */     catch (Exception e) {
/*  562 */       allOK = false;
/*  563 */       Utils.showErrorMessage("Wrong parameter for simulation time.\nThis parameter must be a double.\n", "Wrong parameter!");
/*      */     }
/*      */     
/*      */ 
/*      */ 
/*      */     try
/*      */     {
/*  570 */       if (allOK) {
/*  571 */         Integer.parseInt(this.hoursRepetition.getText());
/*      */       }
/*      */     }
/*      */     catch (Exception e) {
/*  575 */       allOK = false;
/*  576 */       Utils.showErrorMessage("Wrong parameter for repetitions.\nThis parameter must be an integer.\n", "Wrong parameter!");
/*      */     }
/*      */     
/*      */ 
/*      */ 
/*      */ 
/*      */     try
/*      */     {
/*  584 */       if ((this.textParamX.isEnabled()) && (allOK)) {
/*  585 */         Double.parseDouble(this.textParamX.getText());
/*      */       }
/*      */     }
/*      */     catch (Exception e) {
/*  589 */       allOK = false;
/*  590 */       Utils.showErrorMessage("Wrong parameter (first) for the distribution.\nThis parameter must be a double.\n", "Wrong parameter!");
/*      */     }
/*      */     
/*      */ 
/*      */ 
/*      */ 
/*      */     try
/*      */     {
/*  598 */       if ((this.textParamY.isEnabled()) && (allOK)) {
/*  599 */         Double.parseDouble(this.textParamY.getText());
/*      */       }
/*      */     }
/*      */     catch (Exception e) {
/*  603 */       allOK = false;
/*  604 */       Utils.showErrorMessage("Wrong parameter (second) for the distribution.\nThis parameter must be a double.\n", "Wrong parameter!");
/*      */     }
/*      */     
/*      */ 
/*      */ 
/*      */ 
/*      */     try
/*      */     {
/*  612 */       if ((this.textParamZ.isEnabled()) && (allOK)) {
/*  613 */         Double.parseDouble(this.textParamZ.getText());
/*      */       }
/*      */     }
/*      */     catch (Exception e) {
/*  617 */       allOK = false;
/*  618 */       Utils.showErrorMessage("Wrong parameter (third) for the distribution.\nThis parameter must be a double.\n", "Wrong parameter!");
/*      */     }
/*      */     
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */     try
/*      */     {
/*  627 */       while ((currentRow < this.tableTenants.getModel().getRowCount()) && (allOK))
/*      */       {
/*  629 */         Integer.parseInt((String)this.tableTenants.getModel().getValueAt(currentRow, 1));
/*  630 */         currentRow++;
/*      */       }
/*      */     }
/*      */     catch (Exception e)
/*      */     {
/*  635 */       allOK = false;
/*  636 */       Utils.showErrorMessage("Wrong parameter for tenant instances.\nThis parameter must be an integer.\n", "Wrong parameter!");
/*      */     }
/*      */     
/*      */ 
/*      */ 
/*  641 */     return allOK;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   private void loadCloudObjectInPanel(Cloud newCloud)
/*      */   {
/*  653 */     this.textCloudName.setText(newCloud.getName());
/*  654 */     Utils.initCombo(this.comboDataCenter, newCloud.getDataCenter());
/*  655 */     Utils.initCombo(this.comboCloudManager, newCloud.getCloudManager());
/*  656 */     this.textPreLoadTenants1.setText(Integer.toString(newCloud.getPreLoadedTenants()));
/*  657 */     Utils.initCombo(this.comboDistribution, newCloud.getDistribution());
/*  658 */     updateDistributionParameters();
/*      */     
/*  660 */     this.textMaxVMsPerNode.setText(Integer.toString(newCloud.getNumVMsPerNode()));
/*  661 */     Utils.initCombo(this.hypervisorComboBox, newCloud.getHypervisor());
/*      */     
/*  663 */     if (this.textParamX.isEnabled()) {
/*  664 */       this.textParamX.setText(Double.toString(newCloud.getParameterX()));
/*      */     }
/*  666 */     if (this.textParamY.isEnabled()) {
/*  667 */       this.textParamY.setText(Double.toString(newCloud.getParameterY()));
/*      */     }
/*  669 */     if (this.textParamZ.isEnabled()) {
/*  670 */       this.textParamZ.setText(Double.toString(newCloud.getParameterZ()));
/*      */     }
/*  672 */     this.textSimTime.setText(Double.toString(newCloud.getSimTime()));
/*  673 */     this.hoursRepetition.setText(Integer.toString(newCloud.getRepetitions()));
/*      */     
/*  675 */     this.textComment.setText(newCloud.getComment());
/*      */     
/*      */ 
/*  678 */     icancloudgui.Tables.UserTableModel model = new icancloudgui.Tables.UserTableModel();
/*  679 */     Utils.userListToTable(newCloud.getUserData(), model);
/*  680 */     this.tableTenants.setModel(model);
/*  681 */     this.tableTenants.updateUI();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void loadCloudInPanel(String cloudName)
/*      */   {
/*  694 */     Cloud loadedCloud = Utils.loadCloudObject(cloudName);
/*      */     
/*      */ 
/*  697 */     if (loadedCloud != null) {
/*  698 */       loadCloudObjectInPanel(loadedCloud);
/*  699 */       showDescription();
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public Cloud panelToCloudObject()
/*      */   {
/*  714 */     Cloud currentCloud = new Cloud();
/*  715 */     java.util.LinkedList<icancloudgui.TabbedPanels.dataClasses.UserEntry> list = new java.util.LinkedList();
/*      */     
/*      */ 
/*  718 */     currentCloud.setName(this.textCloudName.getText());
/*  719 */     currentCloud.setDataCenter(this.comboDataCenter.getSelectedItem().toString());
/*  720 */     currentCloud.setCloudManager(this.comboCloudManager.getSelectedItem().toString());
/*  721 */     currentCloud.setPreLoadedTenants(Integer.parseInt(this.textPreLoadTenants1.getText()));
/*  722 */     currentCloud.setDistribution(this.comboDistribution.getSelectedItem().toString());
/*  723 */     currentCloud.setHypervisor(this.hypervisorComboBox.getSelectedItem().toString());
/*  724 */     currentCloud.setNumVMsPerNode(Integer.parseInt(this.textMaxVMsPerNode.getText()));
/*      */     
/*  726 */     if (this.textParamX.isEnabled()) {
/*  727 */       currentCloud.setParameterX(Double.parseDouble(this.textParamX.getText()));
/*      */     }
/*  729 */     if (this.textParamY.isEnabled()) {
/*  730 */       currentCloud.setParameterY(Double.parseDouble(this.textParamY.getText()));
/*      */     }
/*  732 */     if (this.textParamZ.isEnabled()) {
/*  733 */       currentCloud.setParameterZ(Double.parseDouble(this.textParamZ.getText()));
/*      */     }
/*  735 */     currentCloud.setSimTime(Double.parseDouble(this.textSimTime.getText()));
/*  736 */     currentCloud.setRepetitions(Integer.parseInt(this.hoursRepetition.getText()));
/*  737 */     currentCloud.setComment(this.textComment.getText());
/*      */     
/*  739 */     Utils.userTableModelToList((icancloudgui.Tables.UserTableModel)this.tableTenants.getModel(), list);
/*  740 */     currentCloud.setUserData(list);
/*      */     
/*  742 */     return currentCloud;
/*      */   }
/*      */   
/*      */ 
/*      */   private JLabel labelDistribution;
/*      */   
/*      */   private JLabel labelHelp;
/*      */   
/*      */   private JLabel labelHypervisor;
/*      */   
/*      */   private JLabel labelMaxVMsPerNode;
/*      */   private JLabel labelParamX;
/*      */   private JLabel labelParamY;
/*      */   private JLabel labelParamZ;
/*      */   private JLabel labelPreLoadTenants;
/*      */   private JLabel labelSimTime;
/*      */   private JLabel saveLabel;
/*      */   
/*      */   private boolean writeCloudtoFile()
/*      */   {
/*  762 */     boolean allOK = checkValues();
/*      */     
/*      */ 
/*  765 */     if (allOK)
/*      */     {
/*      */ 
/*  768 */       Cloud currentCloud = panelToCloudObject();
/*      */       
/*      */ 
/*  771 */       String newCloudFile = icancloudgui.Utils.Configuration.REPOSITORY_CLOUD + java.io.File.separatorChar + currentCloud.getName();
/*      */       
/*      */ 
/*      */       try
/*      */       {
/*  776 */         if (new java.io.File(newCloudFile).exists())
/*      */         {
/*      */ 
/*  779 */           int response = javax.swing.JOptionPane.showConfirmDialog(null, "Overwrite existing Cloud?", "Confirm Overwrite", 2, 3);
/*      */           
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*  786 */           if (response == 0)
/*      */           {
/*      */ 
/*  789 */             java.io.FileOutputStream fout = new java.io.FileOutputStream(newCloudFile);
/*  790 */             java.io.ObjectOutputStream oos = new java.io.ObjectOutputStream(fout);
/*  791 */             oos.writeObject(currentCloud);
/*  792 */             oos.close();
/*  793 */             allOK = true;
/*      */           }
/*      */           
/*      */ 
/*      */         }
/*      */         else
/*      */         {
/*      */ 
/*  801 */           java.io.FileOutputStream fout = new java.io.FileOutputStream(newCloudFile);
/*  802 */           java.io.ObjectOutputStream oos = new java.io.ObjectOutputStream(fout);
/*  803 */           oos.writeObject(currentCloud);
/*  804 */           oos.close();
/*  805 */           allOK = true;
/*      */         }
/*      */       }
/*      */       catch (Exception e) {
/*  809 */         e.printStackTrace();
/*      */       }
/*      */       
/*      */ 
/*      */     }
/*      */     else
/*      */     {
/*  816 */       javax.swing.JOptionPane.showMessageDialog(new javax.swing.JFrame(), "Wrong values. Please, re-check", "Error", 0);
/*      */     }
/*      */     
/*      */ 
/*  820 */     return allOK;
/*      */   }
/*      */   
/*      */   private JTable tableTenants;
/*      */   private JTextField textCloudName;
/*      */   private JTextArea textComment;
/*      */   private JTextArea textConfiguration;
/*      */   private JTextField textMaxVMsPerNode;
/*      */   private JTextField textParamX;
/*      */   private JTextField textParamY;
/*      */   private JTextField textParamZ;
/*      */   private JTextField textPreLoadTenants1;
/*      */   private JTextField textSimTime;
/*      */   private void initComponents()
/*      */   {
/*  835 */     this.textCloudName = new JTextField();
/*  836 */     this.labelCloudName = new JLabel();
/*  837 */     this.comboDataCenter = new JComboBox();
/*  838 */     this.labelDataCenter = new JLabel();
/*  839 */     this.jScrollPane1 = new javax.swing.JScrollPane();
/*  840 */     this.tableTenants = new JTable();
/*  841 */     this.labelMaxVMsPerNode = new JLabel();
/*  842 */     this.comboCloudManager = new JComboBox();
/*  843 */     this.buttonClear = new JLabel();
/*  844 */     this.saveLabel = new JLabel();
/*  845 */     this.labelHelp = new JLabel();
/*  846 */     this.comboDistribution = new JComboBox();
/*  847 */     this.labelDistribution = new JLabel();
/*  848 */     this.labelPreLoadTenants = new JLabel();
/*  849 */     this.textMaxVMsPerNode = new JTextField();
/*  850 */     this.jPanel1 = new javax.swing.JPanel();
/*  851 */     this.labelParamZ = new JLabel();
/*  852 */     this.labelParamY = new JLabel();
/*  853 */     this.labelParamX = new JLabel();
/*  854 */     this.textParamX = new JTextField();
/*  855 */     this.textParamY = new JTextField();
/*  856 */     this.textParamZ = new JTextField();
/*  857 */     this.labelSimTime = new JLabel();
/*  858 */     this.textSimTime = new JTextField();
/*  859 */     this.jScrollPane2 = new javax.swing.JScrollPane();
/*  860 */     this.textConfiguration = new JTextArea();
/*  861 */     this.jScrollPane3 = new javax.swing.JScrollPane();
/*  862 */     this.textComment = new JTextArea();
/*  863 */     this.labelComment = new JLabel();
/*  864 */     this.labelConfiguration = new JLabel();
/*  865 */     this.buttonGenerate = new JLabel();
/*  866 */     this.jLabel1 = new JLabel();
/*  867 */     this.hoursRepetition = new JTextField();
/*  868 */     this.jLabel2 = new JLabel();
/*  869 */     this.hypervisorComboBox = new JComboBox();
/*  870 */     this.labelCloudManager1 = new JLabel();
/*  871 */     this.labelHypervisor = new JLabel();
/*  872 */     this.textPreLoadTenants1 = new JTextField();
/*      */     
/*  874 */     ResourceMap resourceMap = ((icancloudgui.ICanCloudGUIApp)org.jdesktop.application.Application.getInstance(icancloudgui.ICanCloudGUIApp.class)).getContext().getResourceMap(CloudsPanel.class);
/*  875 */     setBackground(resourceMap.getColor("Form.background"));
/*  876 */     setName("Form");
/*  877 */     setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());
/*      */     
/*  879 */     this.textCloudName.setText(resourceMap.getString("textCloudName.text", new Object[0]));
/*  880 */     this.textCloudName.setName("textCloudName");
/*  881 */     add(this.textCloudName, new AbsoluteConstraints(60, 15, 310, -1));
/*      */     
/*  883 */     this.labelCloudName.setText(resourceMap.getString("labelCloudName.text", new Object[0]));
/*  884 */     this.labelCloudName.setToolTipText(resourceMap.getString("labelCloudName.toolTipText", new Object[0]));
/*  885 */     this.labelCloudName.setName("labelCloudName");
/*  886 */     add(this.labelCloudName, new AbsoluteConstraints(20, 20, -1, -1));
/*      */     
/*  888 */     this.comboDataCenter.setName("comboDataCenter");
/*  889 */     this.comboDataCenter.addItemListener(new java.awt.event.ItemListener() {
/*      */       public void itemStateChanged(java.awt.event.ItemEvent evt) {
/*  891 */         CloudsPanel.this.comboDataCenterItemStateChanged(evt);
/*      */       }
/*  893 */     });
/*  894 */     add(this.comboDataCenter, new AbsoluteConstraints(120, 50, 250, -1));
/*      */     
/*  896 */     this.labelDataCenter.setText(resourceMap.getString("labelDataCenter.text", new Object[0]));
/*  897 */     this.labelDataCenter.setToolTipText(resourceMap.getString("labelDataCenter.toolTipText", new Object[0]));
/*  898 */     this.labelDataCenter.setName("labelDataCenter");
/*  899 */     add(this.labelDataCenter, new AbsoluteConstraints(30, 50, -1, -1));
/*      */     
/*  901 */     this.jScrollPane1.setName("jScrollPane1");
/*      */     
/*  903 */     this.tableTenants.setModel(new javax.swing.table.DefaultTableModel(new Object[][] { { null, null }, { null, null }, { null, null }, { null, null } }, new String[] { "Tenant", "# Instances" })
/*      */     {
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*  914 */       Class[] types = { String.class, String.class };
/*      */       
/*      */ 
/*      */       public Class getColumnClass(int columnIndex)
/*      */       {
/*  919 */         return this.types[columnIndex];
/*      */       }
/*  921 */     });
/*  922 */     this.tableTenants.setGridColor(resourceMap.getColor("tableTenants.gridColor"));
/*  923 */     this.tableTenants.setName("tableTenants");
/*  924 */     this.tableTenants.setSelectionForeground(resourceMap.getColor("tableTenants.selectionForeground"));
/*  925 */     this.jScrollPane1.setViewportView(this.tableTenants);
/*  926 */     this.tableTenants.getColumnModel().getColumn(0).setHeaderValue(resourceMap.getString("tableTenants.columnModel.title0", new Object[0]));
/*  927 */     this.tableTenants.getColumnModel().getColumn(1).setHeaderValue(resourceMap.getString("tableTenants.columnModel.title1", new Object[0]));
/*      */     
/*  929 */     add(this.jScrollPane1, new AbsoluteConstraints(20, 190, 350, 110));
/*      */     
/*  931 */     this.labelMaxVMsPerNode.setText(resourceMap.getString("labelMaxVMsPerNode.text", new Object[0]));
/*  932 */     this.labelMaxVMsPerNode.setToolTipText(resourceMap.getString("labelMaxVMsPerNode.toolTipText", new Object[0]));
/*  933 */     this.labelMaxVMsPerNode.setName("labelMaxVMsPerNode");
/*  934 */     add(this.labelMaxVMsPerNode, new AbsoluteConstraints(20, 140, -1, 40));
/*      */     
/*  936 */     this.comboCloudManager.setName("comboCloudManager");
/*  937 */     this.comboCloudManager.addItemListener(new java.awt.event.ItemListener() {
/*      */       public void itemStateChanged(java.awt.event.ItemEvent evt) {
/*  939 */         CloudsPanel.this.comboCloudManagerItemStateChanged(evt);
/*      */       }
/*  941 */     });
/*  942 */     add(this.comboCloudManager, new AbsoluteConstraints(120, 80, 250, -1));
/*      */     
/*  944 */     this.buttonClear.setIcon(resourceMap.getIcon("buttonClear.icon"));
/*  945 */     this.buttonClear.setToolTipText(resourceMap.getString("buttonClear.toolTipText", new Object[0]));
/*  946 */     this.buttonClear.setName("buttonClear");
/*  947 */     this.buttonClear.addMouseListener(new java.awt.event.MouseAdapter() {
/*      */       public void mouseClicked(MouseEvent evt) {
/*  949 */         CloudsPanel.this.buttonClearMouseClicked(evt);
/*      */       }
/*  951 */     });
/*  952 */     add(this.buttonClear, new AbsoluteConstraints(670, 20, -1, -1));
/*      */     
/*  954 */     this.saveLabel.setIcon(resourceMap.getIcon("saveLabel.icon"));
/*  955 */     this.saveLabel.setToolTipText(resourceMap.getString("saveLabel.toolTipText", new Object[0]));
/*  956 */     this.saveLabel.setName("saveLabel");
/*  957 */     this.saveLabel.addMouseListener(new java.awt.event.MouseAdapter() {
/*      */       public void mouseClicked(MouseEvent evt) {
/*  959 */         CloudsPanel.this.saveLabelMouseClicked(evt);
/*      */       }
/*  961 */     });
/*  962 */     add(this.saveLabel, new AbsoluteConstraints(710, 20, -1, -1));
/*      */     
/*  964 */     this.labelHelp.setIcon(resourceMap.getIcon("labelHelp.icon"));
/*  965 */     this.labelHelp.setToolTipText(resourceMap.getString("labelHelp.toolTipText", new Object[0]));
/*  966 */     this.labelHelp.setName("labelHelp");
/*  967 */     this.labelHelp.addMouseListener(new java.awt.event.MouseAdapter() {
/*      */       public void mouseClicked(MouseEvent evt) {
/*  969 */         CloudsPanel.this.labelHelpMouseClicked(evt);
/*      */       }
/*  971 */     });
/*  972 */     add(this.labelHelp, new AbsoluteConstraints(750, 20, -1, -1));
/*      */     
/*  974 */     this.comboDistribution.setName("comboDistribution");
/*  975 */     this.comboDistribution.addItemListener(new java.awt.event.ItemListener() {
/*      */       public void itemStateChanged(java.awt.event.ItemEvent evt) {
/*  977 */         CloudsPanel.this.comboDistributionItemStateChanged(evt);
/*      */       }
/*  979 */     });
/*  980 */     add(this.comboDistribution, new AbsoluteConstraints(160, 340, 210, -1));
/*      */     
/*  982 */     this.labelDistribution.setText(resourceMap.getString("labelDistribution.text", new Object[0]));
/*  983 */     this.labelDistribution.setToolTipText(resourceMap.getString("labelDistribution.toolTipText", new Object[0]));
/*  984 */     this.labelDistribution.setName("labelDistribution");
/*  985 */     add(this.labelDistribution, new AbsoluteConstraints(10, 340, -1, -1));
/*      */     
/*  987 */     this.labelPreLoadTenants.setText(resourceMap.getString("labelPreLoadTenants.text", new Object[0]));
/*  988 */     this.labelPreLoadTenants.setToolTipText(resourceMap.getString("labelPreLoadTenants.toolTipText", new Object[0]));
/*  989 */     this.labelPreLoadTenants.setName("labelPreLoadTenants");
/*  990 */     add(this.labelPreLoadTenants, new AbsoluteConstraints(10, 310, -1, -1));
/*      */     
/*  992 */     this.textMaxVMsPerNode.setText(resourceMap.getString("textMaxVMsPerNode.text", new Object[0]));
/*  993 */     this.textMaxVMsPerNode.setName("textMaxVMsPerNode");
/*  994 */     add(this.textMaxVMsPerNode, new AbsoluteConstraints(180, 150, 190, -1));
/*      */     
/*  996 */     this.jPanel1.setBackground(resourceMap.getColor("jPanel1.background"));
/*  997 */     this.jPanel1.setBorder(javax.swing.BorderFactory.createEtchedBorder());
/*  998 */     this.jPanel1.setName("jPanel1");
/*  999 */     this.jPanel1.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());
/*      */     
/* 1001 */     this.labelParamZ.setText(resourceMap.getString("labelParamZ.text", new Object[0]));
/* 1002 */     this.labelParamZ.setName("labelParamZ");
/* 1003 */     this.jPanel1.add(this.labelParamZ, new AbsoluteConstraints(20, 70, -1, -1));
/*      */     
/* 1005 */     this.labelParamY.setText(resourceMap.getString("labelParamY.text", new Object[0]));
/* 1006 */     this.labelParamY.setName("labelParamY");
/* 1007 */     this.jPanel1.add(this.labelParamY, new AbsoluteConstraints(20, 40, -1, -1));
/*      */     
/* 1009 */     this.labelParamX.setText(resourceMap.getString("labelParamX.text", new Object[0]));
/* 1010 */     this.labelParamX.setName("labelParamX");
/* 1011 */     this.jPanel1.add(this.labelParamX, new AbsoluteConstraints(20, 10, -1, -1));
/*      */     
/* 1013 */     this.textParamX.setText(resourceMap.getString("textParamX.text", new Object[0]));
/* 1014 */     this.textParamX.setName("textParamX");
/* 1015 */     this.jPanel1.add(this.textParamX, new AbsoluteConstraints(170, 10, 130, -1));
/*      */     
/* 1017 */     this.textParamY.setText(resourceMap.getString("textParamY.text", new Object[0]));
/* 1018 */     this.textParamY.setName("textParamY");
/* 1019 */     this.jPanel1.add(this.textParamY, new AbsoluteConstraints(170, 40, 130, -1));
/*      */     
/* 1021 */     this.textParamZ.setText(resourceMap.getString("textParamZ.text", new Object[0]));
/* 1022 */     this.textParamZ.setName("textParamZ");
/* 1023 */     this.jPanel1.add(this.textParamZ, new AbsoluteConstraints(170, 70, 130, -1));
/*      */     
/* 1025 */     add(this.jPanel1, new AbsoluteConstraints(50, 380, 320, 110));
/*      */     
/* 1027 */     this.labelSimTime.setText(resourceMap.getString("labelSimTime.text", new Object[0]));
/* 1028 */     this.labelSimTime.setToolTipText(resourceMap.getString("labelSimTime.toolTipText", new Object[0]));
/* 1029 */     this.labelSimTime.setName("labelSimTime");
/* 1030 */     add(this.labelSimTime, new AbsoluteConstraints(30, 500, 170, -1));
/*      */     
/* 1032 */     this.textSimTime.setText(resourceMap.getString("textSimTime.text", new Object[0]));
/* 1033 */     this.textSimTime.setName("textSimTime");
/* 1034 */     add(this.textSimTime, new AbsoluteConstraints(200, 500, 170, -1));
/*      */     
/* 1036 */     this.jScrollPane2.setName("jScrollPane2");
/*      */     
/* 1038 */     this.textConfiguration.setBackground(resourceMap.getColor("textConfiguration.background"));
/* 1039 */     this.textConfiguration.setColumns(20);
/* 1040 */     this.textConfiguration.setForeground(resourceMap.getColor("textConfiguration.foreground"));
/* 1041 */     this.textConfiguration.setRows(5);
/* 1042 */     this.textConfiguration.setName("textConfiguration");
/* 1043 */     this.jScrollPane2.setViewportView(this.textConfiguration);
/*      */     
/* 1045 */     add(this.jScrollPane2, new AbsoluteConstraints(390, 80, 390, 340));
/*      */     
/* 1047 */     this.jScrollPane3.setName("jScrollPane3");
/*      */     
/* 1049 */     this.textComment.setColumns(20);
/* 1050 */     this.textComment.setRows(5);
/* 1051 */     this.textComment.setName("textComment");
/* 1052 */     this.jScrollPane3.setViewportView(this.textComment);
/*      */     
/* 1054 */     add(this.jScrollPane3, new AbsoluteConstraints(390, 460, 390, 90));
/*      */     
/* 1056 */     this.labelComment.setText(resourceMap.getString("labelComment.text", new Object[0]));
/* 1057 */     this.labelComment.setName("labelComment");
/* 1058 */     add(this.labelComment, new AbsoluteConstraints(390, 440, -1, -1));
/*      */     
/* 1060 */     this.labelConfiguration.setText(resourceMap.getString("labelConfiguration.text", new Object[0]));
/* 1061 */     this.labelConfiguration.setName("labelConfiguration");
/* 1062 */     add(this.labelConfiguration, new AbsoluteConstraints(390, 60, -1, -1));
/*      */     
/* 1064 */     this.buttonGenerate.setIcon(resourceMap.getIcon("buttonGenerate.icon"));
/* 1065 */     this.buttonGenerate.setText(resourceMap.getString("buttonGenerate.text", new Object[0]));
/* 1066 */     this.buttonGenerate.setToolTipText(resourceMap.getString("buttonGenerate.toolTipText", new Object[0]));
/* 1067 */     this.buttonGenerate.setName("buttonGenerate");
/* 1068 */     this.buttonGenerate.addMouseListener(new java.awt.event.MouseAdapter() {
/*      */       public void mouseClicked(MouseEvent evt) {
/* 1070 */         CloudsPanel.this.buttonGenerateMouseClicked(evt);
/*      */       }
/* 1072 */     });
/* 1073 */     add(this.buttonGenerate, new AbsoluteConstraints(630, 20, -1, -1));
/*      */     
/* 1075 */     this.jLabel1.setIcon(resourceMap.getIcon("jLabel1.icon"));
/* 1076 */     this.jLabel1.setText(resourceMap.getString("jLabel1.text", new Object[0]));
/* 1077 */     this.jLabel1.setToolTipText(resourceMap.getString("jLabel1.toolTipText", new Object[0]));
/* 1078 */     this.jLabel1.setName("jLabel1");
/* 1079 */     this.jLabel1.addMouseListener(new java.awt.event.MouseAdapter() {
/*      */       public void mouseClicked(MouseEvent evt) {
/* 1081 */         CloudsPanel.this.jLabel1MouseClicked(evt);
/*      */       }
/* 1083 */     });
/* 1084 */     add(this.jLabel1, new AbsoluteConstraints(590, 20, -1, -1));
/*      */     
/* 1086 */     this.hoursRepetition.setText(resourceMap.getString("hoursRepetitions.text", new Object[0]));
/* 1087 */     this.hoursRepetition.setName("hoursRepetitions");
/* 1088 */     add(this.hoursRepetition, new AbsoluteConstraints(200, 530, 170, 20));
/* 1089 */     this.hoursRepetition.getAccessibleContext().setAccessibleName(resourceMap.getString("hoursRepetitions.AccessibleContext.accessibleName", new Object[0]));
/*      */     
/* 1091 */     this.jLabel2.setText(resourceMap.getString("jLabel2.text", new Object[0]));
/* 1092 */     this.jLabel2.setName("jLabel2");
/* 1093 */     add(this.jLabel2, new AbsoluteConstraints(60, 530, -1, -1));
/*      */     
/* 1095 */     this.hypervisorComboBox.setName("hypervisorComboBox");
/* 1096 */     add(this.hypervisorComboBox, new AbsoluteConstraints(120, 110, 250, -1));
/* 1097 */     this.hypervisorComboBox.getAccessibleContext().setAccessibleName(resourceMap.getString("jComboBox1.AccessibleContext.accessibleName", new Object[0]));
/*      */     
/* 1099 */     this.labelCloudManager1.setText(resourceMap.getString("labelCloudManager1.text", new Object[0]));
/* 1100 */     this.labelCloudManager1.setToolTipText(resourceMap.getString("labelCloudManager1.toolTipText", new Object[0]));
/* 1101 */     this.labelCloudManager1.setName("labelCloudManager1");
/* 1102 */     add(this.labelCloudManager1, new AbsoluteConstraints(10, 70, -1, 40));
/*      */     
/* 1104 */     this.labelHypervisor.setText(resourceMap.getString("labelHypervisor.text", new Object[0]));
/* 1105 */     this.labelHypervisor.setToolTipText(resourceMap.getString("labelHypervisor.toolTipText", new Object[0]));
/* 1106 */     this.labelHypervisor.setName("labelHypervisor");
/* 1107 */     add(this.labelHypervisor, new AbsoluteConstraints(40, 100, -1, 40));
/*      */     
/* 1109 */     this.textPreLoadTenants1.setName("textPreLoadTenants1");
/* 1110 */     add(this.textPreLoadTenants1, new AbsoluteConstraints(220, 310, 150, -1));
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   private void buttonClearMouseClicked(MouseEvent evt)
/*      */   {
/* 1118 */     int response = javax.swing.JOptionPane.showConfirmDialog(null, "Clear current Cloud configuration?", "Confirm to clear this form", 2, 3);
/*      */     
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/* 1125 */     if (response == 0) {
/* 1126 */       clear();
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */   private void saveLabelMouseClicked(MouseEvent evt)
/*      */   {
/* 1134 */     boolean allOK = false;
/*      */     
/*      */ 
/* 1137 */     allOK = writeCloudtoFile();
/*      */     
/*      */ 
/* 1140 */     if (allOK)
/* 1141 */       this.mainFrame.updateTree();
/*      */   }
/*      */   
/*      */   private void labelHelpMouseClicked(MouseEvent evt) {
/* 1145 */     Utils.showHelp(this);
/*      */   }
/*      */   
/*      */   private void comboDistributionItemStateChanged(java.awt.event.ItemEvent evt)
/*      */   {
/* 1150 */     updateDistributionParameters();
/* 1151 */     showDescription();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   private void buttonGenerateMouseClicked(MouseEvent evt)
/*      */   {
/* 1164 */     icancloudgui.Utils.Configuration config = new icancloudgui.Utils.Configuration();
/* 1165 */     int response = 0;
/* 1166 */     boolean allOK = false;
/*      */     
/*      */ 
/* 1169 */     if (checkValues())
/*      */     {
/*      */ 
/* 1172 */       java.io.File simFolder = new java.io.File(config.getProperty("iCanCloudHome") + java.io.File.separatorChar + "simulations");
/*      */       
/*      */ 
/* 1175 */       if (!simFolder.exists())
/*      */       {
/* 1177 */         javax.swing.JOptionPane.showMessageDialog(new javax.swing.JFrame(), "Simulation folder not found. Please, check iCanCloud path and simulation's directory", "Error", 0);
/*      */ 
/*      */ 
/*      */ 
/*      */       }
/*      */       else
/*      */       {
/*      */ 
/*      */ 
/*      */ 
/* 1187 */         response = javax.swing.JOptionPane.showConfirmDialog(null, "Save existing Cloud?", "This cloud model must be saved in the respository for generating configuration files.", 2, 3);
/*      */         
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/* 1194 */         if (response == 0)
/*      */         {
/*      */ 
/* 1197 */           allOK = writeCloudtoFile();
/*      */           
/*      */ 
/* 1200 */           if (allOK) {
/* 1201 */             this.mainFrame.updateTree();
/*      */           }
/*      */           
/* 1204 */           java.io.File newCloudModelPath = new java.io.File(simFolder.getAbsolutePath() + java.io.File.separatorChar + this.textCloudName.getText().replaceAll(" ", "_"));
/*      */           
/*      */ 
/* 1207 */           icancloudgui.Utils.ConfigFilesGenerator generator = new icancloudgui.Utils.ConfigFilesGenerator(this.textCloudName.getText(), newCloudModelPath);
/*      */           
/*      */ 
/* 1210 */           generator.generateNedFile();
/*      */           
/*      */ 
/* 1213 */           generator.generateIniFile();
/*      */           
/*      */ 
/* 1216 */           generator.generateRunScript();
/*      */         }
/*      */       }
/*      */     }
/*      */   }
/*      */   
/*      */   private void comboDataCenterItemStateChanged(java.awt.event.ItemEvent evt) {
/* 1223 */     showDescription();
/*      */   }
/*      */   
/*      */   private void comboCloudManagerItemStateChanged(java.awt.event.ItemEvent evt) {
/* 1227 */     showDescription();
/*      */   }
/*      */   
/*      */   private void jLabel1MouseClicked(MouseEvent evt)
/*      */   {
/* 1232 */     this.charts.setVisible(true);
/*      */   }
/*      */ }


/* Location:              /home/chverma/Descargas/iCanCloudGUI/iCanCloudGUI.jar!/icancloudgui/TabbedPanels/CloudsPanel.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */