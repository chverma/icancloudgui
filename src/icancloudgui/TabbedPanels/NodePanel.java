/*     */ package icancloudgui.TabbedPanels;
/*     */ 
/*     */ import javax.swing.JLabel;
/*     */ 
/*     */ public class NodePanel extends javax.swing.JPanel {
/*     */   icancloudgui.ICanCloudGUIView mainFrame;
/*     */   private JLabel buttonClear;
/*     */   private javax.swing.JComboBox comboCPU;
/*     */   private javax.swing.JComboBox comboDisk;
/*     */   private javax.swing.JComboBox comboHypervisor;
/*     */   private javax.swing.JComboBox comboMemory;
/*     */   private javax.swing.JComboBox comboPSU;
/*     */   private javax.swing.JCheckBox isStorageNode;
/*     */   private JLabel jLabel2;
/*     */   private javax.swing.JScrollPane jScrollPane1;
/*     */   private javax.swing.JScrollPane jScrollPane2;
/*     */   private JLabel labelCPU;
/*     */   private JLabel labelComment;
/*     */   private JLabel labelDisk;
/*     */   private JLabel labelHelp;
/*     */   private JLabel labelHypervisor;
/*     */   private JLabel labelMemory;
/*     */   private JLabel labelName;
/*     */   private JLabel labelNumDisks;
/*     */   private JLabel labelPSU;
/*     */   private JLabel saveLabel;
/*     */   private javax.swing.JTextArea textComment;
/*     */   private javax.swing.JTextArea textDescription;
/*     */   private javax.swing.JTextField textName;
/*     */   private javax.swing.JTextField textNumDisks;
/*     */   
/*  32 */   public NodePanel(icancloudgui.ICanCloudGUIView mainFrame) { this.mainFrame = mainFrame;
/*     */     
/*  34 */     initComponents();
/*     */     
/*  36 */     reloadPanel();
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void reloadPanel()
/*     */   {
/*  49 */     String currentElement = null;
/*     */     
/*  51 */     if (this.comboCPU.getItemCount() > 0) {
/*  52 */       currentElement = this.comboCPU.getSelectedItem().toString();
/*     */     }
/*  54 */     java.util.LinkedList<String> list = icancloudgui.Utils.Utils.getComponentListFromDir(icancloudgui.Utils.Configuration.REPOSITORY_CPUS_FOLDER);
/*  55 */     icancloudgui.Utils.Utils.loadCombo(list, this.comboCPU);
/*     */     
/*  57 */     if (currentElement != null) {
/*  58 */       icancloudgui.Utils.Utils.initCombo(this.comboCPU, currentElement);
/*     */     }
/*     */     
/*     */ 
/*     */ 
/*  63 */     currentElement = null;
/*     */     
/*  65 */     if (this.comboMemory.getItemCount() > 0) {
/*  66 */       currentElement = this.comboMemory.getSelectedItem().toString();
/*     */     }
/*  68 */     list = icancloudgui.Utils.Utils.getComponentListFromDir(icancloudgui.Utils.Configuration.REPOSITORY_MEMORIES_FOLDER);
/*  69 */     icancloudgui.Utils.Utils.loadCombo(list, this.comboMemory);
/*     */     
/*  71 */     if (currentElement != null) {
/*  72 */       icancloudgui.Utils.Utils.initCombo(this.comboMemory, currentElement);
/*     */     }
/*     */     
/*     */ 
/*     */ 
/*  77 */     currentElement = null;
/*     */     
/*  79 */     if (this.comboDisk.getItemCount() > 0) {
/*  80 */       currentElement = this.comboDisk.getSelectedItem().toString();
/*     */     }
/*  82 */     list = icancloudgui.Utils.Utils.getComponentListFromDir(icancloudgui.Utils.Configuration.REPOSITORY_DISKS_FOLDER);
/*  83 */     icancloudgui.Utils.Utils.loadCombo(list, this.comboDisk);
/*     */     
/*  85 */     if (currentElement != null) {
/*  86 */       icancloudgui.Utils.Utils.initCombo(this.comboDisk, currentElement);
/*     */     }
/*     */     
/*     */ 
/*  90 */     currentElement = null;
/*     */     
/*  92 */     if (this.comboPSU.getItemCount() > 0) {
/*  93 */       currentElement = this.comboPSU.getSelectedItem().toString();
/*     */     }
/*  95 */     list = icancloudgui.Utils.Utils.getComponentListFromDir(icancloudgui.Utils.Configuration.REPOSITORY_PSU_FOLDER);
/*  96 */     icancloudgui.Utils.Utils.loadCombo(list, this.comboPSU);
/*     */     
/*  98 */     if (currentElement != null) {
/*  99 */       icancloudgui.Utils.Utils.initCombo(this.comboPSU, currentElement);
/*     */     }
/*     */     
/*     */ 
/* 103 */     list = new java.util.LinkedList();
/* 104 */     list.add("MainHypervisor");
/* 105 */     icancloudgui.Utils.Utils.loadCombo(list, this.comboHypervisor);
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void clear()
/*     */   {
/* 114 */     this.textName.setText("");
/* 115 */     this.textComment.setText("");
/* 116 */     this.textNumDisks.setText("");
/*     */     
/* 118 */     if (this.comboCPU.getItemCount() > 0) {
/* 119 */       this.comboCPU.setSelectedIndex(0);
/*     */     }
/* 121 */     if (this.comboMemory.getItemCount() > 0) {
/* 122 */       this.comboMemory.setSelectedIndex(0);
/*     */     }
/* 124 */     if (this.comboDisk.getItemCount() > 0) {
/* 125 */       this.comboDisk.setSelectedIndex(0);
/*     */     }
/* 127 */     if (this.comboPSU.getItemCount() > 0) {
/* 128 */       this.comboPSU.setSelectedIndex(0);
/*     */     }
/* 130 */     if (this.comboHypervisor.getItemCount() > 0) {
/* 131 */       this.comboHypervisor.setSelectedIndex(0);
/*     */     }
/* 133 */     this.isStorageNode.setSelected(false);
/*     */     
/* 135 */     showDescription();
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void showDescription()
/*     */   {
/* 149 */     String name = "";
/*     */     
/*     */     String text;
/*     */     try
/*     */     {
/* 154 */       if ((this.comboCPU.getItemCount() > 0) && (this.comboDisk.getItemCount() > 0) && (this.comboMemory.getItemCount() > 0) && (this.comboPSU.getItemCount() > 0) && (this.textNumDisks.getText().length() > 0))
/*     */       {
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 160 */         if (this.textName.getText().length() == 0) {
/* 161 */           name = "ENTER A FANCY NAME HERE!";
/*     */         }
/*     */         else {
/* 164 */           name = this.textName.getText();
/*     */           
/* 166 */           if (this.isStorageNode.isSelected()) {
/* 167 */             name = name + " (Storage)";
/*     */           } else {
/* 169 */             name = name + " (Computing)";
/*     */           }
/*     */         }
/*     */         
/* 173 */         int numDisks = Integer.parseInt(this.textNumDisks.getText());
/*     */         
/* 175 */         text = "Configuration of node: " + name + "\n\n";
/*     */         
/*     */ 
/* 178 */         text = text + icancloudgui.Utils.Utils.loadCPUobject(this.comboCPU.getSelectedItem().toString()).toString() + "\n";
/*     */         
/*     */ 
/* 181 */         text = text + this.textNumDisks.getText() + "x " + icancloudgui.Utils.Utils.loadDiskObject(this.comboDisk.getSelectedItem().toString()).toString() + "\n";
/*     */         
/*     */ 
/* 184 */         text = text + icancloudgui.Utils.Utils.loadMemoryObject(this.comboMemory.getSelectedItem().toString()).toString() + "\n";
/*     */         
/*     */ 
/* 187 */         text = text + icancloudgui.Utils.Utils.loadPSUobject(this.comboPSU.getSelectedItem().toString()).toString() + "\n";
/*     */ 
/*     */ 
/*     */       }
/*     */       else
/*     */       {
/*     */ 
/*     */ 
/* 195 */         text = "Please, check that each component is properly configured!";
/*     */       }
/*     */     }
/*     */     catch (NumberFormatException e) {
/* 199 */       text = "Number of disks must be contain an integer value!";
/*     */     }
/*     */     
/*     */ 
/*     */ 
/* 204 */     this.textDescription.setText(text);
/* 205 */     this.textDescription.updateUI();
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public boolean checkValues()
/*     */   {
/* 218 */     boolean allOK = true;
/*     */     
/*     */ 
/* 221 */     if (!icancloudgui.Utils.Utils.checkName(this.textName.getText()))
/*     */     {
/* 223 */       allOK = false;
/* 224 */       icancloudgui.Utils.Utils.showErrorMessage("Invalid name for this node.\nA name must start with a letter and can contain alphanumeric characters, '-' and '_'", "Wrong name!");
/*     */     }
/*     */     
/*     */ 
/*     */ 
/*     */ 
/* 230 */     if ((allOK) && (this.comboCPU.getItemCount() <= 0)) {
/* 231 */       allOK = false;
/*     */       
/* 233 */       icancloudgui.Utils.Utils.showErrorMessage("Invalid CPU for this node.\nA name must start with a letter and can contain alphanumeric characters, '-' and '_'", "Wrong name!");
/*     */     }
/*     */     
/*     */ 
/*     */ 
/*     */ 
/* 239 */     if ((allOK) && (this.comboDisk.getItemCount() <= 0)) {
/* 240 */       allOK = false;
/* 241 */       icancloudgui.Utils.Utils.showErrorMessage("Invalid Disk for this node.\nA name must start with a letter and can contain alphanumeric characters, '-' and '_'", "Wrong name!");
/*     */     }
/*     */     
/*     */ 
/*     */ 
/*     */ 
/* 247 */     if ((allOK) && (this.comboMemory.getItemCount() <= 0)) {
/* 248 */       allOK = false;
/* 249 */       icancloudgui.Utils.Utils.showErrorMessage("Invalid Memory for this node.\nA name must start with a letter and can contain alphanumeric characters, '-' and '_'", "Wrong name!");
/*     */     }
/*     */     
/*     */ 
/*     */ 
/*     */ 
/* 255 */     if ((allOK) && (this.comboPSU.getItemCount() <= 0)) {
/* 256 */       allOK = false;
/* 257 */       icancloudgui.Utils.Utils.showErrorMessage("Invalid PSU for this node.\nA name must start with a letter and can contain alphanumeric characters, '-' and '_'", "Wrong name!");
/*     */     }
/*     */     
/*     */ 
/*     */ 
/*     */     try
/*     */     {
/* 264 */       if (allOK) {
/* 265 */         Integer.parseInt(this.textNumDisks.getText());
/*     */       }
/*     */     }
/*     */     catch (Exception e) {
/* 269 */       allOK = false;
/* 270 */       icancloudgui.Utils.Utils.showErrorMessage("Wrong parameter for number of disks.\nThis parameter must be an integer.\n", "Wrong parameter!");
/*     */     }
/*     */     
/*     */ 
/*     */ 
/*     */ 
/* 276 */     return allOK;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public icancloudgui.TabbedPanels.dataClasses.Node panelToNodeObject()
/*     */   {
/* 290 */     icancloudgui.TabbedPanels.dataClasses.Node currentNode = new icancloudgui.TabbedPanels.dataClasses.Node();
/* 291 */     java.util.LinkedList<icancloudgui.TabbedPanels.dataClasses.EnergyEntry> list = new java.util.LinkedList();
/*     */     
/*     */ 
/* 294 */     currentNode.setName(this.textName.getText());
/* 295 */     currentNode.setCpu(this.comboCPU.getSelectedItem().toString());
/* 296 */     currentNode.setDisk(this.comboDisk.getSelectedItem().toString());
/* 297 */     currentNode.setMemory(this.comboMemory.getSelectedItem().toString());
/* 298 */     currentNode.setPsu(this.comboPSU.getSelectedItem().toString());
/* 299 */     currentNode.setHypervisor(this.comboHypervisor.getSelectedItem().toString());
/* 300 */     currentNode.setNumDisks(Integer.parseInt(this.textNumDisks.getText()));
/* 301 */     currentNode.setIsStorage(this.isStorageNode.isSelected());
/* 302 */     currentNode.setComment(this.textComment.getText());
/*     */     
/* 304 */     return currentNode;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private boolean writeNodetoFile()
/*     */   {
/* 323 */     boolean allOK = checkValues();
/*     */     
/*     */ 
/* 326 */     if (allOK)
/*     */     {
/*     */ 
/* 329 */       icancloudgui.TabbedPanels.dataClasses.Node currentNode = panelToNodeObject();
/*     */       String newNodeFile;
/* 332 */       if (currentNode.isIsStorage()) {
/* 333 */         newNodeFile = icancloudgui.Utils.Configuration.REPOSITORY_STORAGE_NODES_FOLDER + java.io.File.separatorChar + currentNode.getName();
/*     */       } else {
/* 335 */         newNodeFile = icancloudgui.Utils.Configuration.REPOSITORY_COMPUTING_NODES_FOLDER + java.io.File.separatorChar + currentNode.getName();
/*     */       }
/*     */       
/*     */       try
/*     */       {
/* 340 */         if (new java.io.File(newNodeFile).exists())
/*     */         {
/*     */ 
/* 343 */           int response = javax.swing.JOptionPane.showConfirmDialog(null, "Overwrite existing Node?", "Confirm Overwrite", 2, 3);
/*     */           
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 350 */           if (response == 0)
/*     */           {
/*     */ 
/* 353 */             java.io.FileOutputStream fout = new java.io.FileOutputStream(newNodeFile);
/* 354 */             java.io.ObjectOutputStream oos = new java.io.ObjectOutputStream(fout);
/* 355 */             oos.writeObject(currentNode);
/* 356 */             oos.close();
/* 357 */             allOK = true;
/*     */           }
/*     */           
/*     */ 
/*     */         }
/*     */         else
/*     */         {
/*     */ 
/* 365 */           java.io.FileOutputStream fout = new java.io.FileOutputStream(newNodeFile);
/* 366 */           java.io.ObjectOutputStream oos = new java.io.ObjectOutputStream(fout);
/* 367 */           oos.writeObject(currentNode);
/* 368 */           oos.close();
/* 369 */           allOK = true;
/*     */         }
/*     */       }
/*     */       catch (Exception e) {
/* 373 */         e.printStackTrace();
/*     */       }
/*     */       
/*     */ 
/*     */     }
/*     */     else
/*     */     {
/* 380 */       javax.swing.JOptionPane.showMessageDialog(new javax.swing.JFrame(), "Wrong values. Please, re-check", "Error", 0);
/*     */     }
/*     */     
/*     */ 
/* 384 */     return allOK;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private void loadNodeObjectInPanel(icancloudgui.TabbedPanels.dataClasses.Node newNode)
/*     */   {
/* 397 */     this.textName.setText(newNode.getName());
/* 398 */     this.textNumDisks.setText(Integer.toString(newNode.getNumDisks()));
/* 399 */     this.isStorageNode.setSelected(newNode.isIsStorage());
/* 400 */     this.textComment.setText(newNode.getComment());
/* 401 */     icancloudgui.Utils.Utils.initCombo(this.comboCPU, newNode.getCpu());
/* 402 */     icancloudgui.Utils.Utils.initCombo(this.comboDisk, newNode.getDisk());
/* 403 */     icancloudgui.Utils.Utils.initCombo(this.comboMemory, newNode.getMemory());
/* 404 */     icancloudgui.Utils.Utils.initCombo(this.comboPSU, newNode.getPsu());
/* 405 */     icancloudgui.Utils.Utils.initCombo(this.comboHypervisor, newNode.getHypervisor());
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void loadNodeInPanel(boolean isStorage, String nodeName)
/*     */   {
/* 419 */     icancloudgui.TabbedPanels.dataClasses.Node loadedNode = icancloudgui.Utils.Utils.loadNodeObject(isStorage, nodeName);
/*     */     
/*     */ 
/* 422 */     if (loadedNode != null) {
/* 423 */       loadNodeObjectInPanel(loadedNode);
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private void initComponents()
/*     */   {
/* 440 */     this.comboCPU = new javax.swing.JComboBox();
/* 441 */     this.comboMemory = new javax.swing.JComboBox();
/* 442 */     this.comboDisk = new javax.swing.JComboBox();
/* 443 */     this.comboPSU = new javax.swing.JComboBox();
/* 444 */     this.labelMemory = new JLabel();
/* 445 */     this.labelDisk = new JLabel();
/* 446 */     this.labelPSU = new JLabel();
/* 447 */     this.labelName = new JLabel();
/* 448 */     this.textName = new javax.swing.JTextField();
/* 449 */     this.isStorageNode = new javax.swing.JCheckBox();
/* 450 */     this.labelNumDisks = new JLabel();
/* 451 */     this.textNumDisks = new javax.swing.JTextField();
/* 452 */     this.labelHypervisor = new JLabel();
/* 453 */     this.comboHypervisor = new javax.swing.JComboBox();
/* 454 */     this.labelCPU = new JLabel();
/* 455 */     this.jScrollPane1 = new javax.swing.JScrollPane();
/* 456 */     this.textDescription = new javax.swing.JTextArea();
/* 457 */     this.labelHelp = new JLabel();
/* 458 */     this.saveLabel = new JLabel();
/* 459 */     this.jLabel2 = new JLabel();
/* 460 */     this.jScrollPane2 = new javax.swing.JScrollPane();
/* 461 */     this.textComment = new javax.swing.JTextArea();
/* 462 */     this.labelComment = new JLabel();
/* 463 */     this.buttonClear = new JLabel();
/*     */     
/* 465 */     org.jdesktop.application.ResourceMap resourceMap = ((icancloudgui.ICanCloudGUIApp)org.jdesktop.application.Application.getInstance(icancloudgui.ICanCloudGUIApp.class)).getContext().getResourceMap(NodePanel.class);
/* 466 */     setBackground(resourceMap.getColor("Form.background"));
/* 467 */     setName("Form");
/* 468 */     setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());
/*     */     
/* 470 */     this.comboCPU.setName("comboCPU");
/* 471 */     this.comboCPU.addActionListener(new java.awt.event.ActionListener() {
/*     */       public void actionPerformed(java.awt.event.ActionEvent evt) {
/* 473 */         NodePanel.this.comboCPUActionPerformed(evt);
/*     */       }
/* 475 */     });
/* 476 */     add(this.comboCPU, new org.netbeans.lib.awtextra.AbsoluteConstraints(100, 80, 230, -1));
/*     */     
/* 478 */     this.comboMemory.setName("comboMemory");
/* 479 */     this.comboMemory.setVerifyInputWhenFocusTarget(false);
/* 480 */     this.comboMemory.addActionListener(new java.awt.event.ActionListener() {
/*     */       public void actionPerformed(java.awt.event.ActionEvent evt) {
/* 482 */         NodePanel.this.comboMemoryActionPerformed(evt);
/*     */       }
/* 484 */     });
/* 485 */     add(this.comboMemory, new org.netbeans.lib.awtextra.AbsoluteConstraints(100, 125, 230, -1));
/*     */     
/* 487 */     this.comboDisk.setName("comboDisk");
/* 488 */     this.comboDisk.addActionListener(new java.awt.event.ActionListener() {
/*     */       public void actionPerformed(java.awt.event.ActionEvent evt) {
/* 490 */         NodePanel.this.comboDiskActionPerformed(evt);
/*     */       }
/* 492 */     });
/* 493 */     add(this.comboDisk, new org.netbeans.lib.awtextra.AbsoluteConstraints(100, 165, 230, -1));
/*     */     
/* 495 */     this.comboPSU.setName("comboPSU");
/* 496 */     this.comboPSU.addActionListener(new java.awt.event.ActionListener() {
/*     */       public void actionPerformed(java.awt.event.ActionEvent evt) {
/* 498 */         NodePanel.this.comboPSUActionPerformed(evt);
/*     */       }
/* 500 */     });
/* 501 */     add(this.comboPSU, new org.netbeans.lib.awtextra.AbsoluteConstraints(100, 255, 230, -1));
/*     */     
/* 503 */     this.labelMemory.setFont(resourceMap.getFont("labelComment.font"));
/* 504 */     this.labelMemory.setText(resourceMap.getString("labelMemory.text", new Object[0]));
/* 505 */     this.labelMemory.setName("labelMemory");
/* 506 */     add(this.labelMemory, new org.netbeans.lib.awtextra.AbsoluteConstraints(40, 130, -1, -1));
/*     */     
/* 508 */     this.labelDisk.setFont(resourceMap.getFont("labelComment.font"));
/* 509 */     this.labelDisk.setText(resourceMap.getString("labelDisk.text", new Object[0]));
/* 510 */     this.labelDisk.setName("labelDisk");
/* 511 */     add(this.labelDisk, new org.netbeans.lib.awtextra.AbsoluteConstraints(60, 170, -1, -1));
/*     */     
/* 513 */     this.labelPSU.setFont(resourceMap.getFont("labelComment.font"));
/* 514 */     this.labelPSU.setText(resourceMap.getString("labelPSU.text", new Object[0]));
/* 515 */     this.labelPSU.setName("labelPSU");
/* 516 */     add(this.labelPSU, new org.netbeans.lib.awtextra.AbsoluteConstraints(65, 260, -1, -1));
/*     */     
/* 518 */     this.labelName.setFont(resourceMap.getFont("labelComment.font"));
/* 519 */     this.labelName.setText(resourceMap.getString("labelName.text", new Object[0]));
/* 520 */     this.labelName.setName("labelName");
/* 521 */     add(this.labelName, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 40, -1, -1));
/*     */     
/* 523 */     this.textName.setText(resourceMap.getString("textName.text", new Object[0]));
/* 524 */     this.textName.setName("textName");
/* 525 */     add(this.textName, new org.netbeans.lib.awtextra.AbsoluteConstraints(66, 35, 260, -1));
/*     */     
/* 527 */     this.isStorageNode.setFont(resourceMap.getFont("labelComment.font"));
/* 528 */     this.isStorageNode.setText(resourceMap.getString("isStorageNode.text", new Object[0]));
/* 529 */     this.isStorageNode.setHorizontalTextPosition(2);
/* 530 */     this.isStorageNode.setName("isStorageNode");
/* 531 */     this.isStorageNode.addActionListener(new java.awt.event.ActionListener() {
/*     */       public void actionPerformed(java.awt.event.ActionEvent evt) {
/* 533 */         NodePanel.this.isStorageNodeActionPerformed(evt);
/*     */       }
/* 535 */     });
/* 536 */     add(this.isStorageNode, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 350, -1, -1));
/*     */     
/* 538 */     this.labelNumDisks.setFont(resourceMap.getFont("labelComment.font"));
/* 539 */     this.labelNumDisks.setText(resourceMap.getString("labelNumDisks.text", new Object[0]));
/* 540 */     this.labelNumDisks.setName("labelNumDisks");
/* 541 */     add(this.labelNumDisks, new org.netbeans.lib.awtextra.AbsoluteConstraints(45, 215, -1, -1));
/*     */     
/* 543 */     this.textNumDisks.setText(resourceMap.getString("textNumDisks.text", new Object[0]));
/* 544 */     this.textNumDisks.setName("textNumDisks");
/* 545 */     add(this.textNumDisks, new org.netbeans.lib.awtextra.AbsoluteConstraints(100, 210, 230, -1));
/*     */     
/* 547 */     this.labelHypervisor.setFont(resourceMap.getFont("labelComment.font"));
/* 548 */     this.labelHypervisor.setText(resourceMap.getString("labelHypervisor.text", new Object[0]));
/* 549 */     this.labelHypervisor.setName("labelHypervisor");
/* 550 */     add(this.labelHypervisor, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 305, -1, -1));
/*     */     
/* 552 */     this.comboHypervisor.setName("comboHypervisor");
/* 553 */     add(this.comboHypervisor, new org.netbeans.lib.awtextra.AbsoluteConstraints(100, 300, 230, -1));
/*     */     
/* 555 */     this.labelCPU.setFont(resourceMap.getFont("labelComment.font"));
/* 556 */     this.labelCPU.setText(resourceMap.getString("labelCPU.text", new Object[0]));
/* 557 */     this.labelCPU.setName("labelCPU");
/* 558 */     add(this.labelCPU, new org.netbeans.lib.awtextra.AbsoluteConstraints(65, 85, -1, -1));
/*     */     
/* 560 */     this.jScrollPane1.setName("jScrollPane1");
/*     */     
/* 562 */     this.textDescription.setBackground(resourceMap.getColor("textDescription.background"));
/* 563 */     this.textDescription.setColumns(20);
/* 564 */     this.textDescription.setForeground(resourceMap.getColor("textDescription.foreground"));
/* 565 */     this.textDescription.setRows(5);
/* 566 */     this.textDescription.setTabSize(4);
/* 567 */     this.textDescription.setName("textDescription");
/* 568 */     this.jScrollPane1.setViewportView(this.textDescription);
/*     */     
/* 570 */     add(this.jScrollPane1, new org.netbeans.lib.awtextra.AbsoluteConstraints(360, 70, 420, 440));
/*     */     
/* 572 */     this.labelHelp.setIcon(resourceMap.getIcon("labelHelp.icon"));
/* 573 */     this.labelHelp.setText(resourceMap.getString("labelHelp.text", new Object[0]));
/* 574 */     this.labelHelp.setToolTipText(resourceMap.getString("labelHelp.toolTipText", new Object[0]));
/* 575 */     this.labelHelp.setName("labelHelp");
/* 576 */     this.labelHelp.addMouseListener(new java.awt.event.MouseAdapter() {
/*     */       public void mouseClicked(java.awt.event.MouseEvent evt) {
/* 578 */         NodePanel.this.labelHelpMouseClicked(evt);
/*     */       }
/* 580 */     });
/* 581 */     add(this.labelHelp, new org.netbeans.lib.awtextra.AbsoluteConstraints(750, 20, -1, -1));
/*     */     
/* 583 */     this.saveLabel.setIcon(resourceMap.getIcon("saveLabel.icon"));
/* 584 */     this.saveLabel.setText(resourceMap.getString("saveLabel.text", new Object[0]));
/* 585 */     this.saveLabel.setToolTipText(resourceMap.getString("saveLabel.toolTipText", new Object[0]));
/* 586 */     this.saveLabel.setName("saveLabel");
/* 587 */     this.saveLabel.addMouseListener(new java.awt.event.MouseAdapter() {
/*     */       public void mouseClicked(java.awt.event.MouseEvent evt) {
/* 589 */         NodePanel.this.saveLabelMouseClicked(evt);
/*     */       }
/* 591 */     });
/* 592 */     add(this.saveLabel, new org.netbeans.lib.awtextra.AbsoluteConstraints(710, 20, -1, -1));
/*     */     
/* 594 */     this.jLabel2.setFont(resourceMap.getFont("labelComment.font"));
/* 595 */     this.jLabel2.setText(resourceMap.getString("jLabel2.text", new Object[0]));
/* 596 */     this.jLabel2.setName("jLabel2");
/* 597 */     add(this.jLabel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(360, 48, -1, -1));
/*     */     
/* 599 */     this.jScrollPane2.setName("jScrollPane2");
/*     */     
/* 601 */     this.textComment.setColumns(20);
/* 602 */     this.textComment.setRows(5);
/* 603 */     this.textComment.setName("textComment");
/* 604 */     this.jScrollPane2.setViewportView(this.textComment);
/*     */     
/* 606 */     add(this.jScrollPane2, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 430, 310, -1));
/*     */     
/* 608 */     this.labelComment.setFont(resourceMap.getFont("labelComment.font"));
/* 609 */     this.labelComment.setText(resourceMap.getString("labelComment.text", new Object[0]));
/* 610 */     this.labelComment.setName("labelComment");
/* 611 */     add(this.labelComment, new org.netbeans.lib.awtextra.AbsoluteConstraints(15, 405, -1, -1));
/*     */     
/* 613 */     this.buttonClear.setIcon(resourceMap.getIcon("buttonClear.icon"));
/* 614 */     this.buttonClear.setToolTipText(resourceMap.getString("buttonClear.toolTipText", new Object[0]));
/* 615 */     this.buttonClear.setName("buttonClear");
/* 616 */     this.buttonClear.addMouseListener(new java.awt.event.MouseAdapter() {
/*     */       public void mouseClicked(java.awt.event.MouseEvent evt) {
/* 618 */         NodePanel.this.buttonClearMouseClicked(evt);
/*     */       }
/* 620 */     });
/* 621 */     add(this.buttonClear, new org.netbeans.lib.awtextra.AbsoluteConstraints(670, 20, -1, -1));
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private void comboCPUActionPerformed(java.awt.event.ActionEvent evt)
/*     */   {
/* 631 */     showDescription();
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private void comboDiskActionPerformed(java.awt.event.ActionEvent evt)
/*     */   {
/* 641 */     showDescription();
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private void comboMemoryActionPerformed(java.awt.event.ActionEvent evt)
/*     */   {
/* 651 */     showDescription();
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private void comboPSUActionPerformed(java.awt.event.ActionEvent evt)
/*     */   {
/* 661 */     showDescription();
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private void isStorageNodeActionPerformed(java.awt.event.ActionEvent evt)
/*     */   {
/* 671 */     showDescription();
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private void labelHelpMouseClicked(java.awt.event.MouseEvent evt)
/*     */   {
/* 680 */     icancloudgui.Utils.Utils.showHelp(this);
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private void saveLabelMouseClicked(java.awt.event.MouseEvent evt)
/*     */   {
/* 693 */     boolean allOK = writeNodetoFile();
/*     */     
/*     */ 
/* 696 */     if (allOK) {
/* 697 */       this.mainFrame.updateTree();
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */   private void buttonClearMouseClicked(java.awt.event.MouseEvent evt)
/*     */   {
/* 705 */     int response = javax.swing.JOptionPane.showConfirmDialog(null, "Clear current Node configuration?", "Confirm to clear this form", 2, 3);
/*     */     
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 712 */     if (response == 0) {
/* 713 */       clear();
/*     */     }
/*     */   }
/*     */ }


/* Location:              /home/chverma/Descargas/iCanCloudGUI/iCanCloudGUI.jar!/icancloudgui/TabbedPanels/NodePanel.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */