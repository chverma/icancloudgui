/*     */ package icancloudgui.TabbedPanels;
/*     */ 
/*     */ import icancloudgui.ICanCloudGUIApp;
/*     */ import icancloudgui.TabbedPanels.dataClasses.CPU;
/*     */ import icancloudgui.TabbedPanels.dataClasses.Disk;
/*     */ import icancloudgui.TabbedPanels.dataClasses.Memory;
/*     */ import icancloudgui.TabbedPanels.dataClasses.Variant;
/*     */ import icancloudgui.Utils.Configuration;
/*     */ import icancloudgui.Utils.Utils;
/*     */ import java.awt.event.ActionEvent;
/*     */ import java.awt.event.MouseEvent;
/*     */ import java.io.BufferedReader;
/*     */ import java.io.File;
/*     */ import java.io.FileOutputStream;
/*     */ import java.io.PrintStream;
/*     */ import java.io.Writer;
/*     */ import java.util.LinkedList;
/*     */ import javax.swing.JButton;
/*     */ import javax.swing.JCheckBox;
/*     */ import javax.swing.JFileChooser;
/*     */ import javax.swing.JLabel;
/*     */ import javax.swing.JTextField;
/*     */ import org.jdesktop.application.ResourceMap;
/*     */ import org.netbeans.lib.awtextra.AbsoluteConstraints;
/*     */ 
/*     */ public class MetamorphicPanel extends javax.swing.JPanel
/*     */ {
/*  28 */   private final String INI_CPU_ENERGY = "cpuEnergyData";
/*  29 */   private final String INI_CPU_SPEED = ".speed";
/*  30 */   private final String INI_CPU_MIN_SPEED = ".min_speed";
/*  31 */   private final String INI_CPU_NUM_CORES = ".numCores";
/*  32 */   private final String INI_CPU_MAX_VM = ".maxNumVM";
/*     */   
/*  34 */   private final String INI_DISK_ENERGY = "storageEnergyData";
/*  35 */   private final String INI_DISK_SIZE = "storageDeviceSize_GB";
/*  36 */   private final String INI_DISK_READ = "readBandwidth";
/*  37 */   private final String INI_DISK_WRITE = "writeBandwidth";
/*     */   
/*  39 */   private final String INI_MEMORY_ENERGY = "memoryEnergyData";
/*  40 */   private final String INI_MEMORY_SIZE_MB = "memorySize_MB";
/*  41 */   private final String INI_MEMORY_SIZE_KB = "memory.size_KB";
/*  42 */   private final String INI_MEMORY_READ = "memory.readLatencyTime_s";
/*  43 */   private final String INI_MEMORY_WRITE = "memory.writeLatencyTime_s";
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*  49 */   private final String INI_TKENV = "tkenv-plugin-path";
/*  50 */   private final String INI_TKENV_NEW = "tkenv-plugin-path = ../../../../etc/plugins";
/*     */   
/*  52 */   private final String INI_NED_PATH = "ned-path";
/*  53 */   private final String INI_NED_PATH_NEW = "ned-path = ../../../../inet/src";
/*     */   
/*  55 */   private final String NED_NUM_STORAGE_SERVERS = "numNodes_Storage =";
/*     */   private LinkedList<Variant> cpuVariantList;
/*     */   private LinkedList<Variant> storageVariantList;
/*     */   private LinkedList<Variant> memoryVariantList;
/*     */   private LinkedList<Variant> switchesVariantList;
/*     */   private LinkedList<Variant> serversList;
/*     */   private LinkedList<Variant> variantList;
/*     */   private JLabel buttonClear;
/*     */   private JButton buttonGenerateVariants;
/*     */   private JButton buttonSelectCloud;
/*     */   private JCheckBox checkBoxBetter;
/*     */   private JCheckBox checkBoxWorse;
/*     */   private JLabel jLabel1;
/*     */   private JLabel labelCloudPath;
/*     */   private JTextField textCloudPath;
/*     */   private JTextField textMaxComponents;
/*     */   
/*  72 */   public MetamorphicPanel() { initComponents(); }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void clear()
/*     */   {
/*  80 */     this.textCloudPath.setText("");
/*  81 */     this.checkBoxBetter.setSelected(false);
/*  82 */     this.checkBoxWorse.setSelected(false);
/*  83 */     this.textMaxComponents.setText("");
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private boolean checkModelFiles()
/*     */   {
/*     */     boolean existsTopology;
/*     */     
/*     */ 
/*     */ 
/*     */ 
/*  98 */     boolean existsConfiguration = existsTopology = true;
/*  99 */     String path = this.textCloudPath.getText();
/*     */     
/*     */ 
/* 102 */     String completePath = path + File.separatorChar + "omnetpp.ini";
/* 103 */     File file = new File(completePath);
/*     */     
/* 105 */     if (!file.exists()) {
/* 106 */       existsConfiguration = false;
/*     */       
/* 108 */       Utils.showErrorMessage("File omnetpp.ini not found!. Please, re-check the entered cloud path", "File not found!");
/*     */ 
/*     */ 
/*     */     }
/*     */     else
/*     */     {
/*     */ 
/*     */ 
/* 116 */       completePath = path + File.separatorChar + "scenario.ned";
/* 117 */       file = new File(completePath);
/*     */       
/* 119 */       if (!file.exists()) {
/* 120 */         existsTopology = false;
/*     */         
/* 122 */         Utils.showErrorMessage("File scenario.ned not found!. Please, re-check the entered folder", "File not found!");
/*     */       }
/*     */     }
/*     */     
/*     */ 
/* 127 */     return (existsConfiguration) && (existsTopology);
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private boolean checkParameters()
/*     */   {
/* 142 */     boolean allOK = true;
/* 143 */     int max; int maxComponents = max = 0;
/*     */     
/*     */ 
/*     */     try
/*     */     {
/* 148 */       maxComponents = Integer.parseInt(this.textMaxComponents.getText());
/*     */       
/* 150 */       if (this.checkBoxBetter.isSelected()) {
/* 151 */         max++;
/*     */       }
/* 153 */       if (this.checkBoxWorse.isSelected()) {
/* 154 */         max++;
/*     */       }
/* 156 */       if (max > maxComponents)
/*     */       {
/* 158 */         allOK = false;
/*     */         
/* 160 */         Utils.showErrorMessage("Wrong parameter for maximum number of components.\nThis parameter must be >= " + Integer.toString(max) + "\n", "Wrong parameters!");
/*     */       }
/*     */       
/*     */ 
/*     */     }
/*     */     catch (Exception e)
/*     */     {
/* 167 */       allOK = false;
/* 168 */       Utils.showErrorMessage("Wrong parameter for maximum number of components.\nThis parameter must be an integer.\n", "Wrong parameter!");
/*     */     }
/*     */     
/*     */ 
/*     */ 
/* 173 */     return allOK;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private void generateVariants()
/*     */   {
/*     */     String worse;
/*     */     
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 192 */     String better = worse = "";
/* 193 */     int restComponents = Integer.parseInt(this.textMaxComponents.getText());
/* 194 */     int currentSwitch; int currentServer; int currentMemory; int currentCPU; int currentDisk = currentCPU = currentMemory = currentServer = currentSwitch = 0;
/*     */     
/*     */ 
/* 197 */     this.cpuVariantList = new LinkedList();
/* 198 */     this.storageVariantList = new LinkedList();
/* 199 */     this.memoryVariantList = new LinkedList();
/* 200 */     this.serversList = new LinkedList();
/* 201 */     this.switchesVariantList = new LinkedList();
/* 202 */     this.variantList = new LinkedList();
/*     */     
/*     */ 
/* 205 */     if (this.checkBoxBetter.isSelected()) {
/* 206 */       restComponents--;
/*     */     }
/* 208 */     if (this.checkBoxWorse.isSelected()) {
/* 209 */       restComponents--;
/*     */     }
/*     */     
/*     */     try
/*     */     {
/* 214 */       LinkedList<String> componentNamesList = Utils.getComponentListFromDir(Configuration.REPOSITORY_CPUS_FOLDER);
/* 215 */       better = worse = "";
/* 216 */       int currentElement = 0;
/*     */       
/*     */ 
/* 219 */       if (this.checkBoxBetter.isSelected())
/*     */       {
/*     */ 
/* 222 */         better = Utils.getBetterCPU();
/* 223 */         componentNamesList.remove(better);
/* 224 */         CPU cpuObject = Utils.loadCPUobject(better);
/*     */         
/*     */ 
/* 227 */         Variant variant = new Variant();
/*     */         
/* 229 */         variant.setCpuSpeed(cpuObject.getSpeed());
/* 230 */         variant.setCpuMinSpeed(cpuObject.getMinSpeed());
/* 231 */         variant.setCpuNumCores(cpuObject.getNumCores());
/* 232 */         this.cpuVariantList.add(variant);
/*     */       }
/*     */       
/*     */ 
/*     */ 
/* 237 */       if (this.checkBoxWorse.isSelected())
/*     */       {
/*     */ 
/* 240 */         worse = Utils.getWorseCPU();
/* 241 */         componentNamesList.remove(worse);
/* 242 */         CPU cpuObject = Utils.loadCPUobject(worse);
/*     */         
/*     */ 
/* 245 */         Variant variant = new Variant();
/*     */         
/* 247 */         variant.setCpuSpeed(cpuObject.getSpeed());
/* 248 */         variant.setCpuMinSpeed(cpuObject.getMinSpeed());
/* 249 */         variant.setCpuNumCores(cpuObject.getNumCores());
/* 250 */         this.cpuVariantList.add(variant);
/*     */       }
/*     */       
/*     */ 
/* 254 */       while ((currentElement < restComponents) && (componentNamesList.size() > 0))
/*     */       {
/*     */ 
/* 257 */         CPU cpuObject = Utils.loadCPUobject((String)componentNamesList.get(0));
/*     */         
/*     */ 
/* 260 */         Variant variant = new Variant();
/*     */         
/* 262 */         variant.setCpuSpeed(cpuObject.getSpeed());
/* 263 */         variant.setCpuMinSpeed(cpuObject.getMinSpeed());
/* 264 */         variant.setCpuNumCores(cpuObject.getNumCores());
/* 265 */         this.cpuVariantList.add(variant);
/*     */         
/*     */ 
/* 268 */         currentElement++;
/*     */         
/*     */ 
/* 271 */         componentNamesList.remove(0);
/*     */       }
/*     */       
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 282 */       componentNamesList = Utils.getComponentListFromDir(Configuration.REPOSITORY_DISKS_FOLDER);
/* 283 */       better = worse = "";
/* 284 */       currentElement = 0;
/*     */       
/*     */ 
/* 287 */       if (this.checkBoxBetter.isSelected())
/*     */       {
/*     */ 
/* 290 */         better = Utils.getBetterDisk();
/* 291 */         componentNamesList.remove(better);
/* 292 */         Disk diskObject = Utils.loadDiskObject(better);
/*     */         
/*     */ 
/* 295 */         Variant variant = new Variant();
/*     */         
/* 297 */         variant.setDiskSize(diskObject.getSizePerDevice());
/* 298 */         variant.setDiskReadBandwidth(diskObject.getReadBandwidth());
/* 299 */         variant.setDiskWriteBandwidth(diskObject.getWriteBandwidth());
/* 300 */         this.storageVariantList.add(variant);
/*     */       }
/*     */       
/*     */ 
/*     */ 
/* 305 */       if (this.checkBoxWorse.isSelected())
/*     */       {
/*     */ 
/* 308 */         worse = Utils.getWorseDisk();
/* 309 */         componentNamesList.remove(worse);
/* 310 */         Disk diskObject = Utils.loadDiskObject(worse);
/*     */         
/*     */ 
/* 313 */         Variant variant = new Variant();
/*     */         
/* 315 */         variant.setDiskSize(diskObject.getSizePerDevice());
/* 316 */         variant.setDiskReadBandwidth(diskObject.getReadBandwidth());
/* 317 */         variant.setDiskWriteBandwidth(diskObject.getWriteBandwidth());
/* 318 */         this.storageVariantList.add(variant);
/*     */       }
/*     */       
/*     */ 
/* 322 */       while ((currentElement < restComponents) && (componentNamesList.size() > 0))
/*     */       {
/*     */ 
/* 325 */         Disk diskObject = Utils.loadDiskObject((String)componentNamesList.get(0));
/*     */         
/*     */ 
/* 328 */         Variant variant = new Variant();
/*     */         
/* 330 */         variant.setDiskSize(diskObject.getSizePerDevice());
/* 331 */         variant.setDiskReadBandwidth(diskObject.getReadBandwidth());
/* 332 */         variant.setDiskWriteBandwidth(diskObject.getWriteBandwidth());
/* 333 */         this.storageVariantList.add(variant);
/*     */         
/*     */ 
/* 336 */         currentElement++;
/*     */         
/*     */ 
/* 339 */         componentNamesList.remove(0);
/*     */       }
/*     */       
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 350 */       componentNamesList = Utils.getComponentListFromDir(Configuration.REPOSITORY_MEMORIES_FOLDER);
/* 351 */       better = worse = "";
/* 352 */       currentElement = 0;
/*     */       
/*     */ 
/* 355 */       if (this.checkBoxBetter.isSelected())
/*     */       {
/*     */ 
/* 358 */         better = Utils.getBetterMemory();
/* 359 */         componentNamesList.remove(better);
/* 360 */         Memory memoryObject = Utils.loadMemoryObject(better);
/*     */         
/*     */ 
/* 363 */         Variant variant = new Variant();
/*     */         
/* 365 */         variant.setMemorySize(memoryObject.getMemorysize());
/* 366 */         variant.setMemoryReadLatency(memoryObject.getReadLatency());
/* 367 */         variant.setMemoryWriteLatency(memoryObject.getWriteLatency());
/* 368 */         this.memoryVariantList.add(variant);
/*     */       }
/*     */       
/*     */ 
/*     */ 
/* 373 */       if (this.checkBoxWorse.isSelected())
/*     */       {
/*     */ 
/* 376 */         worse = Utils.getWorseMemory();
/* 377 */         componentNamesList.remove(worse);
/* 378 */         Memory memoryObject = Utils.loadMemoryObject(worse);
/*     */         
/*     */ 
/* 381 */         Variant variant = new Variant();
/*     */         
/* 383 */         variant.setMemorySize(memoryObject.getMemorysize());
/* 384 */         variant.setMemoryReadLatency(memoryObject.getReadLatency());
/* 385 */         variant.setMemoryWriteLatency(memoryObject.getWriteLatency());
/* 386 */         this.memoryVariantList.add(variant);
/*     */       }
/*     */       
/*     */ 
/* 390 */       while ((currentElement < restComponents) && (componentNamesList.size() > 0))
/*     */       {
/*     */ 
/* 393 */         Memory memoryObject = Utils.loadMemoryObject((String)componentNamesList.get(0));
/*     */         
/*     */ 
/* 396 */         Variant variant = new Variant();
/*     */         
/* 398 */         variant.setMemorySize(memoryObject.getMemorysize());
/* 399 */         variant.setMemoryReadLatency(memoryObject.getReadLatency());
/* 400 */         variant.setMemoryWriteLatency(memoryObject.getWriteLatency());
/* 401 */         this.memoryVariantList.add(variant);
/*     */         
/*     */ 
/* 404 */         currentElement++;
/*     */         
/*     */ 
/* 407 */         componentNamesList.remove(0);
/*     */       }
/*     */       
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 417 */       better = worse = "";
/* 418 */       currentElement = 0;
/* 419 */       componentNamesList.clear();
/* 420 */       componentNamesList.add("10Mbps");
/* 421 */       componentNamesList.add("100Mbps");
/* 422 */       componentNamesList.add("1Gbps");
/* 423 */       componentNamesList.add("10Gbps");
/* 424 */       componentNamesList.add("40Gbps");
/* 425 */       componentNamesList.add("100Gbps");
/*     */       
/*     */ 
/*     */ 
/* 429 */       if (this.checkBoxBetter.isSelected())
/*     */       {
/* 431 */         better = (String)componentNamesList.get(componentNamesList.size() - 1);
/* 432 */         componentNamesList.remove(better);
/*     */         
/*     */ 
/* 435 */         Variant variant = new Variant();
/* 436 */         variant.setSwitchBandwidth(better);
/* 437 */         this.switchesVariantList.add(variant);
/*     */       }
/*     */       
/*     */ 
/* 441 */       if (this.checkBoxWorse.isSelected())
/*     */       {
/* 443 */         worse = (String)componentNamesList.get(0);
/* 444 */         componentNamesList.remove(worse);
/*     */         
/*     */ 
/* 447 */         Variant variant = new Variant();
/* 448 */         variant.setSwitchBandwidth(worse);
/* 449 */         this.switchesVariantList.add(variant);
/*     */       }
/*     */       
/*     */ 
/* 453 */       while ((currentElement < restComponents) && (componentNamesList.size() > 0))
/*     */       {
/*     */ 
/* 456 */         String bandwidth = (String)componentNamesList.get(0);
/*     */         
/*     */ 
/* 459 */         Variant variant = new Variant();
/* 460 */         variant.setSwitchBandwidth(bandwidth);
/* 461 */         this.switchesVariantList.add(variant);
/*     */         
/*     */ 
/* 464 */         currentElement++;
/*     */         
/*     */ 
/* 467 */         componentNamesList.remove(0);
/*     */       }
/*     */       
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 478 */       this.variantList = new LinkedList();
/*     */       
/*     */ 
/* 481 */       for (currentCPU = 0; currentCPU < this.cpuVariantList.size(); currentCPU++)
/*     */       {
/* 483 */         for (currentDisk = 0; currentDisk < this.storageVariantList.size(); currentDisk++)
/*     */         {
/* 485 */           for (currentMemory = 0; currentMemory < this.memoryVariantList.size(); currentMemory++)
/*     */           {
/* 487 */             for (currentSwitch = 0; currentSwitch < this.switchesVariantList.size(); currentSwitch++)
/*     */             {
/* 489 */               Variant variant = new Variant();
/*     */               
/*     */ 
/* 492 */               variant.setCpuEnergy(((Variant)this.cpuVariantList.get(currentCPU)).getCpuEnergy());
/* 493 */               variant.setCpuSpeed(((Variant)this.cpuVariantList.get(currentCPU)).getCpuSpeed());
/* 494 */               variant.setCpuMinSpeed(((Variant)this.cpuVariantList.get(currentCPU)).getCpuMinSpeed());
/* 495 */               variant.setCpuNumCores(((Variant)this.cpuVariantList.get(currentCPU)).getCpuNumCores());
/*     */               
/*     */ 
/* 498 */               variant.setDiskEnergy(((Variant)this.storageVariantList.get(currentDisk)).getDiskEnergy());
/* 499 */               variant.setDiskSize(((Variant)this.storageVariantList.get(currentDisk)).getDiskSize());
/* 500 */               variant.setDiskReadBandwidth(((Variant)this.storageVariantList.get(currentDisk)).getDiskReadBandwidth());
/* 501 */               variant.setDiskWriteBandwidth(((Variant)this.storageVariantList.get(currentDisk)).getDiskWriteBandwidth());
/*     */               
/*     */ 
/* 504 */               variant.setMemoryEnergy(((Variant)this.memoryVariantList.get(currentMemory)).getMemoryEnergy());
/* 505 */               variant.setMemorySize(((Variant)this.memoryVariantList.get(currentMemory)).getMemorySize());
/* 506 */               variant.setMemoryReadLatency(((Variant)this.memoryVariantList.get(currentMemory)).getMemoryReadLatency());
/* 507 */               variant.setMemoryWriteLatency(((Variant)this.memoryVariantList.get(currentMemory)).getMemoryWriteLatency());
/*     */               
/*     */ 
/* 510 */               variant.setSwitchBandwidth(((Variant)this.switchesVariantList.get(currentSwitch)).getSwitchBandwidth());
/*     */               
/*     */ 
/* 513 */               this.variantList.add(variant);
/*     */             }
/*     */           }
/*     */         }
/*     */       }
/*     */       
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 525 */       int response = javax.swing.JOptionPane.showConfirmDialog(null, "A total of " + Integer.toString(this.variantList.size()) + " variants will be generated.\n" + "Are you OK to procced?", "Generating variants...", 2, 3);
/*     */       
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 533 */       if (response == 0)
/*     */       {
/*     */ 
/* 536 */         writeVariantsToDisk();
/*     */       }
/*     */     }
/*     */     catch (Exception e)
/*     */     {
/* 541 */       Utils.showErrorMessage(e.getMessage(), "Error");
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private void writeVariantsToDisk()
/*     */   {
/* 564 */     Writer writer = null;
/*     */     
/*     */ 
/* 567 */     LinkedList<String> iniFile = new LinkedList();
/* 568 */     LinkedList<String> nedFile = new LinkedList();
/* 569 */     LinkedList<String> variantsIniFile = new LinkedList();
/* 570 */     LinkedList<String> variantsNedFile = new LinkedList();
/* 571 */     String iniFileName = this.textCloudPath.getText() + File.separatorChar + "omnetpp.ini";
/* 572 */     String nedFileName = this.textCloudPath.getText() + File.separatorChar + "scenario.ned";
/* 573 */     boolean vmSectionReached = false;
/* 574 */     String currentStringLine; String variantStringLine = currentStringLine = "";
/*     */     
/*     */ 
/*     */ 
/*     */ 
/* 579 */     for (int i = 0; i < this.variantList.size(); i++) {
/* 580 */       variantsIniFile.add("");
/* 581 */       variantsNedFile.add("");
/*     */     }
/*     */     BufferedReader br;
/*     */     StringBuilder sb;
/*     */     String line;
/*     */     try {
/* 587 */       br = new BufferedReader(new java.io.FileReader(iniFileName));
/* 588 */       sb = new StringBuilder();
/* 589 */       line = br.readLine();
/*     */       
/* 591 */       while (line != null) {
/* 592 */         iniFile.add(line);
/* 593 */         line = br.readLine();
/*     */       }
/*     */     }
/*     */     catch (Exception e) {
/* 597 */       System.out.println(e.getMessage());
/*     */     }
/*     */     
/*     */ 
/*     */     try
/*     */     {
/* 603 */       br = new BufferedReader(new java.io.FileReader(nedFileName));
/* 604 */       sb = new StringBuilder();
/* 605 */       line = br.readLine();
/*     */       
/* 607 */       while (line != null) {
/* 608 */         nedFile.add(line);
/* 609 */         line = br.readLine();
/*     */       }
/*     */     }
/*     */     catch (Exception e) {
/* 613 */       System.out.println(e.getMessage());
/*     */     }
/*     */     
/*     */ 
/*     */ 
/* 618 */     for (int currentLine = 0; currentLine < iniFile.size(); currentLine++)
/*     */     {
/*     */ 
/* 621 */       currentStringLine = (String)iniFile.get(currentLine);
/*     */       
/*     */ 
/* 624 */       for (int i = 0; i < variantsIniFile.size(); i++)
/*     */       {
/*     */ 
/* 627 */         if (currentStringLine.contains("Definition of Virtual Machines")) {
/* 628 */           vmSectionReached = true;
/*     */         }
/*     */         
/* 631 */         if (!vmSectionReached)
/*     */         {
/*     */ 
/* 634 */           if (currentStringLine.contains("cpuEnergyData")) {
/* 635 */             variantStringLine = currentStringLine.substring(0, currentStringLine.lastIndexOf("=") + 1) + " \"" + ((Variant)this.variantList.get(i)).getCpuEnergy() + "\"\n";
/*     */ 
/*     */           }
/* 638 */           else if (currentStringLine.contains(".speed")) {
/* 639 */             variantStringLine = currentStringLine.substring(0, currentStringLine.lastIndexOf("=") + 1) + " " + ((Variant)this.variantList.get(i)).getCpuSpeed() + "\n";
/*     */ 
/*     */           }
/* 642 */           else if (currentStringLine.contains(".min_speed")) {
/* 643 */             variantStringLine = currentStringLine.substring(0, currentStringLine.lastIndexOf("=") + 1) + " " + ((Variant)this.variantList.get(i)).getCpuMinSpeed() + "\n";
/*     */           }
/*     */           else {
/* 646 */             getClass(); if (currentStringLine.contains(".numCores")) {
/* 647 */               variantStringLine = currentStringLine.substring(0, currentStringLine.lastIndexOf("=") + 1) + " " + ((Variant)this.variantList.get(i)).getCpuNumCores() + "\n";
/*     */             }
/*     */             else {
/* 650 */               getClass(); if (currentStringLine.contains(".maxNumVM")) {
/* 651 */                 variantStringLine = currentStringLine.substring(0, currentStringLine.lastIndexOf("=") + 1) + " " + ((Variant)this.variantList.get(i)).getCpuNumCores() + "\n";
/*     */ 
/*     */ 
/*     */ 
/*     */               }
/* 656 */               else if (currentStringLine.contains("storageEnergyData")) {
/* 657 */                 variantStringLine = currentStringLine.substring(0, currentStringLine.lastIndexOf("=") + 1) + " \"" + ((Variant)this.variantList.get(i)).getDiskEnergy() + "\"\n";
/*     */ 
/*     */               }
/* 660 */               else if (currentStringLine.contains("storageDeviceSize_GB")) {
/* 661 */                 variantStringLine = currentStringLine.substring(0, currentStringLine.lastIndexOf("=") + 1) + " " + ((Variant)this.variantList.get(i)).getDiskSize() + "\n";
/*     */               }
/*     */               else {
/* 664 */                 getClass(); if (currentStringLine.contains("readBandwidth")) {
/* 665 */                   variantStringLine = currentStringLine.substring(0, currentStringLine.lastIndexOf("=") + 1) + " " + ((Variant)this.variantList.get(i)).getDiskReadBandwidth() + "\n";
/*     */                 }
/*     */                 else {
/* 668 */                   getClass(); if (currentStringLine.contains("writeBandwidth")) {
/* 669 */                     variantStringLine = currentStringLine.substring(0, currentStringLine.lastIndexOf("=") + 1) + " " + ((Variant)this.variantList.get(i)).getDiskWriteBandwidth() + "\n";
/*     */ 
/*     */ 
/*     */ 
/*     */                   }
/* 674 */                   else if (currentStringLine.contains("memoryEnergyData")) {
/* 675 */                     variantStringLine = currentStringLine.substring(0, currentStringLine.lastIndexOf("=") + 1) + " \"" + ((Variant)this.variantList.get(i)).getMemoryEnergy() + "\"\n";
/*     */ 
/*     */                   }
/* 678 */                   else if (currentStringLine.contains("memorySize_MB")) {
/* 679 */                     variantStringLine = currentStringLine.substring(0, currentStringLine.lastIndexOf("=") + 1) + " " + ((Variant)this.variantList.get(i)).getMemorySize() + "\n";
/*     */ 
/*     */                   }
/* 682 */                   else if (currentStringLine.contains("memory.size_KB")) {
/* 683 */                     variantStringLine = currentStringLine.substring(0, currentStringLine.lastIndexOf("=") + 1) + " " + ((Variant)this.variantList.get(i)).getMemorySize() * 1024 + "\n";
/*     */                   }
/*     */                   else {
/* 686 */                     getClass(); if (currentStringLine.contains("memory.readLatencyTime_s")) {
/* 687 */                       variantStringLine = currentStringLine.substring(0, currentStringLine.lastIndexOf("=") + 1) + " " + ((Variant)this.variantList.get(i)).getMemoryReadLatency() + "\n";
/*     */                     }
/*     */                     else {
/* 690 */                       getClass(); if (currentStringLine.contains("memory.writeLatencyTime_s")) {
/* 691 */                         variantStringLine = currentStringLine.substring(0, currentStringLine.lastIndexOf("=") + 1) + " " + ((Variant)this.variantList.get(i)).getMemoryWriteLatency() + "\n";
/*     */ 
/*     */ 
/*     */                       }
/* 695 */                       else if (currentStringLine.contains("tkenv-plugin-path")) {
/* 696 */                         variantStringLine = "tkenv-plugin-path = ../../../../etc/plugins\n";
/*     */ 
/*     */                       }
/* 699 */                       else if (currentStringLine.contains("ned-path")) {
/* 700 */                         variantStringLine = "ned-path = ../../../../inet/src\n";
/*     */                       }
/*     */                       else
/* 703 */                         variantStringLine = currentStringLine + "\n";
/*     */                     }
/*     */                   }
/* 706 */                 } } } } } else { variantStringLine = currentStringLine + "\n";
/*     */         }
/*     */         
/*     */ 
/* 710 */         variantsIniFile.set(i, (String)variantsIniFile.get(i) + variantStringLine);
/*     */       }
/*     */     }
/*     */     
/*     */ 
/*     */ 
/* 716 */     for (int currentLine = 0; currentLine < nedFile.size(); currentLine++)
/*     */     {
/*     */ 
/* 719 */       currentStringLine = (String)nedFile.get(currentLine);
/*     */       
/*     */ 
/* 722 */       for (int i = 0; i < variantsNedFile.size(); i++)
/*     */       {
/*     */ 
/* 725 */         if (currentStringLine.contains("numNodes_Storage =")) {
/* 726 */           variantStringLine = currentStringLine.substring(0, currentStringLine.lastIndexOf("=") + 1) + ((Variant)this.variantList.get(i)).getNumStorageServers() + ";\n";
/*     */         }
/*     */         
/* 729 */         if (currentStringLine.contains("// RackChannelBandwidth")) {
/* 730 */           variantStringLine = currentStringLine.substring(0, currentStringLine.lastIndexOf("=") + 1) + ((Variant)this.variantList.get(i)).getSwitchBandwidth() + ";\n";
/*     */ 
/*     */         }
/* 733 */         else if (currentStringLine.contains("package")) {
/* 734 */           variantStringLine = currentStringLine.substring(0, currentStringLine.lastIndexOf(";")) + ".variant_" + Integer.toString(i) + ";\n";
/*     */         }
/*     */         else {
/* 737 */           variantStringLine = currentStringLine + "\n";
/*     */         }
/*     */         
/* 740 */         variantsNedFile.set(i, (String)variantsNedFile.get(i) + variantStringLine);
/*     */       }
/*     */     }
/*     */     
/*     */ 
/*     */ 
/* 746 */     for (int i = 0; i < this.variantList.size(); i++)
/*     */     {
/*     */ 
/* 749 */       File dir = new File(this.textCloudPath.getText() + File.separatorChar + "variant_" + Integer.toString(i));
/*     */       
/* 751 */       if (!dir.exists()) {
/* 752 */         dir.mkdir();
/*     */       }
/*     */       
/* 755 */       generateRunScript(dir.getAbsolutePath());
/*     */       
/*     */       File file;
/*     */       
/*     */       try
/*     */       {
/* 761 */         file = new File(dir.getAbsolutePath() + File.separatorChar + "scenario.ned");
/* 762 */         file.createNewFile();
/*     */         
/*     */ 
/* 765 */         writer = new java.io.BufferedWriter(new java.io.OutputStreamWriter(new FileOutputStream(file), "utf-8"));
/* 766 */         writer.write((String)variantsNedFile.get(i));
/* 767 */         writer.flush();
/* 768 */         writer.close();
/*     */       }
/*     */       catch (Exception e) {
/* 771 */         System.out.println(e.getMessage());
/*     */       }
/*     */       
/*     */ 
/*     */ 
/*     */       try
/*     */       {
/* 778 */         file = new File(dir.getAbsolutePath() + File.separatorChar + "omnetpp.ini");
/* 779 */         file.createNewFile();
/*     */         
/*     */ 
/* 782 */         writer = new java.io.BufferedWriter(new java.io.OutputStreamWriter(new FileOutputStream(file), "utf-8"));
/* 783 */         writer.write((String)variantsIniFile.get(i));
/* 784 */         writer.flush();
/* 785 */         writer.close();
/*     */       }
/*     */       catch (Exception e) {
/* 788 */         System.out.println(e.getMessage());
/*     */       }
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private void generateRunScript(String variantFolder)
/*     */   {
/*     */     try
/*     */     {
/* 803 */       File runFile = new File(variantFolder + File.separatorChar + "run");
/*     */       
/* 805 */       if (runFile.exists()) {
/* 806 */         runFile.delete();
/*     */       }
/*     */       
/* 809 */       runFile.createNewFile();
/*     */       
/*     */ 
/* 812 */       runFile.setExecutable(true);
/*     */       
/*     */ 
/* 815 */       Writer writer = new java.io.BufferedWriter(new java.io.OutputStreamWriter(new FileOutputStream(runFile)));
/*     */       
/*     */ 
/* 818 */       String currentElement = " #!/bin/sh\n";
/* 819 */       currentElement = currentElement + ".." + File.separatorChar + ".." + File.separatorChar + ".." + File.separatorChar + "src" + File.separatorChar + "run_iCanCloud $*";
/*     */       
/*     */ 
/* 822 */       writer.write(currentElement);
/*     */       
/*     */ 
/* 825 */       writer.flush();
/* 826 */       writer.close();
/*     */     }
/*     */     catch (Exception e)
/*     */     {
/* 830 */       e.printStackTrace();
/* 831 */       System.out.println(e.getMessage());
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private void initComponents()
/*     */   {
/* 844 */     this.labelCloudPath = new JLabel();
/* 845 */     this.textCloudPath = new JTextField();
/* 846 */     this.buttonClear = new JLabel();
/* 847 */     this.buttonGenerateVariants = new JButton();
/* 848 */     this.buttonSelectCloud = new JButton();
/* 849 */     this.checkBoxBetter = new JCheckBox();
/* 850 */     this.checkBoxWorse = new JCheckBox();
/* 851 */     this.jLabel1 = new JLabel();
/* 852 */     this.textMaxComponents = new JTextField();
/*     */     
/* 854 */     ResourceMap resourceMap = ((ICanCloudGUIApp)org.jdesktop.application.Application.getInstance(ICanCloudGUIApp.class)).getContext().getResourceMap(MetamorphicPanel.class);
/* 855 */     setBackground(resourceMap.getColor("Form.background"));
/* 856 */     setName("Form");
/* 857 */     setPreferredSize(new java.awt.Dimension(500, 641));
/* 858 */     setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());
/*     */     
/* 860 */     this.labelCloudPath.setText(resourceMap.getString("labelCloudPath.text", new Object[0]));
/* 861 */     this.labelCloudPath.setName("labelCloudPath");
/* 862 */     add(this.labelCloudPath, new AbsoluteConstraints(10, 33, -1, -1));
/*     */     
/* 864 */     this.textCloudPath.setFont(resourceMap.getFont("textCloudPath.font"));
/* 865 */     this.textCloudPath.setText(resourceMap.getString("textCloudPath.text", new Object[0]));
/* 866 */     this.textCloudPath.setName("textCloudPath");
/* 867 */     add(this.textCloudPath, new AbsoluteConstraints(90, 27, 250, 28));
/*     */     
/* 869 */     this.buttonClear.setIcon(resourceMap.getIcon("buttonClear.icon"));
/* 870 */     this.buttonClear.setToolTipText(resourceMap.getString("buttonClear.toolTipText", new Object[0]));
/* 871 */     this.buttonClear.setName("buttonClear");
/* 872 */     this.buttonClear.addMouseListener(new java.awt.event.MouseAdapter() {
/*     */       public void mouseClicked(MouseEvent evt) {
/* 874 */         MetamorphicPanel.this.buttonClearMouseClicked(evt);
/*     */       }
/* 876 */     });
/* 877 */     add(this.buttonClear, new AbsoluteConstraints(740, 20, -1, -1));
/*     */     
/* 879 */     this.buttonGenerateVariants.setText(resourceMap.getString("buttonGenerateVariants.text", new Object[0]));
/* 880 */     this.buttonGenerateVariants.setName("buttonGenerateVariants");
/* 881 */     this.buttonGenerateVariants.addActionListener(new java.awt.event.ActionListener() {
/*     */       public void actionPerformed(ActionEvent evt) {
/* 883 */         MetamorphicPanel.this.buttonGenerateVariantsActionPerformed(evt);
/*     */       }
/* 885 */     });
/* 886 */     add(this.buttonGenerateVariants, new AbsoluteConstraints(280, 260, -1, -1));
/*     */     
/* 888 */     this.buttonSelectCloud.setText(resourceMap.getString("buttonSelectCloud.text", new Object[0]));
/* 889 */     this.buttonSelectCloud.setName("buttonSelectCloud");
/* 890 */     this.buttonSelectCloud.addMouseListener(new java.awt.event.MouseAdapter() {
/*     */       public void mouseClicked(MouseEvent evt) {
/* 892 */         MetamorphicPanel.this.buttonSelectCloudMouseClicked(evt);
/*     */       }
/* 894 */     });
/* 895 */     add(this.buttonSelectCloud, new AbsoluteConstraints(350, 30, -1, -1));
/*     */     
/* 897 */     this.checkBoxBetter.setText(resourceMap.getString("checkBoxBetter.text", new Object[0]));
/* 898 */     this.checkBoxBetter.setName("checkBoxBetter");
/* 899 */     add(this.checkBoxBetter, new AbsoluteConstraints(40, 90, -1, -1));
/*     */     
/* 901 */     this.checkBoxWorse.setText(resourceMap.getString("checkBoxWorse.text", new Object[0]));
/* 902 */     this.checkBoxWorse.setName("checkBoxWorse");
/* 903 */     add(this.checkBoxWorse, new AbsoluteConstraints(40, 130, -1, -1));
/*     */     
/* 905 */     this.jLabel1.setText(resourceMap.getString("jLabel1.text", new Object[0]));
/* 906 */     this.jLabel1.setName("jLabel1");
/* 907 */     add(this.jLabel1, new AbsoluteConstraints(60, 180, -1, -1));
/*     */     
/* 909 */     this.textMaxComponents.setText(resourceMap.getString("textMaxComponents.text", new Object[0]));
/* 910 */     this.textMaxComponents.setName("textMaxComponents");
/* 911 */     add(this.textMaxComponents, new AbsoluteConstraints(340, 175, 80, -1));
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private void buttonClearMouseClicked(MouseEvent evt)
/*     */   {
/* 931 */     int response = javax.swing.JOptionPane.showConfirmDialog(null, "Clear current configuration?", "Confirm to clear this form", 2, 3);
/*     */     
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 938 */     if (response == 0) {
/* 939 */       clear();
/*     */     }
/*     */   }
/*     */   
/*     */   private void buttonGenerateVariantsActionPerformed(ActionEvent evt)
/*     */   {
/* 945 */     if ((checkModelFiles()) && 
/* 946 */       (checkParameters())) {
/* 947 */       generateVariants();
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private void buttonSelectCloudMouseClicked(MouseEvent evt)
/*     */   {
/* 960 */     String path = "";
/* 961 */     boolean allOK = false;
/*     */     
/*     */ 
/* 964 */     Configuration configFile = new Configuration();
/*     */     
/*     */ 
/* 967 */     JFileChooser chooser = new JFileChooser();
/* 968 */     chooser.setDialogTitle("Choose the folder that contains the cloud model");
/* 969 */     chooser.setFileSelectionMode(1);
/* 970 */     chooser.setCurrentDirectory(new File(configFile.getProperty("iCanCloudHome") + File.separatorChar + "simulations"));
/*     */     
/*     */ 
/* 973 */     int result = chooser.showOpenDialog((java.awt.Frame)getTopLevelAncestor());
/*     */     
/*     */ 
/* 976 */     if (result == 0)
/*     */     {
/* 978 */       path = chooser.getSelectedFile().getAbsolutePath();
/* 979 */       this.textCloudPath.setText(path);
/*     */     }
/*     */   }
/*     */ }


/* Location:              /home/chverma/Descargas/iCanCloudGUI/iCanCloudGUI.jar!/icancloudgui/TabbedPanels/MetamorphicPanel.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */