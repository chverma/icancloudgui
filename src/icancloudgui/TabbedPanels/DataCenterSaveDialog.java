/*     */ package icancloudgui.TabbedPanels;
/*     */ 
/*     */ import icancloudgui.ICanCloudGUIApp;
/*     */ import icancloudgui.ICanCloudGUIView;
/*     */ import icancloudgui.Utils.Utils;
/*     */ import java.awt.Container;
/*     */ import java.awt.Frame;
/*     */ import java.awt.event.ActionEvent;
/*     */ import java.awt.event.ActionListener;
/*     */ import javax.swing.JButton;
/*     */ import javax.swing.JDialog;
/*     */ import javax.swing.JLabel;
/*     */ import javax.swing.JTextField;
/*     */ import org.jdesktop.application.ResourceMap;
/*     */ import org.netbeans.lib.awtextra.AbsoluteConstraints;
/*     */ 
/*     */ public class DataCenterSaveDialog extends JDialog
/*     */ {
/*     */   ICanCloudGUIView mainFrame;
/*     */   private JButton buttonSaveDataCenter;
/*     */   private JLabel labelDestination;
/*     */   private JLabel labelTitle;
/*     */   private JTextField textDataCenterName;
/*     */   
/*     */   public DataCenterSaveDialog(Frame parent, ICanCloudGUIView frame, String name)
/*     */   {
/*  27 */     super(parent, true);
/*  28 */     initComponents();
/*  29 */     this.textDataCenterName.setText(name);
/*  30 */     this.mainFrame = frame;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   public void clear()
/*     */   {
/*  38 */     this.textDataCenterName.setText("");
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private void initComponents()
/*     */   {
/*  51 */     this.buttonSaveDataCenter = new JButton();
/*  52 */     this.labelDestination = new JLabel();
/*  53 */     this.labelTitle = new JLabel();
/*  54 */     this.textDataCenterName = new JTextField();
/*     */     
/*  56 */     setDefaultCloseOperation(2);
/*  57 */     ResourceMap resourceMap = ((ICanCloudGUIApp)org.jdesktop.application.Application.getInstance(ICanCloudGUIApp.class)).getContext().getResourceMap(DataCenterSaveDialog.class);
/*  58 */     setBackground(resourceMap.getColor("Form.background"));
/*  59 */     setName("Form");
/*  60 */     setPreferredSize(new java.awt.Dimension(540, 260));
/*  61 */     setResizable(false);
/*  62 */     getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());
/*     */     
/*  64 */     this.buttonSaveDataCenter.setForeground(resourceMap.getColor("buttonSaveDataCenter.foreground"));
/*  65 */     this.buttonSaveDataCenter.setText(resourceMap.getString("buttonSaveDataCenter.text", new Object[0]));
/*  66 */     this.buttonSaveDataCenter.setToolTipText(resourceMap.getString("buttonSaveDataCenter.toolTipText", new Object[0]));
/*  67 */     this.buttonSaveDataCenter.setName("buttonSaveDataCenter");
/*  68 */     this.buttonSaveDataCenter.addActionListener(new ActionListener() {
/*     */       public void actionPerformed(ActionEvent evt) {
/*  70 */         DataCenterSaveDialog.this.buttonSaveDataCenterActionPerformed(evt);
/*     */       }
/*  72 */     });
/*  73 */     getContentPane().add(this.buttonSaveDataCenter, new AbsoluteConstraints(200, 200, -1, -1));
/*     */     
/*  75 */     this.labelDestination.setText(resourceMap.getString("labelDestination.text", new Object[0]));
/*  76 */     this.labelDestination.setName("labelDestination");
/*  77 */     getContentPane().add(this.labelDestination, new AbsoluteConstraints(40, 140, -1, -1));
/*     */     
/*  79 */     this.labelTitle.setFont(resourceMap.getFont("labelTitle.font"));
/*  80 */     this.labelTitle.setText(resourceMap.getString("labelTitle.text", new Object[0]));
/*  81 */     this.labelTitle.setName("labelTitle");
/*  82 */     getContentPane().add(this.labelTitle, new AbsoluteConstraints(130, 30, -1, -1));
/*     */     
/*  84 */     this.textDataCenterName.setText(resourceMap.getString("textDataCenterName.text", new Object[0]));
/*  85 */     this.textDataCenterName.setName("textDataCenterName");
/*  86 */     getContentPane().add(this.textDataCenterName, new AbsoluteConstraints(190, 135, 310, -1));
/*     */     
/*  88 */     pack();
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private void buttonSaveDataCenterActionPerformed(ActionEvent evt)
/*     */   {
/* 100 */     if (!Utils.checkName(this.textDataCenterName.getText()))
/*     */     {
/* 102 */       Utils.showErrorMessage("Invalid name for this data-center.\nA name must start with a letter and can contain alphanumeric characters, '-' and '_'", "Wrong name!");
/*     */ 
/*     */ 
/*     */     }
/*     */     else
/*     */     {
/*     */ 
/*     */ 
/* 110 */       this.mainFrame.saveDataCenter(this.textDataCenterName.getText());
/*     */       
/*     */ 
/* 113 */       setVisible(false);
/*     */     }
/*     */   }
/*     */ }


/* Location:              /home/chverma/Descargas/iCanCloudGUI/iCanCloudGUI.jar!/icancloudgui/TabbedPanels/DataCenterSaveDialog.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */