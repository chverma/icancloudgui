/*     */ package icancloudgui.TabbedPanels;
/*     */ 
/*     */ import icancloudgui.ICanCloudGUIApp;
/*     */ import java.awt.Container;
/*     */ import java.awt.event.ActionEvent;
/*     */ import javax.swing.JButton;
/*     */ import javax.swing.JComboBox;
/*     */ import javax.swing.JLabel;
/*     */ import org.jdesktop.application.ResourceMap;
/*     */ import org.netbeans.lib.awtextra.AbsoluteConstraints;
/*     */ 
/*     */ public class ConnectionRemoveDialog extends javax.swing.JDialog
/*     */ {
/*     */   DataCenterPanel dataCenterPanel;
/*     */   private JButton buttonRemoveConnection;
/*     */   private JComboBox comboDestination;
/*     */   private JLabel labelDestination;
/*     */   private JLabel labelSource;
/*     */   private JLabel labelTitle;
/*     */   
/*     */   public ConnectionRemoveDialog(java.awt.Frame parent, DataCenterPanel main)
/*     */   {
/*  23 */     super(parent, true);
/*  24 */     initComponents();
/*  25 */     this.dataCenterPanel = main;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void initPanelToRemoveConnection(String sourceName, java.util.LinkedList<String> list)
/*     */   {
/*  38 */     this.labelSource.setText("Source element: " + sourceName);
/*     */     
/*     */ 
/*  41 */     this.comboDestination.removeAllItems();
/*     */     
/*     */ 
/*  44 */     icancloudgui.Utils.Utils.loadCombo(list, this.comboDestination);
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private void initComponents()
/*     */   {
/*  57 */     this.comboDestination = new JComboBox();
/*  58 */     this.buttonRemoveConnection = new JButton();
/*  59 */     this.labelDestination = new JLabel();
/*  60 */     this.labelTitle = new JLabel();
/*  61 */     this.labelSource = new JLabel();
/*     */     
/*  63 */     setDefaultCloseOperation(2);
/*  64 */     ResourceMap resourceMap = ((ICanCloudGUIApp)org.jdesktop.application.Application.getInstance(ICanCloudGUIApp.class)).getContext().getResourceMap(ConnectionRemoveDialog.class);
/*  65 */     setBackground(resourceMap.getColor("Form.background"));
/*  66 */     setName("Form");
/*  67 */     getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());
/*     */     
/*  69 */     this.comboDestination.setName("comboDestination");
/*  70 */     getContentPane().add(this.comboDestination, new AbsoluteConstraints(200, 150, 300, -1));
/*     */     
/*  72 */     this.buttonRemoveConnection.setForeground(resourceMap.getColor("buttonRemoveConnection.foreground"));
/*  73 */     this.buttonRemoveConnection.setText(resourceMap.getString("buttonRemoveConnection.text", new Object[0]));
/*  74 */     this.buttonRemoveConnection.setToolTipText(resourceMap.getString("buttonRemoveConnection.toolTipText", new Object[0]));
/*  75 */     this.buttonRemoveConnection.setName("buttonRemoveConnection");
/*  76 */     this.buttonRemoveConnection.addActionListener(new java.awt.event.ActionListener() {
/*     */       public void actionPerformed(ActionEvent evt) {
/*  78 */         ConnectionRemoveDialog.this.buttonRemoveConnectionActionPerformed(evt);
/*     */       }
/*  80 */     });
/*  81 */     getContentPane().add(this.buttonRemoveConnection, new AbsoluteConstraints(200, 210, -1, -1));
/*     */     
/*  83 */     this.labelDestination.setFont(resourceMap.getFont("labelNetwork.font"));
/*  84 */     this.labelDestination.setText(resourceMap.getString("labelDestination.text", new Object[0]));
/*  85 */     this.labelDestination.setName("labelDestination");
/*  86 */     getContentPane().add(this.labelDestination, new AbsoluteConstraints(40, 150, -1, -1));
/*     */     
/*  88 */     this.labelTitle.setFont(resourceMap.getFont("labelTitle.font"));
/*  89 */     this.labelTitle.setText(resourceMap.getString("labelTitle.text", new Object[0]));
/*  90 */     this.labelTitle.setName("labelTitle");
/*  91 */     getContentPane().add(this.labelTitle, new AbsoluteConstraints(110, 30, -1, -1));
/*     */     
/*  93 */     this.labelSource.setText(resourceMap.getString("labelSource.text", new Object[0]));
/*  94 */     this.labelSource.setName("labelSource");
/*  95 */     getContentPane().add(this.labelSource, new AbsoluteConstraints(40, 110, -1, -1));
/*     */     
/*  97 */     pack();
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private void buttonRemoveConnectionActionPerformed(ActionEvent evt)
/*     */   {
/* 108 */     this.dataCenterPanel.removeConnection(this.comboDestination.getSelectedItem().toString());
/*     */     
/*     */ 
/* 111 */     setVisible(false);
/*     */   }
/*     */ }


/* Location:              /home/chverma/Descargas/iCanCloudGUI/iCanCloudGUI.jar!/icancloudgui/TabbedPanels/ConnectionRemoveDialog.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */