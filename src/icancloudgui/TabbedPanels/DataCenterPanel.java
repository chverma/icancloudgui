/*     */ package icancloudgui.TabbedPanels;
/*     */ 
/*     */ import icancloudgui.ICanCloudGUIApp;
/*     */ import icancloudgui.ICanCloudGUIView;
/*     */ import icancloudgui.TabbedPanels.dataClasses.Connection;
/*     */ import icancloudgui.TabbedPanels.dataClasses.DataCenter;
/*     */ import icancloudgui.TabbedPanels.dataClasses.DataCenterUnit;
/*     */ import icancloudgui.Utils.Configuration;
/*     */ import icancloudgui.Utils.Utils;
/*     */ import java.awt.Color;
/*     */ import java.awt.Component;
/*     */ import java.awt.Dimension;
/*     */ import java.awt.Frame;
/*     */ import java.awt.Graphics;
/*     */ import java.awt.Graphics2D;
/*     */ import java.awt.Point;
/*     */ import java.awt.Rectangle;
/*     */ import java.awt.RenderingHints;
/*     */ import java.awt.event.ActionEvent;
/*     */ import java.awt.event.ActionListener;
/*     */ import java.awt.event.MouseEvent;
/*     */ import java.io.File;
/*     */ import java.io.FileOutputStream;
/*     */ import java.io.ObjectOutputStream;
/*     */ import java.io.PrintStream;
/*     */ import java.util.Iterator;
/*     */ import java.util.LinkedList;
/*     */ import javax.swing.JFrame;
/*     */ import javax.swing.JLabel;
/*     */ import javax.swing.JMenuItem;
/*     */ import javax.swing.JOptionPane;
/*     */ import javax.swing.JPanel;
/*     */ import javax.swing.JPopupMenu;
/*     */ import javax.swing.JPopupMenu.Separator;
/*     */ import org.jdesktop.application.Application;
/*     */ import org.jdesktop.application.ApplicationContext;
/*     */ import org.jdesktop.application.ResourceMap;
/*     */ 
/*     */ public class DataCenterPanel extends JPanel implements java.awt.event.MouseListener, java.awt.event.MouseMotionListener
/*     */ {
/*     */   ICanCloudGUIView mainFrame;
/*     */   private int mouseOldX;
/*     */   private int mouseOldY;
/*     */   DataCenter dataCenter;
/*     */   ConnectionAddDialog connectionAddDialog;
/*     */   ConnectionRemoveDialog connectionRemoveDialog;
/*     */   JLabel currentLabel;
/*     */   private JPopupMenu dataCenterMenu;
/*     */   private JMenuItem popAddConnection;
/*     */   private JMenuItem popRemoveConnection;
/*     */   private JMenuItem popRemoveElement;
/*     */   private JPopupMenu.Separator popSeparator;
/*     */   
/*     */   public DataCenterPanel(ICanCloudGUIView mainFrame)
/*     */   {
/*  56 */     initComponents();
/*     */     
/*     */ 
/*  59 */     this.mainFrame = mainFrame;
/*     */     
/*     */ 
/*  62 */     createNewDataCenter();
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void addElementToDataCenter(String type, String name)
/*     */   {
/*  76 */     DataCenterUnit unit = this.dataCenter.addElement(type, name, getInsets(), this);
/*     */     
/*     */ 
/*  79 */     add(unit.getLabel());
/*     */     
/*     */ 
/*  82 */     validate();
/*  83 */     repaint();
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */   public void createNewDataCenter()
/*     */   {
/*  90 */     this.dataCenter = new DataCenter();
/*     */     
/*     */ 
/*  93 */     this.connectionAddDialog = new ConnectionAddDialog((Frame)getTopLevelAncestor(), this);
/*  94 */     this.connectionAddDialog.setPreferredSize(new Dimension(560, 420));
/*  95 */     this.connectionAddDialog.setMinimumSize(new Dimension(560, 420));
/*  96 */     this.connectionAddDialog.setMaximumSize(new Dimension(560, 420));
/*  97 */     this.connectionAddDialog.setResizable(false);
/*  98 */     this.connectionAddDialog.setVisible(false);
/*     */     
/*     */ 
/* 101 */     this.connectionRemoveDialog = new ConnectionRemoveDialog((Frame)getTopLevelAncestor(), this);
/* 102 */     this.connectionRemoveDialog.setPreferredSize(new Dimension(560, 300));
/* 103 */     this.connectionRemoveDialog.setMinimumSize(new Dimension(560, 300));
/* 104 */     this.connectionRemoveDialog.setMaximumSize(new Dimension(560, 300));
/* 105 */     this.connectionRemoveDialog.setResizable(false);
/* 106 */     this.connectionRemoveDialog.setVisible(false);
/*     */     
/*     */ 
/* 109 */     this.currentLabel = null;
/*     */     
/* 111 */     removeAll();
/*     */     
/* 113 */     updateConnections();
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void paintComponent(Graphics g)
/*     */   {
/* 130 */     super.paintComponent(g);
/* 131 */     Graphics2D g2d = (Graphics2D)g;
/* 132 */     g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
/* 133 */     g2d.setColor(Color.black);
/*     */     
/*     */ 
/* 136 */     Iterator<Connection> iterator = this.dataCenter.getConnectionListIterator();
/*     */     
/*     */ 
/* 139 */     while (iterator.hasNext())
/*     */     {
/*     */ 
/* 142 */       Connection currentConnection = (Connection)iterator.next();
/*     */       
/*     */ 
/* 145 */       int x1 = this.dataCenter.getElementByCompleteName(currentConnection.getSource()).getX() + this.dataCenter.getElementByCompleteName(currentConnection.getSource()).getWidth() / 2;
/* 146 */       int y1 = this.dataCenter.getElementByCompleteName(currentConnection.getSource()).getY() + this.dataCenter.getElementByCompleteName(currentConnection.getSource()).getHeight() / 2;
/* 147 */       int x2 = this.dataCenter.getElementByCompleteName(currentConnection.getDestination()).getX() + this.dataCenter.getElementByCompleteName(currentConnection.getDestination()).getWidth() / 2;
/* 148 */       int y2 = this.dataCenter.getElementByCompleteName(currentConnection.getDestination()).getY() + this.dataCenter.getElementByCompleteName(currentConnection.getDestination()).getHeight() / 2;
/*     */       
/* 150 */       g2d.setStroke(new java.awt.BasicStroke(1.0F));
/* 151 */       g2d.drawLine(x1, y1, x2, y2);
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   public void updateConnections()
/*     */   {
/* 160 */     repaint();
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void addNewConnection(String destination, String network, double delay, double ber, double per)
/*     */   {
/* 178 */     if (this.dataCenter.existsCurrentConnection(this.currentLabel.getText(), destination))
/*     */     {
/* 180 */       JOptionPane.showMessageDialog(new JFrame(), "Current connection is currently set in this data center", "Error", 0);
/*     */ 
/*     */ 
/*     */     }
/*     */     else
/*     */     {
/*     */ 
/*     */ 
/* 188 */       Connection connection = new Connection();
/* 189 */       connection.setSource(this.currentLabel.getText());
/* 190 */       connection.setDestination(destination);
/* 191 */       connection.setCommunicationLink(network);
/* 192 */       connection.setDelay(delay);
/* 193 */       connection.setBer(ber);
/* 194 */       connection.setPer(per);
/*     */       
/* 196 */       this.dataCenter.addConnection(connection);
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void removeConnection(String destination)
/*     */   {
/* 206 */     this.dataCenter.removeConnection(this.currentLabel.getText(), destination);
/* 207 */     updateConnections();
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void writeDataCentertoFile(String dataCenterName)
/*     */   {
/* 225 */     String newDataCenterFile = Configuration.REPOSITORY_DATA_CENTER + File.separatorChar + dataCenterName;
/*     */     
/*     */ 
/* 228 */     if (!Utils.checkName(dataCenterName))
/*     */     {
/* 230 */       Utils.showErrorMessage("Invalid name for this data-center.\nA name must start with a letter and can contain alphanumeric characters, '-' and '_'", "Wrong name!");
/*     */ 
/*     */ 
/*     */     }
/*     */     else
/*     */     {
/*     */ 
/*     */       try
/*     */       {
/*     */ 
/*     */ 
/* 241 */         this.dataCenter.saveAllElementCoordinates();
/*     */         
/*     */ 
/* 244 */         if (new File(newDataCenterFile).exists())
/*     */         {
/*     */ 
/* 247 */           int response = JOptionPane.showConfirmDialog(null, "Overwrite existing Data Center?", "Confirm Overwrite", 2, 3);
/*     */           
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 254 */           if (response == 0)
/*     */           {
/*     */ 
/* 257 */             FileOutputStream fout = new FileOutputStream(newDataCenterFile);
/* 258 */             ObjectOutputStream oos = new ObjectOutputStream(fout);
/* 259 */             oos.writeObject(this.dataCenter);
/* 260 */             oos.close();
/*     */             
/*     */ 
/* 263 */             this.dataCenter.setName(dataCenterName);
/*     */           }
/*     */           
/*     */ 
/*     */         }
/*     */         else
/*     */         {
/*     */ 
/* 271 */           FileOutputStream fout = new FileOutputStream(newDataCenterFile);
/* 272 */           ObjectOutputStream oos = new ObjectOutputStream(fout);
/* 273 */           oos.writeObject(this.dataCenter);
/* 274 */           oos.close();
/*     */           
/*     */ 
/* 277 */           this.dataCenter.setName(dataCenterName);
/*     */         }
/*     */         
/*     */ 
/* 281 */         this.mainFrame.updateTree();
/*     */       }
/*     */       catch (Exception e) {
/* 284 */         e.printStackTrace();
/*     */       }
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void loadDataCenterInPanel(String dataCenterName)
/*     */   {
/* 298 */     this.dataCenter = Utils.loadDataCenterObject(dataCenterName);
/*     */     
/*     */ 
/* 301 */     if (this.dataCenter != null) {
/* 302 */       loadDataCenterObjectInPanel();
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private void loadDataCenterObjectInPanel()
/*     */   {
/* 313 */     this.dataCenter.loadAllElementCoordinates(getInsets(), this);
/*     */     
/*     */ 
/* 316 */     removeAll();
/*     */     
/*     */ 
/* 319 */     this.dataCenter.loadIcons(this);
/*     */     
/*     */ 
/* 322 */     validate();
/* 323 */     repaint();
/*     */     
/*     */ 
/* 326 */     updateConnections();
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getDataCenterName()
/*     */   {
/* 337 */     String name = "";
/*     */     
/* 339 */     if (this.dataCenter != null) {
/* 340 */       name = this.dataCenter.getName();
/*     */     }
/* 342 */     return name;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   public void clear()
/*     */   {
/* 350 */     removeAll();
/* 351 */     this.dataCenter = new DataCenter();
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private void initComponents()
/*     */   {
/* 363 */     this.dataCenterMenu = new JPopupMenu();
/* 364 */     this.popRemoveElement = new JMenuItem();
/* 365 */     this.popSeparator = new JPopupMenu.Separator();
/* 366 */     this.popAddConnection = new JMenuItem();
/* 367 */     this.popRemoveConnection = new JMenuItem();
/*     */     
/* 369 */     this.dataCenterMenu.setName("dataCenterMenu");
/*     */     
/* 371 */     ResourceMap resourceMap = ((ICanCloudGUIApp)Application.getInstance(ICanCloudGUIApp.class)).getContext().getResourceMap(DataCenterPanel.class);
/* 372 */     this.popRemoveElement.setText(resourceMap.getString("popRemoveElement.text", new Object[0]));
/* 373 */     this.popRemoveElement.setName("popRemoveElement");
/* 374 */     this.popRemoveElement.addActionListener(new ActionListener() {
/*     */       public void actionPerformed(ActionEvent evt) {
/* 376 */         DataCenterPanel.this.popRemoveElementActionPerformed(evt);
/*     */       }
/* 378 */     });
/* 379 */     this.dataCenterMenu.add(this.popRemoveElement);
/*     */     
/* 381 */     this.popSeparator.setName("popSeparator");
/* 382 */     this.dataCenterMenu.add(this.popSeparator);
/*     */     
/* 384 */     this.popAddConnection.setText(resourceMap.getString("popAddConnection.text", new Object[0]));
/* 385 */     this.popAddConnection.setName("popAddConnection");
/* 386 */     this.popAddConnection.addActionListener(new ActionListener() {
/*     */       public void actionPerformed(ActionEvent evt) {
/* 388 */         DataCenterPanel.this.popAddConnectionActionPerformed(evt);
/*     */       }
/* 390 */     });
/* 391 */     this.dataCenterMenu.add(this.popAddConnection);
/*     */     
/* 393 */     this.popRemoveConnection.setText(resourceMap.getString("popRemoveConnection.text", new Object[0]));
/* 394 */     this.popRemoveConnection.setName("popRemoveConnection");
/* 395 */     this.popRemoveConnection.addActionListener(new ActionListener() {
/*     */       public void actionPerformed(ActionEvent evt) {
/* 397 */         DataCenterPanel.this.popRemoveConnectionActionPerformed(evt);
/*     */       }
/* 399 */     });
/* 400 */     this.dataCenterMenu.add(this.popRemoveConnection);
/*     */     
/* 402 */     setName("Form");
/* 403 */     setLayout(null);
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private void popRemoveElementActionPerformed(ActionEvent evt)
/*     */   {
/* 419 */     String elementToRemove = this.currentLabel.getText();
/*     */     
/*     */ 
/* 422 */     int response = JOptionPane.showConfirmDialog(null, "Are you sure to remove " + elementToRemove + "?", "Confirm delete", 2, 3);
/*     */     
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 429 */     if (response == 0)
/*     */     {
/*     */ 
/* 432 */       remove(this.currentLabel);
/*     */       
/*     */ 
/* 435 */       this.dataCenter.removeElementByCompleteName(elementToRemove);
/*     */       
/*     */ 
/* 438 */       this.dataCenter.removeInvolvedConnection(elementToRemove);
/*     */       
/*     */ 
/* 441 */       updateConnections();
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private void popAddConnectionActionPerformed(ActionEvent evt)
/*     */   {
/* 457 */     String sourceName = this.currentLabel.getText();
/*     */     
/*     */ 
/* 460 */     LinkedList<DataCenterUnit> list = this.dataCenter.getPosibleDestinations(sourceName);
/*     */     
/*     */ 
/* 463 */     this.connectionAddDialog.loadComboDestination(list);
/*     */     
/*     */ 
/* 466 */     this.connectionAddDialog.clear();
/* 467 */     this.connectionAddDialog.setVisible(true);
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private void popRemoveConnectionActionPerformed(ActionEvent evt)
/*     */   {
/* 481 */     LinkedList<String> list = new LinkedList();
/*     */     
/*     */ 
/* 484 */     String sourceName = this.currentLabel.getText();
/*     */     
/*     */ 
/* 487 */     list = this.dataCenter.getConnectionsLinkedTo(sourceName);
/*     */     
/*     */ 
/* 490 */     if (list.size() == 0)
/*     */     {
/* 492 */       JOptionPane.showMessageDialog(new JFrame(), "There are no connections from this element", "Error", 0);
/*     */ 
/*     */ 
/*     */ 
/*     */     }
/*     */     else
/*     */     {
/*     */ 
/*     */ 
/* 501 */       this.connectionRemoveDialog.initPanelToRemoveConnection(sourceName, list);
/*     */       
/*     */ 
/* 504 */       this.connectionRemoveDialog.setVisible(true);
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void mouseClicked(MouseEvent arg0) {}
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void mousePressed(MouseEvent evt)
/*     */   {
/*     */     try
/*     */     {
/* 534 */       this.mouseOldX = evt.getX();
/* 535 */       this.mouseOldY = evt.getY();
/*     */       
/* 537 */       if (evt.getButton() == 3) {
/* 538 */         this.currentLabel = ((JLabel)evt.getSource());
/* 539 */         this.dataCenterMenu.show(evt.getComponent(), evt.getX(), evt.getY());
/* 540 */         this.dataCenterMenu.validate();
/*     */       }
/*     */     }
/*     */     catch (Exception e)
/*     */     {
/* 545 */       System.out.println(e.getMessage());
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void mouseReleased(MouseEvent arg0) {}
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void mouseEntered(MouseEvent arg0) {}
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void mouseExited(MouseEvent arg0) {}
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void mouseDragged(MouseEvent evt)
/*     */   {
/*     */     int y;
/*     */     
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 585 */     int x = y = 0;
/*     */     
/*     */     try
/*     */     {
/* 589 */       Point vPoint = evt.getPoint();
/* 590 */       int vDeltaX = (int)vPoint.getX() - this.mouseOldX;
/* 591 */       int vDeltaY = (int)vPoint.getY() - this.mouseOldY;
/* 592 */       Component vComponent = evt.getComponent();
/*     */       
/*     */ 
/* 595 */       if (vComponent.getX() + vDeltaX < 0) {
/* 596 */         x = 0;
/*     */       }
/* 598 */       else if (vComponent.getX() + vDeltaX + vComponent.getWidth() > getWidth()) {
/* 599 */         x = getWidth() - vComponent.getWidth();
/*     */       }
/*     */       else {
/* 602 */         x = vComponent.getX() + vDeltaX;
/*     */       }
/*     */       
/*     */ 
/* 606 */       if (vComponent.getY() + vDeltaY < 0) {
/* 607 */         y = 0;
/*     */       }
/* 609 */       else if (vComponent.getY() + vDeltaY + vComponent.getHeight() > getHeight()) {
/* 610 */         y = getHeight() - vComponent.getHeight();
/*     */       }
/*     */       else {
/* 613 */         y = vComponent.getY() + vDeltaY;
/*     */       }
/*     */       
/* 616 */       Rectangle vBounds = vComponent.getBounds();
/* 617 */       vBounds.setLocation(x, y);
/* 618 */       vComponent.setBounds(vBounds);
/* 619 */       updateConnections();
/*     */     }
/*     */     catch (Exception e)
/*     */     {
/* 623 */       System.out.println(e.getMessage());
/*     */     }
/*     */   }
/*     */   
/*     */   public void mouseMoved(MouseEvent arg0) {}
/*     */ }


/* Location:              /home/chverma/Descargas/iCanCloudGUI/iCanCloudGUI.jar!/icancloudgui/TabbedPanels/DataCenterPanel.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */