/*     */ package icancloudgui.Parser.data;
/*     */ 
/*     */ import java.util.ArrayList;
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ public class TimeStampBlock
/*     */ {
/*     */   private double timeStamp;
/*     */   private ArrayList<NodeData> nodes;
/*     */   private DataCenterData dataCenter;
/*     */   
/*     */   public TimeStampBlock(double timeStamp, int numNodes)
/*     */   {
/*  25 */     this.timeStamp = timeStamp;
/*  26 */     this.nodes = new ArrayList(numNodes);
/*     */   }
/*     */   
/*     */   public void addNode(NodeData node) {
/*  30 */     this.nodes.add(node);
/*     */   }
/*     */   
/*     */   public NodeData getNode(int index) {
/*  34 */     return (NodeData)this.nodes.get(index);
/*     */   }
/*     */   
/*     */   public int getNodesSize() {
/*  38 */     return this.nodes.size();
/*     */   }
/*     */   
/*     */   public void setDataCenter(DataCenterData newDataCenter) {
/*  42 */     this.dataCenter = newDataCenter;
/*     */   }
/*     */   
/*     */   public DataCenterData getDataCenter() {
/*  46 */     return this.dataCenter;
/*     */   }
/*     */   
/*     */   public double getTimeStamp() {
/*  50 */     return this.timeStamp;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public int getMaxConsumptionNodeIndex()
/*     */   {
/*  59 */     double max = Double.MIN_VALUE;
/*  60 */     int index = 0;
/*     */     
/*     */ 
/*  63 */     for (int i = 0; i < this.nodes.size(); i++) {
/*  64 */       if (((NodeData)this.nodes.get(i)).getEnergyJ() > max) {
/*  65 */         max = ((NodeData)this.nodes.get(i)).getEnergyJ();
/*  66 */         index = i;
/*     */       }
/*     */     }
/*     */     
/*  70 */     return index;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public int getMinConsumptionNodeIndex()
/*     */   {
/*  80 */     double min = Double.MAX_VALUE;
/*  81 */     int index = 0;
/*     */     
/*     */ 
/*  84 */     for (int i = 0; i < this.nodes.size(); i++) {
/*  85 */       if (((NodeData)this.nodes.get(i)).getEnergyJ() < min) {
/*  86 */         min = ((NodeData)this.nodes.get(i)).getEnergyJ();
/*  87 */         index = i;
/*     */       }
/*     */     }
/*     */     
/*  91 */     return index;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public int getAverageConsumptionNodeIndex()
/*     */   {
/* 100 */     double average = Math.abs((getMinConsumptionNodeIndex() + getMaxConsumptionNodeIndex()) / 2);
/*     */     
/* 102 */     double distance = Double.MAX_VALUE;
/* 103 */     int index = 0;
/*     */     
/*     */ 
/* 106 */     for (int i = 0; i < this.nodes.size(); i++) {
/* 107 */       if (Math.abs(((NodeData)this.nodes.get(i)).getEnergyJ() - average) < distance) {
/* 108 */         distance = Math.abs(((NodeData)this.nodes.get(i)).getEnergyJ() - average);
/* 109 */         index = i;
/*     */       }
/*     */     }
/*     */     
/* 113 */     return index;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */   public String toString()
/*     */   {
/* 120 */     String data = "TimeStamp:" + this.timeStamp + "\n\n";
/*     */     
/* 122 */     data = data + "Nodes information:\n\n";
/*     */     
/* 124 */     for (int i = 0; i < this.nodes.size(); i++) {
/* 125 */       data = data + ((NodeData)this.nodes.get(i)).toString();
/*     */     }
/*     */     
/* 128 */     data = data + "Data center information:\n" + this.dataCenter.toString();
/*     */     
/* 130 */     return data;
/*     */   }
/*     */ }


/* Location:              /home/chverma/Descargas/iCanCloudGUI/iCanCloudGUI.jar!/icancloudgui/Parser/data/TimeStampBlock.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */