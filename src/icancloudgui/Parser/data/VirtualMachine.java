/*    */ package icancloudgui.Parser.data;
/*    */ 
/*    */ import java.util.ArrayList;
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ public class VirtualMachine
/*    */ {
/*    */   private String name;
/*    */   private ArrayList<TimeInterval> timeIntervals;
/*    */   
/*    */   public VirtualMachine(int numIntervals)
/*    */   {
/* 22 */     this.name = "";
/* 23 */     this.timeIntervals = new ArrayList(numIntervals);
/*    */   }
/*    */   
/*    */   public void setInterval(TimeInterval ti, int index) {
/* 27 */     this.timeIntervals.set(index, ti);
/*    */   }
/*    */   
/*    */   public String getName() {
/* 31 */     return this.name;
/*    */   }
/*    */   
/*    */   public void setName(String name) {
/* 35 */     this.name = name;
/*    */   }
/*    */   
/*    */ 
/*    */ 
/*    */   public String toString()
/*    */   {
/* 42 */     String data = "VM Name:" + this.name;
/*    */     
/* 44 */     for (int i = 0; i < this.timeIntervals.size(); i++) {
/* 45 */       data = data + ((TimeInterval)this.timeIntervals.get(i)).toString();
/*    */     }
/* 47 */     data = data + "\n";
/*    */     
/* 49 */     return data;
/*    */   }
/*    */ }


/* Location:              /home/chverma/Descargas/iCanCloudGUI/iCanCloudGUI.jar!/icancloudgui/Parser/data/VirtualMachine.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */