/*    */ package icancloudgui.Parser.data;
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ public class TimeInterval
/*    */ {
/*    */   private double start;
/*    */   
/*    */ 
/*    */ 
/*    */   private double end;
/*    */   
/*    */ 
/*    */ 
/*    */ 
/*    */   public TimeInterval(double start, double end)
/*    */   {
/* 19 */     this.start = start;
/* 20 */     this.end = end;
/*    */   }
/*    */   
/*    */   public double getInterval() {
/* 24 */     return this.end - this.start;
/*    */   }
/*    */   
/*    */   public double getEnd() {
/* 28 */     return this.end;
/*    */   }
/*    */   
/*    */   public void setEnd(double end) {
/* 32 */     this.end = end;
/*    */   }
/*    */   
/*    */   public double getStart() {
/* 36 */     return this.start;
/*    */   }
/*    */   
/*    */   public void setStart(double start) {
/* 40 */     this.start = start;
/*    */   }
/*    */   
/*    */ 
/*    */ 
/*    */ 
/*    */   public String toString()
/*    */   {
/* 48 */     String data = "Start: " + this.start + "End:" + this.end + "\n";
/*    */     
/* 50 */     return data;
/*    */   }
/*    */ }


/* Location:              /home/chverma/Descargas/iCanCloudGUI/iCanCloudGUI.jar!/icancloudgui/Parser/data/TimeInterval.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */