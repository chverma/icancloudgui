/*    */ package icancloudgui.Parser.data;
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ public class DataCenterData
/*    */ {
/*    */   private double energyJ;
/*    */   
/*    */ 
/*    */ 
/*    */   private double powerW;
/*    */   
/*    */ 
/*    */ 
/*    */ 
/*    */   public DataCenterData()
/*    */   {
/* 20 */     this.energyJ = -1.0D;
/* 21 */     this.powerW = -1.0D;
/*    */   }
/*    */   
/*    */   public String toString()
/*    */   {
/*    */     String data;
/* 28 */     if (this.powerW > 0.0D) {
/* 29 */       data = "Power (W): " + this.powerW + "     Energy (J):" + this.energyJ + "\n";
/*    */     } else {
/* 31 */       data = "Energy (J): " + this.energyJ + "\n";
/*    */     }
/* 33 */     return data;
/*    */   }
/*    */   
/*    */   public double getEnergyJ() {
/* 37 */     return this.energyJ;
/*    */   }
/*    */   
/*    */   public void setEnergyJ(double energyJ) {
/* 41 */     this.energyJ = energyJ;
/*    */   }
/*    */   
/*    */   public double getPowerW() {
/* 45 */     return this.powerW;
/*    */   }
/*    */   
/*    */   public void setPowerW(double powerW) {
/* 49 */     this.powerW = powerW;
/*    */   }
/*    */ }


/* Location:              /home/chverma/Descargas/iCanCloudGUI/iCanCloudGUI.jar!/icancloudgui/Parser/data/DataCenterData.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */