/*     */ package icancloudgui.Parser.data;
/*     */ 
/*     */ 
/*     */ 
/*     */ public class NodeData
/*     */ {
/*     */   private String name;
/*     */   
/*     */ 
/*     */   private String state;
/*     */   
/*     */ 
/*     */   private double energyJ;
/*     */   
/*     */ 
/*     */   private double powerW;
/*     */   
/*     */ 
/*     */   private double cpuEnergyJ;
/*     */   
/*     */ 
/*     */   private double memEnergyJ;
/*     */   
/*     */   private double netEnergyJ;
/*     */   
/*     */   private double stoEnergyJ;
/*     */   
/*     */   private double psuEnergyJ;
/*     */   
/*     */   private double cpuPowerW;
/*     */   
/*     */   private double memPowerW;
/*     */   
/*     */   private double netPowerW;
/*     */   
/*     */   private double stoPowerW;
/*     */   
/*     */   private double psuPowerW;
/*     */   
/*     */ 
/*     */   public NodeData()
/*     */   {
/*  43 */     this.name = "";
/*  44 */     this.state = "";
/*  45 */     this.energyJ = -1.0D;
/*  46 */     this.powerW = -1.0D;
/*     */     
/*  48 */     this.cpuEnergyJ = -1.0D;
/*  49 */     this.memEnergyJ = -1.0D;
/*  50 */     this.netEnergyJ = -1.0D;
/*  51 */     this.stoEnergyJ = -1.0D;
/*  52 */     this.psuEnergyJ = -1.0D;
/*     */     
/*  54 */     this.cpuPowerW = -1.0D;
/*  55 */     this.memPowerW = -1.0D;
/*  56 */     this.netPowerW = -1.0D;
/*  57 */     this.stoPowerW = -1.0D;
/*  58 */     this.psuPowerW = -1.0D;
/*     */   }
/*     */   
/*     */ 
/*     */   private String getPair(String name, double value)
/*     */   {
/*     */     String data;
/*  66 */     if (value == -1.0D)
/*  67 */       data = ""; else {
/*  68 */       data = String.format("%s: %12f  ", new Object[] { name, Double.valueOf(value) });
/*     */     }
/*  70 */     return data;
/*     */   }
/*     */   
/*     */   private String nextLine(String name1, String name2)
/*     */   {
/*     */     String data;
/*  77 */     if ((name1.length() == 0) && (name2.length() == 0)) {
/*  78 */       data = "";
/*     */     } else {
/*  80 */       data = "\n";
/*     */     }
/*  82 */     return data;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */   public String toString()
/*     */   {
/*  89 */     String data = "Name: " + this.name + "     State:" + this.state + "\n";
/*  90 */     data = data + getPair("Power (W)    ", this.powerW) + getPair("Energy (J)    ", this.energyJ) + nextLine(getPair("Power (W)", this.powerW), getPair("Energy (J)", this.energyJ));
/*  91 */     data = data + getPair("CPU Power (W)", this.cpuPowerW) + getPair("CPU Energy (J)", this.cpuEnergyJ) + nextLine(getPair("CPU Power (W)", this.cpuPowerW), getPair("CPU Energy (J)", this.cpuEnergyJ));
/*  92 */     data = data + getPair("Mem Power (W)", this.memPowerW) + getPair("Mem Energy (J)", this.memEnergyJ) + nextLine(getPair("Mem Power (W)", this.memPowerW), getPair("Mem Energy (J)", this.memEnergyJ));
/*  93 */     data = data + getPair("Net Power (W)", this.netPowerW) + getPair("Net Energy (J)", this.netEnergyJ) + nextLine(getPair("Net Power (W)", this.netPowerW), getPair("Net Energy (J)", this.netEnergyJ));
/*  94 */     data = data + getPair("Sto Power (W)", this.stoPowerW) + getPair("Sto Energy (J)", this.stoEnergyJ) + nextLine(getPair("Sto Power (W)", this.stoPowerW), getPair("Sto Energy (J)", this.stoEnergyJ));
/*  95 */     data = data + getPair("PSU Power (W)", this.psuPowerW) + getPair("PSU Energy (J)", this.psuEnergyJ) + nextLine(getPair("PSU Power (W)", this.psuPowerW), getPair("PSU Energy (J)", this.psuEnergyJ));
/*  96 */     data = data + "\n";
/*  97 */     return data;
/*     */   }
/*     */   
/*     */   public String getName()
/*     */   {
/* 102 */     return this.name;
/*     */   }
/*     */   
/*     */   public void setName(String name) {
/* 106 */     this.name = name;
/*     */   }
/*     */   
/*     */   public String getState() {
/* 110 */     return this.state;
/*     */   }
/*     */   
/*     */   public void setState(String state) {
/* 114 */     this.state = state;
/*     */   }
/*     */   
/*     */   public double getEnergyJ() {
/* 118 */     return this.energyJ;
/*     */   }
/*     */   
/*     */   public void setEnergyJ(double energyJ) {
/* 122 */     this.energyJ = energyJ;
/*     */   }
/*     */   
/*     */   public double getPowerW() {
/* 126 */     return this.powerW;
/*     */   }
/*     */   
/*     */   public void setPowerW(double powerW) {
/* 130 */     this.powerW = powerW;
/*     */   }
/*     */   
/*     */   public double getCpuEnergyJ() {
/* 134 */     return this.cpuEnergyJ;
/*     */   }
/*     */   
/*     */   public void setCpuEnergyJ(double cpuEnergyJ) {
/* 138 */     this.cpuEnergyJ = cpuEnergyJ;
/*     */   }
/*     */   
/*     */   public double getMemEnergyJ() {
/* 142 */     return this.memEnergyJ;
/*     */   }
/*     */   
/*     */   public void setMemEnergyJ(double memEnergyJ) {
/* 146 */     this.memEnergyJ = memEnergyJ;
/*     */   }
/*     */   
/*     */   public double getNetEnergyJ() {
/* 150 */     return this.netEnergyJ;
/*     */   }
/*     */   
/*     */   public void setNetEnergyJ(double netEnergyJ) {
/* 154 */     this.netEnergyJ = netEnergyJ;
/*     */   }
/*     */   
/*     */   public double getStoEnergyJ() {
/* 158 */     return this.stoEnergyJ;
/*     */   }
/*     */   
/*     */   public void setStoEnergyJ(double stoEnergyJ) {
/* 162 */     this.stoEnergyJ = stoEnergyJ;
/*     */   }
/*     */   
/*     */   public double getPsuEnergyJ() {
/* 166 */     return this.psuEnergyJ;
/*     */   }
/*     */   
/*     */   public void setPsuEnergyJ(double psuEnergyJ) {
/* 170 */     this.psuEnergyJ = psuEnergyJ;
/*     */   }
/*     */   
/*     */   public double getCpuPowerW() {
/* 174 */     return this.cpuPowerW;
/*     */   }
/*     */   
/*     */   public void setCpuPowerW(double cpuPowerW) {
/* 178 */     this.cpuPowerW = cpuPowerW;
/*     */   }
/*     */   
/*     */   public double getMemPowerW() {
/* 182 */     return this.memPowerW;
/*     */   }
/*     */   
/*     */   public void setMemPowerW(double memPowerW) {
/* 186 */     this.memPowerW = memPowerW;
/*     */   }
/*     */   
/*     */   public double getNetPowerW() {
/* 190 */     return this.netPowerW;
/*     */   }
/*     */   
/*     */   public void setNetPowerW(double netPowerW) {
/* 194 */     this.netPowerW = netPowerW;
/*     */   }
/*     */   
/*     */   public double getStoPowerW() {
/* 198 */     return this.stoPowerW;
/*     */   }
/*     */   
/*     */   public void setStoPowerW(double stoPowerW) {
/* 202 */     this.stoPowerW = stoPowerW;
/*     */   }
/*     */   
/*     */   public double getPsuPowerW() {
/* 206 */     return this.psuPowerW;
/*     */   }
/*     */   
/*     */   public void setPsuPowerW(double psuPowerW) {
/* 210 */     this.psuPowerW = psuPowerW;
/*     */   }
/*     */ }


/* Location:              /home/chverma/Descargas/iCanCloudGUI/iCanCloudGUI.jar!/icancloudgui/Parser/data/NodeData.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */