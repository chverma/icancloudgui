/*    */ package icancloudgui.Parser.data;
/*    */ 
/*    */ import java.util.LinkedList;
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ public class User
/*    */ {
/*    */   private String name;
/*    */   private TimeInterval interval;
/*    */   private LinkedList<VirtualMachine> VMs;
/*    */   
/*    */   public User(TimeInterval interval)
/*    */   {
/* 21 */     this.name = "";
/* 22 */     this.interval = interval;
/* 23 */     this.VMs = new LinkedList();
/*    */   }
/*    */   
/*    */   public void addVM(VirtualMachine v)
/*    */   {
/* 28 */     this.VMs.add(v);
/*    */   }
/*    */   
/*    */   public LinkedList<VirtualMachine> getVMs() {
/* 32 */     return this.VMs;
/*    */   }
/*    */   
/*    */   public TimeInterval getInterval() {
/* 36 */     return this.interval;
/*    */   }
/*    */   
/*    */   public void setInterval(TimeInterval interval) {
/* 40 */     this.interval = interval;
/*    */   }
/*    */   
/*    */   public String getName() {
/* 44 */     return this.name;
/*    */   }
/*    */   
/*    */   public void setName(String name) {
/* 48 */     this.name = name;
/*    */   }
/*    */   
/*    */ 
/*    */ 
/*    */   public String toString()
/*    */   {
/* 55 */     String data = "User Name:" + this.name;
/* 56 */     data = data + "Time interval:" + this.interval.toString();
/*    */     
/* 58 */     for (int i = 0; i < this.VMs.size(); i++) {
/* 59 */       data = data + ((VirtualMachine)this.VMs.get(i)).toString();
/*    */     }
/* 61 */     data = data + "\n";
/*    */     
/* 63 */     return data;
/*    */   }
/*    */ }


/* Location:              /home/chverma/Descargas/iCanCloudGUI/iCanCloudGUI.jar!/icancloudgui/Parser/data/User.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */