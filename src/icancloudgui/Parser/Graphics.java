/*     */ package icancloudgui.Parser;
/*     */ 
/*     */ import icancloudgui.Parser.data.DataCenterData;
/*     */ import icancloudgui.Parser.data.NodeData;
/*     */ import icancloudgui.Parser.data.TimeInterval;
/*     */ import icancloudgui.Parser.data.TimeStampBlock;
/*     */ import icancloudgui.Parser.data.User;
/*     */ import java.io.BufferedWriter;
/*     */ import java.io.File;
/*     */ import java.io.FileWriter;
/*     */ import java.io.IOException;
/*     */ import java.io.PrintStream;
/*     */ import java.util.LinkedList;
/*     */ 
/*     */ 
/*     */ public class Graphics
/*     */ {
/*  18 */   final String NODES_FOLDER = "nodes";
/*     */   
/*     */ 
/*  21 */   final String DATA_FILE_SEPARATOR = "\t";
/*     */   
/*     */ 
/*  24 */   final String DATA_FILE_EXTENSION = ".dat";
/*     */   
/*     */ 
/*  27 */   final String SCRIPT_FILE_EXTENSION = ".gnu";
/*     */   
/*     */ 
/*  30 */   final String CONTINUOUS_SCRIPT = "scripts/continuous.gnu";
/*     */   
/*     */ 
/*  33 */   final String CONTINUOUS3D_SCRIPT = "scripts/continuous3D.gnu";
/*     */   
/*     */ 
/*  36 */   final String CONTINUOUS3D_AGGREGATED_SCRIPT = "scripts/continuous3DAggregated.gnu";
/*     */   
/*     */ 
/*  39 */   final String TOTAL_SCRIPT = "scripts/total.gnu";
/*     */   
/*     */ 
/*  42 */   final String USERS_SCRIPT = "scripts/usersTotal.gnu";
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void generateEnergyContinuousDataCenter(LinkedList<TimeStampBlock> energyList, String targetFolder)
/*     */   {
/*  60 */     String scriptFile = "";
/*  61 */     String dataFile = "";
/*  62 */     int i = 0;
/*     */     
/*     */ 
/*     */     try
/*     */     {
/*  67 */       ParseUtils utils = new ParseUtils();
/*     */       
/*     */ 
/*  70 */       String name = "dataCenter_cont";
/*     */       
/*     */ 
/*  73 */       scriptFile = targetFolder + File.separatorChar + name + ".gnu";
/*     */       
/*     */ 
/*  76 */       dataFile = targetFolder + File.separatorChar + name + ".dat";
/*     */       
/*     */ 
/*  79 */       utils.copyFile("scripts/continuous.gnu", scriptFile, name + ".dat", name);
/*     */       
/*     */ 
/*  82 */       FileWriter fw = new FileWriter(dataFile);
/*  83 */       BufferedWriter textWriter = new BufferedWriter(fw);
/*     */       
/*     */ 
/*  86 */       for (i = 0; i < energyList.size(); i++)
/*     */       {
/*     */ 
/*  89 */         textWriter.write(Double.toString(((TimeStampBlock)energyList.get(i)).getTimeStamp()));
/*     */         
/*     */ 
/*  92 */         textWriter.write("\t");
/*     */         
/*     */ 
/*  95 */         textWriter.write(Double.toString(((TimeStampBlock)energyList.get(i)).getDataCenter().getPowerW()));
/*     */         
/*     */ 
/*  98 */         textWriter.write("\t");
/*     */         
/*     */ 
/* 101 */         textWriter.write(Double.toString(((TimeStampBlock)energyList.get(i)).getDataCenter().getEnergyJ()));
/*     */         
/*     */ 
/* 104 */         textWriter.write("\n");
/*     */       }
/*     */       
/*     */ 
/* 108 */       textWriter.close();
/* 109 */       fw.close();
/*     */       
/*     */ 
/* 112 */       utils.addLineToLaunchScript(targetFolder, name + ".gnu");
/*     */     }
/*     */     catch (IOException ex) {
/* 115 */       System.out.println(ex.getMessage());
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void generateEnergyDataCenter3D(LinkedList<TimeStampBlock> energyList, String targetFolder)
/*     */   {
/* 134 */     String scriptFile = "";
/* 135 */     String dataFile = "";
/* 136 */     int i = 0;
/* 137 */     int j = 0;
/*     */     
/*     */ 
/*     */     try
/*     */     {
/* 142 */       ParseUtils utils = new ParseUtils();
/*     */       
/*     */ 
/* 145 */       String name = "dataCenter_cont3D";
/*     */       
/*     */ 
/* 148 */       scriptFile = targetFolder + File.separatorChar + name + ".gnu";
/*     */       
/*     */ 
/* 151 */       dataFile = targetFolder + File.separatorChar + name + ".dat";
/*     */       
/*     */ 
/* 154 */       utils.copyFile("scripts/continuous3D.gnu", scriptFile, name + ".dat", name);
/*     */       
/*     */ 
/* 157 */       FileWriter fw = new FileWriter(dataFile);
/* 158 */       BufferedWriter textWriter = new BufferedWriter(fw);
/*     */       
/*     */ 
/* 161 */       for (i = 0; i < energyList.size(); i++)
/*     */       {
/*     */ 
/* 164 */         for (j = 0; j < ((TimeStampBlock)energyList.get(i)).getNodesSize(); j++)
/*     */         {
/*     */ 
/* 167 */           textWriter.write(Double.toString(((TimeStampBlock)energyList.get(i)).getTimeStamp()));
/*     */           
/*     */ 
/* 170 */           textWriter.write("\t");
/*     */           
/*     */ 
/* 173 */           textWriter.write(Integer.toString(j));
/*     */           
/*     */ 
/* 176 */           textWriter.write("\t");
/*     */           
/*     */ 
/* 179 */           textWriter.write(Double.toString(((TimeStampBlock)energyList.get(i)).getNode(j).getPowerW()));
/*     */           
/*     */ 
/* 182 */           textWriter.write("\t");
/*     */           
/*     */ 
/* 185 */           textWriter.write(((TimeStampBlock)energyList.get(i)).getNode(j).getName());
/*     */           
/*     */ 
/* 188 */           textWriter.write("\n");
/*     */         }
/*     */       }
/*     */       
/*     */ 
/* 193 */       textWriter.close();
/* 194 */       fw.close();
/*     */       
/*     */ 
/* 197 */       utils.addLineToLaunchScript(targetFolder, name + ".gnu");
/*     */     }
/*     */     catch (IOException ex) {
/* 200 */       System.out.println(ex.getMessage());
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void generateEnergyDataCenter3DAggregated(LinkedList<TimeStampBlock> energyList, String targetFolder)
/*     */   {
/* 219 */     String scriptFile = "";
/* 220 */     String dataFile = "";
/* 221 */     int i = 0;
/* 222 */     int j = 0;
/*     */     
/*     */ 
/*     */     try
/*     */     {
/* 227 */       ParseUtils utils = new ParseUtils();
/*     */       
/*     */ 
/* 230 */       String name = "dataCenter_cont3DAggregated";
/*     */       
/*     */ 
/* 233 */       scriptFile = targetFolder + File.separatorChar + name + ".gnu";
/*     */       
/*     */ 
/* 236 */       dataFile = targetFolder + File.separatorChar + name + ".dat";
/*     */       
/*     */ 
/* 239 */       utils.copyFile("scripts/continuous3DAggregated.gnu", scriptFile, name + ".dat", name);
/*     */       
/*     */ 
/* 242 */       FileWriter fw = new FileWriter(dataFile);
/* 243 */       BufferedWriter textWriter = new BufferedWriter(fw);
/*     */       
/*     */ 
/* 246 */       for (i = 0; i < energyList.size(); i++)
/*     */       {
/*     */ 
/* 249 */         for (j = 0; j < ((TimeStampBlock)energyList.get(i)).getNodesSize(); j++)
/*     */         {
/*     */ 
/* 252 */           textWriter.write(Double.toString(((TimeStampBlock)energyList.get(i)).getTimeStamp()));
/*     */           
/*     */ 
/* 255 */           textWriter.write("\t");
/*     */           
/*     */ 
/* 258 */           textWriter.write(Integer.toString(j));
/*     */           
/*     */ 
/* 261 */           textWriter.write("\t");
/*     */           
/*     */ 
/* 264 */           textWriter.write(Double.toString(((TimeStampBlock)energyList.get(i)).getNode(j).getEnergyJ()));
/*     */           
/*     */ 
/* 267 */           textWriter.write("\t");
/*     */           
/*     */ 
/* 270 */           textWriter.write(((TimeStampBlock)energyList.get(i)).getNode(j).getName());
/*     */           
/*     */ 
/* 273 */           textWriter.write("\n");
/*     */         }
/*     */       }
/*     */       
/*     */ 
/* 278 */       textWriter.close();
/* 279 */       fw.close();
/*     */       
/*     */ 
/* 282 */       utils.addLineToLaunchScript(targetFolder, name + ".gnu");
/*     */     }
/*     */     catch (IOException ex) {
/* 285 */       System.out.println(ex.getMessage());
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void generateEnergyTotal(LinkedList<TimeStampBlock> energyList, String targetFolder)
/*     */   {
/* 305 */     String scriptFile = "";
/* 306 */     String dataFile = "";
/* 307 */     int i = 0;
/*     */     
/*     */ 
/*     */ 
/*     */     try
/*     */     {
/* 313 */       ParseUtils utils = new ParseUtils();
/*     */       
/*     */ 
/* 316 */       int indexTotal = energyList.size() - 1;
/*     */       
/*     */ 
/* 319 */       String name = "dataCenter_total";
/*     */       
/*     */ 
/* 322 */       scriptFile = targetFolder + File.separatorChar + name + ".gnu";
/*     */       
/*     */ 
/* 325 */       dataFile = targetFolder + File.separatorChar + name + ".dat";
/*     */       
/*     */ 
/* 328 */       utils.copyFile("scripts/total.gnu", scriptFile, name + ".dat", name);
/*     */       
/*     */ 
/* 331 */       FileWriter fw = new FileWriter(dataFile);
/* 332 */       BufferedWriter textWriter = new BufferedWriter(fw);
/*     */       
/*     */ 
/* 335 */       for (i = 0; i < ((TimeStampBlock)energyList.get(indexTotal)).getNodesSize(); i++)
/*     */       {
/*     */ 
/* 338 */         if (((TimeStampBlock)energyList.get(indexTotal)).getNode(i).getName().length() > 0)
/*     */         {
/*     */ 
/* 341 */           textWriter.write(((TimeStampBlock)energyList.get(indexTotal)).getNode(i).getName());
/* 342 */           textWriter.write("\t");
/*     */           
/*     */ 
/* 345 */           textWriter.write(Double.toString(((TimeStampBlock)energyList.get(indexTotal)).getNode(i).getEnergyJ()));
/* 346 */           textWriter.write("\t");
/*     */           
/*     */ 
/* 349 */           textWriter.write(Double.toString(((TimeStampBlock)energyList.get(indexTotal)).getDataCenter().getEnergyJ()));
/* 350 */           textWriter.write("\n");
/*     */         }
/*     */       }
/*     */       
/*     */ 
/* 355 */       textWriter.close();
/* 356 */       fw.close();
/*     */       
/*     */ 
/* 359 */       utils.addLineToLaunchScript(targetFolder, name + ".gnu");
/*     */     }
/*     */     catch (IOException ex) {
/* 362 */       System.out.println(ex.getMessage());
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void generateEnergyContinuousNode(LinkedList<TimeStampBlock> energyList, String targetFolder, int nodeIndex, String newChartPrefix)
/*     */   {
/* 385 */     String chartName = "";
/* 386 */     String nodeName = "";
/* 387 */     String scriptFile = "";
/* 388 */     String dataFile = "";
/*     */     
/* 390 */     int i = 0;
/*     */     
/*     */     try
/*     */     {
/* 394 */       if (((TimeStampBlock)energyList.get(0)).getNodesSize() > 0)
/*     */       {
/*     */ 
/* 397 */         ParseUtils utils = new ParseUtils();
/*     */         
/*     */ 
/* 400 */         File nodesFolder = new File(targetFolder + File.separatorChar + "nodes");
/*     */         
/* 402 */         if (!nodesFolder.exists()) {
/* 403 */           nodesFolder.mkdir();
/*     */         }
/*     */         
/*     */ 
/* 407 */         nodeName = ((TimeStampBlock)energyList.get(0)).getNode(nodeIndex).getName();
/*     */         
/*     */ 
/* 410 */         chartName = newChartPrefix + "_cont_" + nodeName;
/*     */         
/*     */ 
/* 413 */         scriptFile = targetFolder + File.separatorChar + "nodes" + File.separatorChar + chartName + ".gnu";
/*     */         
/*     */ 
/* 416 */         dataFile = targetFolder + File.separatorChar + "nodes" + File.separatorChar + chartName + ".dat";
/*     */         
/*     */ 
/* 419 */         utils.copyFile("scripts/continuous.gnu", scriptFile, "nodes" + File.separatorChar + chartName + ".dat", "nodes" + File.separatorChar + chartName);
/*     */         
/*     */ 
/* 422 */         FileWriter fw = new FileWriter(dataFile);
/* 423 */         BufferedWriter textWriter = new BufferedWriter(fw);
/*     */         
/*     */ 
/* 426 */         for (i = 0; i < energyList.size(); i++)
/*     */         {
/*     */ 
/* 429 */           if (!nodeName.equals(((TimeStampBlock)energyList.get(i)).getNode(nodeIndex).getName())) {
/* 430 */             System.out.println("Error generating chart for node: " + nodeName);
/* 431 */             System.out.println("Node in block [" + i + "], timeStamp = " + ((TimeStampBlock)energyList.get(i)).getTimeStamp() + " does not match the name!");
/* 432 */             System.exit(0);
/*     */           }
/*     */           
/*     */ 
/* 436 */           textWriter.write(Double.toString(((TimeStampBlock)energyList.get(i)).getTimeStamp()));
/*     */           
/*     */ 
/* 439 */           textWriter.write("\t");
/*     */           
/*     */ 
/* 442 */           textWriter.write(Double.toString(((TimeStampBlock)energyList.get(i)).getNode(nodeIndex).getPowerW()));
/*     */           
/*     */ 
/* 445 */           textWriter.write("\t");
/*     */           
/*     */ 
/* 448 */           textWriter.write(Double.toString(((TimeStampBlock)energyList.get(i)).getNode(nodeIndex).getEnergyJ()));
/*     */           
/*     */ 
/* 451 */           textWriter.write("\n");
/*     */         }
/*     */         
/*     */ 
/* 455 */         textWriter.close();
/* 456 */         fw.close();
/*     */         
/*     */ 
/* 459 */         utils.addLineToLaunchScript(targetFolder, "nodes" + File.separatorChar + chartName + ".gnu");
/*     */       }
/*     */     }
/*     */     catch (Exception ex) {
/* 463 */       System.out.println(ex.getMessage());
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void generateUsersTotal(LinkedList<User> userList, String fileName, String targetFolder)
/*     */   {
/* 479 */     String scriptFile = "";
/* 480 */     String dataFile = "";
/* 481 */     int i = 0;
/*     */     
/*     */ 
/*     */     try
/*     */     {
/* 486 */       ParseUtils utils = new ParseUtils();
/*     */       
/*     */ 
/* 489 */       String name = "users_total";
/*     */       
/*     */ 
/* 492 */       scriptFile = targetFolder + File.separatorChar + name + ".gnu";
/*     */       
/*     */ 
/* 495 */       dataFile = targetFolder + File.separatorChar + name + ".dat";
/*     */       
/*     */ 
/* 498 */       utils.copyFile("scripts/usersTotal.gnu", scriptFile, name + ".dat", name);
/*     */       
/*     */ 
/* 501 */       FileWriter fw = new FileWriter(dataFile);
/* 502 */       BufferedWriter textWriter = new BufferedWriter(fw);
/*     */       
/*     */ 
/* 505 */       for (i = 0; i < userList.size(); i++)
/*     */       {
/*     */ 
/* 508 */         textWriter.write(((User)userList.get(i)).getName());
/* 509 */         textWriter.write("\t");
/*     */         
/*     */ 
/* 512 */         textWriter.write(Double.toString(((User)userList.get(i)).getInterval().getInterval()));
/* 513 */         textWriter.write("\t");
/*     */       }
/*     */       
/*     */ 
/* 517 */       textWriter.close();
/* 518 */       fw.close();
/*     */       
/*     */ 
/* 521 */       utils.addLineToLaunchScript(targetFolder, name + ".gnu");
/*     */     }
/*     */     catch (IOException ex) {
/* 524 */       System.out.println(ex.getMessage());
/*     */     }
/*     */   }
/*     */ }


/* Location:              /home/chverma/Descargas/iCanCloudGUI/iCanCloudGUI.jar!/icancloudgui/Parser/Graphics.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */