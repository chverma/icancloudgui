### output for PFD
set terminal pdf
set output 'OUTPUT_FILE.pdf'

### Histogram
set boxwidth 0.7 relative
set style fill noborder transparent solid 0.7
set key outside center bottom maxrows 1 box lt 1 lc 0
set style data histograms

### Main title
unset label
set title "Total energy consumption"

### x-title and font
unset xtics
set xlabel "Nodes"

### y-title and font
set ytics nomirror
set y2tics nomirror
set ylabel "Energy consumed by each node (J)"
set y2label "Energy consumed by the data-center (J)"

### Ranges and scale
set autoscale x
set autoscale y

### Plot
plot 'DATA_FILE' using 2 with boxes title 'Nodes' axes x1y1, \
'DATA_FILE' using 3 with lines lt 1 lc 0 title 'Data-Center' axes x1y2