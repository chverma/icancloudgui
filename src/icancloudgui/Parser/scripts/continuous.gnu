### output for EPS
#set terminal postscript enhanced "Verdana" 22
#set output 'energy.eps'

### output for PFD
set terminal pdf
set output 'OUTPUT_FILE.pdf'

### Histogram
set boxwidth 0.7 relative
set style fill noborder transparent solid 0.7
set key outside center bottom maxrows 1 box lt 1 lc 0

### Main title
unset label
#set title "Energy and Power" font "Verdana, 24"
set title "Energy and Power"

### x-title and font
#set xtics font "Verdana,13"
#set xlabel "Time" font "Verdana, 22"
set xtics nomirror
set xlabel "Time"

### y-title and font
#set ytics font "Verdana,20"
#set ylabel "Power (W)" font "Verdana, 22"
#set y2label "Energy (J)" font "Verdana, 22"
set ytics nomirror
set y2tics nomirror
set ylabel "Power (W)"
set y2label "Energy (J)"


### Ranges and scale
set autoscale x
set autoscale y
set autoscale y2


### Plot
plot 'DATA_FILE' using 1:3 with boxes title 'Energy' axes x1y2, 'DATA_FILE' using 1:2 with lines lt 1 lc 0 title 'Power' axes x1y1 