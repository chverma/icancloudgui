### output for PFD
set terminal pdf
set output 'OUTPUT_FILE.pdf'

### Histogram
set boxwidth 0.7 relative
set style fill noborder transparent solid 0.7
unset key
set style data histograms

### Main title
unset label
set title "Execution time for each user"

### x-title and font
unset xtics
set xlabel "Users"

### y-title and font
set ytics nomirror
set ylabel "Time"

### Ranges and scale
set autoscale x
set autoscale y

### Plot
plot 'DATA_FILE' using 2 with boxes title 'Nodes' axes