### Set output
set terminal pdf
set output 'OUTPUT_FILE.pdf'

### Set View
set view 120, 30, 1, 1

### Key
unset key

### Tile
set title "Aggregated Energy (J) of each node"

### Ticks
set ticslevel 0
set ytics offset -1,-1,0
set hidden

### Labels
set xlabel "Time"
set ylabel "Nodes" 
set zlabel "Energy (J)" rotate left
unset clabel

### Set 3D
set contour surface
set dgrid3d
set pm3d at s hidden3d 100 
set style line 10 linecolor rgb "black" 
unset hidden3d 
unset surf 

### Plot
sp "DATA_FILE" using 1:2:3:ytic(4) with pm3d

