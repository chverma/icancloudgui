/*     */ package icancloudgui.Parser;
/*     */ 
/*     */ import icancloudgui.Parser.data.DataCenterData;
/*     */ import icancloudgui.Parser.data.NodeData;
/*     */ import icancloudgui.Parser.data.TimeInterval;
/*     */ import icancloudgui.Parser.data.TimeStampBlock;
/*     */ import icancloudgui.Parser.data.User;
/*     */ import icancloudgui.Parser.data.VirtualMachine;
/*     */ import icancloudgui.Utils.Utils;
/*     */ import java.io.BufferedReader;
/*     */ import java.io.BufferedWriter;
/*     */ import java.io.File;
/*     */ import java.io.FileNotFoundException;
/*     */ import java.io.FileReader;
/*     */ import java.io.FileWriter;
/*     */ import java.io.IOException;
/*     */ import java.io.PrintStream;
/*     */ import java.util.LinkedList;
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ public class LoadData
/*     */ {
/*  28 */   final boolean PRING_DEBUG_MESSAGES = false;
/*     */   
/*     */ 
/*  31 */   final String LOG_INSTANT = "@Logger-mode";
/*     */   
/*     */ 
/*  34 */   final String LOG_TOTAL = "@Total-mode";
/*     */   
/*     */ 
/*  37 */   final String ENERGY_NODE = "(nJ)";
/*     */   
/*     */ 
/*  40 */   final String POWER_NODE = "(nW)";
/*     */   
/*     */ 
/*  43 */   final String ENERGY_COMPONENT = "(cJ)";
/*     */   
/*     */ 
/*  46 */   final String POWER_COMPONENT = "(cW)";
/*     */   
/*     */ 
/*  49 */   final String TEST_FILE_EXTENSION = ".icp";
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   private LinkedList<TimeStampBlock> energyList;
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   private LinkedList<User> userList;
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public LoadData()
/*     */   {
/*  67 */     this.energyList = new LinkedList();
/*     */     
/*     */ 
/*  70 */     this.userList = new LinkedList();
/*     */   }
/*     */   
/*     */   public LinkedList<TimeStampBlock> getEnergyList()
/*     */   {
/*  75 */     return this.energyList;
/*     */   }
/*     */   
/*     */   public LinkedList<User> getUserList()
/*     */   {
/*  80 */     return this.userList;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void loadEnergyValues(String fileName)
/*     */   {
/*     */     int numNodes;
/*     */     
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 105 */     int lineNumber = numNodes = 0;
/* 106 */     String currentLine = "";
/*     */     
/*     */ 
/* 109 */     this.energyList.clear();
/*     */     
/*     */ 
/*     */ 
/* 113 */     System.out.println("Loading data...");
/*     */     
/*     */ 
/*     */     try
/*     */     {
/* 118 */       FileReader fr = new FileReader(fileName);
/* 119 */       BufferedReader textReader = new BufferedReader(fr);
/*     */       
/*     */ 
/* 122 */       lineNumber++;
/*     */       
/*     */ 
/* 125 */       currentLine = textReader.readLine();
/* 126 */       String[] splitLine = currentLine.split(";");
/*     */       
/*     */ 
/*     */ 
/* 130 */       if (splitLine.length != 2) {
/* 131 */         printErrorMessage(fileName, currentLine, "First line does not contain 2 parameters!", lineNumber);
/*     */       }
/*     */       else
/*     */       {
/* 135 */         getClass(); if (splitLine[0].equals("@Logger-mode"))
/*     */         {
/*     */ 
/* 138 */           numNodes = Integer.parseInt(splitLine[1]);
/*     */           
/*     */ 
/* 141 */           currentLine = textReader.readLine();
/* 142 */           lineNumber++;
/*     */           
/*     */ 
/* 145 */           while ((currentLine != null) && (currentLine.length() > 0))
/*     */           {
/*     */ 
/* 148 */             double timeStamp = Double.parseDouble(currentLine);
/*     */             
/*     */ 
/* 151 */             TimeStampBlock currentTimeStamp = new TimeStampBlock(timeStamp, numNodes);
/*     */             
/*     */ 
/* 154 */             for (int i = 0; i < numNodes; i++)
/*     */             {
/*     */ 
/* 157 */               NodeData currentNode = new NodeData();
/*     */               
/*     */ 
/* 160 */               currentLine = textReader.readLine();
/* 161 */               splitLine = currentLine.split(";");
/* 162 */               lineNumber++;
/* 163 */               int currentElement = 0;
/*     */               
/*     */ 
/* 166 */               currentNode.setName(splitLine[currentElement]);
/* 167 */               currentElement++;
/*     */               
/*     */ 
/* 170 */               currentNode.setState(splitLine[currentElement]);
/* 171 */               currentElement++;
/*     */               
/*     */ 
/*     */ 
/* 175 */               while (currentElement < splitLine.length)
/*     */               {
/*     */ 
/*     */ 
/*     */ 
/* 180 */                 getClass(); if (splitLine[currentElement].startsWith("(nW)")) {
/* 181 */                   getClass();currentNode.setPowerW(Double.parseDouble(splitLine[currentElement].substring("(nW)".length())));
/* 182 */                   currentElement++;
/*     */                 }
/*     */                 else
/*     */                 {
/* 186 */                   getClass(); if (splitLine[currentElement].startsWith("(nJ)")) {
/* 187 */                     getClass();currentNode.setEnergyJ(Double.parseDouble(splitLine[currentElement].substring("(nJ)".length())));
/* 188 */                     currentElement++;
/*     */                   }
/*     */                   else
/*     */                   {
/* 192 */                     getClass(); if (splitLine[currentElement].startsWith("(cW)"))
/*     */                     {
/*     */ 
/* 195 */                       getClass();currentNode.setCpuPowerW(Double.parseDouble(splitLine[currentElement].substring("(cW)".length())));
/* 196 */                       currentElement++;
/*     */                       
/*     */ 
/* 199 */                       currentNode.setMemPowerW(Double.parseDouble(splitLine[currentElement]));
/* 200 */                       currentElement++;
/*     */                       
/*     */ 
/* 203 */                       currentNode.setNetPowerW(Double.parseDouble(splitLine[currentElement]));
/* 204 */                       currentElement++;
/*     */                       
/*     */ 
/* 207 */                       currentNode.setStoPowerW(Double.parseDouble(splitLine[currentElement]));
/* 208 */                       currentElement++;
/*     */                       
/*     */ 
/* 211 */                       currentNode.setPsuPowerW(Double.parseDouble(splitLine[currentElement]));
/* 212 */                       currentElement++;
/*     */                     }
/*     */                     else
/*     */                     {
/* 216 */                       getClass(); if (splitLine[currentElement].startsWith("(cJ)"))
/*     */                       {
/*     */ 
/* 219 */                         getClass();currentNode.setCpuEnergyJ(Double.parseDouble(splitLine[currentElement].substring("(cJ)".length())));
/* 220 */                         currentElement++;
/*     */                         
/*     */ 
/* 223 */                         currentNode.setMemEnergyJ(Double.parseDouble(splitLine[currentElement]));
/* 224 */                         currentElement++;
/*     */                         
/*     */ 
/* 227 */                         currentNode.setNetEnergyJ(Double.parseDouble(splitLine[currentElement]));
/* 228 */                         currentElement++;
/*     */                         
/*     */ 
/* 231 */                         currentNode.setStoEnergyJ(Double.parseDouble(splitLine[currentElement]));
/* 232 */                         currentElement++;
/*     */                         
/*     */ 
/* 235 */                         currentNode.setPsuEnergyJ(Double.parseDouble(splitLine[currentElement]));
/* 236 */                         currentElement++;
/*     */                       }
/*     */                       else
/*     */                       {
/* 240 */                         printErrorMessage(fileName, currentLine, "Wrong parameter format!", lineNumber);
/*     */                       }
/*     */                     }
/*     */                   }
/*     */                 } }
/* 245 */               currentTimeStamp.addNode(currentNode);
/*     */             }
/*     */             
/*     */ 
/*     */ 
/*     */ 
/* 251 */             splitLine = textReader.readLine().split(";");
/* 252 */             lineNumber++;
/*     */             
/*     */ 
/* 255 */             if ((splitLine[0].startsWith("#")) && (splitLine.length == 2))
/*     */             {
/*     */ 
/* 258 */               DataCenterData currentDataCenter = new DataCenterData();
/*     */               
/*     */ 
/* 261 */               currentDataCenter.setPowerW(Double.parseDouble(splitLine[0].substring("#".length())));
/* 262 */               currentDataCenter.setEnergyJ(Double.parseDouble(splitLine[1]));
/*     */               
/*     */ 
/* 265 */               currentTimeStamp.setDataCenter(currentDataCenter);
/*     */ 
/*     */             }
/*     */             else
/*     */             {
/*     */ 
/* 271 */               printErrorMessage(fileName, currentLine, "Wrong parameter format!", lineNumber);
/*     */             }
/*     */             
/*     */ 
/* 275 */             this.energyList.add(currentTimeStamp);
/*     */             
/*     */ 
/* 278 */             currentLine = textReader.readLine();
/* 279 */             lineNumber++;
/*     */           }
/*     */         }
/*     */         
/*     */ 
/* 284 */         getClass(); if (splitLine[0].equals("@Total-mode"))
/*     */         {
/*     */ 
/* 287 */           numNodes = Integer.parseInt(splitLine[1]);
/*     */           
/*     */ 
/* 290 */           TimeStampBlock currentTimeStamp = new TimeStampBlock(-1.0D, numNodes);
/*     */           
/*     */ 
/* 293 */           for (int i = 0; i < numNodes; i++)
/*     */           {
/*     */ 
/* 296 */             int currentElement = 0;
/*     */             
/*     */ 
/* 299 */             NodeData currentNode = new NodeData();
/*     */             
/*     */ 
/* 302 */             currentLine = textReader.readLine();
/* 303 */             splitLine = currentLine.split(";");
/* 304 */             lineNumber++;
/*     */             
/*     */ 
/* 307 */             currentNode.setName(splitLine[currentElement]);
/* 308 */             currentElement++;
/*     */             
/*     */ 
/* 311 */             currentNode.setEnergyJ(Double.parseDouble(splitLine[currentElement]));
/*     */             
/*     */ 
/* 314 */             currentTimeStamp.addNode(currentNode);
/*     */           }
/*     */           
/*     */ 
/* 318 */           currentLine = textReader.readLine();
/*     */           
/*     */ 
/* 321 */           DataCenterData currentDataCenter = new DataCenterData();
/*     */           
/*     */ 
/* 324 */           currentDataCenter.setEnergyJ(Double.parseDouble(currentLine.substring("#".length())));
/*     */           
/*     */ 
/* 327 */           currentTimeStamp.setDataCenter(currentDataCenter);
/*     */           
/*     */ 
/* 330 */           this.energyList.add(currentTimeStamp);
/*     */ 
/*     */         }
/*     */         else
/*     */         {
/* 335 */           printErrorMessage(fileName, currentLine, "Wrong First line!", lineNumber);
/*     */         }
/*     */       }
/*     */       
/* 339 */       textReader.close();
/* 340 */       fr.close();
/*     */       
/*     */ 
/* 343 */       System.out.println("Data loaded successfully!");
/*     */     }
/*     */     catch (FileNotFoundException ex) {
/* 346 */       printErrorMessage(fileName, currentLine, ex.getMessage(), lineNumber);
/*     */     } catch (IOException ex) {
/* 348 */       printErrorMessage(fileName, currentLine, ex.getMessage(), lineNumber);
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */   public void showEnergyValues()
/*     */   {
/* 355 */     for (int i = 0; i < this.energyList.size(); i++)
/*     */     {
/* 357 */       System.out.println(((TimeStampBlock)this.energyList.get(i)).toString());
/* 358 */       System.out.println("---------------------------------------------------------");
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void writeEnergyValues(String fileName, String folder)
/*     */   {
/* 368 */     String newFile = "";
/* 369 */     int i = 0;
/*     */     
/*     */ 
/*     */     try
/*     */     {
/* 374 */       newFile = folder + File.separatorChar + fileName.substring(0, fileName.lastIndexOf('.')) + ".icp";
/*     */       
/*     */ 
/* 377 */       FileWriter fw = new FileWriter(newFile);
/* 378 */       BufferedWriter textWriter = new BufferedWriter(fw);
/*     */       
/*     */ 
/* 381 */       textWriter.write("This file contains parsed data from file " + fileName + " (checking purposes only!)\n\n");
/*     */       
/*     */ 
/* 384 */       for (i = 0; i < this.energyList.size(); i++) {
/* 385 */         textWriter.write(((TimeStampBlock)this.energyList.get(i)).toString());
/* 386 */         textWriter.write("---------------------------------------------------------\n\n");
/*     */       }
/*     */       
/*     */ 
/* 390 */       textWriter.close();
/* 391 */       fw.close();
/*     */     }
/*     */     catch (IOException ex) {
/* 394 */       printErrorMessage(fileName, "", ex.getMessage(), i);
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void loadUserValues(String fileName)
/*     */   {
/*     */     int i;
/*     */     
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 415 */     int lineNumber = i = 0;
/* 416 */     String currentLine = "";
/*     */     
/* 418 */     this.userList.clear();
/*     */     
/*     */ 
/*     */     try
/*     */     {
/* 423 */       FileReader fr = new FileReader(fileName);
/* 424 */       BufferedReader textReader = new BufferedReader(fr);
/*     */       
/*     */ 
/* 427 */       lineNumber++;
/*     */       
/*     */ 
/* 430 */       currentLine = textReader.readLine();
/* 431 */       lineNumber++;
/*     */       
/*     */ 
/* 434 */       while (currentLine != null)
/*     */       {
/*     */ 
/* 437 */         String[] splitLine = currentLine.split(";");
/*     */         
/*     */ 
/* 440 */         if (splitLine.length != 3) {
/* 441 */           printErrorMessage(fileName, currentLine, "First line does not contain 3 parameters!", lineNumber);
/*     */ 
/*     */ 
/*     */         }
/*     */         else
/*     */         {
/*     */ 
/* 448 */           User currentUser = new User(new TimeInterval(Double.parseDouble(splitLine[1]), Double.parseDouble(splitLine[2])));
/* 449 */           currentUser.setName(splitLine[0]);
/*     */           
/*     */ 
/* 452 */           currentLine = textReader.readLine();
/* 453 */           lineNumber++;
/*     */           
/*     */ 
/* 456 */           while (currentLine.startsWith("#"))
/*     */           {
/*     */ 
/* 459 */             splitLine = currentLine.split(";");
/*     */             
/*     */ 
/* 462 */             if ((splitLine.length < 3) || (splitLine.length % 2 != 1)) {
/* 463 */               printErrorMessage(fileName, currentLine, "Wrong number of intervals in VM!", lineNumber);
/*     */ 
/*     */ 
/*     */             }
/*     */             else
/*     */             {
/*     */ 
/* 470 */               VirtualMachine currentVM = new VirtualMachine(splitLine.length / 2);
/* 471 */               currentVM.setName(splitLine[0]);
/* 472 */               i = 1;
/*     */               
/*     */ 
/* 475 */               while (i < splitLine.length)
/*     */               {
/*     */ 
/* 478 */                 currentVM.setInterval(new TimeInterval(Double.parseDouble(splitLine[i]), Double.parseDouble(splitLine[(i + 1)])), i / 2);
/*     */                 
/*     */ 
/* 481 */                 i += 2;
/*     */               }
/*     */               
/*     */ 
/* 485 */               currentUser.addVM(currentVM);
/*     */             }
/*     */             
/*     */ 
/* 489 */             currentLine = textReader.readLine();
/* 490 */             lineNumber++;
/*     */           }
/*     */           
/*     */ 
/* 494 */           this.userList.add(currentUser);
/*     */         }
/*     */       }
/*     */       
/*     */ 
/* 499 */       textReader.close();
/* 500 */       fr.close();
/*     */     }
/*     */     catch (FileNotFoundException ex) {
/* 503 */       printErrorMessage(fileName, currentLine, ex.getMessage(), lineNumber);
/*     */     } catch (IOException ex) {
/* 505 */       printErrorMessage(fileName, currentLine, ex.getMessage(), lineNumber);
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   public void showUsers()
/*     */   {
/* 514 */     for (int i = 0; i < this.energyList.size(); i++)
/*     */     {
/*     */ 
/* 517 */       System.out.println(((User)this.userList.get(i)).toString());
/* 518 */       System.out.println("---------------------------------------------------------");
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   public void writeUsers(String fileName, String folder)
/*     */   {
/* 527 */     String newFile = "";
/* 528 */     int i = 0;
/*     */     
/*     */ 
/*     */     try
/*     */     {
/* 533 */       newFile = folder + File.separatorChar + fileName.substring(0, fileName.lastIndexOf('.')) + ".icp";
/*     */       
/*     */ 
/* 536 */       FileWriter fw = new FileWriter(newFile);
/* 537 */       BufferedWriter textWriter = new BufferedWriter(fw);
/*     */       
/*     */ 
/* 540 */       textWriter.write("This file contains parsed data from file " + fileName + " (checking purposes only!)\n\n");
/*     */       
/*     */ 
/* 543 */       for (i = 0; i < this.userList.size(); i++) {
/* 544 */         textWriter.write(((User)this.userList.get(i)).toString());
/* 545 */         textWriter.write("---------------------------------------------------------\n\n");
/*     */       }
/*     */       
/*     */ 
/* 549 */       textWriter.close();
/* 550 */       fw.close();
/*     */     }
/*     */     catch (IOException ex) {
/* 553 */       printErrorMessage(fileName, "", ex.getMessage(), i);
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   void printErrorMessage(String fileName, String currentLine, String message, int lineNumber)
/*     */   {
/* 562 */     Utils.showErrorMessage("Error processing file: " + fileName + "\n" + "line [" + Integer.toString(lineNumber) + "] -> " + currentLine + "\n" + "Error: " + message + "\n", "Error while parsing data!!!");
/*     */   }
/*     */   
/*     */   void debugCurrentLine(String fileName, String currentLine, int line) {}
/*     */ }


/* Location:              /home/chverma/Descargas/iCanCloudGUI/iCanCloudGUI.jar!/icancloudgui/Parser/LoadData.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */