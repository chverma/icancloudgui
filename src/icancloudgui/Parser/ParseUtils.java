/*     */ package icancloudgui.Parser;
/*     */ 
/*     */ import java.io.BufferedReader;
/*     */ import java.io.BufferedWriter;
/*     */ import java.io.File;
/*     */ import java.io.FileWriter;
/*     */ import java.io.IOException;
/*     */ import java.io.InputStreamReader;
/*     */ import java.io.PrintStream;
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ public class ParseUtils
/*     */ {
/*  17 */   final String DATA_FILE = "DATA_FILE";
/*     */   
/*     */ 
/*  20 */   final String OUTPUT_FILE = "OUTPUT_FILE";
/*     */   
/*     */ 
/*  23 */   final String TARGET_FOLDER = "charts";
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   static final String LAUNCH_SCRIPT_FILE = "generateCharts.sh";
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void copyFile(String in, String out, String dataFile, String outputName)
/*     */     throws IOException
/*     */   {
/*     */     try
/*     */     {
/*  50 */       InputStreamReader isr = new InputStreamReader(getClass().getResourceAsStream(in));
/*  51 */       BufferedReader br = new BufferedReader(isr);
/*     */       
/*     */ 
/*  54 */       FileWriter fw = new FileWriter(out);
/*  55 */       BufferedWriter textWriter = new BufferedWriter(fw);
/*     */       
/*     */ 
/*  58 */       String line = br.readLine();
/*  59 */       String data = "";
/*     */       
/*     */ 
/*  62 */       while (line != null) {
/*  63 */         data = data + line + "\n";
/*  64 */         line = br.readLine();
/*     */       }
/*     */       
/*     */ 
/*  68 */       data = data.replaceAll("DATA_FILE", dataFile);
/*     */       
/*     */ 
/*  71 */       data = data.replaceAll("OUTPUT_FILE", outputName);
/*     */       
/*     */ 
/*  74 */       textWriter.write(data);
/*     */       
/*     */ 
/*  77 */       textWriter.close();
/*  78 */       fw.close();
/*  79 */       br.close();
/*  80 */       isr.close();
/*     */     }
/*     */     catch (IOException e) {
/*  83 */       throw e;
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void generateLaunchScript(String targetFolder)
/*     */   {
/*     */     try
/*     */     {
/*  99 */       File launchFile = new File(targetFolder + File.separatorChar + "generateCharts.sh");
/*     */       
/*     */ 
/* 102 */       if (launchFile.exists()) {
/* 103 */         launchFile.delete();
/*     */       }
/*     */       
/* 106 */       launchFile.setExecutable(true, false);
/* 107 */       launchFile.createNewFile();
/*     */       
/*     */ 
/* 110 */       Runtime.getRuntime().exec("chmod +x " + launchFile);
/*     */     }
/*     */     catch (IOException ex) {
/* 113 */       System.out.println(ex.getMessage());
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void addLineToLaunchScript(String folder, String scriptName)
/*     */   {
/*     */     try
/*     */     {
/* 134 */       FileWriter fw = new FileWriter(folder + File.separatorChar + "generateCharts.sh", true);
/* 135 */       BufferedWriter textWriter = new BufferedWriter(fw);
/*     */       
/*     */ 
/* 138 */       textWriter.write("gnuplot " + scriptName + "\n");
/*     */       
/*     */ 
/* 141 */       textWriter.close();
/* 142 */       fw.close();
/*     */     }
/*     */     catch (IOException ex) {
/* 145 */       System.out.println(ex.getMessage());
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void launchScript(String targetFolder)
/*     */     throws InterruptedException
/*     */   {
/* 162 */     ProcessBuilder pb = new ProcessBuilder(new String[] { "./generateCharts.sh" });
/* 163 */     pb.directory(new File(System.getProperty("user.dir") + File.separatorChar + targetFolder));
              Process p;
/*     */     try
/*     */     {
/* 166 */       p = pb.start();
/*     */     } catch (IOException ex) { 
/* 168 */       System.out.println(ex.getMessage());
/*     */     }
/*     */   }
/*     */ }


/* Location:              /home/chverma/Descargas/iCanCloudGUI/iCanCloudGUI.jar!/icancloudgui/Parser/ParseUtils.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */